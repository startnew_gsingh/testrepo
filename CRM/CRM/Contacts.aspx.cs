﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;
using Microsoft.ApplicationBlocks.Data;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace CRM
{
    public partial class Contacts : System.Web.UI.Page
    {
        //List<SalesProcessStages> dsSalesProcess = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            Session["ObjectID"] = 2;//Contact
            if (!Page.IsPostBack)
            {
                //bindFiltersDropDown();
                //bindContactsGrid();
                CRMBLLUsers objCRMBLLUsers = new CRMBLLUsers();
                DataTable dtContacts = objCRMBLLUsers.getUserObjectRoles(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"])).Tables[0];
                if (dtContacts.Rows.Count > 0 && Convert.ToBoolean(dtContacts.Rows[0]["Read"]) == true)
                {
                    bindFiltersDropDown();
                    bindContactsGrid();
                    getUserObjectRoles(dtContacts);
                    bindgridColumnsDDL();
                    lblListmsg.Text = "";
                    trFilter.Visible = true;
                    trGridView.Visible = true;
                    trButtons.Visible = true;
                    trallButtons.Visible = true;
                }
                else
                {
                    lblListmsg.Text = "You are not authorize to view this page.";
                    trFilter.Visible = false;
                    trGridView.Visible = false;
                    trButtons.Visible = false;
                    trallButtons.Visible = false;
                }
            }
            else
            {
                string btnTarget = getPostBackControlID();
                if (btnTarget == null || btnTarget == "btnUpload")//btnTarget == "btnSave" && btnTarget == "btnSaveAndClose" && btnTarget != "dataGridDeals" && btnTarget != "ddlFilters" && btnTarget != "imgPageFirst" && btnTarget != "imgPagePrevious" && btnTarget != "ddCurrentPage" && btnTarget != "imgPageNext" && btnTarget != "imgPageLast")
                {
                    DataSet myDataSet = GetViewState();

                    DataTable myDataTable = myDataSet.Tables[0];
                    dataGridContact.DataSource = SortDataTable(myDataTable, true);
                    dataGridContact.DataBind();
                }
            }
        }

        private string getPostBackControlID()
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            if (control != null)
                return control.ID;
            else
                return null;
        }

        protected void btnrefreshUpdatePanel_Click(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            CRMBLLContact objCRMContact = new CRMBLLContact();
            DataSet dsContact = objCRMContact.getContactData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), Convert.ToInt32(ViewState["PageIndex"]), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]), Convert.ToString(ViewState["filterQuery"]));
            Session["gridData"] = dsContact.Tables[2];
            Session["data"] = dsContact.Tables[2];
            SetViewState(dsContact);

            DataTable myDataTable = dsContact.Tables[0];
            dataGridContact.DataSource = SortDataTable(myDataTable, true);
            dataGridContact.DataBind();
        }

        protected void btnRefreshFilterList_Click(Object Sender, EventArgs e)
        {
            bindFiltersDropDown();
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            DataSet myDataSet = GetViewState();

            DataTable myDataTable = myDataSet.Tables[0];
            dataGridContact.DataSource = SortDataTable(myDataTable, true);
            dataGridContact.DataBind();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Contacts.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dataGridContact.AllowPaging = false;
            //Change the Header Row back to white color
            dataGridContact.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < dataGridContact.HeaderRow.Cells.Count; i++)
            {
                dataGridContact.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
            }
            int j = 1;
            //This loop is used to apply stlye to cells based on particular row
            foreach (GridViewRow gvrow in dataGridContact.Rows)
            {
                gvrow.BackColor = Color.White;
                if (j <= dataGridContact.Rows.Count)
                {
                    if (j % 2 != 0)
                    {
                        for (int k = 0; k < gvrow.Cells.Count; k++)
                        {
                            gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                        }
                    }
                }
                j++;
            }
            dataGridContact.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        public void getUserObjectRoles(DataTable dtContacts)
        {
            btnCreateContact.Visible = true;
            btnEdit.Visible = true;
            btnDeleteAll.Visible = true;
            btnAdvanceFind.Visible = true;
            btnCustomizeObject.Visible = true;
            btnExcel.Visible = true;
            btnImport.Visible = true;

            if (Convert.ToBoolean(dtContacts.Rows[0]["Create"]) != true)
                btnCreateContact.Visible = false;

            if (Convert.ToBoolean(dtContacts.Rows[0]["Edit"]) != true)
                btnEdit.Visible = false;

            if (Convert.ToBoolean(dtContacts.Rows[0]["Delete"]) != true)
                btnDeleteAll.Visible = false;

            if (Convert.ToBoolean(dtContacts.Rows[0]["AdvanceFind"]) != true)
                btnAdvanceFind.Visible = false;

            if (Convert.ToBoolean(dtContacts.Rows[0]["Customize"]) != true)
                btnCustomizeObject.Visible = false;

            if (Convert.ToBoolean(dtContacts.Rows[0]["Export"]) != true)
                btnExcel.Visible = false;

            if (Convert.ToBoolean(dtContacts.Rows[0]["Import"]) != true)
                btnImport.Visible = false;
        }

        public void bindContactsGrid()
        {
            CRMBLLContact objCRMContact = new CRMBLLContact();

            DataSet dsContact = objCRMContact.getContactData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), 1, 10, Convert.ToString(ViewState["filterQuery"]));
            if (dsContact.Tables.Count > 0 && dsContact != null && dsContact.Tables[0].Rows.Count > 0)
            {
                dataGridContact.DataSource = dsContact;
                dataGridContact.DataBind();
                Session["gridData"] = dsContact.Tables[2];
                Session["data"] = dsContact.Tables[2];
                SetViewState(dsContact);

                bindOwnerChartData();
                ddlChartType.SelectedValue = "1";

                ddCurrentPage.Items.Clear();
                ViewState["PageIndex"] = 1;
                Int64 recordCount = Convert.ToInt64(dsContact.Tables[1].Rows[0][0]);
                if (recordCount > 10)
                {
                    dvPaging.Visible = true;
                    Int64 totalPages = 1;
                    if ((recordCount % Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"])) == 0)
                    {
                        totalPages = recordCount / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
                    }
                    else
                    {
                        totalPages = (recordCount / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"])) + 1;
                    }

                    for (int c = 1; c <= totalPages; c++)
                    {
                        ddCurrentPage.Items.Add(new ListItem(c.ToString(), c.ToString()));
                    }
                }
                else
                {
                    dvPaging.Visible = false;
                }
                imgPageFirst.Enabled = true;
                imgPagePrevious.Enabled = true;
                imgPageLast.Enabled = true;
                imgPageNext.Enabled = true;
                if (ddCurrentPage.Items.Count == Convert.ToInt32(ViewState["PageIndex"]))
                {
                    imgPageLast.Enabled = false;
                    imgPageNext.Enabled = false;
                }
                if (Convert.ToInt32(ViewState["PageIndex"]) == 1)
                {
                    imgPageFirst.Enabled = false;
                    imgPagePrevious.Enabled = false;
                }
            }
            else
            {
                dataGridContact.DataSource = null;
                dataGridContact.DataBind();
                Session["data"] = null;
            }
        }

        protected void ddlChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlChartType.SelectedValue == "1")
            {
                bindOwnerChartData();
            }
            else
            {
                //bindStatusChartData();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "chartButton();", true);
        }

        protected void bindOwnerChartData()
        {
            try
            {
                DataTable dtDeals = Session["data"] as DataTable;
                DataView dvDeals = dtDeals.DefaultView;
                dvDeals.RowFilter = "Owner='" + GlobalVariables.LoginUser.UserName + "'";

                List<chartData> _objChartData = new List<chartData>();
                chartData cd = new chartData();
                cd.Label = GlobalVariables.LoginUser.UserName;
                cd.Value1 = dvDeals.Count;
                _objChartData.Add(cd);

                Chart1.DataSource = _objChartData;
                Chart1.DataBind();
                Chart1.Visible = true;

                //statusChart.DataSource = null;
                //statusChart.DataBind();
                //statusChart.Visible = false;
            }
            catch { }
        }

        protected void bindgridColumnsDDL()
        {
            CRMBLLContact objCRMContact = new CRMBLLContact();

            DataSet dsContact = GetViewState();//objCRMContact.getContactData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue));

            if (dsContact != null && dsContact.Tables.Count > 0)
            {
                DataTable dtContact = dsContact.Tables[0];
                for (int c = 1; c < dtContact.Columns.Count; c++)
                {
                    ddlGridColumns1.Items.Add(new ListItem(dtContact.Columns[c].ColumnName, dtContact.Columns[c].ColumnName));
                }

                foreach (ListItem item in ddlGridColumns1.Items)
                {
                    item.Selected = true;
                }
            }
        }

        protected void checkBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            //selectedItemsPanel.Controls.Clear();
            string strSelValue = "";
            for (int li = 0; li < (sender as ListControl).Items.Count; li++)
            {
                if (!(sender as ListControl).Items[li].Selected)
                {
                    if (strSelValue != "")
                        strSelValue = strSelValue + ",";

                    strSelValue = strSelValue + (li + 2).ToString();
                }
            }

            ViewState["SelDDLValues"] = strSelValue;
            bindContactsGrid();
        }

        public void bindFiltersDropDown()
        {
            CRMBLLContact objCRMContact = new CRMBLLContact();
            ddlFilters.DataSource = objCRMContact.getFilters(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]));
            ddlFilters.DataTextField = "FilterName";
            ddlFilters.DataValueField = "ID";
            ddlFilters.DataBind();
            ddlFilters.Items.Insert(0, new ListItem("All Contacts", "0"));
        }

        protected void ddlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridContact.DataSource = null;
            dataGridContact.DataBind();
            bindContactsGrid();
        }

        protected void dataGridContact_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            GridViewSortExpression = e.SortExpression;

            //Gets the Pageindex of the GridView.
            dataGridContact.DataSource = SortDataTable(myDataTable, false);
            dataGridContact.DataBind();
        }

        //Gets or Sets the GridView SortDirection Property
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        //Gets or Sets the GridView SortExpression Property
        private string GridViewSortExpression
        {
            get
            {
                return ViewState["SortExpression"] as string ?? string.Empty;
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        //Toggles between the Direction of the Sorting
        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        //Sorts the ResultSet based on the SortExpression and the Selected Column.
        protected DataView SortDataTable(DataTable myDataTable, bool isPageIndexChanging)
        {
            if (myDataTable != null)
            {
                DataView myDataView = new DataView(myDataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GetSortDirection());
                    }
                }
                return myDataView;
            }
            else
            {

                return new DataView();
            }
        }

        protected void dataGridContact_DataBound(object sender, EventArgs e)
        {
            //Custom Paging
            GridViewRow dataGridContactRow = dataGridContact.BottomPagerRow;

            if (dataGridContactRow == null) return;

            //Get your Controls from the GridView, in this case 
            //I use a DropDown Control for Paging
            DropDownList ddCurrentPage =
        (DropDownList)dataGridContactRow.Cells[0].FindControl("ddCurrentPage");
            Label lblTotalPage = (Label)dataGridContactRow.Cells[0].FindControl("lblTotalPage");

            if (ddCurrentPage != null)
            {
                //Populate Pager
                for (int i = 0; i < dataGridContact.PageCount; i++)
                {
                    int iPageNumber = i + 1;
                    ListItem myListItem = new ListItem(iPageNumber.ToString());

                    if (i == dataGridContact.PageIndex)
                        myListItem.Selected = true;

                    ddCurrentPage.Items.Add(myListItem);
                }
            }

            // Populate the Page Count
            if (lblTotalPage != null)
                lblTotalPage.Text = dataGridContact.PageCount.ToString();

        }

        private DataSet GetViewState()
        {
            //Gets the ViewState
            return (DataSet)ViewState["contactDataSet"];
        }

        private void SetViewState(DataSet contactDataSet)
        {
            //Sets the ViewState
            ViewState["contactDataSet"] = contactDataSet;
        }

        protected void dataGridcontact_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            dataGridContact.DataSource = SortDataTable(myDataTable, true);

            dataGridContact.PageIndex = e.NewPageIndex;
            dataGridContact.DataBind();
        }

        //Change to a different page when the DropDown Page is changed
        protected void ddCurrentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["PageIndex"] = ddCurrentPage.SelectedValue;

            //Popultate the GridView Control
            CRMBLLContact objCRMContact = new CRMBLLContact();
            DataSet dsContact = objCRMContact.getContactData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), Convert.ToInt32(ViewState["PageIndex"]), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]), Convert.ToString(ViewState["filterQuery"]));

            Session["data"] = dsContact.Tables[2];
            SetViewState(dsContact);
            if (dsContact.Tables.Count > 0)
            {
                DataTable myDataTable = dsContact.Tables[0];
                dataGridContact.DataSource = SortDataTable(myDataTable, true);
                dataGridContact.DataBind();
            }
            else
            {
                dataGridContact.DataSource = null;
                dataGridContact.DataBind();
            }
        }

        protected void imgPageFirst_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void imgPagePrevious_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void imgPageNext_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void imgPageLast_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void Paginate(object sender, CommandEventArgs e)
        {
            // Get the Current Page Selected
            int iCurrentIndex = Convert.ToInt32(ViewState["PageIndex"]);// dataGridDeals.PageIndex;

            switch (e.CommandArgument.ToString().ToLower())
            {
                case "first":
                    ViewState["PageIndex"] = 1;//dataGridDeals.PageIndex = 0;
                    break;
                case "prev":
                    if (Convert.ToInt32(ViewState["PageIndex"]) != 1)
                    {
                        ViewState["PageIndex"] = iCurrentIndex - 1;
                    }
                    break;
                case "next":
                    ViewState["PageIndex"] = iCurrentIndex + 1;
                    break;
                case "last":
                    ViewState["PageIndex"] = ddCurrentPage.Items[ddCurrentPage.Items.Count - 1].Value;// dataGridDeals.PageCount;
                    break;
            }
            ddCurrentPage.SelectedIndex = Convert.ToInt32(ViewState["PageIndex"]) - 1;
            //Populate the GridView Control
            CRMBLLContact objCRMContact = new CRMBLLContact();
            DataSet dsContact = objCRMContact.getContactData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), Convert.ToInt32(ViewState["PageIndex"]), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]), Convert.ToString(ViewState["filterQuery"]));

            Session["data"] = dsContact.Tables[2];
            SetViewState(dsContact);
            if (dsContact.Tables.Count > 0)
            {
                DataTable myDataTable = dsContact.Tables[0];
                dataGridContact.DataSource = SortDataTable(myDataTable, true);
                dataGridContact.DataBind();
            }
            else
            {
                dataGridContact.DataSource = null;
                dataGridContact.DataBind();
            }
            imgPageFirst.Enabled = true;
            imgPagePrevious.Enabled = true;
            imgPageLast.Enabled = true;
            imgPageNext.Enabled = true;
            if (ddCurrentPage.Items.Count == Convert.ToInt32(ViewState["PageIndex"]))
            {
                imgPageLast.Enabled = false;
                imgPageNext.Enabled = false;
            }
            if (Convert.ToInt32(ViewState["PageIndex"]) == 1)
            {
                imgPageFirst.Enabled = false;
                imgPagePrevious.Enabled = false;
            }
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLContact objCRMContact = new CRMBLLContact();
            string IDS = "";
            for (int_index = 0; int_index <= dataGridContact.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridContact.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(dataGridContact.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMContact.deleteContacts(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Contact Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindContactsGrid();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int int_index;
            CRMBLLContact objCRMContact = new CRMBLLContact();
            Int64 IDS = 0;
            for (int_index = 0; int_index <= dataGridContact.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridContact.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = (Convert.ToInt64(dataGridContact.DataKeys[int_index].Value));
            }
            DataSet dsSelectedContact = objCRMContact.getSelectedContact(IDS);
            CreateFields(IDS);
            //getSalesStage();
            if (IDS == 0)
            {
            }
            else
            {
                //btnAdditems.Click += new EventHandler(btnAdditems_click);
                //bindDocumentsList();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createContact();", true);
        }

        protected void btnGridColumn_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            CRMBLLContact objCRMContact = new CRMBLLContact();
            Int64 IDS = Convert.ToInt64(src.CommandArgument);

            DataSet dsSelectedContact = objCRMContact.getSelectedContact(IDS);
            CreateFields(IDS);
            if (IDS == 0)
            {
            }
            else
            {
                //btnAdditems.Click += new EventHandler(btnAdditems_click);
                //bindDocumentsList();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createContact();", true);
        }

        protected void dataGridContact_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[1].Visible = false; // hides the first column
                if (ViewState["SelDDLValues"] != null)
                {
                    string[] words = ViewState["SelDDLValues"].ToString().Split(',');
                    for (int w = 0; w < words.Count(); w++)
                    {
                        e.Row.Cells[Convert.ToInt32(words[w])].Visible = false; // hides the first column
                    }
                }
            }
            catch
            {
            }
        }

        protected void dataGridContact_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var rowItem = e.Row.DataItem;
                int drViewIndex = 2;
                try
                {
                    drViewIndex = ((System.Data.DataRowView)(rowItem)).DataView.Table.Columns["First Name"].Ordinal + 1;
                }
                catch { }
                var firstCell = e.Row.Cells[drViewIndex];
                firstCell.Controls.Clear();
                LinkButton lnkEditFirstCol = new LinkButton();
                lnkEditFirstCol.ID = e.Row.Cells[1].Text;

                lnkEditFirstCol.Click += new EventHandler(btnGridColumn_Click);
                lnkEditFirstCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();
                lnkEditFirstCol.Attributes.Add("runat", "server");
                lnkEditFirstCol.Text = firstCell.Text;
                firstCell.Controls.Add(lnkEditFirstCol);
                TableCell cell = new TableCell();

                LinkButton lnkNotesLastCol = new LinkButton();
                lnkNotesLastCol.Click += new EventHandler(btnGridNotes_Click);
                lnkNotesLastCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();

                lnkNotesLastCol.Attributes.Add("runat", "server");
                lnkNotesLastCol.Text = "Notes";
                cell.Controls.Add(lnkNotesLastCol);

                string NextCall = "No Call Schedule";
                CRMBLLDeals _objDeals = new CRMBLLDeals();
                DataView dtColumns = _objDeals.getUpcomingPhoneCall(GlobalVariables.LoginUser.OrgID, 2, Convert.ToInt64(e.Row.Cells[1].Text)).Tables[0].DefaultView;
                if (dtColumns.Table.Rows.Count > 0)
                    NextCall = dtColumns.Table.Rows[0]["DueDate"].ToString();

                TableCell cellPhoneCalls = new TableCell();
                LinkButton lnkPhoneCallsLastCol = new LinkButton();
                lnkPhoneCallsLastCol.Click += new EventHandler(btnPhoneCall_Click);
                lnkPhoneCallsLastCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();
                lnkPhoneCallsLastCol.Attributes.Add("runat", "server");
                lnkPhoneCallsLastCol.Text = NextCall;
                cellPhoneCalls.Controls.Add(lnkPhoneCallsLastCol);

                e.Row.Controls.Add(cell);
                e.Row.Controls.Add(cellPhoneCalls);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int i = e.Row.Cells.Count;
                var rowItem = e.Row.DataItem;
                Dictionary<string, string> filterColValues = new Dictionary<string, string>();

                if (ViewState["filterArrayList"] != null)
                    filterColValues = (Dictionary<string, string>)ViewState["filterArrayList"];

                for (int c = 1; c < i; c++)
                {
                    Panel newPanel = new Panel();
                    newPanel.CssClass = "filterDiv";
                    e.Row.Cells[c].Controls.Add(new LiteralControl("<br />"));
                    TextBox tx = new TextBox();  //here i m adding a control.
                    tx.ID = "txtFilter" + c;
                    tx.CssClass = "filterTextBox";
                    tx.Width = Unit.Pixel(100);
                    tx.Attributes.Add("runat", "server");
                    if (filterColValues.ContainsKey("txtFilter" + c))
                        tx.Text = filterColValues.FirstOrDefault(k => k.Key == "txtFilter" + c).Value;
                    //e.Row.Cells[c].Controls.Add(tx);
                    newPanel.Controls.Add(tx);

                    AutoCompleteExtender _objautoCompleteList = new AutoCompleteExtender();
                    _objautoCompleteList.CompletionListItemCssClass = "CompletionListItemCssClass";
                    _objautoCompleteList.CompletionListCssClass = "CompletionListCssClass";

                    _objautoCompleteList.ID = "ACE" + c;
                    _objautoCompleteList.TargetControlID = "txtFilter" + c;
                    _objautoCompleteList.ServiceMethod = "filterGridrecords";
                    _objautoCompleteList.ServicePath = "CustomList.asmx";
                    _objautoCompleteList.ContextKey = ((System.Web.UI.WebControls.DataControlFieldCell)(e.Row.Cells[c])).ContainingField.ToString();
                    _objautoCompleteList.UseContextKey = true;
                    _objautoCompleteList.EnableCaching = false;
                    //_objautoCompleteList.EnableClientState = true;
                    _objautoCompleteList.MinimumPrefixLength = 1;

                    //e.Row.Cells[c].Controls.Add(_objautoCompleteList);
                    newPanel.Controls.Add(_objautoCompleteList);


                    Button imgBtnFind = new Button();
                    imgBtnFind.ID = "imgBtnFind" + c;
                    imgBtnFind.CssClass = "imgfilter";
                    imgBtnFind.Click += new EventHandler(imgBtnFind_click);
                    imgBtnFind.Attributes.Add("runat", "server");
                    newPanel.Controls.Add(imgBtnFind);

                    e.Row.Cells[c].Controls.Add(newPanel);

                }
                TableHeaderCell hcell = new TableHeaderCell();
                hcell.Text = "Notes";
                e.Row.Cells.Add(hcell);

                TableHeaderCell hcellCalls = new TableHeaderCell();
                hcellCalls.Text = "Next Call";
                e.Row.Cells.Add(hcellCalls);
            }
        }

        protected void imgBtnFind_click(object sender, EventArgs e)
        {
            Dictionary<string, string> filterValues = new Dictionary<string, string>();
            string query = "";
            DataTable Griddata = (DataTable)Session["gridData"];
            int rCount = Griddata.Columns.Count;
            for (int c = 1; c < rCount; c++)
            {
                string value = Request.Form["ctl00$ContentPlaceHolder1$dataGridContact$ctl01$" + "txtFilter" + c];
                if (value != null && value != "")
                {
                    filterValues.Add("txtFilter" + c, value);
                    if (query == "")
                        query = query + " [" + Griddata.Columns[c - 1].ColumnName + "] like '%" + value + "%'";
                    else
                        query = query + " And [" + Griddata.Columns[c - 1].ColumnName + "] like '%" + value + "%'";
                }
            }
            ViewState["filterArrayList"] = filterValues;
            ViewState["filterQuery"] = query;
            bindContactsGrid();
        }

        protected DataTable getImportDataTable()
        {
            DataTable tableImport = new DataTable();
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataSet dsObjectCol = _objDeals.getObjectsColumns(Convert.ToInt64(Session["ObjectID"]), GlobalVariables.LoginUser.OrgID, 0);
            Session["objectColumnsDataSet"] = dsObjectCol;

            tableImport.Columns.Add("ContactID", typeof(Int64));
            tableImport.Columns.Add("LeadID", typeof(Int64));
            tableImport.Columns.Add("OrgID", typeof(string));
            tableImport.Columns.Add("IsActive", typeof(bool));
            tableImport.Columns.Add("CreatedDate", typeof(DateTime));
            tableImport.Columns.Add("CreatedBy", typeof(string));
            tableImport.Columns.Add("ModifiedDate", typeof(DateTime));
            tableImport.Columns.Add("ModifiedBy", typeof(string));
            for (int c = 0; c < dsObjectCol.Tables[0].Rows.Count; c++)
            {
                tableImport.Columns.Add(Convert.ToString(dsObjectCol.Tables[0].Rows[c]["ColumnName"]), typeof(string));
            }
            return tableImport;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fleImport.HasFile)
            {
                try
                {
                    String strConnection = SqlHelper.GetConnectionString(); //"Data Source=MySystem;Initial Catalog=MySamplesDB;Integrated Security=True";

                    //file upload path
                    string path = string.Concat(Server.MapPath("~/Upload/" + fleImport.FileName));
                    fleImport.SaveAs(path);

                    // Connection String to Excel Workbook
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
                    OleDbConnection excelConnection = new OleDbConnection();
                    excelConnection.ConnectionString = excelConnectionString;
                    OleDbCommand command = new OleDbCommand("select * from [Sheet1$]", excelConnection);
                    excelConnection.Open();
                    // Create DbDataReader to Data Worksheet

                    OleDbDataReader dReader = command.ExecuteReader();
                    DataTable dtImport = getImportDataTable();
                    DataSet dsObjectColumns = (DataSet)Session["objectColumnsDataSet"];
                    if (dsObjectColumns.Tables[0].Rows.Count == dReader.FieldCount)
                    {
                        while (dReader.Read())
                        {
                            DataRow dr = dtImport.NewRow();
                            dr["OrgID"] = GlobalVariables.LoginUser.OrgID;
                            dr["IsActive"] = true;
                            dr["CreatedDate"] = DateTime.Now;
                            dr["CreatedBy"] = "Admin";
                            dr["ModifiedDate"] = DateTime.Now;
                            dr["ModifiedBy"] = "Admin";

                            for (int c = 0; c < dsObjectColumns.Tables[0].Rows.Count; c++)
                            {
                                dr[Convert.ToString(dsObjectColumns.Tables[0].Rows[c]["ColumnName"])] = dReader[Convert.ToString(dsObjectColumns.Tables[0].Rows[c]["ColumnName"])];
                            }
                            dtImport.Rows.Add(dr);
                        }

                        SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnection);
                        //Give your Destination table name
                        sqlBulk.DestinationTableName = "CRM_Contact";
                        sqlBulk.WriteToServer(dtImport);
                        excelConnection.Close();
                        lblListmsg.Text = "Records imported successfully.";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "refreshPage();", true);
                    }
                    else
                    {
                        try
                        {
                            string checkPath = string.Concat(Server.MapPath("~/Upload/" + fleImport.FileName));
                            if (File.Exists(checkPath))
                                File.Delete(checkPath);
                        }
                        catch { }
                        lblListmsg.Text = "File columns are not matched with table.";
                    }
                }
                catch
                {
                    lblListmsg.Text = "Some error while importing records.";
                }
            }
            else
            {
                lblListmsg.Text = "Please choose file to import records.";
            }
        }



        /*Create Contact*/
        public void CreateFields(Int64 ContactID)
        {
            ViewState["ContactID"] = ContactID;
            Session["selObjectUserID"] = ContactID;
            clsCommonFunctions.CheckSession();
            CRMBLLContact _objContact = new CRMBLLContact();
            DataView dtColumns = _objContact.getColumnControls(Convert.ToInt64(Session["ObjectID"])).Tables[0].DefaultView;
            DataTable dtPlaceholders = _objContact.getPlaceholders(Convert.ToInt64(Session["ObjectID"]), GlobalVariables.LoginUser.OrgID).Tables[0];

            DataTable dtContact = _objContact.getSelectedContact(ContactID).Tables[0];

            lblContactID.Text = ContactID.ToString();
            placeholderGeneralInfo.Controls.Add(new LiteralControl("<table style='width:100%;'><tr><td style='padding-right:50px;vertical-align:top;'>"));
            for (int p = 0; p < dtPlaceholders.Rows.Count; p++)
            {
                // Create Header Panel
                Panel panelHead = new Panel();
                if (p != 0)
                    panelHead.Attributes.Add("style", "Padding-top:15px;");
                else
                    panelHead.Attributes.Add("style", "Padding-top:2px;");

                panelHead.ID = "pH" + dtPlaceholders.Rows[p]["ID"].ToString();
                Label lblHead = new Label();
                lblHead.ID = "lblHeader" + dtPlaceholders.Rows[p]["ID"].ToString();
                panelHead.Controls.Add(lblHead);

                Panel _objNewPlaceholder = new Panel();
                _objNewPlaceholder.ID = dtPlaceholders.Rows[p]["ID"].ToString();

                Panel _objSystemField = new Panel();
                dtColumns.RowFilter = "Placeholder=" + dtPlaceholders.Rows[p]["ID"].ToString();
                bool IsSystemField = Convert.ToBoolean(dtPlaceholders.Rows[p]["IsSystemField"]);

                if (IsSystemField != true)
                {
                    //string strHTML = "<span style='font-weight:bold;'>" + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + "</span>" + "</br></br>";
                    //_objNewPlaceholder.Controls.Add(new LiteralControl(strHTML));
                }
                _objNewPlaceholder.Controls.Add(new LiteralControl("<table style='width:100%;'>"));

                for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
                {
                    bool IsControlSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[col]["IsSystemField"]);

                    if (IsSystemField == false)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("<tr style='vertical-align:top;height:30px;width:120px;'>"));
                    else if (col == 0)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("<tr style='vertical-align:top;height:30px;width:800px;'>"));
                    if (IsSystemField != true)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("<td style='width:120px;'>"));
                    else
                        _objNewPlaceholder.Controls.Add(new LiteralControl("<td style='background-color: #f4d89d;'>"));

                    int formID = Convert.ToInt32(dtColumns.Table.DefaultView[col]["ID"]);
                    Label _objLabel = new Label();
                    _objLabel.Text = dtColumns.Table.DefaultView[col]["LabelName"].ToString();
                    _objNewPlaceholder.Controls.Add(_objLabel);
                    if (IsSystemField != true)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("</td><td>"));
                    else
                        _objNewPlaceholder.Controls.Add(new LiteralControl("</br>"));

                    int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
                    if (fieldType == 1)
                    {
                        createTextFormatControl(col, _objNewPlaceholder, dtColumns.Table.DefaultView, dtContact, IsSystemField);
                    }
                    else if (fieldType == 3 || fieldType == 4 || fieldType == 5)
                    {
                        DataTable dtColumnChoices = _objContact.getControlChoices(formID).Tables[0];
                        createSingleChoiceFormatControl(col, _objNewPlaceholder, dtColumns.Table.DefaultView, dtColumnChoices, dtContact, _objSystemField, IsSystemField);
                    }
                    if (IsControlSystemField)
                        placeholderHeaderInfo.Controls.Add(_objSystemField);

                    if (IsSystemField == true)
                    {
                        _objNewPlaceholder.Controls.Add(new LiteralControl("</td>"));

                    }
                    else
                        _objNewPlaceholder.Controls.Add(new LiteralControl("</td></tr>"));
                }
                //_objNewPlaceholder.Controls.Add(new LiteralControl("</table>"));

                CollapsiblePanelExtender cpe = new CollapsiblePanelExtender();
                cpe.ID = "CPE" + _objNewPlaceholder.ID;
                cpe.TargetControlID = _objNewPlaceholder.ID;
                cpe.ExpandControlID = panelHead.ID;
                cpe.CollapseControlID = panelHead.ID;
                cpe.ScrollContents = false;
                cpe.Collapsed = false;
                cpe.AutoExpand = true;
                cpe.ExpandDirection =
                CollapsiblePanelExpandDirection.Vertical;
                cpe.SuppressPostBack = true;
                cpe.TextLabelID = lblHead.ID;
                //cpe.CollapsedImage = "images/collapse.jpg";
                //cpe.ExpandedImage = "images/icon_expand.gif";
                cpe.CollapsedText = "<b><span style='font-size:23px;'>+</span> " + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + " tab</b>";
                cpe.ExpandedText = "<b><span style='font-size:23px;'>-</span> " + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + " tab</b>";

                if (IsSystemField == true)
                {
                    _objNewPlaceholder.Controls.Add(new LiteralControl("</tr></table>"));

                    placeholderHeaderInfo.Controls.Add(_objNewPlaceholder);
                }
                else
                {
                    _objNewPlaceholder.Controls.Add(new LiteralControl("</table>"));
                    placeholderGeneralInfo.Controls.Add(panelHead);
                    placeholderGeneralInfo.Controls.Add(_objNewPlaceholder);
                    placeholderGeneralInfo.Controls.Add(cpe);
                }
            }
            placeholderGeneralInfo.Controls.Add(new LiteralControl("</td>"));
            placeholderGeneralInfo.Controls.Add(new LiteralControl("</tr></table>"));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataTable dtMCAnswers = new DataTable();
            dtMCAnswers.Columns.Add(new DataColumn("OrgID", typeof(Int64)));
            dtMCAnswers.Columns.Add(new DataColumn("ObjectID", typeof(Int64)));
            dtMCAnswers.Columns.Add(new DataColumn("ObjectUserID", typeof(Int64)));
            dtMCAnswers.Columns.Add(new DataColumn("ControlChoiceID", typeof(string)));


            CRMBLLContact _objContact = new CRMBLLContact();
            string colValues = "";
            List<string> _objColumns = new List<string>();
            DataView dtColumns = _objContact.getColumnControls(Convert.ToInt64(Session["ObjectID"])).Tables[0].DefaultView;
            for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
            {
                bool IsSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[col]["IsSystemField"]);
                string colName = dtColumns.Table.DefaultView[col]["ColumnName"].ToString();
                if (!_objColumns.Contains(colName))
                {
                    _objColumns.Add(colName);
                    int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
                    int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldFormat"]);
                    if (col != 0)
                    {
                        colValues = colValues + ",";
                    }
                    if (fieldType == 1)
                    {
                        //TextBox _objtxt = (TextBox)placeholderGeneralInfo.FindControl(dtColumns.Table.DefaultView[col]["ID"].ToString());
                        string txtValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        //colValues = colValues + _objtxt.Text;
                        colValues = colValues + txtValue;
                    }
                    else if (fieldType == 3 || fieldType == 4)
                    {
                        //if (fieldFormat == 3)//Radio Button List
                        //{
                        string rdbValue = "";
                        if (IsSystemField)
                            rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + "txt" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        else
                            rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        //RadioButtonList _objRdbList = (RadioButtonList)placeholderGeneralInfo.FindControl(dtColumns.Table.DefaultView[col]["ID"].ToString());
                        colValues = colValues + rdbValue;
                        //}
                    }

                    else if (fieldType == 5)
                    {
                        string countValue = Request.Form["ctl00$ContentPlaceHolder1$hidden" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        for (int i = 0; i < Convert.ToInt32(countValue); i++)
                        {
                            string chkStr = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString() + "$" + i];
                            if (chkStr != null && chkStr != "")
                            {
                                //CheckBoxList chkListValue = (CheckBoxList)Form.FindControl(Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()]);
                                DataRow dr = null;
                                dr = dtMCAnswers.NewRow();
                                dr["OrgID"] = GlobalVariables.LoginUser.OrgID;

                                dr["ObjectID"] = Convert.ToInt64(Session["ObjectID"]);
                                dr["ObjectUserID"] = Convert.ToInt64(ViewState["ContactID"]);

                                dr["ControlChoiceID"] = chkStr;
                                dtMCAnswers.Rows.Add(dr);
                            }
                        }
                    }
                }
            }
            Int64 ContactID = _objContact.insertContact(GlobalVariables.LoginUser.OrgID, colValues, Convert.ToInt64(lblContactID.Text), "", "");
            if (dtMCAnswers != null && dtMCAnswers.Rows.Count > 0)
            {
                Int64 recordsInserted = _objContact.insertMCAnswers(dtMCAnswers, ContactID);
            }
            //uploadDocuments(GlobalVariables.LoginUser.OrgID, 2, ContactID);
            //Int64 LeadID = _objDeals.insertDeals(GlobalVariables.LoginUser.OrgID, colValues, Convert.ToInt64(lblContactID.Text), salesColNames, salesColValues);
            ViewState["ContactID"] = ContactID;
            //Response.Redirect("Contacts.aspx");

            if (((System.Web.UI.Control)(sender)).ID == "btnSave")
                CreateFields(ContactID);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);


        }

        //protected void saveformData()
        //{
        //    CRMBLLContact _objContact = new CRMBLLContact();
        //    string colValues = "";
        //    List<string> _objColumns = new List<string>();
        //    DataView dtColumns = _objContact.getColumnControls(Convert.ToInt64(Session["ObjectID"])).Tables[0].DefaultView;
        //    for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
        //    {
        //        string colName = dtColumns.Table.DefaultView[col]["ColumnName"].ToString();
        //        if (!_objColumns.Contains(colName))
        //        {
        //            _objColumns.Add(colName);
        //            int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
        //            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldFormat"]);
        //            if (colValues != "")
        //            {
        //                colValues = colValues + ",";
        //            }
        //            if (fieldType == 1)
        //            {
        //                //TextBox _objtxt = (TextBox)placeholderGeneralInfo.FindControl(dtColumns.Table.DefaultView[col]["ID"].ToString());
        //                string txtValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
        //                //colValues = colValues + _objtxt.Text;
        //                colValues = colValues + txtValue;
        //            }
        //            else if (fieldType == 3 || fieldType == 4)
        //            {
        //                //if (fieldFormat == 3)//Radio Button List
        //                //{
        //                string rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
        //                //RadioButtonList _objRdbList = (RadioButtonList)placeholderGeneralInfo.FindControl(dtColumns.Table.DefaultView[col]["ID"].ToString());
        //                colValues = colValues + rdbValue;
        //                //}
        //            }
        //        }
        //    }
        //    saveSalesProcess(colValues);
        //}

        //protected void saveSalesProcess(string colValues)
        //{
        //    SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
        //    string salesColValues = "";
        //    string salesColNames = "";
        //    DataView dtColumns = objSalesProcessBLL.getSalesStageControls(Convert.ToInt64(ViewState["StageID"])).Tables[0].DefaultView;
        //    for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
        //    {
        //        int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
        //        int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldFormat"]);
        //        if (salesColValues != "")
        //        {
        //            salesColValues = salesColValues + ",";
        //            salesColNames = salesColNames + ",";
        //        }
        //        if (fieldType == 1)
        //        {
        //            string txtValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[col]["ColumnName"].ToString()];
        //            salesColValues = salesColValues + "[" + dtColumns.Table.DefaultView[col]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[col]["ColumnName"].ToString() + "]" + "='" + txtValue + "'";
        //            //salesColNames = salesColNames + dtColumns.Table.DefaultView[col]["ColumnName"].ToString();
        //        }
        //        else if (fieldType == 3 || fieldType == 4)
        //        {
        //            string rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[col]["ColumnName"].ToString()];
        //            salesColValues = salesColValues + "[" + dtColumns.Table.DefaultView[col]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[col]["ColumnName"].ToString() + "]" + "='" + rdbValue + "'";
        //            //salesColNames = salesColNames + dtColumns.Table.DefaultView[col]["ColumnName"].ToString();
        //        }
        //    }
        //    CRMBLLContact _objContact = new CRMBLLContact();
        //    Int64 ContactID = _objContact.insertContact(GlobalVariables.LoginUser.OrgID, colValues, Convert.ToInt64(lblContactID.Text), salesColNames, salesColValues);
        //    uploadDocuments(GlobalVariables.LoginUser.OrgID, 2, ContactID);
        //    //Int64 LeadID = _objDeals.insertDeals(GlobalVariables.LoginUser.OrgID, colValues, Convert.ToInt64(lblContactID.Text), salesColNames, salesColValues);
        //    ViewState["ContactID"] = ContactID;
        //}

        public void createTextFormatControl(int index, Panel _objplaceholder, DataView dtColumns, DataTable dtContact, bool IsSystemField)
        {
            string strColumnName = dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
            string Value = "";
            if (dtContact != null && dtContact.Rows.Count > 0)
                Value = dtContact.Rows[0][strColumnName].ToString();


            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
            if (fieldFormat == 1)//Simple text box
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                if (!IsSystemField)
                    _objTextBox.Width = Unit.Percentage(93);
                _objplaceholder.Controls.Add(_objTextBox);
            }
            else if (fieldFormat == 2)//Multiline text Box
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.TextMode = TextBoxMode.MultiLine;
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                if (!IsSystemField)
                    _objTextBox.Width = Unit.Percentage(93);
                _objplaceholder.Controls.Add(_objTextBox);
            }
            else if (fieldFormat == 3)//Email
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                if (!IsSystemField)
                    _objTextBox.Width = Unit.Percentage(93);
                _objplaceholder.Controls.Add(_objTextBox);
            }
            bool IsRequired = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsRequired"]);
            if (IsRequired)
            {
                RequiredFieldValidator myRFV = new RequiredFieldValidator();
                myRFV.ControlToValidate = dtColumns.Table.DefaultView[index]["ID"].ToString();
                myRFV.Display = ValidatorDisplay.Dynamic;
                myRFV.SetFocusOnError = true;
                myRFV.ValidationGroup = "createValidation";
                myRFV.ErrorMessage = "*";
                myRFV.CssClass = "validationerror";
                _objplaceholder.Controls.Add(myRFV);
            }
        }

        public void createSingleChoiceFormatControl(int index, Panel _objplaceholder, DataView dtColumns, DataTable dtControlChoices, DataTable dtContact, Panel _objSystemField, bool IsPlcSystemField)
        {
            string strColumnName = dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
            bool IsSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsSystemField"]);
            Int64 ObjectID = Convert.ToInt64(dtColumns.Table.DefaultView[index]["ObjectID"]);
            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
            string Value = "";
            if (dtContact != null && dtContact.Rows.Count > 0)
                Value = dtContact.Rows[0][strColumnName].ToString();

            if (fieldFormat == 9)//CheckBox List
            {
                CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
                DataSet dsObjects = objCRMBLLDeals.getMCAnswers(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["ContactID"]));

                TextBox txtHidden = new TextBox();
                txtHidden.ID = "hidden" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                txtHidden.Text = dtControlChoices.Rows.Count.ToString();
                txtHidden.Style.Add("display", "none");

                CheckBoxList _objChkList = new CheckBoxList();
                _objChkList.RepeatColumns = 3;
                //_objChkList.Enabled = leadIsActive;

                _objChkList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                for (int c = 0; c < dtControlChoices.Rows.Count; c++)
                {
                    _objChkList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
                    if (dsObjects.Tables[0].Rows.Count > 0)
                    {
                        dsObjects.Tables[0].DefaultView.RowFilter = "ControlChoiceID='" + dtControlChoices.Rows[c]["ControlChoices"].ToString() + "'";

                        if (dsObjects.Tables[0].DefaultView.Count > 0)
                            _objChkList.Items[c].Selected = true;
                    }
                    else
                        _objChkList.Items[0].Selected = true;
                    _objplaceholder.Controls.Add(_objChkList);
                }
                _objplaceholder.Controls.Add(txtHidden);
            }

            if (fieldFormat == 6)//Radio Button List
            {
                RadioButtonList _objRdbList = new RadioButtonList();
                _objRdbList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                for (int c = 0; c < dtControlChoices.Rows.Count; c++)
                {
                    _objRdbList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
                    if (Value != "")
                    {
                        if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
                            _objRdbList.Items[c].Selected = true;
                    }
                    else
                        _objRdbList.Items[0].Selected = true;
                    _objplaceholder.Controls.Add(_objRdbList);
                }

            }
            if (fieldFormat == 7)//DropDownList
            {

                if (IsSystemField == true)
                {
                    TextBox _objTextBox = new TextBox();
                    if (!IsPlcSystemField)
                        _objTextBox.Width = Unit.Percentage(93);
                    _objTextBox.ID = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    _objTextBox.Attributes.Add("Onchange", "checkExists('" + dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString() + "','" + _objTextBox.ID + "','" + GlobalVariables.LoginUser.OrgID + "')");
                    _objTextBox.Attributes.Add("autocomplete", "off");
                    _objTextBox.Attributes.Add("runat", "server");


                    System.Web.UI.WebControls.Image _imgSuccess = new System.Web.UI.WebControls.Image();
                    _imgSuccess.ID = "imgSuccess" + Convert.ToString(_objTextBox.ID);
                    _imgSuccess.ImageUrl = "images/update.jpg";
                    _imgSuccess.Attributes.Add("style", "height:20px;width:20px; display:none;");

                    System.Web.UI.WebControls.Image _imgFailure = new System.Web.UI.WebControls.Image();
                    _imgFailure.ID = "imgFailure" + Convert.ToString(_objTextBox.ID);
                    _imgFailure.ImageUrl = "images/Cancel.jpg";
                    _imgFailure.Attributes.Add("style", "height:20px;width:20px;display:none;");

                    AutoCompleteExtender _objautoCompleteList = new AutoCompleteExtender();
                    _objautoCompleteList.CompletionListItemCssClass = "CompletionListItemCssClass";
                    _objautoCompleteList.CompletionListCssClass = "CompletionListCssClass";

                    _objautoCompleteList.ID = "ACE" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    _objautoCompleteList.TargetControlID = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    _objautoCompleteList.ServiceMethod = "GetCompletionList";
                    _objautoCompleteList.ServicePath = "CustomList.asmx";
                    _objautoCompleteList.ContextKey = dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString() + "," + GlobalVariables.LoginUser.OrgID.ToString();
                    _objautoCompleteList.UseContextKey = true;
                    _objautoCompleteList.EnableCaching = true;
                    _objautoCompleteList.MinimumPrefixLength = 1;


                    //CRMBLLDeals _objDeals = new CRMBLLDeals();
                    //DataTable dtObjectChoices = _objDeals.getObjectChoices(dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString(), GlobalVariables.LoginUser.OrgID).Tables[0];
                    string linkTableValue = dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString();
                    if (Value != "")
                    {
                        _objTextBox.Text = Value;
                    }
                    else if (linkTableValue == "CRM_Users")
                    {
                        _objTextBox.Text = GlobalVariables.LoginUser.UserName;
                    }
                    else if (linkTableValue == "CRM_Territory")
                    {
                        _objTextBox.Text = GlobalVariables.LoginUser.Territory;
                    }
                    else if (linkTableValue == "CRM_Account")
                    {
                        _objTextBox.Text = GlobalVariables.LoginUser.Customer;
                    }
                    else if (linkTableValue == "CRM_Contact")
                    {
                        _objTextBox.Text = GlobalVariables.LoginUser.Contact;
                    }

                    _objplaceholder.Controls.Add(_objTextBox);
                    _objplaceholder.Controls.Add(_imgSuccess);
                    _objplaceholder.Controls.Add(_imgFailure);
                    _objSystemField.Controls.Add(_objautoCompleteList);

                    RequiredFieldValidator myRFV = new RequiredFieldValidator();
                    myRFV.ControlToValidate = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    myRFV.Display = ValidatorDisplay.Dynamic;
                    myRFV.SetFocusOnError = true;
                    myRFV.ValidationGroup = "createValidation";
                    myRFV.ErrorMessage = "*";
                    myRFV.CssClass = "validationerror";
                    _objplaceholder.Controls.Add(myRFV);

                    //CRMBLLDeals _objDeals = new CRMBLLDeals();
                    //DataTable dtObjectChoices = _objDeals.getObjectChoices(dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString(), GlobalVariables.LoginUser.OrgID).Tables[0];

                    //for (int c = 0; c < dtObjectChoices.Rows.Count; c++)
                    //{
                    //    _objDDLList.Items.Add(new ListItem(dtObjectChoices.Rows[c]["Name"].ToString(), dtObjectChoices.Rows[c]["ID"].ToString()));
                    //    if (Value != "")
                    //    {
                    //        if (dtObjectChoices.Rows[c]["ID"].ToString() == Value)
                    //            _objDDLList.Items[c].Selected = true;
                    //    }
                    //    else
                    //        _objDDLList.Items[0].Selected = true;
                    //    _objplaceholder.Controls.Add(_objDDLList);
                    //}
                }
                else
                {
                    DropDownList _objDDLList = new DropDownList();
                    if (!IsPlcSystemField)
                        _objDDLList.Width = Unit.Percentage(93);
                    _objDDLList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    for (int c = 0; c < dtControlChoices.Rows.Count; c++)
                    {
                        _objDDLList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
                        if (Value != "")
                        {
                            if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
                                _objDDLList.Items[c].Selected = true;
                        }
                        else
                            _objDDLList.Items[0].Selected = true;
                        _objplaceholder.Controls.Add(_objDDLList);
                    }
                }
            }
        }




        /*Sales Process*/

        //public void getSalesStage(Int64 stageID = 0)
        //{
        //    SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
        //    DataSet dsSalesProcess = objSalesProcessBLL.getSalesStages();
        //    for (int sp = 0; sp < dsSalesProcess.Tables[0].Rows.Count; sp++)
        //    {
        //        dvStages.Controls.Add(new LiteralControl("<div style='float:left; font-weight:bold; cursor:pointer; margin-right:20px;' onclick=changeProcessStage(" + Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ID"]) + ")>" + dsSalesProcess.Tables[0].Rows[sp]["StageName"].ToString() + "</div>"));
        //    }
        //    if (stageID != 0)
        //        CreateSalesProcessFields(stageID);
        //    else
        //    {
        //        if (dsSalesProcess.Tables[0].Rows.Count > 0)
        //        {
        //            CreateSalesProcessFields(Convert.ToInt64(dsSalesProcess.Tables[0].Rows[0]["ID"]));
        //            txtStageID.Text = dsSalesProcess.Tables[0].Rows[0]["ID"].ToString();
        //            ViewState["StageID"] = txtStageID.Text;
        //        }
        //    }
        //}

        //public void getSalesStageByObjectID(Int64 ObjectID)
        //{
        //    SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
        //    dsSalesProcess = objSalesProcessBLL.getSalesStageSteps(ObjectID);
        //    if (dsSalesProcess.Count > 0)
        //    {
        //        lsSalesProcessStages.DataSource = dsSalesProcess;
        //        lsSalesProcessStages.DataBind();
        //        tbNewStage.Visible = false;
        //        lsSalesProcessStages.Visible = true;
        //    }
        //    else
        //    {
        //        SaleProcessSteps _objSaleProcessSteps = objSalesProcessBLL.listObjectColumns(ObjectID);
        //        lsSalesProcessStages.DataSource = null;
        //        lsSalesProcessStages.DataBind();

        //        ddlNewInsertObjectColumns.DataSource = _objSaleProcessSteps.EntityColumns;
        //        ddlNewInsertObjectColumns.DataBind();
        //        lsSalesProcessStages.Visible = false;
        //        tbNewStage.Visible = true;
        //    }
        //}

        //public void getObjects()
        //{
        //    CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
        //    DataSet dsObjects = objCRMBLLDeals.getAllObjects();
        //    for (int ob = 0; ob < dsObjects.Tables[0].Rows.Count; ob++)
        //    {
        //        dvObjectColumns.Controls.Add(new LiteralControl("<div style='float:left; margin-right:20px;' onclick=changeProcessStageStep(" + Convert.ToInt64(dsObjects.Tables[0].Rows[ob]["ID"]) + ")>" + dsObjects.Tables[0].Rows[ob]["ObjectName"].ToString() + "</div>"));
        //    }
        //    txtObjectID.Text = "2";
        //    getSalesStageByObjectID(2);
        //}

        //protected void btnEditSalesProcess_Click(object sender, EventArgs e)
        //{
        //    getObjects();
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editSalesProcess();", true);
        //}

        //protected void ddlColumns_load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DropDownList _objddlColumns = (DropDownList)sender;
        //        ListViewDataItem row = (ListViewDataItem)_objddlColumns.NamingContainer;
        //        _objddlColumns.SelectedValue = ((CRMBLL.SaleProcessSteps)(row.DataItem)).FormID;
        //    }
        //    catch
        //    {
        //    }
        //}

        //protected void btnDelete_Click(object sender, EventArgs e)
        //{
        //    Button _objButton = (Button)sender;
        //    ListViewDataItem row = (ListViewDataItem)_objButton.NamingContainer;
        //    Label lblStepID = ((Label)row.FindControl("lblStepID"));

        //    SalesProcessBLL _objSalesProcessBLL = new SalesProcessBLL();
        //    _objSalesProcessBLL.deleteSalesStage(Convert.ToInt64(_objButton.CommandName), Convert.ToInt64(lblStepID.Text));

        //    CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
        //    DataSet dsObjects = objCRMBLLDeals.getObjects();
        //    for (int ob = 0; ob < dsObjects.Tables[0].Rows.Count; ob++)
        //    {
        //        dvObjectColumns.Controls.Add(new LiteralControl("<div style='float:left; margin-right:20px;' onclick=changeProcessStageStep(" + Convert.ToInt64(dsObjects.Tables[0].Rows[ob]["ID"]) + ")>" + dsObjects.Tables[0].Rows[ob]["ObjectName"].ToString() + "</div>"));
        //    }

        //    getSalesStageByObjectID(Convert.ToInt64(txtObjectID.Text));
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editSalesProcess();", true);
        //}

        //protected void ddlInsertObjectColumns_load(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DropDownList _objddlColumns = (DropDownList)sender;
        //        _objddlColumns.DataSource = dsSalesProcess.ElementAtOrDefault(0).ProcessSetps.ElementAtOrDefault(0).EntityColumns;
        //        _objddlColumns.DataBind();

        //        //_objddlColumns.Items.Add(new ListItem("asd", "dfg"));
        //    }
        //    catch
        //    {
        //    }
        //}

        //protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "Insert")
        //    {
        //        //Int64 StageID = 0;
        //        ListView lvStage = (ListView)(((System.Web.UI.Control)(e.Item)).DataKeysContainer).DataKeysContainer;
        //        ListViewItem dataItem = (ListViewItem)e.Item;

        //        string clientID = dataItem.NamingContainer.ClientID;
        //        string rowID = clientID.Substring(clientID.IndexOf("lvStageFields_") + 14);

        //        Int64 stageID = Convert.ToInt64(lvStage.DataKeys[Convert.ToInt32(rowID)].Values[0].ToString());

        //        Label lblStageID = (Label)e.Item.FindControl("lblStageID");
        //        TextBox txtStepName = (TextBox)e.Item.FindControl("txtStepName");
        //        DropDownList ddlInsertObjectColumns = (DropDownList)e.Item.FindControl("ddlInsertObjectColumns");
        //        saveSalesProcessStep(Convert.ToInt64(txtObjectID.Text), "", stageID, txtStepName.Text, Convert.ToInt64(ddlInsertObjectColumns.SelectedValue));
        //    }
        //}

        //protected void lsSalesProcessStages_ItemCommand(object sender, ListViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "Insert")
        //    {
        //        TextBox txtNewStageName = (TextBox)e.Item.FindControl("txtNewStageName");
        //        TextBox txtNewStageStep = (TextBox)e.Item.FindControl("txtNewStageStep");
        //        DropDownList ddlNewInsertObjectColumns = (DropDownList)e.Item.FindControl("ddlNewInsertObjectColumns");
        //        saveSalesProcessStep(Convert.ToInt64(txtObjectID.Text), txtNewStageName.Text, 0, txtNewStageStep.Text, Convert.ToInt64(ddlNewInsertObjectColumns.SelectedValue));
        //    }
        //}

        //protected void btnSaveProcess_Click(object sender, EventArgs e)
        //{
        //    saveSalesProcessStep(Convert.ToInt64(txtObjectID.Text), txtNewStageName.Text, 0, txtNewStageStep.Text, Convert.ToInt64(ddlNewInsertObjectColumns.SelectedValue));
        //}

        //public void saveSalesProcessStep(Int64 ObjectID, string StageName, Int64 StageID, string StepName, Int64 FormID)
        //{
        //    SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
        //    objSalesProcessBLL.insertSalesProcessStage(ObjectID, StageName, StageID, StepName, FormID);
        //    CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
        //    DataSet dsObjects = objCRMBLLDeals.getObjects();
        //    for (int ob = 0; ob < dsObjects.Tables[0].Rows.Count; ob++)
        //    {
        //        dvObjectColumns.Controls.Add(new LiteralControl("<div style='float:left; margin-right:20px;' onclick=changeProcessStageStep(" + Convert.ToInt64(dsObjects.Tables[0].Rows[ob]["ID"]) + ")>" + dsObjects.Tables[0].Rows[ob]["ObjectName"].ToString() + "</div>"));
        //    }
        //    getSalesStageByObjectID(Convert.ToInt64(txtObjectID.Text));
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editSalesProcess();", true);
        //}

        //protected void txtObjectID_Click(object sender, EventArgs e)
        //{
        //    CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
        //    DataSet dsObjects = objCRMBLLDeals.getObjects();
        //    for (int ob = 0; ob < dsObjects.Tables[0].Rows.Count; ob++)
        //    {
        //        dvObjectColumns.Controls.Add(new LiteralControl("<div style='float:left; margin-right:20px;' onclick=changeProcessStageStep(" + Convert.ToInt64(dsObjects.Tables[0].Rows[ob]["ID"]) + ")>" + dsObjects.Tables[0].Rows[ob]["ObjectName"].ToString() + "</div>"));
        //    }
        //    getSalesStageByObjectID(Convert.ToInt64(txtObjectID.Text));
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editSalesProcess();", true);
        //}

        //public void CreateSalesProcessFields(Int64 stageID)
        //{
        //    clsCommonFunctions.CheckSession();
        //    SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
        //    CRMBLLDeals _objDeals = new CRMBLLDeals();
        //    DataView dtColumns = objSalesProcessBLL.getSalesStageControls(stageID).Tables[0].DefaultView;
        //    placeholderSalesProcess.Controls.Add(new LiteralControl("<table>"));
        //    int val = 1;
        //    DataTable dtSalesProcess = objSalesProcessBLL.getSalesProcess(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ViewState["ContactID"])).Tables[0];
        //    for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
        //    {
        //        if (col == 0 || (col / 2) != 0)
        //        {
        //            placeholderSalesProcess.Controls.Add(new LiteralControl("<tr style='vertical-align:top;height:30px;'>"));
        //        }
        //        placeholderSalesProcess.Controls.Add(new LiteralControl("<td style='padding-left:40px;'>"));
        //        int formID = Convert.ToInt32(dtColumns.Table.DefaultView[col]["ID"]);
        //        Label _objLabel = new Label();
        //        _objLabel.Text = dtColumns.Table.DefaultView[col]["StepName"].ToString() + ":";
        //        placeholderSalesProcess.Controls.Add(_objLabel);
        //        placeholderSalesProcess.Controls.Add(new LiteralControl("</td><td>"));

        //        int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
        //        if (fieldType == 1)
        //        {
        //            createSalesTextFormatControl(col, placeholderSalesProcess, dtColumns.Table.DefaultView, dtSalesProcess);
        //        }
        //        else if (fieldType == 3 || fieldType == 4)
        //        {
        //            DataTable dtColumnChoices = _objDeals.getControlChoices(formID).Tables[0];
        //            createSalesSingleChoiceFormatControl(col, placeholderSalesProcess, dtColumns.Table.DefaultView, dtColumnChoices, dtSalesProcess);
        //        }
        //        placeholderSalesProcess.Controls.Add(new LiteralControl("</td>"));
        //        if (val == 2)
        //        {
        //            val = 0;
        //            placeholderSalesProcess.Controls.Add(new LiteralControl("</tr>"));
        //        }
        //        val = val + 1;
        //    }
        //    placeholderSalesProcess.Controls.Add(new LiteralControl("</table>"));
        //}

        //public void createSalesTextFormatControl(int index, PlaceHolder _objplaceholder, DataView dtColumns, DataTable dtSalesProcess)
        //{
        //    string strColumnName = dtColumns.Table.DefaultView[index]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
        //    string Value = "";
        //    if (dtSalesProcess != null && dtSalesProcess.Rows.Count > 0)
        //        Value = dtSalesProcess.Rows[0][strColumnName].ToString();


        //    int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
        //    if (fieldFormat == 1)//Simple text box
        //    {
        //        TextBox _objTextBox = new TextBox();
        //        _objTextBox.ID = strColumnName;// "sales" + dtColumns.Table.DefaultView[index]["ID"].ToString();
        //        _objTextBox.Text = Value;
        //        _objplaceholder.Controls.Add(_objTextBox);
        //    }
        //    else if (fieldFormat == 2)//Multiline text Box
        //    {
        //        TextBox _objTextBox = new TextBox();
        //        _objTextBox.TextMode = TextBoxMode.MultiLine;
        //        _objTextBox.ID = strColumnName; //"sales" + dtColumns.Table.DefaultView[index]["ID"].ToString();
        //        _objTextBox.Text = Value;
        //        _objplaceholder.Controls.Add(_objTextBox);
        //    }
        //    else if (fieldFormat == 3)//Email
        //    {
        //        TextBox _objTextBox = new TextBox();
        //        _objTextBox.ID = strColumnName; //"sales" + dtColumns.Table.DefaultView[index]["ID"].ToString();
        //        _objTextBox.Text = Value;
        //        _objplaceholder.Controls.Add(_objTextBox);
        //    }
        //}

        //public void createSalesSingleChoiceFormatControl(int index, PlaceHolder _objplaceholder, DataView dtColumns, DataTable dtControlChoices, DataTable dtDeal)
        //{
        //    string strColumnName = dtColumns.Table.DefaultView[index]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
        //    bool IsSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsSystemField"]);
        //    Int64 ObjectID = Convert.ToInt64(dtColumns.Table.DefaultView[index]["ObjectID"]);
        //    int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
        //    string Value = "";
        //    if (dtDeal != null && dtDeal.Rows.Count > 0)
        //        Value = dtDeal.Rows[0][strColumnName].ToString();

        //    if (fieldFormat == 6)//Radio Button List
        //    {
        //        RadioButtonList _objRdbList = new RadioButtonList();
        //        _objRdbList.ID = strColumnName; //"sales" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
        //        for (int c = 0; c < dtControlChoices.Rows.Count; c++)
        //        {
        //            _objRdbList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));

        //            if (Value != "")
        //            {
        //                if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
        //                    _objRdbList.Items[c].Selected = true;
        //            }
        //            else
        //                _objRdbList.Items[0].Selected = true;
        //            _objplaceholder.Controls.Add(_objRdbList);
        //        }
        //    }
        //    if (fieldFormat == 7)//DropDownList
        //    {
        //        DropDownList _objDDLList = new DropDownList();
        //        _objDDLList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
        //        if (IsSystemField == true)
        //        {
        //            CRMBLLDeals _objDeals = new CRMBLLDeals();
        //            DataTable dtObjectChoices = _objDeals.getObjectChoices(ObjectID, GlobalVariables.LoginUser.OrgID).Tables[0];

        //            for (int c = 0; c < dtObjectChoices.Rows.Count; c++)
        //            {
        //                _objDDLList.Items.Add(new ListItem(dtObjectChoices.Rows[c]["ColName"].ToString(), dtObjectChoices.Rows[c]["ContactID"].ToString()));
        //                if (Value != "")
        //                {
        //                    if (dtObjectChoices.Rows[c]["ContactID"].ToString() == Value)
        //                        _objDDLList.Items[c].Selected = true;
        //                }
        //                else
        //                    _objDDLList.Items[0].Selected = true;
        //                _objplaceholder.Controls.Add(_objDDLList);
        //            }
        //        }
        //        else
        //        {
        //            for (int c = 0; c < dtControlChoices.Rows.Count; c++)
        //            {
        //                _objDDLList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
        //                _objDDLList.Items[0].Selected = true;
        //                _objplaceholder.Controls.Add(_objDDLList);
        //            }
        //        }
        //    }
        //}

        //protected void btnStageID_Click(object sender, EventArgs e)
        //{
        //    saveformData();
        //    //int int_index;
        //    CRMBLLDeals objCRMDeals = new CRMBLLDeals();
        //    Int64 IDS = Convert.ToInt64(ViewState["ContactID"]);
        //    //for (int_index = 0; int_index <= dataGridDeals.Rows.Count - 1; int_index++)
        //    //{
        //    //    Boolean bcheck = ((CheckBox)dataGridDeals.Rows[int_index].FindControl("chkBoxRowID")).Checked;
        //    //    if (bcheck == true) IDS = (Convert.ToInt64(dataGridDeals.DataKeys[int_index].Value));
        //    //}
        //    CreateFields(IDS);
        //    getSalesStage(Convert.ToInt64(txtStageID.Text));
        //    ViewState["StageID"] = txtStageID.Text;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createContact();", true);
        //}





        /* Phone Calls */
        protected void btnPhoneCall_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            ViewState["sObjectUserID"] = src.CommandArgument;
            bindPhoneCallsList();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "objectPhoneCall();", true);
        }

        protected void bindPhoneCallsList()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDocumets = objDeals.getPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"]));
            gvPhoneCalls.DataSource = dsDocumets;
            gvPhoneCalls.DataBind();
            txtPhoneCallDescription.Text = "";
            txtCallDate.Text = "";
            //ddlDirection.SelectedValue = "Outgoing";
        }

        protected void btnUploadPhoneCalls_Click(object sender, EventArgs e)
        {
            uploadphonecalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ViewState["sObjectUserID"]));
            bindPhoneCallsList();
            txtPhoneCallDescription.Text = "";
            txtCallDate.Text = "";
            ddlDirection.SelectedIndex = 0;
        }

        private void uploadphonecalls(Int64 orgid, Int64 dealid)
        {
            try
            {
                CRMBLLDeals objblldeals = new CRMBLLDeals();
                objblldeals.insertPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["objectid"]), dealid, txtPhoneCallDescription.Text, Convert.ToDateTime(txtCallDate.Text), ddlDirection.SelectedValue, Convert.ToInt64(ViewState["sObjectUserID"]), "", false);
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnDeleteAllCall_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            string IDS = "";
            for (int_index = 0; int_index <= gvPhoneCalls.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvPhoneCalls.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(gvPhoneCalls.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMDeals.deletePhoneCalls(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Phone Calls Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindPhoneCallsList();
        }


        /* Notes */
        protected void btnGridNotes_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            ViewState["sObjectUserID"] = src.CommandArgument;
            bindNotes();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "objectNotes();", true);
        }

        protected void bindNotes()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataView dsDocumets = objDeals.getDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"])).Tables[0].DefaultView;
            dsDocumets.RowFilter = "IsDocsOnly=0";
            gvNotes.DataSource = dsDocumets.Table.DefaultView;
            gvNotes.DataBind();
        }

        protected void btnUploadDocs_Click(object sender, EventArgs e)
        {
            try
            {
                //string path = Server.MapPath("~\\MyFiles\\" + Session["ObjectID"].ToString() + GlobalVariables.LoginUser.OrgID.ToString() + ViewState["sObjectUserID"].ToString());

                //if (!Directory.Exists(path))
                //{
                //    Directory.CreateDirectory(path);
                //}
                string documentName = "";
                string documentPath = "";
                //if (fleUpload.FileName != "")
                //{
                //    fleUpload.SaveAs(path + "\\" + System.IO.Path.GetFileName(fleUpload.FileName));
                //    documentName = fleUpload.FileName;
                //    documentPath = path;
                //}
                //if (documentName != "")
                //{
                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                objBLLDeals.insertDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"]), documentName, documentPath, Convert.ToInt64(ViewState["LeadID"]), "", txtNotes.Text, false, false);
                txtNotes.Text = "";
                bindNotes();
                //}
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDeleteNotes_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            string IDS = "";
            for (int_index = 0; int_index <= gvNotes.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvNotes.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(gvNotes.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMDeals.deleteNotes(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Note Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindNotes();
        }
    }
}