﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdvanceFind.aspx.cs" Inherits="CRM.AdvanceFind" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/ModalWindow.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <style>
        .ui-widget-header
        {
            font-family: Segoe\000020UI,Tahoma,Arial;
            font-size: 13px;
            background: #eee;
            border-bottom: 1px solid #ccc;
            font-weight: normal;
            padding: 5px;
        }
        h1
        {
            margin: 0;
            font-family: Segoe\000020UI,Tahoma,Arial;
            font-size: 13px;
            background: #eee;
            border-bottom: 1px solid #ccc;
        }
        .ui-widget-content, .ui-accordion-header
        {
            min-height: 200px !important;
            padding: 5px !important;
        }
        #fields
        {
            float: left;
            width: 200px;
        }
        #placeholder
        {
            float: left;
            width: 845px;
        }
        
        #placeholder ol
        {
            margin: 0;
            padding: 1em 0 1em 0;
            width: 100%;
            height: 200px;
        }
        .clr
        {
            clear: both;
        }
        #fields ul
        {
            padding: 0;
            margin: 0;
        }
        #fields ul li
        {
            border-bottom: 1px solid #CCCCCC;
            list-style: none;
            padding: 5px;
        }
        .ui-accordion .ui-accordion-icons
        {
            border-radius: 0 0 0 0;
            margin-top: 0;
        }
        #divIframe
        {
            font-family: Segoe\000020UI,Tahoma,Arial;
            font-size: 13px;
        }
        #fields
        {
            margin-right: 0;
        }
        .ui-accordion .ui-accordion-icons
        {
            border-radius: 0 0 0 0;
            margin-top: 0;
            padding-left: 5px;
        }
        
        #placeholder ol
        {
            list-style: none;
            padding: 0;
            float: left;
            margin: 0;
        }
        #placeholder ol li
        {
            float: left;
            width: 115px;
        }
    </style>
    <script>

        function checkExists() {

            var txtValue = $("#txtFilterName").val();
            var orgValue = $("#lblOrgID").html();
            var objectID = $("#lblSelObjectID").html();

            $.ajax({
                type: "POST",
                url: "CustomList.asmx/checkFilterAvailability",
                data: '{Value: "' + txtValue + '",orgID: "' + orgValue + '",objectID: "' + objectID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    switch (response.d) {
                        case 0:
                            $("#imgSuccess").show();
                            $("#imgFailure").hide();
                            $("#btnSave").removeAttr("disabled");
                            break;
                        case 1:
                            $("#imgSuccess").hide();
                            $("#imgFailure").show();
                            $("#btnSave").attr("disabled", "disabled");
                            break;
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        }

        function getAllLiteralControls() {
            var str = "";
            var strID = "";
            $("#olPlaceholderControls li").each(function () {
                str = $(this).text() + "," + str;
                strID = $(this).attr('id') + "," + strID;
            });
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "AdvanceFind.aspx/getAllLiteralControls",
                data: JSON.stringify({ liValues: str, liID: strID }),
                dataType: "json",
                success: function (data) {
                    //$("#btnAfterSaveFilterCols").click();
                    alert("Columns edited successfully.");
                },
                error: function (result) {
                    alert("There is some error while processing your request, Please try again later.");
                }
            });
        }



        function editColumns() {
            var editColumnsFindBox = "#div_EditColumns-box";
            //Fade in the Popup
            $(editColumnsFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(editColumnsFindBox).height() + 24) / 2;
            var popMargLeft = ($(editColumnsFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
            });
            return false;
        }

        function MakeElementsDraggable() {
            $(document).ready(function () {
                $("#fieldName li").draggable({ helper: 'clone' });

                $("#olPlaceholderControls").droppable({
                    accept: ":not(.ui-sortable-helper)",
                    activeClass: 'ui-state-default',
                    hoverClass: "ui-state-hover",
                    drop: function (ev, ui) {
                        var cID = ui.draggable.attr('id').toString().replace(" ", "%");
                        $("<li id=" + cID + "s" + "></li>").text(ui.draggable.text()).appendTo(this);
                    }
                });
            });
        }

        //$("#olPlaceholderControls").sortable();

        $(document).ready(function () {
            // MakeElementsDraggable();

            var prm = Sys.WebForms.PageRequestManager.getInstance();
            prm.add_endRequest(function () {
                MakeElementsDraggable();
            });

        });

        //        $(function () {
        //            $("#fieldName").accordion();

        //            $("#fieldName li").draggable({
        //                appendTo: "body",
        //                helper: "clone"
        //            });

        //            $("#placeholder ol").droppable({
        //                activeClass: "ui-state-default",
        //                hoverClass: "ui-state-hover",
        //                accept: ":not(.ui-sortable-helper)",
        //                drop: function (event, ui) {
        //                    $(this).find(".placeholder").remove();
        //                    var cID = ui.draggable.attr('id').toString().replace(" ", "%");
        //                    $("<li id=" + cID + "></li>").text(ui.draggable.text()).appendTo(this);
        //                }
        //            }).sortable({
        //                items: "li:not(.placeholder)",
        //                sort: function () {
        //                    // gets added unintentionally by droppable interacting with sortable
        //                    // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
        //                    $(this).removeClass("ui-state-default");
        //                }
        //            });
        //        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <style>
        th
        {
            /*display: none;*/
        }
        .savebtn
        {
            background: url("images/save.png") no-repeat scroll 2px 3px #EEEEEE;
            border: 1px solid #CCCCCC;
            padding: 2px 2px 2px 19px;
        }
        .edit
        {
            background: url("images/edit.png") no-repeat scroll 2px 3px #EEEEEE;
            border: 1px solid #CCCCCC;
            padding: 2px 2px 2px 19px;
            margin-left: 5px;
        }
        .result
        {
            background: url("images/result.png") no-repeat scroll 2px 3px #EEEEEE;
            border: 1px solid #CCCCCC;
            padding: 2px 2px 2px 19px;
        }
        .clear
        {
            background: url("images/celar.png") no-repeat scroll 2px 3px #EEEEEE;
            border: 1px solid #CCCCCC;
            padding: 2px 2px 2px 19px;
        }
        .new
        {
            background: url("images/new.png") no-repeat scroll 2px 3px #EEEEEE;
            border: 1px solid #CCCCCC;
            padding: 2px 2px 2px 19px;
        }
    </style>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress2" runat="server">
        <ProgressTemplate>
            <div style="position: absolute; width: 100%; height: 100%; opacity: 0.8; z-index: 999999;
                background: #000; filter: alpha(opacity=80); -moz-opacity: 0.8; opacity: 0.8;">
                <img style="position: absolute; left: 45%; top: 45%;" src="images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="div_EditColumns-box" class="message-popup" style="top: 2% !important;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="showpage" style="display: block;">
            <div id="divIframe" style="overflow: auto; width: 100%; height: 100% !important;">
                <div>
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                        <ContentTemplate>
                            <div>
                                <asp:Button ID="btnSaveFilterColumns" CssClass="btn btn-primary" Text="Save" runat="server"
                                    OnClientClick="getAllLiteralControls()" />
                                <%--<asp:Button ID="btnAfterSaveFilterCols" Style="display: none;" Text="Save" OnClick="btnAfterSaveFilterCols_Click" runat="server" />--%>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="updatepanelchild" runat="server">
                        <ContentTemplate>
                            <div id="fields">
                                <h1 class="ui-widget-header">
                                    <b>Fields</b></h1>
                                <div id="fieldName">
                                    <div>
                                        <ul runat="server" id="ulFields" class="placeholder">
                                        </ul>
                                        <div class="clr">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="placeholder" style="float: left;">
                                <h1 class="ui-widget-header">
                                    <b>Drag Columns for Filter</b></h1>
                                <div class="ui-widget-content">
                                    <ol id="olPlaceholderControls" runat="server">
                                    </ol>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="clr">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <table style="width: 100%;" id="mainTable" class="pClass">
                <tr>
                    <td colspan="4" style="text-align: right;">
                        <%--<asp:UpdatePanel ID="UpdatePanel1" runat="server" style="float: right;">
                    <ContentTemplate>--%>
                        <asp:Button ID="btnSave" runat="server" ValidationGroup="filter" CssClass="savebtn"
                            Text="Save" OnClick="ButtonSave_Click" />
                        <asp:Button ID="btnbBindQueryResults" runat="server" CssClass="result" Text="Results"
                            OnClick="btnbBindQueryResults_Click" />
                        <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="clear" OnClick="btnNewFilter_Click" />
                        <asp:Button ID="btnNewFilter" runat="server" Text="New" CssClass="new" OnClick="btnNewFilter_Click" />
                        <asp:Button ID="btnEditFilterColumns" CssClass="edit" OnClientClick="editColumns();"
                            Text="Edit Columns" runat="server" />
                        <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="height: 10px;">
                    </td>
                </tr>
                <tr>
                    <td>
                        Look For:
                        <asp:DropDownList Width="150px" ID="ddlLookfor" AutoPostBack="true" OnSelectedIndexChanged="ddlLookfor_SelectedIndexChanged"
                            runat="server">
                        </asp:DropDownList>
                        <asp:Label ID="lblOrgID" Style="display: none;" Text="0" runat="server"></asp:Label>
                        <asp:Label ID="lblSelObjectID" Style="display: none;" Text="0" runat="server"></asp:Label>
                    </td>
                    <td>
                        Use Saved View:
                        <asp:DropDownList Width="150px" ID="ddlSavedView" AutoPostBack="true" runat="server"
                            OnSelectedIndexChanged="ddlSavedView_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        Filter Name:
                        <asp:TextBox Width="150px" onchange="checkExists()" ID="txtFilterName" runat="server"></asp:TextBox>
                        <image style="height: 20px; width: 20px; display: none;" src="images/update.jpg"
                            id="imgSuccess"></image>
                        <image style="height: 20px; width: 20px; display: none;" src="images/Cancel.jpg"
                            id="imgFailure"></image>
                        <asp:RequiredFieldValidator ID="rqFilter" SetFocusOnError="true" ValidationGroup="filter"
                            runat="server" ControlToValidate="txtFilterName" ErrorMessage="*" Style="color: Red;
                            font-weight: bold;"></asp:RequiredFieldValidator>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="height: 10px;">
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="background: #eee; padding: 5px">
                        <%-- <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                    <ContentTemplate>--%>
                        <asp:GridView ID="gvFilters" runat="server" GridLines="None" AutoGenerateColumns="false"
                            OnRowDeleting="gvFilters_RowDeleting" Style="width: 100%;">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlColumns" Width="150px" runat="server" OnSelectedIndexChanged="ddlColumns_SelectedIndexChanged"
                                            AutoPostBack="true" OnLoad="ddl1_load">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlFilterCondition" Width="150px" runat="server">
                                            <asp:ListItem Value="=" Text="Equals"></asp:ListItem>
                                            <asp:ListItem Value="!=" Text="Does Not Equal"></asp:ListItem>
                                            <asp:ListItem Value="Like" Text="Contains"></asp:ListItem>
                                            <asp:ListItem Value="Not Like" Text="Does Not Contain"></asp:ListItem>
                                            <asp:ListItem Value="Begin With" Text="Begins With"></asp:ListItem>
                                            <asp:ListItem Value="Does Not Begins With" Text="Does Not Begins With"></asp:ListItem>
                                            <asp:ListItem Value="Ends With" Text="Ends With"></asp:ListItem>
                                            <asp:ListItem Value="Does Not Ends With" Text="Does Not Ends With"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtBoxValue" runat="server"></asp:TextBox>
                                        <asp:DropDownList ID="ddlControlValues" runat="server">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlRelation" Width="150px" runat="server">
                                            <asp:ListItem Value="0" Text="Select Relation"></asp:ListItem>
                                            <asp:ListItem Value="AND" Text="AND"></asp:ListItem>
                                            <asp:ListItem Value="OR" Text="OR"></asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:CommandField ShowDeleteButton="True" />
                            </Columns>
                        </asp:GridView>
                        <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: center">
                        <asp:Button ID="ButtonAdd" OnClick="ButtonAdd_Click" runat="server" Text="Add New Row" />
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" colspan="4">
                        <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server">
                    <ContentTemplate>--%>
                        <asp:GridView ID="dataGridDeals" runat="server" ShowHeader="true" AutoGenerateColumns="true"
                            OnRowCreated="dataGridDeals_RowCreated" Style="width: 100%">
                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" BackColor="#0072c6" />
                        </asp:GridView>
                        <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
