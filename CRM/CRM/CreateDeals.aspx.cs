﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Web.UI.HtmlControls;
using System.Data;

namespace CRM
{
    public partial class CreateDeals : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            //CreateFields();
        }

        public void CreateFields()
        {
            clsCommonFunctions.CheckSession();
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataView dtColumns = _objDeals.getColumnControls(1).Tables[0].DefaultView;
            DataTable dtPlaceholders = _objDeals.getPlaceholders(1, 1).Tables[0];

            DataTable dtDeal = _objDeals.getSelectedDeal(11).Tables[0];

            for (int p = 0; p < dtPlaceholders.Rows.Count; p++)
            {
                PlaceHolder _objNewPlaceholder = new PlaceHolder();
                dtColumns.RowFilter = "Placeholder=" + dtPlaceholders.Rows[p]["ID"].ToString();

                string strHTML = "<span style='background-color:Gray; border:1px solid Black;color:white;'>" + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + "</span>" + "<hr />";
                _objNewPlaceholder.Controls.Add(new LiteralControl(strHTML));

                for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
                {
                    int formID = Convert.ToInt32(dtColumns.Table.DefaultView[col]["ID"]);
                    Label _objLabel = new Label();
                    _objLabel.Text = dtColumns.Table.DefaultView[col]["LabelName"].ToString();
                    _objNewPlaceholder.Controls.Add(_objLabel);

                    int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
                    if (fieldType == 1)
                    {
                        createTextFormatControl(col, _objNewPlaceholder, dtColumns.Table.DefaultView, dtDeal);
                    }
                    else if (fieldType == 3)
                    {
                        DataTable dtColumnChoices = _objDeals.getControlChoices(formID).Tables[0];
                        createSingleChoiceFormatControl(col, _objNewPlaceholder, dtColumns.Table.DefaultView, dtColumnChoices, dtDeal);
                    }
                    string strHTMLBreak = "<br />";
                    _objNewPlaceholder.Controls.Add(new LiteralControl(strHTMLBreak));
                }
                placeholderGeneralInfo.Controls.Add(_objNewPlaceholder);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            string colValues = "";
            DataTable dtColumns = _objDeals.getColumnControls(1).Tables[0];
            for (int col = 0; col < dtColumns.Rows.Count; col++)
            {
                int fieldType = Convert.ToInt32(dtColumns.Rows[col]["FieldType"]);
                int fieldFormat = Convert.ToInt32(dtColumns.Rows[col]["FieldFormat"]);
                if (colValues != "")
                {
                    colValues = colValues + ",";
                }
                if (fieldType == 1)
                {
                    TextBox _objtxt = (TextBox)placeholderGeneralInfo.FindControl(dtColumns.Rows[col]["ID"].ToString());
                    colValues = colValues + _objtxt.Text;
                }
                else if (fieldType == 3)
                {
                    if (fieldFormat == 3)//Radio Button List
                    {
                        RadioButtonList _objRdbList = (RadioButtonList)placeholderGeneralInfo.FindControl(dtColumns.Rows[col]["ID"].ToString());
                        colValues = colValues + _objRdbList.SelectedItem.Text;
                    }
                }
            }
            _objDeals.insertDeals(1, colValues, 0);
        }

        public void createTextFormatControl(int index, PlaceHolder _objplaceholder, DataView dtColumns, DataTable dtDeal)
        {
            string strColumnName = dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
            string Value = dtDeal.Rows[0][strColumnName].ToString();


            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
            if (fieldFormat == 1)//Simple text box
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                _objplaceholder.Controls.Add(_objTextBox);
            }
            else if (fieldFormat == 2)//Multiline text Box
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.TextMode = TextBoxMode.MultiLine;
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                _objplaceholder.Controls.Add(_objTextBox);
            }
            else if (fieldFormat == 3)//Email
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                _objplaceholder.Controls.Add(_objTextBox);
            }
        }

        public void createSingleChoiceFormatControl(int index, PlaceHolder _objplaceholder, DataView dtColumns, DataTable dtControlChoices, DataTable dtDeal)
        {
            string strColumnName = dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
            string Value = dtDeal.Rows[0][strColumnName].ToString();
            if (fieldFormat == 3)//Radio Button List
            {
                RadioButtonList _objRdbList = new RadioButtonList();
                _objRdbList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                for (int c = 0; c < dtControlChoices.Rows.Count; c++)
                {
                    _objRdbList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ID"].ToString()));
                    if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
                        _objRdbList.Items[c].Selected = true;
                    _objplaceholder.Controls.Add(_objRdbList);
                }

            }
        }
    }
}