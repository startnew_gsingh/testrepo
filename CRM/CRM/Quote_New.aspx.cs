﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;

namespace CRM
{
    public partial class Quote1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            if (!Page.IsPostBack)
            {
                bindQuoteGrid();
                bindFiltersDropDown();
            }
        }

        public void bindQuoteGrid()
        {
            CRMBLLQuote objCRMQuote = new CRMBLLQuote();

            DataSet dsQuote = objCRMQuote.getQuoteData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(0));
            if (dsQuote != null && dsQuote.Tables[0].Rows.Count > 0)
            {
                dataGridQuote.DataSource = dsQuote;
                dataGridQuote.DataBind();
            }
            else
            {
                dataGridQuote.DataSource = null;
                dataGridQuote.DataBind();
            }
        }

        public void bindFiltersDropDown()
        {
            CRMBLLQuote objCRMQuote = new CRMBLLQuote();
            ddlFilters.DataSource = objCRMQuote.getFilters(GlobalVariables.LoginUser.OrgID, 5);
            ddlFilters.DataTextField = "FilterName";
            ddlFilters.DataValueField = "ID";
            ddlFilters.DataBind();
            ddlFilters.Items.Insert(0, new ListItem("All Quote", "0"));
        }

        protected void checkBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            //selectedItemsPanel.Controls.Clear();
            string strSelValue = "";
            for (int li = 0; li < (sender as ListControl).Items.Count; li++)
            {
                if (!(sender as ListControl).Items[li].Selected)
                {
                    if (strSelValue != "")
                        strSelValue = strSelValue + ",";

                    strSelValue = strSelValue + (li + 2).ToString();
                }
            }

            ViewState["SelDDLValues"] = strSelValue;
            bindQuoteGrid();
        }

        protected void ddlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridQuote.DataSource = null;
            dataGridQuote.DataBind();
            bindQuoteGrid();
        }

        protected void dataGridQuote_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            GridViewSortExpression = e.SortExpression;

            //Gets the Pageindex of the GridView.
            int iPageIndex = dataGridQuote.PageIndex;
            dataGridQuote.DataSource = SortDataTable(myDataTable, false);
            dataGridQuote.DataBind();
            dataGridQuote.PageIndex = iPageIndex;
        }

        //Gets or Sets the GridView SortDirection Property
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        //Gets or Sets the GridView SortExpression Property
        private string GridViewSortExpression
        {
            get
            {
                return ViewState["SortExpression"] as string ?? string.Empty;
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        //Toggles between the Direction of the Sorting
        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        //Sorts the ResultSet based on the SortExpression and the Selected Column.
        protected DataView SortDataTable(DataTable myDataTable, bool isPageIndexChanging)
        {
            if (myDataTable != null)
            {
                DataView myDataView = new DataView(myDataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GetSortDirection());
                    }
                }
                return myDataView;
            }
            else
            {

                return new DataView();
            }
        }

        protected void dataGridQuote_DataBound(object sender, EventArgs e)
        {
            //Custom Paging
            GridViewRow dataGridQuoteRow = dataGridQuote.BottomPagerRow;

            if (dataGridQuoteRow == null) return;

            //Get your Controls from the GridView, in this case 
            //I use a DropDown Control for Paging
            DropDownList ddCurrentPage =
        (DropDownList)dataGridQuoteRow.Cells[0].FindControl("ddCurrentPage");
            Label lblTotalPage = (Label)dataGridQuoteRow.Cells[0].FindControl("lblTotalPage");

            if (ddCurrentPage != null)
            {
                //Populate Pager
                for (int i = 0; i < dataGridQuote.PageCount; i++)
                {
                    int iPageNumber = i + 1;
                    ListItem myListItem = new ListItem(iPageNumber.ToString());

                    if (i == dataGridQuote.PageIndex)
                        myListItem.Selected = true;

                    ddCurrentPage.Items.Add(myListItem);
                }
            }

            // Populate the Page Count
            if (lblTotalPage != null)
                lblTotalPage.Text = dataGridQuote.PageCount.ToString();

        }

        private DataSet GetViewState()
        {
            //Gets the ViewState
            return (DataSet)ViewState["QuoteDataSet"];
        }

        private void SetViewState(DataSet QuoteDataSet)
        {
            //Sets the ViewState
            ViewState["QuoteDataSet"] = QuoteDataSet;
        }

        protected void dataGridQuote_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            dataGridQuote.DataSource = SortDataTable(myDataTable, true);

            dataGridQuote.PageIndex = e.NewPageIndex;
            dataGridQuote.DataBind();
        }

        //Change to a different page when the DropDown Page is changed
        protected void ddCurrentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                GridViewRow dataGridQuoteRow = dataGridQuote.BottomPagerRow;
                DropDownList ddCurrentPage =
            (DropDownList)dataGridQuoteRow.Cells[0].FindControl("ddCurrentPage");

                dataGridQuote.PageIndex = ddCurrentPage.SelectedIndex;

                //Popultate the GridView Control
                DataSet myDataSet = GetViewState();
                DataTable myDataTable = myDataSet.Tables[0];

                dataGridQuote.DataSource = SortDataTable(myDataTable, true);
                dataGridQuote.DataBind();
            }
        }

        protected void imgPageFirst_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPagePrevious_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageNext_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageLast_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void Paginate(object sender, CommandEventArgs e)
        {
            // Get the Current Page Selected
            int iCurrentIndex = dataGridQuote.PageIndex;

            switch (e.CommandArgument.ToString().ToLower())
            {
                case "first":
                    dataGridQuote.PageIndex = 0;
                    break;
                case "prev":
                    if (dataGridQuote.PageIndex != 0)
                    {
                        dataGridQuote.PageIndex = iCurrentIndex - 1;
                    }
                    break;
                case "next":
                    dataGridQuote.PageIndex = iCurrentIndex + 1;
                    break;
                case "last":
                    dataGridQuote.PageIndex = dataGridQuote.PageCount;
                    break;
            }

            //Populate the GridView Control
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];

            dataGridQuote.DataSource = SortDataTable(myDataTable, true);
            dataGridQuote.DataBind();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLQuote objCRMQuote = new CRMBLLQuote();
            string IDS = "";
            for (int_index = 0; int_index <= dataGridQuote.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridQuote.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(dataGridQuote.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMQuote.deleteQuotes(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Quote Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindQuoteGrid();
        }

        protected void dataGridQuote_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[1].Visible = false; // hides the first column
                if (ViewState["SelDDLValues"] != null)
                {
                    string[] words = ViewState["SelDDLValues"].ToString().Split(',');
                    for (int w = 0; w < words.Count(); w++)
                    {
                        e.Row.Cells[Convert.ToInt32(words[w])].Visible = false; // hides the first column
                    }
                }
            }
            catch
            {
            }
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int int_index;
            Int64 IDS = 0;
            for (int_index = 0; int_index <= dataGridQuote.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridQuote.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = (Convert.ToInt64(dataGridQuote.DataKeys[int_index].Value));
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createQuote();", true);
        }
    }
}