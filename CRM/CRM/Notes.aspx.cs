﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Net;

namespace CRM
{
    public partial class Notes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                bindUsersGrid();
            }
            bindDocumentsList();
            bindTasksList();
            bindDocumentsOnlyList();
            bindPhoneCallsList();
            bindDiscussionsList();
            countActivities();
        }

        public void bindUsersGrid()
        {
            CRMBLLUsers objCRMUsers = new CRMBLLUsers();

            DataSet dsUser = objCRMUsers.getUsers();
            if (dsUser != null && dsUser.Tables[0].Rows.Count > 0)
            {
                dataGridUser.DataSource = dsUser;
                dataGridUser.DataBind();

                gvUsers.DataSource = dsUser;
                gvUsers.DataBind();
            }
            else
            {
                dataGridUser.DataSource = null;
                dataGridUser.DataBind();

                gvUsers.DataSource = null;
                gvUsers.DataBind();
            }
        }

        protected void countActivities()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataView dsDocumets = objDeals.countActivities(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["selObjectUserID"])).Tables[0].DefaultView;
            lblNotes1.Text = dsDocumets.Table.Rows[0]["TotalNotes"].ToString();
            lblNotes2.Text = dsDocumets.Table.Rows[0]["TotalNotes"].ToString();
            lblNotes3.Text = dsDocumets.Table.Rows[0]["TotalNotes"].ToString();
            lblNotes4.Text = dsDocumets.Table.Rows[0]["TotalNotes"].ToString();
            Label1.Text = dsDocumets.Table.Rows[0]["TotalNotes"].ToString();


            lblActivities1.Text = dsDocumets.Table.Rows[0]["TotalActivities"].ToString();
            lblActivities2.Text = dsDocumets.Table.Rows[0]["TotalActivities"].ToString();
            lblActivities3.Text = dsDocumets.Table.Rows[0]["TotalActivities"].ToString();
            lblActivities4.Text = dsDocumets.Table.Rows[0]["TotalActivities"].ToString();
            Label2.Text = dsDocumets.Table.Rows[0]["TotalActivities"].ToString();


            lblPhoneCalls1.Text = dsDocumets.Table.Rows[0]["TotalPhoneCalls"].ToString();
            lblPhoneCalls2.Text = dsDocumets.Table.Rows[0]["TotalPhoneCalls"].ToString();
            lblPhoneCalls3.Text = dsDocumets.Table.Rows[0]["TotalPhoneCalls"].ToString();
            lblPhoneCalls4.Text = dsDocumets.Table.Rows[0]["TotalPhoneCalls"].ToString();
            Label3.Text = dsDocumets.Table.Rows[0]["TotalPhoneCalls"].ToString();


            lblDocuments1.Text = dsDocumets.Table.Rows[0]["TotalDocuments"].ToString();
            lblDocuments2.Text = dsDocumets.Table.Rows[0]["TotalDocuments"].ToString();
            lblDocuments3.Text = dsDocumets.Table.Rows[0]["TotalDocuments"].ToString();
            lblDocuments4.Text = dsDocumets.Table.Rows[0]["TotalDocuments"].ToString();
            Label4.Text = dsDocumets.Table.Rows[0]["TotalDocuments"].ToString();


            Label8.Text = dsDocumets.Table.Rows[0]["TotalDiscussion"].ToString();
            Label7.Text = dsDocumets.Table.Rows[0]["TotalDiscussion"].ToString();
            Label6.Text = dsDocumets.Table.Rows[0]["TotalDiscussion"].ToString();
            Label5.Text = dsDocumets.Table.Rows[0]["TotalDiscussion"].ToString();
            lblDiscussionCount.Text = dsDocumets.Table.Rows[0]["TotalDiscussion"].ToString();
        }




        protected void btnUploadNotes_Click(object sender, EventArgs e)
        {
            uploadDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["selObjectUserID"]));
            //bindTasksList();
            //bindDocumentsList();
            //bindDocumentsOnlyList();
            //bindPhoneCallsList();
            //bindDiscussionsList();

            txtCallDate.Text = "";
            txtDescription.Text = "";
            txtDelPhoneCalls.Text = "";

            txtNotes.Text = "";
            txtSubject.Text = "";
            txtDescription.Text = "";
            txtDueDate.Text = "";
            txtDelTasks.Text = "";
            txtDelDocuments.Text = "";
            txtDelDocumentsOnly.Text = "";
            txtDelPhoneCalls.Text = "";
            txtDiscussionSubject.Text = "";
            txtDiscussionDescription.Text = "";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "emailToUsers();", true);
        }

        private void bindDocumentsList()
        {
            lstDocuments.Controls.Clear();
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataView dsDocumets = objDeals.getDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["selObjectUserID"])).Tables[0].DefaultView;
            //List<Utility.clsCompanyDocuments> objclsCompanyDocuments = objCompany.getCompanyDocuments(companyId);
            dsDocumets.RowFilter = "IsDocsOnly=0";

            for (int d = 0; d < dsDocumets.Table.DefaultView.Count; d++)
            {
                Table tblDocuments = new Table();
                tblDocuments.CssClass = "docTable";

                TableRow trDocuments = new TableRow();
                trDocuments.ID = "del" + dsDocumets.Table.DefaultView[d]["ID"].ToString();
                trDocuments.CssClass = "docTr";

                TableCell tcDocuments = new TableCell();
                tcDocuments.CssClass = "docTd";
                Label lbDocName = new Label();
                lbDocName.Text = "x";
                lbDocName.CssClass = "cross";
                lbDocName.Attributes.Add("onclick", "delDocument('del" + dsDocumets.Table.DefaultView[d]["ID"] + "','notes" + dsDocumets.Table.DefaultView[d]["ID"] + "');");
                tcDocuments.Controls.Add(lbDocName);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);



                trDocuments = new TableRow();
                trDocuments.CssClass = "docTr";
                trDocuments.ID = "notes" + dsDocumets.Table.DefaultView[d]["ID"].ToString();

                tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.CssClass = "docTd";
                //Label lbNotes = new Label();
                //lbNotes.Text = dsDocumets.Table.DefaultView[d]["Notes"].ToString();
                LinkButton lbNotes = new LinkButton();
                lbNotes.Text = dsDocumets.Table.DefaultView[d]["Notes"].ToString();
                lbNotes.CommandName = "editNote";
                lbNotes.CommandArgument = dsDocumets.Table.DefaultView[d]["Notes"].ToString();
                lbNotes.Command += new CommandEventHandler(notesLink_Click);


                tcDocuments.Controls.Add(lbNotes);
                trDocuments.Controls.Add(tcDocuments);



                tcDocuments = new TableCell();
                //HtmlAnchor anc = new HtmlAnchor();
                HyperLink anc = new HyperLink();
                anc.Text = dsDocumets.Table.DefaultView[d]["DocumentName"].ToString();
                anc.CssClass = "docTr";
                anc.NavigateUrl = "DownloadFile.aspx?name=" + dsDocumets.Table.DefaultView[d]["DocumentPath"].ToString() + "/" + dsDocumets.Table.DefaultView[d]["DocumentName"].ToString();

                tcDocuments.Controls.Add(anc);
                trDocuments.Controls.Add(tcDocuments);

                tblDocuments.Controls.Add(trDocuments);
                lstDocuments.Controls.Add(tblDocuments);
            }
        }

        protected void notesLink_Click(object sender, CommandEventArgs e)
        {
            ViewState["Notes"] = e.CommandArgument;
            ViewState["Subject"] = "Notes";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "emailToUsers();", true);
        }

        private void uploadDocuments(Int64 orgID, Int64 DealID)
        {
            try
            {
                //string path = Server.MapPath("~\\MyFiles\\" + Session["ObjectID"].ToString() + orgID.ToString() + DealID.ToString());

                //if (!Directory.Exists(path))
                //{
                //    Directory.CreateDirectory(path);
                //}
                string documentName = "";
                string documentPath = "";
                //if (fleUpload.FileName != "")
                //{
                //    fleUpload.SaveAs(path + "\\" + System.IO.Path.GetFileName(fleUpload.FileName));
                //    documentName = fleUpload.FileName;
                //    documentPath = path;
                //}
                //if (documentName != "")
                //{
                ViewState["Notes"] = txtNotes.Text;
                ViewState["Subject"] = "Notes";
                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                objBLLDeals.insertDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), DealID, documentName, documentPath, Convert.ToInt64(Session["selObjectUserID"]), txtDelDocuments.Text, txtNotes.Text, false, false);
                //countActivities();

                //}
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDeleteNotes_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objBLLDeals = new CRMBLLDeals();
            objBLLDeals.insertDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), 0, "", "", Convert.ToInt64(Session["selObjectUserID"]), txtDelDocuments.Text, txtNotes.Text, false, true);
            countActivities();
            //bindPhoneCallsList();
            //bindTasksList();
            //bindDocumentsOnlyList();
            bindDocumentsList();
            //bindDiscussionsList();
            txtDelDocuments.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "Notes();", true);
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            sendEmail();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            //bindDocumentsOnlyList();
            //bindDocumentsList();
            //bindTasksList();
            //bindPhoneCallsList();
            //bindDiscussionsList();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeAfterSaveWindow('" + ViewState["Subject"].ToString() + "');", true);
        }

        protected void sendEmail()
        {
            const string SERVER = "mail.i-srs.us";
            MailMessage oMail = new MailMessage();
            oMail.From = new MailAddress("postmaster@i-srs.us");

            for (int int_index = 0; int_index <= dataGridUser.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridUser.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) //IDS = IDS + ((Label)dataGridUser.Rows[int_index].FindControl("lblUserName")).Text + ";";
                    oMail.To.Add(((Label)dataGridUser.Rows[int_index].FindControl("lblUserName")).Text);
            }

            oMail.Subject = "CRM - " + ViewState["Subject"].ToString();
            oMail.IsBodyHtml = true;
            oMail.Priority = MailPriority.High; // enumeration
            oMail.Body = ViewState["Notes"].ToString() + "<br><br />Best Regards";
            SmtpClient smtp = new SmtpClient(SERVER);
            NetworkCredential Credentials = new NetworkCredential("postmaster@i-srs.us", "srs1234");
            smtp.Credentials = Credentials;
            smtp.Send(oMail);
            oMail = null; // free up resources
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }



        protected void btnUploadDocs_Click(object sender, EventArgs e)
        {
            uploadDocumentsOnly(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["selObjectUserID"]));
            //bindTasksList();
            //bindDocumentsList();
            //bindDocumentsOnlyList();
            //bindPhoneCallsList();
            //bindDiscussionsList();

            txtCallDate.Text = "";
            txtDescription.Text = "";
            txtDelPhoneCalls.Text = "";

            txtNotes.Text = "";
            txtSubject.Text = "";
            txtDescription.Text = "";
            txtDueDate.Text = "";
            txtDelTasks.Text = "";
            txtDelDocuments.Text = "";
            txtDelDocumentsOnly.Text = "";
            txtDelPhoneCalls.Text = "";
            txtDiscussionSubject.Text = "";
            txtDiscussionDescription.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "Documents();", true);
        }

        private void bindDocumentsOnlyList()
        {
            lstOnlyDocuments.Controls.Clear();
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataView dsDocumets = objDeals.getDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["selObjectUserID"])).Tables[0].DefaultView;
            dsDocumets.RowFilter = "IsDocsOnly=1";


            for (int d = 0; d < dsDocumets.Table.DefaultView.Count; d++)
            {
                Table tblDocuments = new Table();
                tblDocuments.CssClass = "docTable";

                TableRow trDocuments = new TableRow();
                trDocuments.ID = "deldocs" + dsDocumets.Table.DefaultView[d]["ID"].ToString();
                trDocuments.CssClass = "docTr";

                TableCell tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.CssClass = "docTd";
                Label lbDocName = new Label();
                lbDocName.Text = "x";
                lbDocName.CssClass = "cross";
                lbDocName.Attributes.Add("onclick", "delDocumentOnly('deldocs" + dsDocumets.Table.DefaultView[d]["ID"] + "','docs" + dsDocumets.Table.DefaultView[d]["ID"] + "');");
                tcDocuments.Controls.Add(lbDocName);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);



                trDocuments = new TableRow();
                trDocuments.CssClass = "docTr";
                trDocuments.ID = "docs" + dsDocumets.Table.DefaultView[d]["ID"].ToString();

                tcDocuments = new TableCell();
                tcDocuments.CssClass = "docTd";
                Label lbNotes = new Label();
                lbNotes.Text = dsDocumets.Table.DefaultView[d]["Notes"].ToString();
                tcDocuments.Controls.Add(lbNotes);
                trDocuments.Controls.Add(tcDocuments);




                tcDocuments = new TableCell();
                //HtmlAnchor anc = new HtmlAnchor();
                HyperLink anc = new HyperLink();
                anc.Text = dsDocumets.Table.DefaultView[d]["DocumentName"].ToString();
                anc.CssClass = "docTr";
                anc.NavigateUrl = "DownloadFile.aspx?name=" + dsDocumets.Table.DefaultView[d]["DocumentPath"].ToString() + "/" + dsDocumets.Table.DefaultView[d]["DocumentName"].ToString();

                tcDocuments.Controls.Add(anc);
                trDocuments.Controls.Add(tcDocuments);

                tblDocuments.Controls.Add(trDocuments);
                //lstDocuments.Controls.Add(tblDocuments);

                lstOnlyDocuments.Controls.Add(tblDocuments);
            }
        }

        private void uploadDocumentsOnly(Int64 orgID, Int64 DealID)
        {
            //try
            //{
            string path = Server.MapPath("~\\MyFiles\\" + Session["ObjectID"].ToString() + orgID.ToString() + DealID.ToString());

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            string documentName = "";
            string documentPath = "";
            if (fleUploadDocuments.FileName != "")
            {
                fleUploadDocuments.SaveAs(path + "\\" + System.IO.Path.GetFileName(fleUploadDocuments.FileName));
                documentName = fleUploadDocuments.FileName;
                documentPath = path;

                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                objBLLDeals.insertDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), DealID, documentName, documentPath, Convert.ToInt64(Session["selObjectUserID"]), txtDelDocumentsOnly.Text, "", true, false);
                //countActivities();
            }
            //}
            //catch (Exception ex)
            //{

            //}
        }

        protected void btnDelDocs_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objBLLDeals = new CRMBLLDeals();
            objBLLDeals.insertDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), 0, "", "", Convert.ToInt64(Session["selObjectUserID"]), txtDelDocumentsOnly.Text, "", true, true);
            countActivities();
            //bindPhoneCallsList();
            //bindTasksList();
            bindDocumentsOnlyList();
            //bindDocumentsList();
            //bindDiscussionsList();
            txtDelDocumentsOnly.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "Documents();", true);
        }




        //Upload Tasks
        protected void btnUploadTasks_Click(object sender, EventArgs e)
        {
            uploadTasks(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["selObjectUserID"]));
            //bindTasksList();
            //bindDocumentsList();
            //bindDocumentsOnlyList();
            //bindPhoneCallsList();
            //bindDiscussionsList();

            txtCallDate.Text = "";
            txtDescription.Text = "";
            txtDelPhoneCalls.Text = "";

            txtNotes.Text = "";
            txtSubject.Text = "";
            txtDescription.Text = "";
            txtDueDate.Text = "";
            txtDelTasks.Text = "";
            txtDelDocuments.Text = "";
            txtDelDocumentsOnly.Text = "";
            txtDelPhoneCalls.Text = "";
            txtDiscussionSubject.Text = "";
            txtDiscussionDescription.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "emailToUsers();", true);
        }

        private void bindTasksList()
        {
            lsUploadTasks.Controls.Clear();

            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDocumets = objDeals.getTasks(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["selObjectUserID"]));
            //List<Utility.clsCompanyDocuments> objclsCompanyDocuments = objCompany.getCompanyDocuments(companyId);


            for (int d = 0; d < dsDocumets.Tables[0].Rows.Count; d++)
            {
                Table tblDocuments = new Table();
                tblDocuments.CssClass = "docTable";

                TableRow trDocuments = new TableRow();
                trDocuments.ID = "delTasks" + dsDocumets.Tables[0].Rows[d]["ID"].ToString();
                trDocuments.CssClass = "docTr";

                TableCell tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.CssClass = "docTd";
                Label lbDocName = new Label();
                lbDocName.Text = "x";
                lbDocName.CssClass = "cross";
                lbDocName.Attributes.Add("onclick", "delTask('delTasks" + dsDocumets.Tables[0].Rows[d]["ID"] + "','tasks" + dsDocumets.Tables[0].Rows[d]["ID"] + "','tasksDesc" + dsDocumets.Tables[0].Rows[d]["ID"] + "');");
                tcDocuments.Controls.Add(lbDocName);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);




                trDocuments = new TableRow();
                trDocuments.CssClass = "docTr";
                trDocuments.ID = "tasks" + dsDocumets.Tables[0].Rows[d]["ID"].ToString();

                tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.CssClass = "docTd";
                //Label lbNotes = new Label();
                //lbNotes.Text = dsDocumets.Tables[0].Rows[d]["Subject"].ToString();

                LinkButton lbNotes = new LinkButton();
                lbNotes.CommandArgument = dsDocumets.Tables[0].Rows[d]["Subject"].ToString() + "<br><br />" + dsDocumets.Tables[0].Rows[d]["Description"].ToString();
                lbNotes.Text = dsDocumets.Tables[0].Rows[d]["Subject"].ToString();
                lbNotes.CommandName = "editDocument";
                lbNotes.Command += new CommandEventHandler(tasksLink_Click);

                tcDocuments.Controls.Add(lbNotes);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);



                trDocuments = new TableRow();
                trDocuments.CssClass = "docTr";
                trDocuments.ID = "tasksDesc" + dsDocumets.Tables[0].Rows[d]["ID"].ToString();

                tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                Label anc = new Label();
                anc.Text = dsDocumets.Tables[0].Rows[d]["Description"].ToString();
                anc.CssClass = "docTr";

                tcDocuments.Controls.Add(anc);
                trDocuments.Controls.Add(tcDocuments);

                tblDocuments.Controls.Add(trDocuments);
                lsUploadTasks.Controls.Add(tblDocuments);
            }
        }

        protected void tasksLink_Click(object sender, CommandEventArgs e)
        {
            ViewState["Notes"] = e.CommandArgument;
            ViewState["Subject"] = "Tasks";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "emailToUsers();", true);
        }

        private void uploadTasks(Int64 orgID, Int64 DealID)
        {
            try
            {
                ViewState["Notes"] = txtSubject.Text + "<br><br />" + txtDescription.Text;
                ViewState["Subject"] = "Tasks";

                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                objBLLDeals.insertTasks(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), DealID, txtSubject.Text, txtDescription.Text, Convert.ToDateTime(txtDueDate.Text), ddlPrioirty.SelectedValue, Convert.ToInt64(Session["selObjectUserID"]), txtDelTasks.Text, false);
                //countActivities();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDeleteTasks_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objBLLDeals = new CRMBLLDeals();
            objBLLDeals.insertTasks(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), 0, txtSubject.Text, txtDescription.Text, DateTime.Now, ddlPrioirty.SelectedValue, Convert.ToInt64(Session["selObjectUserID"]), txtDelTasks.Text + ",", true);
            countActivities();
            //bindPhoneCallsList();
            bindTasksList();
            //bindDocumentsOnlyList();
            //bindDocumentsList();
            //bindDiscussionsList();
            txtDelTasks.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "Tasks();", true);
        }




        //Upload PhoneCalls
        protected void btnUploadPhoneCalls_Click(object sender, EventArgs e)
        {
            uploadphoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["selObjectUserID"]));
            //bindTasksList();
            //bindDocumentsList();
            //bindDocumentsOnlyList();
            //bindPhoneCallsList();
            //bindDiscussionsList();

            txtPhoneCallDescription.Text = "";
            txtCallDate.Text = "";
            txtDescription.Text = "";
            txtDelPhoneCalls.Text = "";

            txtNotes.Text = "";
            txtSubject.Text = "";
            txtDescription.Text = "";
            txtDueDate.Text = "";
            txtDelTasks.Text = "";
            txtDelDocuments.Text = "";
            txtDelDocumentsOnly.Text = "";
            txtDiscussionSubject.Text = "";
            txtDiscussionDescription.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "emailToUsers();", true);
        }

        private void bindPhoneCallsList()
        {
            lsUploadPhoneCalls.Controls.Clear();
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDocumets = objDeals.getPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["selObjectUserID"]));
            //List<Utility.clsCompanyDocuments> objclsCompanyDocuments = objCompany.getCompanyDocuments(companyId);


            for (int d = 0; d < dsDocumets.Tables[0].Rows.Count; d++)
            {
                Table tblDocuments = new Table();
                tblDocuments.CssClass = "docTable";

                TableRow trDocuments = new TableRow();
                trDocuments.ID = "delPhoneCalls" + dsDocumets.Tables[0].Rows[d]["ID"].ToString();
                trDocuments.CssClass = "docTr";

                TableCell tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.CssClass = "docTd";
                Label lbDocName = new Label();
                lbDocName.Text = "x";
                lbDocName.CssClass = "cross";
                lbDocName.Attributes.Add("onclick", "delPhoneCalls('delPhoneCalls" + dsDocumets.Tables[0].Rows[d]["ID"] + "','PhoneCalls" + dsDocumets.Tables[0].Rows[d]["ID"] + "','phoneCallsDesc" + dsDocumets.Tables[0].Rows[d]["ID"] + "','descPhoneCalls" + dsDocumets.Tables[0].Rows[d]["ID"] + "');");
                tcDocuments.Controls.Add(lbDocName);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);



                trDocuments = new TableRow();
                trDocuments.CssClass = "docTr";
                trDocuments.ID = "PhoneCalls" + dsDocumets.Tables[0].Rows[d]["ID"].ToString();

                tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.CssClass = "docTd";
                Label lbNotes = new Label();
                lbNotes.Text = dsDocumets.Tables[0].Rows[d]["Description"].ToString();
                tcDocuments.Controls.Add(lbNotes);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);



                trDocuments = new TableRow();
                trDocuments.CssClass = "docTr";
                trDocuments.ID = "phoneCallsDesc" + dsDocumets.Tables[0].Rows[d]["ID"].ToString();
                tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                //HtmlAnchor anc = new HtmlAnchor();
                //Label anc = new Label();
                //anc.Text = dsDocumets.Tables[0].Rows[d]["Description"].ToString();

                LinkButton anc = new LinkButton();
                anc.Text = dsDocumets.Tables[0].Rows[d]["Description"].ToString();
                anc.CommandName = "editDocument";
                anc.CommandArgument = dsDocumets.Tables[0].Rows[d]["Description"].ToString() + "<br><br />" + dsDocumets.Tables[0].Rows[d]["DueDate"].ToString() + "<br><br />" + dsDocumets.Tables[0].Rows[d]["Direction"].ToString();
                anc.Command += new CommandEventHandler(phoneCallsLink_Click);
                anc.CssClass = "docTr";

                tcDocuments.Controls.Add(anc);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);


                trDocuments = new TableRow();
                trDocuments.ID = "descPhoneCalls" + dsDocumets.Tables[0].Rows[d]["ID"].ToString();
                trDocuments.CssClass = "docTr";

                tcDocuments = new TableCell();
                tcDocuments.CssClass = "docTd";

                Label ancPhoneCallsDate = new Label();
                ancPhoneCallsDate.Text = dsDocumets.Tables[0].Rows[d]["DueDate"].ToString();
                ancPhoneCallsDate.CssClass = "docTr";
                tcDocuments.Controls.Add(ancPhoneCallsDate);
                trDocuments.Controls.Add(tcDocuments);

                tcDocuments = new TableCell();
                tcDocuments.CssClass = "docTd";
                Label ancPhoneCallsDirection = new Label();
                ancPhoneCallsDirection.Text = dsDocumets.Tables[0].Rows[d]["Direction"].ToString();
                ancPhoneCallsDirection.CssClass = "docTr";
                tcDocuments.Controls.Add(ancPhoneCallsDirection);
                trDocuments.Controls.Add(tcDocuments);


                tblDocuments.Controls.Add(trDocuments);
                lsUploadPhoneCalls.Controls.Add(tblDocuments);
            }
        }

        protected void phoneCallsLink_Click(object sender, CommandEventArgs e)
        {
            ViewState["Notes"] = e.CommandArgument;
            ViewState["Subject"] = "PhoneCalls";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "emailToUsers();", true);
        }

        private void uploadphoneCalls(Int64 orgID, Int64 DealID)
        {
            try
            {
                ViewState["Notes"] = txtPhoneCallDescription.Text + "<br><br />" + txtCallDate.Text + "<br><br />" + ddlDirection.SelectedValue;
                ViewState["Subject"] = "PhoneCalls";
                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                objBLLDeals.insertPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), DealID, txtPhoneCallDescription.Text, Convert.ToDateTime(txtCallDate.Text), ddlDirection.SelectedValue, Convert.ToInt64(Session["selObjectUserID"]), txtDelPhoneCalls.Text, false);
                //countActivities();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDelPhoneCalls_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objBLLDeals = new CRMBLLDeals();
            objBLLDeals.insertPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), 0, txtPhoneCallDescription.Text, DateTime.Now, ddlDirection.SelectedValue, Convert.ToInt64(Session["selObjectUserID"]), txtDelPhoneCalls.Text, true);
            countActivities();
            bindPhoneCallsList();
            //bindTasksList();
            //bindDocumentsOnlyList();
            //bindDocumentsList();
            //bindDiscussionsList();
            txtDelPhoneCalls.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "PhoneCalls();", true);
        }




        //Upload Discussion
        protected void btnSaveDiscussion_Click(object sender, EventArgs e)
        {
            uploadDiscussion(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["selObjectUserID"]));
            //bindTasksList();
            //bindDocumentsList();
            //bindDocumentsOnlyList();
            //bindPhoneCallsList();
            //bindDiscussionsList();

            txtCallDate.Text = "";
            txtDescription.Text = "";
            txtDelPhoneCalls.Text = "";

            txtNotes.Text = "";
            txtSubject.Text = "";
            txtDescription.Text = "";
            txtDueDate.Text = "";
            txtDelTasks.Text = "";
            txtDelDocuments.Text = "";
            txtDelDocumentsOnly.Text = "";
            txtDelPhoneCalls.Text = "";
            txtDiscussionSubject.Text = "";
            txtDiscussionDescription.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "discussionUsers();", true);
        }

        private void bindDiscussionsList()
        {
            lsUploadDiscussion.Controls.Clear();
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDiscussion = objDeals.getDiscussions(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["selObjectUserID"]));

            for (int d = 0; d < dsDiscussion.Tables[0].Rows.Count; d++)
            {
                Table tblDocuments = new Table();
                tblDocuments.CssClass = "docTable";

                TableRow trDocuments = new TableRow();
                trDocuments.ID = "delDiscussion" + dsDiscussion.Tables[0].Rows[d]["ID"].ToString();
                trDocuments.CssClass = "docTr";

                TableCell tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.CssClass = "docTd";
                Label lbDocName = new Label();
                lbDocName.Text = "x";
                lbDocName.CssClass = "cross";
                lbDocName.Attributes.Add("onclick", "delDiscussion('delDiscussion" + dsDiscussion.Tables[0].Rows[d]["ID"] + "','discussion" + dsDiscussion.Tables[0].Rows[d]["ID"] + "','discussionDesc" + dsDiscussion.Tables[0].Rows[d]["ID"] + "');");
                tcDocuments.Controls.Add(lbDocName);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);




                trDocuments = new TableRow();
                trDocuments.CssClass = "docTr";
                trDocuments.ID = "discussion" + dsDiscussion.Tables[0].Rows[d]["ID"].ToString();

                tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                tcDocuments.CssClass = "docTd";
                //Label lbNotes = new Label();
                //lbNotes.Text = dsDiscussion.Tables[0].Rows[d]["Subject"].ToString();

                LinkButton lbNotes = new LinkButton();
                lbNotes.Text = dsDiscussion.Tables[0].Rows[d]["Subject"].ToString();
                lbNotes.CommandName = "editDocument";
                //lbNotes.CommandArgument = dsDocumets.Tables[0].Rows[d]["Description"].ToString() + "<br><br />" + dsDocumets.Tables[0].Rows[d]["DueDate"].ToString() + "<br><br />" + dsDocumets.Tables[0].Rows[d]["Direction"].ToString();
                lbNotes.Command += new CommandEventHandler(discussionLink_Click);

                tcDocuments.Controls.Add(lbNotes);
                trDocuments.Controls.Add(tcDocuments);
                tblDocuments.Controls.Add(trDocuments);



                trDocuments = new TableRow();
                trDocuments.CssClass = "docTr";
                trDocuments.ID = "discussionDesc" + dsDiscussion.Tables[0].Rows[d]["ID"].ToString();

                tcDocuments = new TableCell();
                tcDocuments.Attributes.Add("colspan", "2");
                Label anc = new Label();
                anc.Text = dsDiscussion.Tables[0].Rows[d]["Description"].ToString();
                anc.CssClass = "docTr";

                tcDocuments.Controls.Add(anc);
                trDocuments.Controls.Add(tcDocuments);

                tblDocuments.Controls.Add(trDocuments);
                lsUploadDiscussion.Controls.Add(tblDocuments);
            }
        }

        protected void discussionLink_Click(object sender, CommandEventArgs e)
        {
            //ViewState["Notes"] = e.CommandArgument;
            //ViewState["Subject"] = "Discussion";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "discussionUsers();", true);
        }

        private void uploadDiscussion(Int64 orgID, Int64 DealID)
        {
            try
            {
                ViewState["Notes"] = txtDiscussionSubject.Text + "<br><br />" + txtDiscussionDescription.Text;
                ViewState["Subject"] = "Discussion";

                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                ViewState["DiscussionID"] = objBLLDeals.insertDiscussion(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), DealID, txtDiscussionSubject.Text, txtDiscussionDescription.Text, Convert.ToInt64(Session["selObjectUserID"]), txtDelDiscussion.Text, false, ddlEntity.SelectedValue);
                //countActivities();
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDeleteDiscussion_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objBLLDeals = new CRMBLLDeals();
            objBLLDeals.insertDiscussion(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), 0, txtDiscussionSubject.Text, txtDiscussionDescription.Text, Convert.ToInt64(Session["selObjectUserID"]), txtDelDiscussion.Text, true, "");
            countActivities();
            //bindPhoneCallsList();
            //bindTasksList();
            //bindDocumentsOnlyList();
            //bindDocumentsList();
            bindDiscussionsList();
            txtDelDiscussion.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "Discussion();", true);
        }




        protected void btnSendDiscussion_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            int int_index = 1;
            for (int_index = 0; int_index <= gvUsers.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvUsers.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) objDeals.insertUserDiscussion(Convert.ToInt64(ViewState["DiscussionID"]), Convert.ToInt64(gvUsers.DataKeys[int_index].Value));
            }
            //DataSet dsDiscussion = objDeals.insertUserDiscussion();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }
    }
}