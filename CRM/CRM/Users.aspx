﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Users.aspx.cs" Inherits="CRM.Users" %>

<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <style>
        .message-popup
        {
            width: 36%;
            margin: 0 31%;
            min-height: 200px;
            max-height: 500px;
        }
    </style>
    <script type="text/javascript">
        function checkExists() {
            var txtValue = $("#ContentPlaceHolder1_txtUserName").val();
            var selUserValue = $("#ContentPlaceHolder1_lblUserID").html();


            $.ajax({
                type: "POST",
                url: "CustomList.asmx/checkUserAvailability",
                data: '{Value: "' + txtValue + '",selUserID: "' + selUserValue + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    switch (response.d) {
                        case 0:
                            $("#imgSuccess").show();
                            $("#imgFailure").hide();
                            //$("#ContentPlaceHolder1_btnSave").removeAttr("disabled");
                            checkEmailExists(0);
                            break;
                        case 1:
                            $("#imgSuccess").hide();
                            $("#imgFailure").show();
                            $("#ContentPlaceHolder1_btnSave").attr("disabled", "disabled");
                            checkEmailExists(1);
                            break;
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        }

        function checkEmailExists(sValue) {
            var txtEmail = $("#ContentPlaceHolder1_txtEmail").val();
            var selUserValue = $("#ContentPlaceHolder1_lblUserID").html();

            $.ajax({
                type: "POST",
                url: "CustomList.asmx/checkEmailAvailability",
                data: '{Value: "' + txtEmail + '",selUserID: "' + selUserValue + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    switch (response.d) {
                        case 0:
                            $("#imgSuccessemail").show();
                            $("#imgFailureemail").hide();
                            if (sValue == 0)
                                $("#ContentPlaceHolder1_btnSave").removeAttr("disabled");
                            break;
                        case 1:
                            $("#imgSuccessemail").hide();
                            $("#imgFailureemail").show();
                            $("#ContentPlaceHolder1_btnSave").attr("disabled", "disabled");
                            break;
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        }

        function createUser() {
            var createUserBox = "#div_CreateUser-box";
            //Fade in the Popup
            $(createUserBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createUserBox).height() + 24) / 2;
            var popMargLeft = ($(createUserBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);
            checkExists();
            return true;
        };

        function btnassignRoles() {
            document.getElementById("ContentPlaceHolder1_btnAssignRoles").click();

        }

        function btneditUser() {
            document.getElementById("ContentPlaceHolder1_btnEdit").click();

        }

        function assignRoles() {
            var createUserBox = "#div_AssignRoles-box";
            //Fade in the Popup
            $(createUserBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createUserBox).height() + 24) / 2;
            var popMargLeft = ($(createUserBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };


        function editRolePermissions() {
            var editRolePermissionsBox = "#div_editRolePermissions-box";
            //Fade in the Popup
            $(editRolePermissionsBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(editRolePermissionsBox).height() + 24) / 2;
            var popMargLeft = ($(editRolePermissionsBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                //btneditUser();
                document.getElementById("ContentPlaceHolder1_btnRefreshUsers").click();
            });
            return false;
        }
        function closeUserWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                //window.location = "Users.aspx";
                document.getElementById("ContentPlaceHolder1_btnRefreshUsers").click();
            });
            return false;
        }

        // When clicking on the button close or the mask layer the popup closed
        function closePermissionsWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
            });
            assignRoles();
            return false;
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="div_editRolePermissions-box" class="message-popup" style="overflow: hidden;
                width: 65%; left: -15%;">
                <a href="#" class="close" style="top: 0px; right: -22px;">
                    <asp:ImageButton ImageUrl="images/close_pop.png" OnClick="img_ClosePopup" class="btn_close"
                        runat="server" /></a>
                <div id="Div1" style="display: block;">
                    <div id="div4" runat="server" style="overflow: auto; width: 100%; height: 100% !important;">
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:ListView ID="lstRolePermissions" runat="server" DataKeyNames="ID">
                                        <LayoutTemplate>
                                            <table border="0" cellpadding="1" width="100%">
                                                <tr style="background-color: #E5E5FE; color: White;">
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkentity" Style="color: White;" runat="server">Entity</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkCreate" Style="color: White;" runat="server">Create</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkEdit" Style="color: White;" runat="server">Edit</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkDelete" Style="color: White;" runat="server">Delete</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkImport" Style="color: White;" runat="server">Import</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkExport" Style="color: White;" runat="server">Export</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkRead" Style="color: White;" runat="server">Read</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="LinkButton1" Style="color: White;" runat="server">Customize</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="LinkButton2" Style="color: White;" runat="server">Advance Find</asp:LinkButton>
                                                    </th>
                                                </tr>
                                                <tr id="itemPlaceholder" runat="server">
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label runat="server" ID="chkEntity" Text='<%#Eval("ObjectName") %>'></asp:Label>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" Enabled="false" ID="Create" Checked='<%#Eval("Create") %>'>
                                                    </asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" Enabled="false" ID="Edit" Checked='<%#Eval("Edit") %>'>
                                                    </asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" Enabled="false" ID="Delete" Checked='<%#Eval("Delete") %>'>
                                                    </asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" Enabled="false" ID="Import" Checked='<%#Eval("Import") %>'>
                                                    </asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" Enabled="false" ID="Export" Checked='<%#Eval("Export") %>'>
                                                    </asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" Enabled="false" ID="Read" Checked='<%#Eval("Read") %>'>
                                                    </asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" Enabled="false" ID="Customize" Checked='<%#Eval("Customize") %>'>
                                                    </asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" Enabled="false" ID="AdvanceFind" Checked='<%#Eval("AdvanceFind") %>'>
                                                    </asp:CheckBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnRefreshUsers" Style="display: none;" runat="server" CssClass="button"
                                        OnClick="btnRefreshUsers_Click" CausesValidation="False" />
                                    <%--<asp:Button ID="btnRolePermissions" runat="server" Text="Save" OnClick="btnRolePermissions_Click" />--%>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div id="div_AssignRoles-box" class="message-popup" style="overflow: hidden;">
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div2" style="display: block;">
                    <div id="div3" runat="server" style="overflow: auto; width: 100%; height: 100% !important;">
                        <table>
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <div class="btn btn-primary" onclick="btneditUser()">
                                                EDIT USER</div>
                                            <%--<div style="float: right; background-color: #0072c6; padding: 4px; color: White;
                                                border-radius: 5px;">
                                                ASSIGN ROLES</div>--%>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvUserRoles" runat="server" AutoGenerateColumns="false" Width="100%"
                                        DataKeyField="ID" DataKeyNames="ID" EmptyDataText="No Record Found" OnRowDataBound="gvUserRoles_RowDataBound">
                                        <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                    <asp:Label ID="lblRoleID" runat="server" Text='<%#Eval("ID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Name
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkName" OnClick="div_editRolePermissions" CommandArgument='<%#Eval("ID") %>'
                                                        runat="server" Text='<%#Eval("RoleName") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <table id="Table1" runat="server">
                                        <tr>
                                            <td align="left">
                                                <asp:Image ID="Image2" runat="server" ImageUrl="images/arrow_ltr.png" />
                                            </td>
                                            <td class="NormalText">
                                                &nbsp; by selecting checkboxes &nbsp; &nbsp; &nbsp;&nbsp;
                                            </td>
                                            <td>
                                                <asp:Button ID="Button1" runat="server" CssClass="button" Text="Assign Role(s)" OnClick="btnAssignUserRoles_Click"
                                                    OnClientClick="return CheckboxSelection();" ToolTip="Add/Remove selected role to User"
                                                    CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div id="div_CreateUser-box" class="message-popup" style="overflow: hidden;">
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeUserWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="showpage" style="display: block;">
                    <div id="divIframe" runat="server" style="overflow: auto; width: 100%; height: 100% !important;">
                        <table width="100%">
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <div style="margin-right: 10px;" class="btn btn-primary" onclick="btnassignRoles()">
                                        Assign Roles</div>
                                    <asp:Button ID="btnSave" CssClass="btn btn-primary" Text="Save" runat="server" OnClick="btnSave_Click"
                                        ValidationGroup="Users" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px;">
                                    <asp:Label ID="lblUserName" Text="User Name" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtUserName" onchange="checkExists()" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <image style="height: 20px; width: 20px; display: none;" src="images/update.jpg"
                                        id="imgSuccess"></image>
                                    <image style="height: 20px; width: 20px; display: none;" src="images/Cancel.jpg"
                                        id="imgFailure"></image>
                                    <asp:Label ID="lblUserID" Style="display: none;" Text="0" runat="server"></asp:Label>
                                    <asp:RequiredFieldValidator ID="rqName" runat="server" ControlToValidate="txtUserName"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPassword" Text="Password" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPassword" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtPassword"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblFirstName" Text="First Name" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtFirstName" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFirstName"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblLastName" Text="Last Name" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtLastName" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtLastName"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblEmail" Text="Email" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server" onchange="checkExists()" Style="width: 300px;"></asp:TextBox>
                                    <image style="height: 20px; width: 20px; display: none;" src="images/update.jpg"
                                        id="imgSuccessemail"></image>
                                    <image style="height: 20px; width: 20px; display: none;" src="images/Cancel.jpg"
                                        id="imgFailureemail"></image>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtEmail"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAddress1" Text="Address 1" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress1" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAddress1"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblAddress2" Text="Address 2" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAddress2" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtAddress2"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblPhone" Text="Phone" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtPhone" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtPhone"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblCountry" Text="Country" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCountry" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtCountry"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblState" Text="State" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtState" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtState"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblCity" Text="City" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtCity" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtCity"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblZipCode" Text="Zip Code" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtZipCode" runat="server" Style="width: 300px;"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtZipCode"
                                        ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Users"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lblTerritory" Text="Territory" runat="server"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTerritory" runat="server" Style="width: 300px;">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="gradientbginner" style="width: 90%; overflow: auto;">
                <table width="100%">
                    <tr>
                        <td width="18px">
                        </td>
                        <td>
                            <div class="maincontent" style="width: 100%;">
                                <div class="searchboxcaption">
                                    <span>Users</span></div>
                                <div class="searchbox">
                                    <div>
                                        <table width="100%">
                                            <tr>
                                                <div style="text-align: center;">
                                                    <asp:Label ID="lblListmsg" Style="text-align: center; color: Red; font-size: 13px;"
                                                        runat="server" EnableViewState="False"></asp:Label>
                                                </div>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" style="border-right: gray 1px solid; border-top: gray 1px solid;
                                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                                        <tr>
                                                            <th align="left" colspan="2">
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 350px; display: none;">
                                                                <asp:Button ID="btnAssignRoles" CssClass="button" OnClick="btnAssignRoles_Click"
                                                                    runat="server" Text="Assign Roles" Style="display: none;" />
                                                            </td>
                                                            <td style="float: left;">
                                                                <asp:Button ID="btnAddUser" CssClass="create_dtl" OnClick="btnEdit_Click" runat="server"
                                                                    Text="" />
                                                                <asp:Button ID="btnDeleteAll" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAll_Click"
                                                                    OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected User" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="max-height: 230px;">
                                                    <div style="overflow: auto; max-height: 230px; width: 100%; float: left;" class="customwd">
                                                        <asp:GridView ID="dataGridUser" runat="server" AutoGenerateColumns="false" Width="100%"
                                                            DataKeyField="OrgID" DataKeyNames="OrgID" AllowPaging="true" PageSize="10" AllowSorting="true"
                                                            OnSorting="dataGridUser_Sorting" OnPageIndexChanging="dataGridUser_PageIndexChanging"
                                                            EmptyDataText="No Record Found" OnRowDataBound="dataGridUser_RowDataBound" OnDataBound="dataGridUser_DataBound"
                                                            OnRowCreated="dataGridUser_RowCreated">
                                                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        Name
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkUser" OnClick="div_editUser" CommandArgument='<%#Eval("OrgID") %>'
                                                                            runat="server" Text='<%#Eval("FirstName") %>'></asp:LinkButton>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        Phone
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <%#Eval("Phone") %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        Country
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <%#Eval("Country") %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        State
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <%#Eval("State") %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        City
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <%#Eval("City") %>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Name
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("UserName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <asp:Button ID="imgPageFirst" runat="server" Text="First" CommandArgument="First"
                                                                    CommandName="Page" OnCommand="imgPageFirst_Command" />
                                                                <asp:Button ID="imgPagePrevious" runat="server" Text="Prev" CommandArgument="Prev"
                                                                    CommandName="Page" OnCommand="imgPagePrevious_Command" />
                                                                <asp:DropDownList ID="ddCurrentPage" runat="server" CssClass="Normal" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddCurrentPage_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblOf" runat="server" Text="of" CssClass="Normal"></asp:Label>
                                                                <asp:Label ID="lblTotalPage" runat="server" CssClass="Normal"></asp:Label>
                                                                <asp:Button ID="imgPageNext" runat="server" Text="Next" CommandArgument="Next" CommandName="Page"
                                                                    OnCommand="imgPageNext_Command"></asp:Button>
                                                                <asp:Button ID="imgPageLast" runat="server" Text="Last" CommandArgument="Last" CommandName="Page"
                                                                    OnCommand="imgPageLast_Command"></asp:Button>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table id="tblDeleteAll" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                            </td>
                                                            <td class="NormalText">
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit User" OnClick="btnEdit_Click"
                                                                    Style="display: none;" ToolTip="Edit Selected User" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
