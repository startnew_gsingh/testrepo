﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

namespace CRM
{
    public partial class Dashboard : System.Web.UI.Page
    {
        Dictionary<string, int> chartData = new Dictionary<string, int>();
        Dictionary<string, int> leadsmonthChartData = new Dictionary<string, int>();
        Dictionary<string, int> statusChartData = new Dictionary<string, int>();

        Dictionary<string, int> opportunitymonthChartData = new Dictionary<string, int>();
        Dictionary<string, int> oppotunitystatusChartData = new Dictionary<string, int>();

        protected void Page_Load(object sender, EventArgs e)
        {
            BindColor();
            SetColor();
            if (!IsPostBack)
            {
                // bind chart type names to ddl
                bindTasks();
                bindDiscussions();
                ddlChartType.DataSource = Enum.GetNames(typeof(SeriesChartType));
                ddlChartType.DataBind();
                ddlChartType.SelectedIndex = ddlChartType.Items.Count - 1;
            }
            //DataBind();
            bindChartdata();
            OnPreRender();
        }

        protected void bindChartdata()
        {
            //base.OnDataBinding(e);
            CRMBLLDeals objBLLDeals = new CRMBLLDeals();
            DataSet dtSales = objBLLDeals.getSalesDashboard(2013, Convert.ToInt32(ddlMonthList.SelectedValue), Convert.ToInt32(ddlOpporMonthList.SelectedValue));

            for (int i = 0; i < dtSales.Tables[0].Rows.Count; i++)
            {
                chartData.Add(dtSales.Tables[0].Rows[i]["Owner"].ToString(), Convert.ToInt32(dtSales.Tables[0].Rows[i]["Total"]));
            }

            cTestChart.Series["salesChart"].Points.DataBind(chartData, "Key", "Value", string.Empty);


            //Leads Charts
            for (int i = 0; i < dtSales.Tables[1].Rows.Count; i++)
            {
                leadsmonthChartData.Add(Convert.ToString(dtSales.Tables[1].Rows[i]["xMonth"]), Convert.ToInt32(dtSales.Tables[1].Rows[i]["TotalFirstCount"]));
            }

            leadMonthChart.Series[0].Points.DataBindXY(leadsmonthChartData.Keys, leadsmonthChartData.Values);
            leadMonthChart.Series[0].ChartType = SeriesChartType.Column;
            leadMonthChart.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;


            for (int i = 0; i < dtSales.Tables[2].Rows.Count; i++)
            {
                statusChartData.Add(dtSales.Tables[2].Rows[i]["Status"].ToString(), Convert.ToInt32(dtSales.Tables[2].Rows[i]["Total"]));
            }

            leadmonthPieChart.Series["salesChart"].Points.DataBind(statusChartData, "Key", "Value", string.Empty);




            //Opportunity Charts
            for (int i = 0; i < dtSales.Tables[3].Rows.Count; i++)
            {
                opportunitymonthChartData.Add(Convert.ToString(dtSales.Tables[3].Rows[i]["xMonth"]), Convert.ToInt32(dtSales.Tables[3].Rows[i]["TotalFirstCount"]));
            }

            opportunityMonthChart.Series[0].Points.DataBindXY(opportunitymonthChartData.Keys, opportunitymonthChartData.Values);
            opportunityMonthChart.Series[0].ChartType = SeriesChartType.Column;
            opportunityMonthChart.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;


            for (int i = 0; i < dtSales.Tables[4].Rows.Count; i++)
            {
                oppotunitystatusChartData.Add(dtSales.Tables[4].Rows[i]["Status"].ToString(), Convert.ToInt32(dtSales.Tables[4].Rows[i]["Total"]));
            }

            opportunityMonthPieChart.Series["salesChart"].Points.DataBind(oppotunitystatusChartData, "Key", "Value", string.Empty);
        }

        protected void OnPreRender()
        {
            //base.OnPreRender(e);

            // update chart rendering
            cTestChart.Series["salesChart"].ChartTypeName = ddlChartType.SelectedValue;
            cTestChart.ChartAreas[0].Area3DStyle.Enable3D = true;
            cTestChart.ChartAreas[0].Area3DStyle.Inclination = Convert.ToInt32(20);

            leadmonthPieChart.Series["salesChart"].ChartType = SeriesChartType.Pie;
            leadmonthPieChart.ChartAreas[0].Area3DStyle.Enable3D = true;
            leadmonthPieChart.ChartAreas[0].Area3DStyle.Inclination = Convert.ToInt32(20);

            opportunityMonthPieChart.Series["salesChart"].ChartType = SeriesChartType.Pie;
            opportunityMonthPieChart.ChartAreas[0].Area3DStyle.Enable3D = true;
            opportunityMonthPieChart.ChartAreas[0].Area3DStyle.Inclination = Convert.ToInt32(20);
        }

        protected void bindDiscussions()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDiscussion = objDeals.getDashboardDiscussions(GlobalVariables.LoginUser.OrgID);
            gvDiscussions.DataSource = dsDiscussion;
            gvDiscussions.DataBind();
        }

        protected void bindTasks()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsTasks = objDeals.getDashboardTasks(GlobalVariables.LoginUser.OrgID);
            gvTasks.DataSource = dsTasks;
            gvTasks.DataBind();
        }

        protected void lnkDiscussionSubjectRead_Read(object sender, EventArgs e)
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsTasks = objDeals.getDashboardDiscussions(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(((System.Web.UI.WebControls.LinkButton)(sender)).CommandArgument));
            txtDiscussionSubject.Text = dsTasks.Tables[0].Rows[0]["Subject"].ToString();
            txtDiscussionDescription.Text = dsTasks.Tables[0].Rows[0]["Description"].ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "discussions();", true);
        }

        protected void lnkTasksSubjectRead_Read(object sender, EventArgs e)
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsTasks = objDeals.getDashboardTasks(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(((System.Web.UI.WebControls.LinkButton)(sender)).CommandArgument));
            lblTasksSubject.Text = dsTasks.Tables[0].Rows[0]["Subject"].ToString();
            txtTasksDescription.Text = dsTasks.Tables[0].Rows[0]["Description"].ToString();
            lblDueDate.Text = dsTasks.Tables[0].Rows[0]["DueDate"].ToString();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "tasks();", true);
        }

        public void BindColor()
        {
            Dictionary<string, int> chartData = new Dictionary<string, int>();
            chartData.Add("Ming1", 20);
            chartData.Add("Ming2", 15);
            chartData.Add("Ming3", 10);
            chartData.Add("Ming4", 13);
            chartData.Add("Ming5", 10);
            chartData.Add("Ming6", 32);
            this.cTestChart.Series[0].Points.DataBindXY(chartData.Keys, chartData.Values);
            this.cTestChart.Series[0].Label = "#VALX (#VALY)";

            this.leadmonthPieChart.Series[0].Points.DataBindXY(chartData.Keys, chartData.Values);
            this.leadmonthPieChart.Series[0].Label = "#VALX (#VALY)";

            this.opportunityMonthPieChart.Series[0].Points.DataBindXY(chartData.Keys, chartData.Values);
            this.opportunityMonthPieChart.Series[0].Label = "#VALX (#VALY)";
        }

        public void SetColor()
        {
            Color[] myPalette = new Color[6]{
            Color.FromKnownColor(KnownColor.Green), 
            Color.FromKnownColor(KnownColor.GreenYellow),
            Color.FromKnownColor(KnownColor.LawnGreen), 
            Color.FromKnownColor(KnownColor.LightGreen),
            Color.FromKnownColor(KnownColor.LightPink),
            Color.FromKnownColor(KnownColor.LightSalmon)
        };
            this.cTestChart.Palette = ChartColorPalette.None;
            this.cTestChart.PaletteCustomColors = myPalette;

            //this.leadMonthChart.Palette = ChartColorPalette.None;
            //this.leadMonthChart.PaletteCustomColors = myPalette;

            this.leadmonthPieChart.Palette = ChartColorPalette.None;
            this.leadmonthPieChart.PaletteCustomColors = myPalette;

            this.opportunityMonthPieChart.Palette = ChartColorPalette.None;
            this.opportunityMonthPieChart.PaletteCustomColors = myPalette;
        }

        protected void btnDelDiscussion_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            string IDS = "";
            int int_index = 1;
            for (int_index = 0; int_index <= gvDiscussions.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvDiscussions.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(gvDiscussions.DataKeys[int_index].Value)) + ",";
            }
            objDeals.deleteDiscussion(IDS);
            bindDiscussions();
        }

        protected void btnDelTasks_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            string IDS = "";
            int int_index = 1;
            for (int_index = 0; int_index <= gvTasks.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvTasks.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(gvTasks.DataKeys[int_index].Value)) + ",";
            }
            objDeals.insertTasks(1, 1, 1, "", "", DateTime.Now, "", 1, IDS, true);
            bindTasks();
        }

        //protected void btnSaveDiscussion_Click(object sender, EventArgs e)
        //{
        //    CRMBLLDeals objBLLDeals = new CRMBLLDeals();
        //    objBLLDeals.insertDiscussion(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), DealID, txtDiscussionSubject.Text, txtDiscussionDescription.Text, Convert.ToInt64(Session["selObjectUserID"]), txtDelDiscussion.Text, false);
        //}
    }
}