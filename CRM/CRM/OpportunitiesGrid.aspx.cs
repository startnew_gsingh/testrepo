﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.IO;
using System.Data;

namespace CRM
{
    public partial class OpportunitiesGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["selObjectUserID"] != null)
                    BindOpportunityGrid(Convert.ToInt64(Session["selObjectUserID"]));
            }
        }

        protected void BindOpportunityGrid(Int64 IDS)
        {
            CRMBLLOpportunities _objOpportunities = new CRMBLLOpportunities();
            DataSet ds = _objOpportunities.getOpportunitiesGridData(GlobalVariables.LoginUser.OrgID, IDS);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvDetails.DataSource = ds;
                gvDetails.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvDetails.DataSource = ds;
                gvDetails.DataBind();
                int columncount = gvDetails.Rows[0].Cells.Count;
                gvDetails.Rows[0].Cells.Clear();
                gvDetails.Rows[0].Cells.Add(new TableCell());
                gvDetails.Rows[0].Cells[0].ColumnSpan = columncount;
                gvDetails.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void gvDetails_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDetails.EditIndex = e.NewEditIndex;
            BindOpportunityGrid(Convert.ToInt64(Session["selObjectUserID"]));
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createOpportunities();", true);
        }

        protected void gvDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int Id = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Value.ToString());
            TextBox txteditLine = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txteditLine");
            TextBox txtProductID = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtProductID");

            TextBox txtProductDesc = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtProductDesc");
            TextBox txtQuantity = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtQuantity");

            TextBox txtUnitPrice = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtUnitPrice");
            TextBox txtDiscountPercent = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtDiscountPercent");

            TextBox txtDiscountedAmount = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtDiscountedAmount");
            TextBox txtExtendedAmount = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtExtendedAmount");

            CRMBLLOpportunities _objOpportunities = new CRMBLLOpportunities();
            Int64 ds = _objOpportunities.insertOpportunityGrid(Id, Convert.ToInt64(Session["selObjectUserID"]), GlobalVariables.LoginUser.OrgID, txteditLine.Text, txtProductID.Text, txtProductDesc.Text,
                                                                    txtQuantity.Text, txtUnitPrice.Text, txtDiscountPercent.Text, txtDiscountedAmount.Text, txtExtendedAmount.Text);

            gvDetails.EditIndex = -1;
            BindOpportunityGrid(Convert.ToInt64(Session["selObjectUserID"]));
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createOpportunities();", true);
        }

        protected void gvDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDetails.EditIndex = -1;
            BindOpportunityGrid(Convert.ToInt64(Session["selObjectUserID"]));
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createOpportunities();", true);
        }

        protected void gvDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int selID = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Value.ToString());
            CRMBLLOpportunities _objOpportunities = new CRMBLLOpportunities();
            int delCount = _objOpportunities.deleteOpportunitiesGrid(selID);

            //if (delCount == 1)
            //{
            BindOpportunityGrid(Convert.ToInt64(Session["selObjectUserID"]));
            //}
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createOpportunities();", true);
        }

        protected void gvDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                TextBox txteditLine = (TextBox)gvDetails.FooterRow.FindControl("txtftrLine");
                TextBox txtProductID = (TextBox)gvDetails.FooterRow.FindControl("txtftrProductID");

                TextBox txtProductDesc = (TextBox)gvDetails.FooterRow.FindControl("txtftrProductDesc");
                TextBox txtQuantity = (TextBox)gvDetails.FooterRow.FindControl("txtftrQuantity");

                TextBox txtUnitPrice = (TextBox)gvDetails.FooterRow.FindControl("txtftrUnitPrice");
                TextBox txtDiscountPercent = (TextBox)gvDetails.FooterRow.FindControl("txtftrDiscountPercent");

                TextBox txtDiscountedAmount = (TextBox)gvDetails.FooterRow.FindControl("txtftrDiscountedAmount");
                TextBox txtExtendedAmount = (TextBox)gvDetails.FooterRow.FindControl("txtftrExtendedAmount");

                CRMBLLOpportunities _objOpportunities = new CRMBLLOpportunities();
                Int64 ds = _objOpportunities.insertOpportunityGrid(0, Convert.ToInt64(Session["selObjectUserID"]), GlobalVariables.LoginUser.OrgID, txteditLine.Text, txtProductID.Text, txtProductDesc.Text,
                                                                        txtQuantity.Text, txtUnitPrice.Text, txtDiscountPercent.Text, txtDiscountedAmount.Text, txtExtendedAmount.Text);


                BindOpportunityGrid(Convert.ToInt64(Session["selObjectUserID"]));
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createOpportunities();", true);
            }
        }
    }
}