﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CRMBLL;

namespace CRM
{
    public partial class ContactChart : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindOwnerChartData();
            ddlChartType.SelectedValue = "1";
        }

        protected void ddlChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlChartType.SelectedValue == "1")
            {
                bindOwnerChartData();
            }
            else
            {
                //bindStatusChartData();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "chartButton();", true);
        }

        protected void bindOwnerChartData()
        {
            try
            {
                DataTable dtDeals = Session["data"] as DataTable;
                DataView dvDeals = dtDeals.DefaultView;
                dvDeals.RowFilter = "Owner='" + GlobalVariables.LoginUser.UserName + "'";

                List<chartData> _objChartData = new List<chartData>();
                chartData cd = new chartData();
                cd.Label = GlobalVariables.LoginUser.UserName;
                cd.Value1 = dvDeals.Count;
                _objChartData.Add(cd);

                Chart1.DataSource = _objChartData;
                Chart1.DataBind();
                Chart1.Visible = true;

                //statusChart.DataSource = null;
                //statusChart.DataBind();
                //statusChart.Visible = false;
            }
            catch { }
        }
    }
}