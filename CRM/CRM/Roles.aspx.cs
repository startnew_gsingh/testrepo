﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;

namespace CRM
{
    public partial class Roles : System.Web.UI.Page
    {
        static DataTable dtPermissions = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            if (!Page.IsPostBack)
            {
                //bindFiltersDropDown();
                bindRolesGrid();
            }
        }

        protected void btnRoles_Click(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            bindRolesGrid();
        }

        public void bindRolesGrid()
        {
            CRMBLLUsers objCRMUsers = new CRMBLLUsers();

            DataSet dsRole = objCRMUsers.getRolesData();
            if (dsRole != null && dsRole.Tables[0].Rows.Count > 0)
            {
                dataGridRole.DataSource = dsRole;
                dataGridRole.DataBind();
                Session["data"] = dsRole.Tables[0];
                SetViewState(dsRole);
            }
            else
            {
                dataGridRole.DataSource = null;
                dataGridRole.DataBind();
                Session["data"] = null;
            }
        }

        //public void bindFiltersDropDown()
        //{
        //    CRMBLLRole objCRMRole = new CRMBLLRole();
        //    ddlFilters.DataSource = objCRMRole.getFilters(GlobalVariables.LoginUser.OrgID, 2);
        //    ddlFilters.DataTextField = "FilterName";
        //    ddlFilters.DataValueField = "ID";
        //    ddlFilters.DataBind();
        //    ddlFilters.Items.Insert(0, new ListItem("All Roles", "0"));
        //}

        //protected void ddlFilters_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    dataGridRole.DataSource = null;
        //    dataGridRole.DataBind();
        //    bindRolesGrid();
        //}

        protected void dataGridRole_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            GridViewSortExpression = e.SortExpression;

            //Gets the Pageindex of the GridView.
            int iPageIndex = dataGridRole.PageIndex;
            dataGridRole.DataSource = SortDataTable(myDataTable, false);
            dataGridRole.DataBind();
            dataGridRole.PageIndex = iPageIndex;
        }

        //Gets or Sets the GridView SortDirection Property
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        //Gets or Sets the GridView SortExpression Property
        private string GridViewSortExpression
        {
            get
            {
                return ViewState["SortExpression"] as string ?? string.Empty;
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        //Toggles between the Direction of the Sorting
        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        //Sorts the ResultSet based on the SortExpression and the Selected Column.
        protected DataView SortDataTable(DataTable myDataTable, bool isPageIndexChanging)
        {
            if (myDataTable != null)
            {
                DataView myDataView = new DataView(myDataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GetSortDirection());
                    }
                }
                return myDataView;
            }
            else
            {

                return new DataView();
            }
        }

        protected void dataGridRole_DataBound(object sender, EventArgs e)
        {
            //Custom Paging
            GridViewRow dataGridRoleRow = dataGridRole.BottomPagerRow;

            if (dataGridRoleRow == null) return;

            //Get your Controls from the GridView, in this case 
            //I use a DropDown Control for Paging
            DropDownList ddCurrentPage =
        (DropDownList)dataGridRoleRow.Cells[0].FindControl("ddCurrentPage");
            Label lblTotalPage = (Label)dataGridRoleRow.Cells[0].FindControl("lblTotalPage");

            if (ddCurrentPage != null)
            {
                //Populate Pager
                for (int i = 0; i < dataGridRole.PageCount; i++)
                {
                    int iPageNumber = i + 1;
                    ListItem myListItem = new ListItem(iPageNumber.ToString());

                    if (i == dataGridRole.PageIndex)
                        myListItem.Selected = true;

                    ddCurrentPage.Items.Add(myListItem);
                }
            }

            // Populate the Page Count
            if (lblTotalPage != null)
                lblTotalPage.Text = dataGridRole.PageCount.ToString();

        }

        private DataSet GetViewState()
        {
            //Gets the ViewState
            return (DataSet)ViewState["RoleDataSet"];
        }

        private void SetViewState(DataSet RoleDataSet)
        {
            //Sets the ViewState
            ViewState["RoleDataSet"] = RoleDataSet;
        }

        protected void dataGridRole_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            dataGridRole.DataSource = SortDataTable(myDataTable, true);

            dataGridRole.PageIndex = e.NewPageIndex;
            dataGridRole.DataBind();
        }

        //Change to a different page when the DropDown Page is changed
        protected void ddCurrentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                GridViewRow dataGridRoleRow = dataGridRole.BottomPagerRow;
                DropDownList ddCurrentPage =
            (DropDownList)dataGridRoleRow.Cells[0].FindControl("ddCurrentPage");

                dataGridRole.PageIndex = ddCurrentPage.SelectedIndex;

                //Popultate the GridView Control
                DataSet myDataSet = GetViewState();
                DataTable myDataTable = myDataSet.Tables[0];

                dataGridRole.DataSource = SortDataTable(myDataTable, true);
                dataGridRole.DataBind();
            }
        }

        protected void imgPageFirst_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPagePrevious_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageNext_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageLast_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void Paginate(object sender, CommandEventArgs e)
        {
            // Get the Current Page Selected
            int iCurrentIndex = dataGridRole.PageIndex;

            switch (e.CommandArgument.ToString().ToLower())
            {
                case "first":
                    dataGridRole.PageIndex = 0;
                    break;
                case "prev":
                    if (dataGridRole.PageIndex != 0)
                    {
                        dataGridRole.PageIndex = iCurrentIndex - 1;
                    }
                    break;
                case "next":
                    dataGridRole.PageIndex = iCurrentIndex + 1;
                    break;
                case "last":
                    dataGridRole.PageIndex = dataGridRole.PageCount;
                    break;
            }

            //Populate the GridView Control
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];

            dataGridRole.DataSource = SortDataTable(myDataTable, true);
            dataGridRole.DataBind();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLUsers objCRMRole = new CRMBLLUsers();
            string IDS = "";
            for (int_index = 0; int_index <= dataGridRole.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridRole.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(dataGridRole.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMRole.deleteRole(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Role Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindRolesGrid();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            //int int_index;
            //CRMBLLUsers objCRMusers = new CRMBLLUsers();

            //DataSet dsSelectedRole = objCRMusers.getRolesData(0);
            lblRoleID.Text = "0";
            lblRoleID.Visible = false;
            txtRoleName.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createRole();", true);
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int int_index;
            CRMBLLUsers objCRMusers = new CRMBLLUsers();
            Int64 IDS = 0;
            for (int_index = 0; int_index <= dataGridRole.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridRole.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = (Convert.ToInt64(dataGridRole.DataKeys[int_index].Value));
            }
            DataSet dsSelectedRole = objCRMusers.getRolesData(IDS);
            if (IDS != 0)
            {
                lblRoleID.Text = dsSelectedRole.Tables[0].Rows[0]["ID"].ToString();
                lblRoleID.Visible = false;
                txtRoleName.Text = dsSelectedRole.Tables[0].Rows[0]["RoleName"].ToString();
            }
            else
            {
                lblRoleID.Text = "0";
                lblRoleID.Visible = false;
                txtRoleName.Text = "";
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createRole();", true);
        }

        //protected void dataGridRole_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        e.Row.Cells[1].Visible = false; // hides the first column
        //    }
        //    catch
        //    {
        //    }
        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {
            CRMBLLUsers _objUsers = new CRMBLLUsers();
            _objUsers.insertRole(txtRoleName.Text, Convert.ToInt64(lblRoleID.Text));
            lblRoleID.Text = "0";
            bindRolesGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }

        protected void dataGridRole_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // e.Row.Cells[1].Visible = false; // hides the first column
            }
            catch
            {
            }
        }

        protected void dataGridRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    var firstCell = e.Row.Cells[2];
            //    firstCell.Controls.Clear();
            //    LinkButton lnkEditPermissions = new LinkButton();
            //    lnkEditPermissions.ID = e.Row.Cells[1].Text;
            //    lnkEditPermissions.Click += new EventHandler(this.div_editRolePermissions);
            //    //lnkEditPermissions.CommandArgument = e.Row.Cells[1].Text;
            //    lnkEditPermissions.Attributes.Add("runat", "server");
            //    lnkEditPermissions.Text = firstCell.Text;
            //    firstCell.Controls.Add(lnkEditPermissions);
            //}
        }

        protected void div_editRolePermissions(object sender, EventArgs e)
        {
            //Int64 RoleID=
            bindRolePermissions(Convert.ToInt64(((System.Web.UI.WebControls.LinkButton)(sender)).CommandArgument));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editRolePermissions();", true);
        }

        public void bindRolePermissions(Int64 RoleID)
        {
            dtPermissions = new DataTable();
            DataColumn dcPermissions = new DataColumn();

            dcPermissions.ColumnName = "ID";
            dtPermissions.Columns.Add(dcPermissions);

            dcPermissions = new DataColumn();
            dcPermissions.ColumnName = "ColumnName";
            dtPermissions.Columns.Add(dcPermissions);

            dcPermissions = new DataColumn();
            dcPermissions.ColumnName = "Value";
            dtPermissions.Columns.Add(dcPermissions);

            CRMBLLUsers objCRMUsers = new CRMBLLUsers();

            DataSet dsRole = objCRMUsers.getRolesPermissions(RoleID);
            if (dsRole != null && dsRole.Tables[0].Rows.Count > 0)
            {
                lstRolePermissions.DataSource = dsRole;
                lstRolePermissions.DataBind();
            }
            else
            {
                lstRolePermissions.DataSource = null;
                lstRolePermissions.DataBind();
            }
        }

        public void Check_Clicked(Object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            ListViewItem item = (ListViewItem)cb.NamingContainer;
            ListViewDataItem dataItem = (ListViewDataItem)item;
            string code = lstRolePermissions.DataKeys[dataItem.DisplayIndex].Value.ToString();

            string columnName = ((System.Web.UI.Control)(sender)).ID;
            bool chkValue = ((System.Web.UI.WebControls.CheckBox)(sender)).Checked;

            bool rowExists = false;
            foreach (DataRow drP in dtPermissions.Rows)
            {
                string codeExist = drP["ID"].ToString();
                string columnNameExist = drP["ColumnName"].ToString();
                if (code == codeExist && columnNameExist == columnName)
                {
                    drP["Value"] = chkValue;
                    rowExists = true;
                }
            }
            if (!rowExists)
            {
                DataRow dr = dtPermissions.NewRow();
                dr["ID"] = code;
                dr["ColumnName"] = columnName;
                dr["Value"] = chkValue;
                dtPermissions.Rows.Add(dr);
            }
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editRolePermissions();", true);
        }

        protected void btnRolePermissions_Click(object sender, EventArgs e)
        {
            CRMBLLUsers _objUsers = new CRMBLLUsers();
            _objUsers.insertRolePermissions(dtPermissions);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }
    }
}