﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TransportValley.Company
{
    public partial class DownloadFile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string fileName = Request.QueryString["name"].ToString();

            Response.ContentType = "application/octet-stream";

            Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);

            Response.TransmitFile(fileName);

            Response.End();
        }
    }
}