﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WorkOrderGrid.aspx.cs"
    Inherits="CRM.WorkOrderGrid" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <link href="css/multiview.css" rel="stylesheet" type="text/css" />
    <link href="css/css.css" rel="stylesheet" type="text/css" />
    <link href="css/Mystyle.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="Script/MasterJS.js"></script>
    <script language="javascript" type="text/javascript" src="Script/GridView.js"></script>
</head>
<body style="background: none !important;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="scMGR" runat="server">
    </asp:ScriptManager>

    <asp:UpdateProgress ID="UpdateProgress2" runat="server">
        <ProgressTemplate>
            <div style="position: absolute; width: 100%; height: 100%; opacity: 0.8; z-index: 999999;
                background: #000; filter: alpha(opacity=80); -moz-opacity: 0.8; opacity: 0.8;">
                <img style="position: absolute; left: 45%; top: 45%;" src="images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <asp:UpdatePanel ID="upPanel" runat="server">
        <ContentTemplate>
            <asp:GridView ID="gvDetails" DataKeyNames="ID" runat="server" AutoGenerateColumns="false"
                Width="60%" CssClass="Gridview" HeaderStyle-BackColor="#61A6F8" ShowFooter="true"
                HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" OnRowCancelingEdit="gvDetails_RowCancelingEdit"
                OnRowDeleting="gvDetails_RowDeleting" OnRowEditing="gvDetails_RowEditing" OnRowUpdating="gvDetails_RowUpdating"
                OnRowCommand="gvDetails_RowCommand">
                <Columns>
                    <asp:TemplateField>
                        <EditItemTemplate>
                            <asp:ImageButton ID="imgbtnUpdate" CommandName="Update" runat="server" ImageUrl="~/images/update.jpg"
                                ToolTip="Update" Height="20px" Width="20px" />
                            <asp:ImageButton ID="imgbtnCancel" runat="server" CommandName="Cancel" ImageUrl="~/Images/Cancel.jpg"
                                ToolTip="Cancel" Height="20px" Width="20px" />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Images/Edit.jpg"
                                ToolTip="Edit" Height="20px" Width="20px" />
                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" runat="server" ImageUrl="~/Images/delete.jpg"
                                ToolTip="Delete" Height="20px" Width="20px" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:ImageButton ID="imgbtnAdd" runat="server" ImageUrl="~/Images/AddNewitem.jpg"
                                CommandName="AddNew" Width="30px" Height="30px" ToolTip="New" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Line">
                        <EditItemTemplate>
                            <asp:TextBox ID="txteditLine" runat="server" Text='<%#Eval("Line") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblitemLine" runat="server" Text='<%#Eval("Line") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrLine" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvLine" runat="server" ControlToValidate="txtftrLine"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product ID">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtProductID" runat="server" Text='<%#Eval("ProductID") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProductID" runat="server" Text='<%#Eval("ProductID") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrProductID" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvProductID" runat="server" ControlToValidate="txtftrProductID"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product Desc.">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtProductDesc" runat="server" Text='<%#Eval("ProductDesc") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblProductDesc" runat="server" Text='<%#Eval("ProductDesc") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrProductDesc" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvProductDesc" runat="server" ControlToValidate="txtftrProductDesc"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Quantity">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantity" runat="server" Text='<%#Eval("Quantity") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantity" runat="server" Text='<%#Eval("Quantity") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrQuantity" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvQuantity" runat="server" ControlToValidate="txtftrQuantity"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Unit Price">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUnitPrice" runat="server" Text='<%#Eval("UnitPrice") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblUnitPrice" runat="server" Text='<%#Eval("UnitPrice") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrUnitPrice" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvUnitPrice" runat="server" ControlToValidate="txtftrUnitPrice"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Discount Percent">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDiscountPercent" runat="server" Text='<%#Eval("DiscountPercent") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDiscountPercent" runat="server" Text='<%#Eval("DiscountPercent") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrDiscountPercent" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvDiscountPercent" runat="server" ControlToValidate="txtftrDiscountPercent"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Discounted Amount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtDiscountedAmount" runat="server" Text='<%#Eval("DiscountedAmount") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblDiscountedAmount" runat="server" Text='<%#Eval("DiscountedAmount") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrDiscountedAmount" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvDiscountedAmount" runat="server" ControlToValidate="txtftrDiscountedAmount"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ExtendedAmount">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtExtendedAmount" runat="server" Text='<%#Eval("ExtendedAmount") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblExtendedAmount" runat="server" Text='<%#Eval("ExtendedAmount") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrExtendedAmount" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvExtendedAmount" runat="server" ControlToValidate="txtftrExtendedAmount"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="Shipped Date">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtShippedDate" runat="server" Text='<%#Eval("ShippedDate") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblShippedDate" runat="server" Text='<%#Eval("ShippedDate") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrShippedDate" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvShippedDate" runat="server" ControlToValidate="txtftrShippedDate"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>



                    <asp:TemplateField HeaderText="Quantitytoinvoice">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantitytoinvoice" runat="server" Text='<%#Eval("Quantitytoinvoice") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantitytoinvoice" runat="server" Text='<%#Eval("Quantitytoinvoice") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrQuantitytoinvoice" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvQuantitytoinvoice" runat="server" ControlToValidate="txtftrQuantitytoinvoice"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="QuantityFulfill">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantityFulfill" runat="server" Text='<%#Eval("QuantityFulfill") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantityFulfill" runat="server" Text='<%#Eval("QuantityFulfill") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrQuantityFulfill" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvQuantityFulfill" runat="server" ControlToValidate="txtftrQuantityFulfill"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="QuantityCancelledFulfill">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtQuantityCancelled" runat="server" Text='<%#Eval("QuantityCancelled") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblQuantityCancelled" runat="server" Text='<%#Eval("QuantityCancelled") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrQuantityCancelled" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvQuantityCancelled" runat="server" ControlToValidate="txtftrQuantityCancelled"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField HeaderText="MONumber">
                        <EditItemTemplate>
                            <asp:TextBox ID="txtMONumber" runat="server" Text='<%#Eval("MONumber") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblMONumber" runat="server" Text='<%#Eval("MONumber") %>' />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:TextBox ID="txtftrMONumber" runat="server" />
                            <asp:RequiredFieldValidator ID="rfvMONumber" runat="server" ControlToValidate="txtftrMONumber"
                                Text="*" Style="color: Red;" ValidationGroup="validaiton" />
                        </FooterTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
</html>
