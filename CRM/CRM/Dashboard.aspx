﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Dashboard.aspx.cs" Inherits="CRM.Dashboard" %>

<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script>
        function discussions() {
            var sendEmail = "#div_Discussion_box";
            //Fade in the Popup
            $(sendEmail).fadeIn(300);

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function tasks() {
            var sendEmail = "#div_tasks_box";
            //Fade in the Popup
            $(sendEmail).fadeIn(300);

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function closeWindow() {
            $('#mask , .email-popup').fadeOut(300);
            $('#mask').remove();
            document.getElementById("btnRefresh").click();
            return false;
        }
    </script>
    <div id="div_Discussion_box" class="email-popup" style="overflow: hidden;">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div2" style="display: block;">
                    <div id="div3" style="overflow: scroll; width: 100%; height: 97% !important;">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="txtDiscussionSubject" Style="width: 99%;" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtDiscussionDescription" TextMode="MultiLine" Style="min-height: 70px;
                                    min-width: 99%;" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="div_tasks_box" class="email-popup" style="overflow: hidden;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div4" style="display: block;">
                    <div id="div5" style="overflow: scroll; width: 100%; height: 97% !important;">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblTasksSubject" Style="width: 99%;" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <br />
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblDueDate" Style="width: 99%;" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtTasksDescription" TextMode="MultiLine" Style="min-height: 70px;
                                    min-width: 99%;" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <style type="text/css">
        h2
        {
            margin-left: 5px;
        }
        
        .clearfix:after
        {
            content: ".";
            display: block;
            clear: both;
            visibility: hidden;
            line-height: 0;
            height: 0;
        }
        
        .clearfix
        {
            display: inline-block;
        }
        
        .box
        {
            float: left;
            width: 300px;
            margin: 10px;
            padding: 10px;
            border: 1px solid #ccc;
        }
    </style>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div style="width: 100%;">
                <div style="width: 39%; float: left;">
                    <asp:Panel ID="pnlClick" runat="server" CssClass="boxHeader">
                        <div style="height: 10px; vertical-align: middle">
                            <div style="float: left; padding: 5px 5px 0 0">
                                <h2>
                                    Discussions</h2>
                            </div>
                            <div style="float: right; color: White; padding: 5px 5px 0 0">
                                <%--<asp:LinkButton ID="lnkAddDiscussion" runat="server" Text="New" OnClick=""></asp:LinkButton>--%>
                                <asp:Image ID="imgArrows" runat="server" />
                            </div>
                            <div style="clear: both">
                            </div>
                        </div>
                    </asp:Panel>
                    <div style="clear: both">
                    </div>
                    <asp:Panel ID="pnlCollapsable" runat="server" Height="0">
                        <table align="center" width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="gvDiscussions" runat="server" AutoGenerateColumns="false" Width="100%"
                                        DataKeyField="ID" DataKeyNames="ID" ShowHeader="false" AllowPaging="false" AllowSorting="true"
                                        GridLines="None" EmptyDataText="No Record Found">
                                        <RowStyle HorizontalAlign="Left" CssClass="dashboardGrid" />
                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSubjectRead" CommandArgument='<%#Eval("ID") %>' OnClick="lnkDiscussionSubjectRead_Read"
                                                        runat="server" Text='<%#Eval("Subject") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%#Eval("Entity") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%#Eval("CreatedBy") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <asp:Button ID="btnDelDiscussion" runat="server" CssClass="button" Text="Delete"
                                        OnClick="btnDelDiscussion_Click" OnClientClick="return CheckboxSelection();"
                                        ToolTip="Add/Remove selected discussion" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxtoolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                        CollapseControlID="pnlClick" Collapsed="false" ExpandControlID="pnlClick" TextLabelID="lblMessage"
                        CollapsedText="" ExpandedText="" ImageControlID="imgArrows" CollapsedImage="~/images/toggle_closed.gif"
                        ExpandedImage="~/images/toggle_open.gif" ExpandDirection="Vertical" TargetControlID="pnlCollapsable"
                        ScrollContents="false">
                    </ajaxtoolkit:CollapsiblePanelExtender>
                    <asp:Panel ID="pnkClickTasks" runat="server" CssClass="boxHeader">
                        <div style="height: 10px; vertical-align: middle">
                            <div style="float: left; padding: 5px 5px 0 0">
                                <h2>
                                    Tasks</h2>
                            </div>
                            <div style="float: right; color: White; padding: 5px 5px 0 0">
                                <asp:Image ID="imgArrowsTasks" runat="server" />
                            </div>
                            <div style="clear: both">
                            </div>
                        </div>
                    </asp:Panel>
                    <div style="clear: both">
                    </div>
                    <asp:Panel ID="pnlTasks" runat="server" Height="0">
                        <table align="center" width="100%">
                            <tr>
                                <td>
                                    <asp:GridView ID="gvTasks" runat="server" ShowHeader="false" AutoGenerateColumns="false"
                                        Width="100%" GridLines="None" DataKeyField="ID" DataKeyNames="ID" AllowPaging="false"
                                        AllowSorting="true" EmptyDataText="No Record Found">
                                        <RowStyle HorizontalAlign="Left" CssClass="dashboardGrid" />
                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkSubjectRead" CommandArgument='<%#Eval("ID") %>' OnClick="lnkTasksSubjectRead_Read"
                                                        runat="server" Text='<%#Eval("Subject") %>'></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <%#Eval("DueDate") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" valign="top">
                                    <%--<table id="Table1" runat="server">
                                        <tr>
                                            <td align="left">
                                                <asp:Image ID="Image2" runat="server" ImageUrl="images/arrow_ltr.png" />
                                            </td>
                                            <td class="NormalText">
                                                &nbsp; by selecting checkboxes &nbsp; &nbsp; &nbsp;&nbsp;
                                            </td>
                                            <td>--%>
                                    <asp:Button ID="btnDelTasks" runat="server" CssClass="button" Text="Delete" OnClick="btnDelTasks_Click"
                                        OnClientClick="return CheckboxSelection();" ToolTip="Add/Remove selected task"
                                        CausesValidation="False" />
                                    <%--</td>
                                        </tr>
                                    </table>--%>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <ajaxtoolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                        CollapseControlID="pnkClickTasks" Collapsed="false" ExpandControlID="pnkClickTasks"
                        TextLabelID="lblMessage" CollapsedText="" ExpandedText="" ImageControlID="imgArrowsTasks"
                        CollapsedImage="~/images/toggle_closed.gif" ExpandedImage="~/images/toggle_open.gif"
                        ExpandDirection="Vertical" TargetControlID="pnlTasks" ScrollContents="false">
                    </ajaxtoolkit:CollapsiblePanelExtender>
                </div>
                <div style="width: 60%; float: right;">
                    <%--</asp:Panel>--%>
                    <div style="clear: both">
                    </div>
                    <%--<asp:Panel ID="Panel2" runat="server" Height="0">--%>
                    <div style="float: left; width: 100%;">
                        <div>
                            <asp:DropDownList ID="ddlChartType" runat="server" AutoPostBack="True">
                            </asp:DropDownList>
                        </div>
                        <div>
                            <asp:Chart ID="cTestChart" runat="server" Height="400px" Width="800px">
                                <Legends>
                                    <asp:Legend Alignment="Far" Docking="Top" Name="Legend1">
                                    </asp:Legend>
                                </Legends>
                                <Series>
                                    <asp:Series Name="salesChart" ToolTip="Leads: #VALY" Url="Deals.aspx?Owner=#VALX"
                                        Legend="Legend1" CustomProperties="PieLineColor=BlanchedAlmond, PieDrawingStyle=SoftEdge">
                                    </asp:Series>
                                </Series>
                                <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1">
                                        <Area3DStyle />
                                    </asp:ChartArea>
                                </ChartAreas>
                                <BorderSkin BackColor="BurlyWood" BackGradientStyle="LeftRight" BackSecondaryColor="Blue"
                                    SkinStyle="Emboss" />
                            </asp:Chart>
                        </div>
                    </div>
                    <%--</asp:Panel>--%>
                    <%--<ajaxtoolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender3" runat="server"
                        CollapseControlID="Panel1" Collapsed="false" ExpandControlID="Panel1" TextLabelID="lblMessage"
                        CollapsedText="" ExpandedText="" ImageControlID="imgArrowsCharts" CollapsedImage="~/images/toggle_closed.gif"
                        ExpandedImage="~/images/toggle_open.gif" ExpandDirection="Vertical" TargetControlID="Panel2"
                        ScrollContents="false">
                    </ajaxtoolkit:CollapsiblePanelExtender>--%>
                </div>
            </div>
            <div>
                <div style="float: left; padding: 5px 5px 0 0; margin-top: 20px; width: 100%;" class="boxHeader">
                    <h2>
                        Leads</h2>
                </div>
                <div style="clear: both">
                </div>
                <div style="float: left;">
                    <asp:Chart ID="leadMonthChart" runat="server" Height="400px" Width="550px">
                        <Titles>
                            <asp:Title ShadowOffset="3" Name="Items" />
                        </Titles>
                        <Series>
                            <asp:Series Name="Default" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                <div style="float: right;">
                    <div>
                        <asp:DropDownList ID="ddlMonthList" runat="server" AutoPostBack="True">
                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Jan - March 2013" Value="3"></asp:ListItem>
                            <asp:ListItem Text="April - June 2013" Value="6"></asp:ListItem>
                            <asp:ListItem Text="July - Sep 2013" Value="9"></asp:ListItem>
                            <asp:ListItem Text="Oct - Dec 2013" Value="12"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Chart ID="leadmonthPieChart" runat="server" Height="400px" Width="795px">
                        <Legends>
                            <asp:Legend Alignment="Far" Docking="Top" Name="Legend1">
                            </asp:Legend>
                        </Legends>
                        <Series>
                            <asp:Series Name="salesChart" ToolTip="Leads: #VALY" Legend="Legend1" CustomProperties="PieLineColor=BlanchedAlmond, PieDrawingStyle=SoftEdge">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1">
                                <Area3DStyle />
                            </asp:ChartArea>
                        </ChartAreas>
                        <BorderSkin BackColor="BurlyWood" BackGradientStyle="LeftRight" BackSecondaryColor="Blue"
                            SkinStyle="Emboss" />
                    </asp:Chart>
                </div>
            </div>
            <div>
                <div style="float: left; padding: 5px 5px 0 0; margin-top: 20px; width: 100%;" class="boxHeader">
                    <h2>
                        Opportunities</h2>
                </div>
                <div style="clear: both">
                </div>
                <div style="float: left;">
                    <asp:Chart ID="opportunityMonthChart" runat="server" Height="400px" Width="550px">
                        <Titles>
                            <asp:Title ShadowOffset="3" Name="Items" />
                        </Titles>
                        <Series>
                            <asp:Series Name="Default" />
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1" />
                        </ChartAreas>
                    </asp:Chart>
                </div>
                <div style="float: right;">
                    <div>
                        <asp:DropDownList ID="ddlOpporMonthList" runat="server" AutoPostBack="True">
                            <asp:ListItem Text="All" Value="0"></asp:ListItem>
                            <asp:ListItem Text="Jan - March 2013" Value="3"></asp:ListItem>
                            <asp:ListItem Text="April - June 2013" Value="6"></asp:ListItem>
                            <asp:ListItem Text="July - Sep 2013" Value="9"></asp:ListItem>
                            <asp:ListItem Text="Oct - Dec 2013" Value="12"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <asp:Chart ID="opportunityMonthPieChart" runat="server" Height="400px" Width="795px">
                        <Legends>
                            <asp:Legend Alignment="Far" Docking="Top" Name="Legend1">
                            </asp:Legend>
                        </Legends>
                        <Series>
                            <asp:Series Name="salesChart" ToolTip="Opportunities: #VALY" Legend="Legend1" CustomProperties="PieLineColor=BlanchedAlmond, PieDrawingStyle=SoftEdge">
                            </asp:Series>
                        </Series>
                        <ChartAreas>
                            <asp:ChartArea Name="ChartArea1">
                                <Area3DStyle />
                            </asp:ChartArea>
                        </ChartAreas>
                        <BorderSkin BackColor="BurlyWood" BackGradientStyle="LeftRight" BackSecondaryColor="Blue"
                            SkinStyle="Emboss" />
                    </asp:Chart>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
