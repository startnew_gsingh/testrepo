﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMDAL;
using CRMBLL;
using System.Web.UI.HtmlControls;
using System.Data;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;
using System.Data.SqlClient;
using System.Data.OleDb;
using Microsoft.ApplicationBlocks.Data;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxClasses;

namespace CRM
{
    public partial class NewDeals : System.Web.UI.Page
    {
        List<SalesProcessStages> dsSalesProcess = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["Owner"] != null)
                    ViewState["qrySTRValue"] = Request.QueryString["Owner"].ToString();

                Session["ObjectID"] = 1;//Deal
                ViewState["OpportunityID"] = null;
                //bindFiltersDropDown();
                //bindDealsGrid();

                string querystrValue = Convert.ToString(Request.QueryString["Owner"]);
                CRMBLLUsers objCRMBLLUsers = new CRMBLLUsers();
                DataTable dtDeals = objCRMBLLUsers.getUserObjectRoles(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"])).Tables[0];

                if (dtDeals.Rows.Count > 0 && Convert.ToBoolean(dtDeals.Rows[0]["Read"]) == true)
                {
                    bindFiltersDropDown();
                    bindDealsGrid();
                    getUserObjectRoles(dtDeals);
                    bindgridColumnsDDL();
                    lblListmsg.Text = "";
                    trFilter.Visible = true;
                    trGridView.Visible = true;
                    trButtons.Visible = true;
                    trallButtons.Visible = true;
                }
                else
                {
                    lblListmsg.Text = "You are not authorize to view this page.";
                    trFilter.Visible = false;
                    trGridView.Visible = false;
                    trButtons.Visible = false;
                    trallButtons.Visible = false;
                }
            }
            //bindDealsGrid();
        }

        private string getPostBackControlID()
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            if (control != null)
                return control.ID;
            else
                return null;
        }

        protected void btnRefreshUpdatePanelChart_Click(Object Sender, EventArgs e)
        {
            bindStatusChartData();
            ddlChartType.SelectedValue = "2";
        }

        protected void btnRefreshFilterList_Click(Object Sender, EventArgs e)
        {
        }

        protected void btnrefreshUpdatePanel_Click(object sender, EventArgs e)
        {
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {

        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        public void getUserObjectRoles(DataTable dtDeals)
        {
            btnCreateDeal.Visible = true;
            btnEdit.Visible = true;
            btnDeleteAll.Visible = true;
            btnAdvanceFind.Visible = true;
            btnCustomizeObject.Visible = true;
            btnExcel.Visible = true;
            btnImport.Visible = true;

            if (Convert.ToBoolean(dtDeals.Rows[0]["Create"]) != true)
                btnCreateDeal.Visible = false;

            if (Convert.ToBoolean(dtDeals.Rows[0]["Edit"]) != true)
                btnEdit.Visible = false;

            if (Convert.ToBoolean(dtDeals.Rows[0]["Delete"]) != true)
                btnDeleteAll.Visible = false;

            if (Convert.ToBoolean(dtDeals.Rows[0]["AdvanceFind"]) != true)
                btnAdvanceFind.Visible = false;

            if (Convert.ToBoolean(dtDeals.Rows[0]["Customize"]) != true)
                btnCustomizeObject.Visible = false;

            if (Convert.ToBoolean(dtDeals.Rows[0]["Export"]) != true)
                btnExcel.Visible = false;

            if (Convert.ToBoolean(dtDeals.Rows[0]["Import"]) != true)
                btnImport.Visible = false;
        }

        public void bindDealsGrid()
        {
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            DataSet dsDeals = null;
            if (ViewState["qrySTRValue"] != null)
                dsDeals = objCRMDeals.getDealsData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), 1, 10, Convert.ToString(ViewState["filterQuery"]), "owner='" + ViewState["qrySTRValue"].ToString() + "'");
            else
                dsDeals = objCRMDeals.getDealsData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), 1, 10, Convert.ToString(ViewState["filterQuery"]));

            if (dsDeals.Tables.Count > 0)
            {
                //ASPxGridView1.DataSource = dsDeals.Tables[0];
                //ASPxGridView1.DataBind();

                dataGridLeads.DataSource = dsDeals.Tables[0];
                dataGridLeads.DataBind();
                Session["gridData"] = dsDeals.Tables[2];
                Session["data"] = dsDeals.Tables[2];

                bindStatusChartData();
                ddlChartType.SelectedValue = "2";
            }
            else
            {
                dataGridLeads.DataSource = null;
                dataGridLeads.DataBind();
                Session["data"] = null;
            }
        }

        protected void ddlChartType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlChartType.SelectedValue == "1")
            {
                bindOwnerChartData();
            }
            else
            {
                bindStatusChartData();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "chartButton();", true);
        }

        protected void bindOwnerChartData()
        {
            try
            {
                DataTable dtDeals = Session["data"] as DataTable;
                DataView dvDeals = dtDeals.DefaultView;
                dvDeals.RowFilter = "Owner='" + GlobalVariables.LoginUser.UserName + "'";

                List<chartData> _objChartData = new List<chartData>();
                chartData cd = new chartData();
                cd.Label = GlobalVariables.LoginUser.UserName;
                cd.Value1 = dvDeals.Count;
                _objChartData.Add(cd);

                Chart1.DataSource = _objChartData;
                Chart1.DataBind();
                Chart1.Visible = true;

                statusChart.DataSource = null;
                statusChart.DataBind();
                statusChart.Visible = false;
            }
            catch { }
        }

        protected void bindStatusChartData()
        {
            try
            {
                DataTable dtDeals = Session["data"] as DataTable;
                DataView dvDeals = dtDeals.DefaultView;

                List<chartData> _objChartData = new List<chartData>();
                for (int c = 0; c <= 2; c++)
                {
                    if (c == 0)
                    {
                        dvDeals.RowFilter = "Status='New' or Status=''";
                        chartData cd = new chartData();
                        cd.Label = "New";
                        cd.Value1 = dvDeals.Count;
                        _objChartData.Add(cd);
                    }
                    else if (c == 1)
                    {
                        dvDeals.RowFilter = "Status='Qualify'";
                        chartData cd = new chartData();
                        cd.Label = "Qualify";
                        cd.Value1 = dvDeals.Count;
                        _objChartData.Add(cd);
                    }
                    else
                    {
                        dvDeals.RowFilter = "Status='Disqualify'";
                        chartData cd = new chartData();
                        cd.Label = "Disqualify";
                        cd.Value1 = dvDeals.Count;
                        _objChartData.Add(cd);
                    }
                }

                Chart1.DataSource = null;
                Chart1.DataBind();
                Chart1.Visible = false;

                statusChart.DataSource = _objChartData;
                statusChart.DataBind();
                statusChart.Visible = true;
            }
            catch { }
        }

        protected void bindgridColumnsDDL()
        {

        }

        protected void checkBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            //selectedItemsPanel.Controls.Clear();
            string strSelValue = "";
            for (int li = 0; li < (sender as ListControl).Items.Count; li++)
            {
                if (!(sender as ListControl).Items[li].Selected)
                {
                    if (strSelValue != "")
                        strSelValue = strSelValue + ",";

                    strSelValue = strSelValue + (li + 2).ToString();
                }
            }

            ViewState["SelDDLValues"] = strSelValue;
            bindDealsGrid();
        }

        public void bindFiltersDropDown()
        {
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            ddlFilters.DataSource = objCRMDeals.getFilters(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]));
            ddlFilters.DataTextField = "FilterName";
            ddlFilters.DataValueField = "ID";
            ddlFilters.DataBind();
            ddlFilters.Items.Insert(0, new ListItem("All Leads", "0"));
        }

        protected void ddlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridLeads.DataSource = null;
            dataGridLeads.DataBind();
            ViewState["filterQuery"] = "";
            bindDealsGrid();
        }



        protected void dataGridLeads_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
        }

        //Change to a different page when the DropDown Page is changed
        protected void ddCurrentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Popultate the GridView Control
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            DataSet dsDeals = objCRMDeals.getDealsData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), Convert.ToInt32(ViewState["PageIndex"]), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]), Convert.ToString(ViewState["filterQuery"]));
            Session["data"] = dsDeals.Tables[2];
            if (dsDeals.Tables.Count > 0)
            {
                DataTable myDataTable = dsDeals.Tables[0];
                dataGridLeads.DataSource = myDataTable;
                dataGridLeads.DataBind();
            }
            else
            {
                dataGridLeads.DataSource = null;
                dataGridLeads.DataBind();
            }

        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {

        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

        }

        protected void btnAdvanceFind_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "advanceFind();", true);
        }

        protected void btnGridColumn_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            Int64 IDS = Convert.ToInt64(src.CommandArgument);

            DataSet dsSelectedDeal = objCRMDeals.getSelectedDeal(IDS, Convert.ToInt64(Session["ObjectID"]));
            CreateFields(IDS);
            getSalesStage(IDS);
            if (IDS == 0)
            {
                btnQualify.Visible = false;
                btnDisqualify.Visible = false;
                btnReactivate.Visible = false;
            }
            else
            {
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createDeal();", true);
        }

        protected void dataGridLeads_ClientLayout(object sender, ASPxClientLayoutArgs e)
        {
            try
            {
                //dataGridLeads.Columns[0].Visible = false;

                //dataGridLeads.AllColumns[dataGridLeads.AllColumns.Count - 3].Visible = false;
                //dataGridLeads.AllColumns[dataGridLeads.AllColumns.Count - 2].Visible = false;
                //dataGridLeads.AllColumns[dataGridLeads.AllColumns.Count - 1].Visible = false;
            }
            catch
            {
            }
        }

        protected void dataGridLeads_CustomCallBack(object sender, ASPxGridViewCustomButtonCallbackEventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createDeal();", true);
        }

        protected void dataGridLeads_HtmlRowPrepared(object sender, ASPxGridViewTableRowEventArgs e)
        {
            //if (e.RowType == GridViewRowType.Data)
            //{
            //    int drViewIndex = 2;
            //    try
            //    {
            //        drViewIndex = dataGridLeads.AllColumns["Lead Name"].VisibleIndex;
            //    }
            //    catch { }
            //    try
            //    {
            //        var firstCell = e.Row.Cells[drViewIndex - 1];
            //        if (firstCell != null)
            //        {
            //            firstCell.Controls.Clear();
            //            GridViewCommandColumn firstCol = new GridViewCommandColumn();
            //            firstCol.VisibleIndex = drViewIndex;
            //            firstCol.Caption = ((System.Data.DataRowView)(dataGridLeads.GetRow(0))).Row.ItemArray[drViewIndex].ToString();
            //            dataGridLeads.Columns.Add(firstCol);
            //            //LinkButton lnkEditFirstCol = new LinkButton();
            //            //lnkEditFirstCol.ID = e.KeyValue.ToString(); //((DevExpress.Web.ASPxGridView.GridViewDataColumn)(dataGridLeads.AllColumns[drViewIndex])).FieldName;



            //            //lnkEditFirstCol.Attributes.Add("onclick", "firstColumnClick(" + ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0] + ");");
            //            //lnkEditFirstCol.Click += new EventHandler(btnGridColumn_Click);
            //            //lnkEditFirstCol.CommandArgument = e.KeyValue.ToString();
            //            //lnkEditFirstCol.CommandName = "EditLead";
            //            //lnkEditFirstCol.Attributes.Add("runat", "server");
            //            //lnkEditFirstCol.Text = ((System.Data.DataRowView)(dataGridLeads.GetRow(0))).Row.ItemArray[drViewIndex].ToString();
            //            //firstCell.Controls.Add(firstCol);

            //            TableCell cell = new TableCell();
            //            LinkButton lnkNotesLastCol = new LinkButton();
            //            lnkNotesLastCol.ID = "Notes" + e.KeyValue.ToString();
            //            //lnkNotesLastCol.Attributes.Add("onclick", "notesColumnClick(" + ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0] + ");");
            //            lnkNotesLastCol.Click += new EventHandler(btnGridNotes_Click);
            //            lnkNotesLastCol.CommandArgument = e.KeyValue.ToString();
            //            lnkNotesLastCol.CommandName = "Notes";
            //            lnkNotesLastCol.Attributes.Add("runat", "server");
            //            lnkNotesLastCol.Text = "Notes";
            //            cell.Controls.Add(lnkNotesLastCol);

            //            string NextCall = "No Call Schedule";
            //            CRMBLLDeals _objDeals = new CRMBLLDeals();
            //            DataView dtColumns = _objDeals.getUpcomingPhoneCall(GlobalVariables.LoginUser.OrgID, 1, Convert.ToInt64(e.KeyValue)).Tables[0].DefaultView;
            //            if (dtColumns.Table.Rows.Count > 0)
            //                NextCall = dtColumns.Table.Rows[0]["DueDate"].ToString();

            //            TableCell cellPhoneCalls = new TableCell();
            //            LinkButton lnkPhoneCallsLastCol = new LinkButton();
            //            lnkPhoneCallsLastCol.ID = "Call" + e.KeyValue.ToString();
            //            //lnkPhoneCallsLastCol.Attributes.Add("onclick", "callColumnClick(" + ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0] + ");");
            //            //lnkPhoneCallsLastCol.Click += new EventHandler(btnPhoneCall_Click);
            //            lnkPhoneCallsLastCol.CommandArgument = e.KeyValue.ToString();
            //            lnkPhoneCallsLastCol.CommandName = "Call";
            //            lnkPhoneCallsLastCol.Attributes.Add("runat", "server");
            //            lnkPhoneCallsLastCol.Text = NextCall;
            //            cellPhoneCalls.Controls.Add(lnkPhoneCallsLastCol);

            //            e.Row.Controls.Add(cell);
            //            e.Row.Controls.Add(cellPhoneCalls);
            //        }
            //    }
            //    catch { }
            //}
        }

        protected void dataGridLeads_DataBound(object sender, EventArgs e)
        {
            ASPxGridView grid = sender as ASPxGridView;
            GridViewCommandColumn col = new GridViewCommandColumn();
            col.Name = "Status";
            col.Caption = "test";
            col.VisibleIndex = 2;
            GridViewCommandColumnCustomButton btnSelectAll = new GridViewCommandColumnCustomButton();
            btnSelectAll.ID = "SelectRow";
            btnSelectAll.Text = "google";
            
            col.CustomButtons.Add(btnSelectAll);
            grid.Columns.Add(col);
        }

        protected void dataGridLeads_RowCommand(object sender, ASPxGridViewRowCommandEventArgs e)
        {
        }

        protected void dataGridLeads_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var rowItem = e.Row.DataItem;
                int drViewIndex = 2;
                try
                {
                    drViewIndex = ((System.Data.DataRowView)(rowItem)).DataView.Table.Columns["Lead name"].Ordinal + 1;
                }
                catch { }
                var firstCell = e.Row.Cells[drViewIndex];
                firstCell.Controls.Clear();
                LinkButton lnkEditFirstCol = new LinkButton();
                lnkEditFirstCol.ID = e.Row.Cells[1].Text;

                //lnkEditFirstCol.Attributes.Add("onclick", "firstColumnClick(" + ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0] + ");");
                lnkEditFirstCol.Click += new EventHandler(btnGridColumn_Click);
                lnkEditFirstCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();
                lnkEditFirstCol.Attributes.Add("runat", "server");
                lnkEditFirstCol.Text = firstCell.Text;
                firstCell.Controls.Add(lnkEditFirstCol);


                TableCell cell = new TableCell();
                LinkButton lnkNotesLastCol = new LinkButton();
                //lnkNotesLastCol.Attributes.Add("onclick", "notesColumnClick(" + ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0] + ");");
                lnkNotesLastCol.Click += new EventHandler(btnGridNotes_Click);
                lnkNotesLastCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();

                lnkNotesLastCol.Attributes.Add("runat", "server");
                lnkNotesLastCol.Text = "Notes";
                cell.Controls.Add(lnkNotesLastCol);

                string NextCall = "No Call Schedule";
                CRMBLLDeals _objDeals = new CRMBLLDeals();
                DataView dtColumns = _objDeals.getUpcomingPhoneCall(GlobalVariables.LoginUser.OrgID, 1, Convert.ToInt64(e.Row.Cells[1].Text)).Tables[0].DefaultView;
                if (dtColumns.Table.Rows.Count > 0)
                    NextCall = dtColumns.Table.Rows[0]["DueDate"].ToString();

                TableCell cellPhoneCalls = new TableCell();
                LinkButton lnkPhoneCallsLastCol = new LinkButton();
                //lnkPhoneCallsLastCol.Attributes.Add("onclick", "callColumnClick(" + ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0] + ");");
                lnkPhoneCallsLastCol.Click += new EventHandler(btnPhoneCall_Click);
                lnkPhoneCallsLastCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();
                lnkPhoneCallsLastCol.Attributes.Add("runat", "server");
                lnkPhoneCallsLastCol.Text = NextCall;
                cellPhoneCalls.Controls.Add(lnkPhoneCallsLastCol);

                e.Row.Controls.Add(cell);
                e.Row.Controls.Add(cellPhoneCalls);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int i = e.Row.Cells.Count;
                var rowItem = e.Row.DataItem;
                Dictionary<string, string> filterColValues = new Dictionary<string, string>();

                if (ViewState["filterArrayList"] != null)
                    filterColValues = (Dictionary<string, string>)ViewState["filterArrayList"];

                for (int c = 1; c < i; c++)
                {
                    string colValue = ((System.Web.UI.WebControls.DataControlFieldCell)(e.Row.Cells[c])).ContainingField.ToString();
                    Panel newPanel = new Panel();
                    newPanel.CssClass = "filterDiv";
                    e.Row.Cells[c].Controls.Add(new LiteralControl("<br />"));
                    TextBox tx = new TextBox();  //here i m adding a control.
                    tx.ID = "txtFilter" + c;
                    tx.CssClass = "filterTextBox";
                    tx.Width = Unit.Pixel(100);
                    tx.Attributes.Add("runat", "server");
                    if (colValue.ToLower() == "owner" && ViewState["qrySTRValue"] != null)
                    {
                        tx.Text = ViewState["qrySTRValue"].ToString();

                        Dictionary<string, string> filterValues = new Dictionary<string, string>();

                        filterValues.Add("txtFilter1", ViewState["qrySTRValue"].ToString());
                        ViewState["filterArrayList"] = filterValues;
                        ViewState["filterQuery"] = "[owner] like '%" + ViewState["qrySTRValue"].ToString() + "%'";

                        ViewState["qrySTRValue"] = null;
                    }
                    if (filterColValues.ContainsKey("txtFilter" + c))
                        tx.Text = filterColValues.FirstOrDefault(k => k.Key == "txtFilter" + c).Value;
                    //e.Row.Cells[c].Controls.Add(tx);
                    newPanel.Controls.Add(tx);

                    AutoCompleteExtender _objautoCompleteList = new AutoCompleteExtender();
                    _objautoCompleteList.CompletionListItemCssClass = "CompletionListItemCssClass";
                    _objautoCompleteList.CompletionListCssClass = "CompletionListCssClass";

                    _objautoCompleteList.ID = "ACE" + c;
                    _objautoCompleteList.TargetControlID = "txtFilter" + c;
                    _objautoCompleteList.ServiceMethod = "filterGridrecords";
                    _objautoCompleteList.ServicePath = "CustomList.asmx";
                    _objautoCompleteList.ContextKey = colValue;// ((System.Web.UI.WebControls.DataControlFieldCell)(e.Row.Cells[c])).ContainingField.ToString();
                    _objautoCompleteList.UseContextKey = true;
                    _objautoCompleteList.EnableCaching = false;
                    //_objautoCompleteList.EnableClientState = true;
                    _objautoCompleteList.MinimumPrefixLength = 1;

                    //e.Row.Cells[c].Controls.Add(_objautoCompleteList);
                    newPanel.Controls.Add(_objautoCompleteList);


                    Button imgBtnFind = new Button();
                    imgBtnFind.ID = "imgBtnFind" + c;
                    imgBtnFind.CssClass = "imgfilter";
                    imgBtnFind.Click += new EventHandler(imgBtnFind_click);
                    imgBtnFind.Attributes.Add("runat", "server");

                    //ImageButton imgBtnFind = new ImageButton();  //here i m adding a control.
                    //imgBtnFind.ID = "imgBtnFind" + c;
                    //imgBtnFind.CssClass = "imgfilter";
                    //imgBtnFind.Click += new ImageClickEventHandler(imgBtnFind_click);
                    //imgBtnFind.Attributes.Add("runat", "server");
                    //imgBtnFind.ImageUrl = "../images/grid_search.JPG";
                    ////e.Row.Cells[c].Controls.Add(imgBtnFind);
                    newPanel.Controls.Add(imgBtnFind);

                    e.Row.Cells[c].Controls.Add(newPanel);
                }

                TableHeaderCell hcell = new TableHeaderCell();
                hcell.Text = "Notes";
                e.Row.Cells.Add(hcell);

                TableHeaderCell hcellCalls = new TableHeaderCell();
                hcellCalls.Text = "Next Call";
                e.Row.Cells.Add(hcellCalls);
            }

        }

        protected void imgBtnFind_click(object sender, EventArgs e)
        {
            Dictionary<string, string> filterValues = new Dictionary<string, string>();
            string query = "";
            DataTable Griddata = (DataTable)Session["gridData"];
            int rCount = Griddata.Columns.Count;
            for (int c = 1; c < rCount; c++)
            {
                string value = Request.Form["ctl00$ContentPlaceHolder1$dataGridLeads$ctl01$" + "txtFilter" + c];
                if (value != null && value != "")
                {
                    filterValues.Add("txtFilter" + c, value);
                    if (query == "")
                        query = query + " [" + Griddata.Columns[c - 1].ColumnName + "] like '%" + value + "%'";
                    else
                        query = query + " And [" + Griddata.Columns[c - 1].ColumnName + "] like '%" + value + "%'";
                }
            }
            ViewState["filterArrayList"] = filterValues;
            ViewState["filterQuery"] = query;
            bindDealsGrid();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {

        }


        /*Create Deal*/
        public void CreateFields(Int64 LeadID)
        {
            ViewState["LeadID"] = LeadID;
            Session["selObjectUserID"] = LeadID;
            ViewState["SelectedLeadID"] = LeadID;

            if (Convert.ToInt64(Session["ObjectID"]) == 1)
                spObjectText.InnerText = "Lead";
            else
                spObjectText.InnerText = "Opportunity";

            clsCommonFunctions.CheckSession();
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataView dtColumns = _objDeals.getColumnControls(Convert.ToInt64(Session["ObjectID"]), 0).Tables[0].DefaultView;
            DataTable dtPlaceholders = _objDeals.getPlaceholders(Convert.ToInt64(Session["ObjectID"]), GlobalVariables.LoginUser.OrgID).Tables[0];

            DataTable dtDeal = _objDeals.getSelectedDeal(LeadID, Convert.ToInt64(Session["ObjectID"])).Tables[0];


            lblLeadID.Text = LeadID.ToString();
            placeholderGeneralInfo.Controls.Add(new LiteralControl("<table style='width:100%;'><tr><td style='padding-right:50px;vertical-align:top;'>"));
            for (int p = 0; p < dtPlaceholders.Rows.Count; p++)
            {
                // Create Header Panel
                Panel panelHead = new Panel();
                if (p != 0)
                    panelHead.Attributes.Add("style", "Padding-top:15px;");
                else
                    panelHead.Attributes.Add("style", "Padding-top:2px;");

                panelHead.ID = "pH" + dtPlaceholders.Rows[p]["ID"].ToString();
                Label lblHead = new Label();
                lblHead.ID = "lblHeader" + dtPlaceholders.Rows[p]["ID"].ToString();
                panelHead.Controls.Add(lblHead);

                Panel _objNewPlaceholder = new Panel();
                _objNewPlaceholder.ID = dtPlaceholders.Rows[p]["ID"].ToString();

                Panel _objSystemField = new Panel();
                dtColumns.RowFilter = "Placeholder=" + dtPlaceholders.Rows[p]["ID"].ToString();
                bool IsSystemField = Convert.ToBoolean(dtPlaceholders.Rows[p]["IsSystemField"]);


                if (IsSystemField != true)
                {
                    //string strHTML = "<span style='font-weight:bold;'>" + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + "</span>" + "</br></br>";
                    //_objNewPlaceholder.Controls.Add(new LiteralControl(strHTML));
                }
                _objNewPlaceholder.Controls.Add(new LiteralControl("<table style='width:100%;'>"));

                for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
                {
                    bool IsControlSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[col]["IsSystemField"]);

                    if (IsSystemField == false)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("<tr style='vertical-align:top;height:30px;width:120px;'>"));
                    else if (col == 0)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("<tr style='vertical-align:top;height:30px;width:800px;'>"));

                    if (IsSystemField != true)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("<td style='width:120px;'>"));
                    else
                        _objNewPlaceholder.Controls.Add(new LiteralControl("<td style='background-color: #f4d89d;'>"));

                    int formID = Convert.ToInt32(dtColumns.Table.DefaultView[col]["ID"]);
                    Label _objLabel = new Label();
                    _objLabel.Text = dtColumns.Table.DefaultView[col]["LabelName"].ToString();
                    _objNewPlaceholder.Controls.Add(_objLabel);
                    //_objNewPlaceholder.Controls.Add(new LiteralControl("</td><td>"));

                    if (IsSystemField != true)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("</td><td>"));
                    else
                        _objNewPlaceholder.Controls.Add(new LiteralControl("</br>"));

                    int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
                    if (fieldType == 1)
                    {
                        createTextFormatControl(col, _objNewPlaceholder, dtColumns.Table.DefaultView, dtDeal, IsSystemField);
                    }
                    else if (fieldType == 3 || fieldType == 4)
                    {
                        DataTable dtColumnChoices = _objDeals.getControlChoices(formID).Tables[0];
                        createSingleChoiceFormatControl(col, _objNewPlaceholder, dtColumns.Table.DefaultView, dtColumnChoices, dtDeal, _objSystemField, IsSystemField);
                    }
                    if (IsControlSystemField)
                        placeholderHeaderInfo.Controls.Add(_objSystemField);

                    if (IsSystemField == true)
                        _objNewPlaceholder.Controls.Add(new LiteralControl("</td>"));
                    else
                        _objNewPlaceholder.Controls.Add(new LiteralControl("</td></tr>"));
                }
                //_objNewPlaceholder.Controls.Add(new LiteralControl("</table>"));


                CollapsiblePanelExtender cpe = new CollapsiblePanelExtender();
                cpe.ID = "CPE" + _objNewPlaceholder.ID;
                cpe.TargetControlID = _objNewPlaceholder.ID;
                cpe.ExpandControlID = panelHead.ID;
                cpe.CollapseControlID = panelHead.ID;
                cpe.ScrollContents = false;
                cpe.Collapsed = false;
                cpe.AutoExpand = true;
                cpe.ExpandDirection =
                CollapsiblePanelExpandDirection.Vertical;
                cpe.SuppressPostBack = true;
                cpe.TextLabelID = lblHead.ID;
                //cpe.CollapsedImage = "images/collapse.jpg";
                //cpe.ExpandedImage = "images/icon_expand.gif";
                cpe.CollapsedText = "<b><span style='font-size:23px;'>+</span> " + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + " tab</b>";
                cpe.ExpandedText = "<b><span style='font-size:23px;'>-</span> " + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + " tab</b>";

                if (IsSystemField == true)
                {
                    _objNewPlaceholder.Controls.Add(new LiteralControl("</tr></table>"));
                    //placeholderGeneralInfo.Controls.Add(cpExtender);
                    placeholderHeaderInfo.Controls.Add(_objNewPlaceholder);
                }
                else
                {
                    _objNewPlaceholder.Controls.Add(new LiteralControl("</table>"));

                    placeholderGeneralInfo.Controls.Add(panelHead);
                    placeholderGeneralInfo.Controls.Add(_objNewPlaceholder);
                    placeholderGeneralInfo.Controls.Add(cpe);
                }
            }
            placeholderGeneralInfo.Controls.Add(new LiteralControl("</td>"));
            placeholderGeneralInfo.Controls.Add(new LiteralControl("</tr></table>"));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            string colValues = "";
            List<string> _objColumns = new List<string>();
            DataView dtColumns = _objDeals.getColumnControls(Convert.ToInt64(Session["ObjectID"]), 1).Tables[0].DefaultView;
            for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
            {
                bool IsSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[col]["IsSystemField"]);
                string colName = dtColumns.Table.DefaultView[col]["ColumnName"].ToString();
                if (!_objColumns.Contains(colName))
                {
                    _objColumns.Add(colName);
                    int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
                    int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldFormat"]);
                    if (col != 0)
                    {
                        colValues = colValues + ",";
                    }
                    if (fieldType == 1)
                    {
                        //TextBox _objtxt = (TextBox)placeholderGeneralInfo.FindControl(dtColumns.Table.DefaultView[col]["ID"].ToString());
                        string txtValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        //colValues = colValues + _objtxt.Text;
                        colValues = colValues + txtValue;
                    }
                    else if (fieldType == 3 || fieldType == 4)
                    {
                        //if (fieldFormat == 3)//Radio Button List
                        //{
                        string rdbValue = "";
                        if (IsSystemField)
                            rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + "txt" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        else
                            rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        //RadioButtonList _objRdbList = (RadioButtonList)placeholderGeneralInfo.FindControl(dtColumns.Table.DefaultView[col]["ID"].ToString());
                        colValues = colValues + rdbValue;
                        //}
                    }
                }
            }
            saveSalesProcess(colValues);
            //_objDeals.insertDeals(GlobalVariables.LoginUser.OrgID, colValues, Convert.ToInt64(lblLeadID.Text));
            //Response.Redirect("Deals.aspx");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
            if (((System.Web.UI.Control)(sender)).ID == "btnSave")
            {
                CreateFields(Convert.ToInt64(ViewState["LeadID"]));
                getSalesStage(Convert.ToInt64(ViewState["LeadID"]));
            }
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }

        protected void saveformData()
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            string colValues = "";
            List<string> _objColumns = new List<string>();
            DataView dtColumns = _objDeals.getColumnControls(Convert.ToInt64(Session["ObjectID"]), 1).Tables[0].DefaultView;
            for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
            {
                bool IsSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[col]["IsSystemField"]);
                string colName = dtColumns.Table.DefaultView[col]["ColumnName"].ToString();
                if (!_objColumns.Contains(colName))
                {
                    _objColumns.Add(colName);
                    int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
                    int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldFormat"]);
                    if (col != 0)
                    {
                        colValues = colValues + ",";
                    }
                    if (fieldType == 1)
                    {
                        //TextBox _objtxt = (TextBox)placeholderGeneralInfo.FindControl(dtColumns.Table.DefaultView[col]["ID"].ToString());
                        string txtValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        //colValues = colValues + _objtxt.Text;
                        colValues = colValues + txtValue;
                    }
                    else if (fieldType == 3 || fieldType == 4)
                    {
                        //if (fieldFormat == 3)//Radio Button List
                        //{
                        string rdbValue = "";
                        if (IsSystemField)
                            rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + "txt" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        else
                            rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ID"].ToString()];
                        //RadioButtonList _objRdbList = (RadioButtonList)placeholderGeneralInfo.FindControl(dtColumns.Table.DefaultView[col]["ID"].ToString());
                        colValues = colValues + rdbValue;
                        //}
                    }
                }
            }
            saveSalesProcess(colValues);
        }

        protected void saveSalesProcess(string colValues)
        {
            SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
            string salesColValues = "";
            string salesColNames = "";
            DataView dtColumns = objSalesProcessBLL.getSalesStageControls(Convert.ToInt64(ViewState["StageID"]), 1).Tables[0].DefaultView;
            for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
            {
                int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
                int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldFormat"]);
                if (salesColValues != "")
                {
                    salesColValues = salesColValues + ",";
                    salesColNames = salesColNames + ",";
                }
                if (fieldType == 1)
                {
                    string txtValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[col]["ColumnName"].ToString()];
                    salesColValues = salesColValues + "[" + dtColumns.Table.DefaultView[col]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[col]["ColumnName"].ToString() + "]" + "='" + txtValue + "'";
                    //salesColNames = salesColNames + dtColumns.Table.DefaultView[col]["ColumnName"].ToString();
                }
                else if (fieldType == 3 || fieldType == 4)
                {
                    string rdbValue = Request.Form["ctl00$ContentPlaceHolder1$" + dtColumns.Table.DefaultView[col]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[col]["ColumnName"].ToString()];
                    salesColValues = salesColValues + "[" + dtColumns.Table.DefaultView[col]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[col]["ColumnName"].ToString() + "]" + "='" + rdbValue + "'";
                    //salesColNames = salesColNames + dtColumns.Table.DefaultView[col]["ColumnName"].ToString();
                }
            }

            if (ViewState["OpportunityID"] != null)
            {
                CRMBLLOpportunities _objDeals = new CRMBLLOpportunities();
                Int64 OpportunityID = _objDeals.insertOpportunities(GlobalVariables.LoginUser.OrgID, colValues, Convert.ToInt64(ViewState["OpportunityID"]), salesColNames, salesColValues, Convert.ToInt64(txtStageID.Text));
                //uploadDocuments(GlobalVariables.LoginUser.OrgID, 4, OpportunityID);
                //uploadTasks(GlobalVariables.LoginUser.OrgID, 4, OpportunityID);
                ViewState["LeadID"] = OpportunityID;
            }
            else
            {
                CRMBLLDeals _objDeals = new CRMBLLDeals();
                Int64 LeadID = _objDeals.insertDeals(GlobalVariables.LoginUser.OrgID, colValues, Convert.ToInt64(lblLeadID.Text), salesColNames, salesColValues, Convert.ToInt64(txtStageID.Text));
                //uploadDocuments(GlobalVariables.LoginUser.OrgID, 1, LeadID);
                //uploadTasks(GlobalVariables.LoginUser.OrgID, 1, LeadID);
                ViewState["LeadID"] = LeadID;
                ViewState["SelectedLeadID"] = LeadID;
                lblLeadID.Text = LeadID.ToString();
            }
        }

        public void createTextFormatControl(int index, Panel _objplaceholder, DataView dtColumns, DataTable dtDeal, bool IsSystemField)
        {
            string strColumnName = dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
            bool leadIsActive = true;
            if (dtDeal.Rows.Count > 0)
                leadIsActive = Convert.ToBoolean(dtDeal.Rows[0]["IsActive"].ToString());

            if (!leadIsActive)
            {
                btnSave.Enabled = false;
                btnQualify.Enabled = false;
                btnDisqualify.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
                btnQualify.Enabled = true;
                btnDisqualify.Enabled = true;
            }

            string Value = "";
            if (dtDeal != null && dtDeal.Rows.Count > 0)
                Value = dtDeal.Rows[0][strColumnName].ToString();


            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
            if (fieldFormat == 1)//Simple text box
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Enabled = leadIsActive;
                _objTextBox.Text = Value;
                if (!IsSystemField)
                    _objTextBox.Width = Unit.Percentage(93);
                _objplaceholder.Controls.Add(_objTextBox);

            }
            else if (fieldFormat == 2)//Multiline text Box
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.TextMode = TextBoxMode.MultiLine;
                _objTextBox.Enabled = leadIsActive;
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                if (!IsSystemField)
                    _objTextBox.Width = Unit.Percentage(93);
                _objplaceholder.Controls.Add(_objTextBox);
            }
            else if (fieldFormat == 3)//Email
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.Enabled = leadIsActive;
                _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                if (!IsSystemField)
                    _objTextBox.Width = Unit.Percentage(93);
                _objplaceholder.Controls.Add(_objTextBox);
            }
            bool IsRequired = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsRequired"]);
            if (IsRequired)
            {
                RequiredFieldValidator myRFV = new RequiredFieldValidator();
                myRFV.ControlToValidate = dtColumns.Table.DefaultView[index]["ID"].ToString();
                myRFV.Display = ValidatorDisplay.Dynamic;
                myRFV.SetFocusOnError = true;
                myRFV.ValidationGroup = "createValidation";
                myRFV.ErrorMessage = "*";
                myRFV.CssClass = "validationerror";
                _objplaceholder.Controls.Add(myRFV);
            }
        }

        public void createSingleChoiceFormatControl(int index, Panel _objplaceholder, DataView dtColumns, DataTable dtControlChoices, DataTable dtDeal, Panel _objSystemField, bool IsPlcSystemField)
        {
            string strColumnName = dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
            bool IsSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsSystemField"]);
            Int64 ObjectID = Convert.ToInt64(dtColumns.Table.DefaultView[index]["ObjectID"]);
            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
            bool leadIsActive = true;
            if (dtDeal.Rows.Count > 0)
                leadIsActive = Convert.ToBoolean(dtDeal.Rows[0]["IsActive"].ToString());
            if (!leadIsActive)
            {
                btnSave.Enabled = false;
                btnQualify.Enabled = false;
                btnDisqualify.Enabled = false;
            }
            else
            {
                btnSave.Enabled = true;
                btnQualify.Enabled = true;
                btnDisqualify.Enabled = true;
            }

            string Value = "";
            if (dtDeal != null && dtDeal.Rows.Count > 0)
                Value = dtDeal.Rows[0][strColumnName].ToString();

            if (fieldFormat == 6)//Radio Button List
            {
                RadioButtonList _objRdbList = new RadioButtonList();
                _objRdbList.Enabled = leadIsActive;

                _objRdbList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                for (int c = 0; c < dtControlChoices.Rows.Count; c++)
                {
                    _objRdbList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
                    if (Value != "")
                    {
                        if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
                            _objRdbList.Items[c].Selected = true;
                    }
                    else
                        _objRdbList.Items[0].Selected = true;
                    _objplaceholder.Controls.Add(_objRdbList);
                }
            }
            if (fieldFormat == 7)//DropDownList
            {
                if (IsSystemField == true)
                {
                    TextBox _objTextBox = new TextBox();
                    _objTextBox.Enabled = leadIsActive;
                    if (!IsPlcSystemField)
                        _objTextBox.Width = Unit.Percentage(93);
                    _objTextBox.ID = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    _objTextBox.Attributes.Add("Onchange", "checkExists('" + dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString() + "','" + _objTextBox.ID + "','" + GlobalVariables.LoginUser.OrgID + "')");
                    _objTextBox.Attributes.Add("autocomplete", "off");
                    _objTextBox.Attributes.Add("runat", "server");


                    System.Web.UI.WebControls.Image _imgSuccess = new System.Web.UI.WebControls.Image();
                    _imgSuccess.ID = "imgSuccess" + Convert.ToString(_objTextBox.ID);
                    _imgSuccess.ImageUrl = "images/update.jpg";
                    _imgSuccess.Attributes.Add("style", "height:20px;width:20px; display:none;");

                    System.Web.UI.WebControls.Image _imgFailure = new System.Web.UI.WebControls.Image();
                    _imgFailure.ID = "imgFailure" + Convert.ToString(_objTextBox.ID);
                    _imgFailure.ImageUrl = "images/Cancel.jpg";
                    _imgFailure.Attributes.Add("style", "height:20px;width:20px;display:none;");

                    AutoCompleteExtender _objautoCompleteList = new AutoCompleteExtender();
                    _objautoCompleteList.CompletionListItemCssClass = "CompletionListItemCssClass";
                    _objautoCompleteList.CompletionListCssClass = "CompletionListCssClass";

                    _objautoCompleteList.ID = "ACE" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    _objautoCompleteList.TargetControlID = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    _objautoCompleteList.ServiceMethod = "GetCompletionList";
                    _objautoCompleteList.ServicePath = "CustomList.asmx";
                    _objautoCompleteList.ContextKey = dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString() + "," + GlobalVariables.LoginUser.OrgID.ToString();
                    _objautoCompleteList.UseContextKey = true;
                    _objautoCompleteList.EnableCaching = true;
                    _objautoCompleteList.MinimumPrefixLength = 1;


                    //CRMBLLDeals _objDeals = new CRMBLLDeals();
                    //DataTable dtObjectChoices = _objDeals.getObjectChoices(dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString(), GlobalVariables.LoginUser.OrgID).Tables[0];
                    string linkTableValue = dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString();
                    if (Value != "")
                    {
                        _objTextBox.Text = Value;
                    }
                    else if (linkTableValue == "CRM_Users")
                    {
                        _objTextBox.Text = GlobalVariables.LoginUser.UserName;
                    }
                    else if (linkTableValue == "CRM_Territory")
                    {
                        _objTextBox.Text = GlobalVariables.LoginUser.Territory;
                    }
                    else if (linkTableValue == "CRM_Account")
                    {
                        _objTextBox.Text = GlobalVariables.LoginUser.Customer;
                    }
                    else if (linkTableValue == "CRM_Contact")
                    {
                        _objTextBox.Text = GlobalVariables.LoginUser.Contact;
                    }

                    _objplaceholder.Controls.Add(_objTextBox);
                    _objplaceholder.Controls.Add(_imgSuccess);
                    _objplaceholder.Controls.Add(_imgFailure);
                    _objSystemField.Controls.Add(_objautoCompleteList);


                    RequiredFieldValidator myRFV = new RequiredFieldValidator();
                    myRFV.ControlToValidate = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    myRFV.Display = ValidatorDisplay.Dynamic;
                    myRFV.SetFocusOnError = true;
                    myRFV.ValidationGroup = "createValidation";
                    myRFV.ErrorMessage = "*";
                    myRFV.CssClass = "validationerror";
                    _objplaceholder.Controls.Add(myRFV);

                    //for (int c = 0; c < dtObjectChoices.Rows.Count; c++)
                    //{
                    //    _objDDLList.Items.Add(new ListItem(dtObjectChoices.Rows[c]["Name"].ToString(), dtObjectChoices.Rows[c]["ID"].ToString()));
                    //    if (Value != "")
                    //    {
                    //        if (dtObjectChoices.Rows[c]["ID"].ToString() == Value)
                    //            _objDDLList.Items[c].Selected = true;
                    //    }
                    //    else
                    //        _objDDLList.Items[0].Selected = true;
                    //    _objplaceholder.Controls.Add(_objDDLList);
                    //}
                }
                else
                {
                    DropDownList _objDDLList = new DropDownList();
                    _objDDLList.Enabled = leadIsActive;

                    if (!IsPlcSystemField)
                        _objDDLList.Width = Unit.Percentage(93);
                    _objDDLList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                    if (Convert.ToInt64(ViewState["LeadID"]) == 0 && Convert.ToInt64(dtColumns.Table.DefaultView[index]["ID"]) == 2)
                        _objDDLList.Enabled = false;

                    for (int c = 0; c < dtControlChoices.Rows.Count; c++)
                    {
                        _objDDLList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
                        if (Value != "")
                        {
                            if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
                                _objDDLList.Items[c].Selected = true;
                        }
                        else
                            _objDDLList.Items[0].Selected = true;
                        _objplaceholder.Controls.Add(_objDDLList);
                    }
                    if (Convert.ToInt64(dtColumns.Table.DefaultView[index]["ID"]) == 2)
                    {
                        if (_objDDLList.SelectedIndex > 0)
                        {
                            btnReactivate.Visible = true;
                            btnQualify.Visible = false;
                            btnDisqualify.Visible = false;
                        }
                        else
                        {
                            btnReactivate.Visible = false;
                            btnQualify.Visible = true;
                            btnDisqualify.Visible = true;
                        }
                    }
                }

            }
        }

        protected void btnQualify_Click(object sender, EventArgs e)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            saveformData();
            int opportunityID = _objDeals.changeLeadStatus(Convert.ToInt64(ViewState["LeadID"]), "Qualify");
            Session["ObjectID"] = 4;
            CreateFields(Convert.ToInt64(ViewState["LeadID"]));
            getSalesStage(Convert.ToInt64(ViewState["LeadID"]));
            ViewState["OpportunityID"] = opportunityID;
            btnReactivate.Visible = false;
            btnQualify.Visible = false;
            btnDisqualify.Visible = false;

            btnCloseWon.Visible = true;
            btnCloseasLost.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createDeal();", true);
        }

        protected void btnDisqualify_Click(object sender, EventArgs e)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            int opportunityID = _objDeals.changeLeadStatus(Convert.ToInt64(ViewState["LeadID"]), "Disqualify");
            Session["ObjectID"] = 4;
            CreateFields(Convert.ToInt64(ViewState["LeadID"]));
            getSalesStage(Convert.ToInt64(ViewState["LeadID"]));
            ViewState["OpportunityID"] = opportunityID;
            btnReactivate.Visible = true;
            btnQualify.Visible = false;
            btnDisqualify.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createDeal();", true);
        }

        protected void btnReactivate_Click(object sender, EventArgs e)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            int opportunityID = _objDeals.reactivateDeal(Convert.ToInt64(ViewState["LeadID"]));
            //Response.Redirect("Deals.aspx");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }

        protected void btnCloseWon_Click(object sender, EventArgs e)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            int opportunityID = _objDeals.changeOpportunityStatus(Convert.ToInt64(ViewState["OpportunityID"]), "Close as Won", false);
            //Response.Redirect("Deals.aspx");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }

        protected void btnCloseLost_Click(object sender, EventArgs e)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            int opportunityID = _objDeals.changeOpportunityStatus(Convert.ToInt64(ViewState["OpportunityID"]), "Close as Lost", false);
            //Response.Redirect("Deals.aspx");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }

        /*Sales Process*/

        public void getSalesStage(Int64 objectUserID, Int64 stageID = 0)
        {
            SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
            DataSet dsSalesProcess = objSalesProcessBLL.getSalesStages(Convert.ToInt32(Session["ObjectID"]));

            Int64 ActiveStageID = 0;
            try
            {
                DataSet dsActiveSalesStage = objSalesProcessBLL.getActiveSalesStages(objectUserID, 1);
                if (dsActiveSalesStage.Tables[0].Rows.Count > 0)
                    ActiveStageID = Convert.ToInt64(dsActiveSalesStage.Tables[0].Rows[0]["UserActiveStageID"]);
            }
            catch { }

            for (int sp = 0; sp < dsSalesProcess.Tables[0].Rows.Count; sp++)
            {
                string backColor = "#d7d0d0";
                string color = "Black";
                if (stageID != 0 && Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ID"]) == stageID)
                {
                    backColor = "#0072c6";
                    color = "White";
                }
                else if (stageID == 0 && sp == 0)
                {
                    color = "White";
                    backColor = "#0072c6";
                }

                if (ActiveStageID == Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ID"]) || (ActiveStageID == 0 && sp == 0))
                    dvStages.Controls.Add(new LiteralControl("<div style='background-color: " + backColor + "; color: " + color + "; float:left; cursor:pointer; margin-right:10px; text-align:center; min-width:150px;' id=" + "'" + Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ID"]) + "'" + " onclick=changeProcessStage(" + Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ID"]) + "," + Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ObjectID"]) + ")>" + dsSalesProcess.Tables[0].Rows[sp]["StageName"].ToString() + "(Active)" + "</div>"));
                else
                    dvStages.Controls.Add(new LiteralControl("<div style='background-color: " + backColor + "; color: " + color + "; float:left; cursor:pointer; margin-right:10px; text-align:center; min-width:150px;' id=" + "'" + Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ID"]) + "'" + "onclick=changeProcessStage(" + Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ID"]) + "," + Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ObjectID"]) + ")>" + dsSalesProcess.Tables[0].Rows[sp]["StageName"].ToString() + "</div>"));

                bool plcHide = true;
                if (sp == 0)
                {
                    plcHide = false;
                }
                CreateSalesProcessFields(Convert.ToInt64(dsSalesProcess.Tables[0].Rows[sp]["ID"]), plcHide);
            }
            if (stageID != 0)
            {
                //CreateSalesProcessFields(stageID);
            }
            else
            {
                if (dsSalesProcess.Tables[0].Rows.Count > 0)
                {
                    txtStageID.Text = dsSalesProcess.Tables[0].Rows[0]["ID"].ToString();
                    ViewState["StageID"] = txtStageID.Text;
                }
            }
        }

        public void getSalesStageByObjectID(Int64 ObjectID)
        {
            lsSalesProcessStages.DataSource = null;
            lsSalesProcessStages.DataBind();

            SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
            dsSalesProcess = objSalesProcessBLL.getSalesStageSteps(ObjectID);
            if (dsSalesProcess.Count > 0)
            {

                lsSalesProcessStages.DataSource = dsSalesProcess;
                lsSalesProcessStages.DataBind();
                tbNewStage.Visible = false;
                lsSalesProcessStages.Visible = true;
            }
            else
            {
                SaleProcessSteps _objSaleProcessSteps = objSalesProcessBLL.listObjectColumns(ObjectID);
                //lsSalesProcessStages.DataSource = null;
                //lsSalesProcessStages.DataBind();

                ddlNewInsertObjectColumns.DataSource = _objSaleProcessSteps.EntityColumns;
                ddlNewInsertObjectColumns.DataBind();
                lsSalesProcessStages.Visible = false;
                tbNewStage.Visible = true;
            }
        }

        public void getObjects()
        {
            CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
            DataSet dsObjects = objCRMBLLDeals.getAllObjects();
            for (int ob = 0; ob < dsObjects.Tables[0].Rows.Count; ob++)
            {
                dvObjectColumns.Controls.Add(new LiteralControl("<div style='float:left;cursor:pointer; margin-right:20px;' onclick=changeProcessStageStep(" + Convert.ToInt64(dsObjects.Tables[0].Rows[ob]["ID"]) + ")>" + dsObjects.Tables[0].Rows[ob]["ObjectName"].ToString() + "</div>"));
            }
            txtObjectID.Text = "1";
            getSalesStageByObjectID(1);
        }

        protected void btnRefreshSalesProcess_Click(object sender, EventArgs e)
        {
            CreateFields(Convert.ToInt64(ViewState["LeadID"]));
            getSalesStage(Convert.ToInt64(ViewState["LeadID"]));
        }

        protected void btnEditSalesProcess_Click(object sender, EventArgs e)
        {
            getObjects();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editSalesProcess();", true);
        }

        protected void ddlColumns_load(object sender, EventArgs e)
        {
            try
            {
                DropDownList _objddlColumns = (DropDownList)sender;
                ListViewDataItem row = (ListViewDataItem)_objddlColumns.NamingContainer;
                _objddlColumns.SelectedValue = ((CRMBLL.SaleProcessSteps)(row.DataItem)).FormID;
            }
            catch
            {
            }
        }

        protected void btnDelete_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton _objButton = (ImageButton)sender;
            ListViewDataItem row = (ListViewDataItem)_objButton.NamingContainer;
            Label lblStepID = ((Label)row.FindControl("lblStepID"));

            SalesProcessBLL _objSalesProcessBLL = new SalesProcessBLL();
            _objSalesProcessBLL.deleteSalesStage(Convert.ToInt64(_objButton.CommandName), Convert.ToInt64(lblStepID.Text));

            CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
            DataSet dsObjects = objCRMBLLDeals.getAllObjects();
            for (int ob = 0; ob < dsObjects.Tables[0].Rows.Count; ob++)
            {
                dvObjectColumns.Controls.Add(new LiteralControl("<div style='float:left;cursor:pointer; margin-right:20px;' onclick=changeProcessStageStep(" + Convert.ToInt64(dsObjects.Tables[0].Rows[ob]["ID"]) + ")>" + dsObjects.Tables[0].Rows[ob]["ObjectName"].ToString() + "</div>"));
            }

            getSalesStageByObjectID(Convert.ToInt64(txtObjectID.Text));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editSalesProcess();", true);
        }

        protected void ddlInsertObjectColumns_load(object sender, EventArgs e)
        {
            try
            {
                DropDownList _objddlColumns = (DropDownList)sender;
                _objddlColumns.DataSource = dsSalesProcess.ElementAtOrDefault(0).ProcessSetps.ElementAtOrDefault(0).EntityColumns;
                _objddlColumns.DataBind();

                //_objddlColumns.Items.Add(new ListItem("asd", "dfg"));
            }
            catch
            {
            }
        }

        protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                //Int64 StageID = 0;
                ListView lvStage = (ListView)(((System.Web.UI.Control)(e.Item)).DataKeysContainer).DataKeysContainer;
                ListViewItem dataItem = (ListViewItem)e.Item;

                string clientID = dataItem.NamingContainer.ClientID;
                string rowID = clientID.Substring(clientID.IndexOf("lvStageFields_") + 14);

                Int64 stageID = Convert.ToInt64(lvStage.DataKeys[Convert.ToInt32(rowID)].Values[0].ToString());

                Label lblStageID = (Label)e.Item.FindControl("lblStageID");
                TextBox txtStepName = (TextBox)e.Item.FindControl("txtStepName");
                DropDownList ddlInsertObjectColumns = (DropDownList)e.Item.FindControl("ddlInsertObjectColumns");
                saveSalesProcessStep(Convert.ToInt64(txtObjectID.Text), "", stageID, txtStepName.Text, Convert.ToInt64(ddlInsertObjectColumns.SelectedValue));
            }
        }
        protected void ListView1_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
        }

        protected void lsSalesProcessStages_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                TextBox txtNewStageName = (TextBox)e.Item.FindControl("txtNewStageName");
                TextBox txtNewStageStep = (TextBox)e.Item.FindControl("txtNewStageStep");
                DropDownList ddlNewInsertObjectColumns = (DropDownList)e.Item.FindControl("ddlNewInsertObjectColumns");
                saveSalesProcessStep(Convert.ToInt64(txtObjectID.Text), txtNewStageName.Text, 0, txtNewStageStep.Text, Convert.ToInt64(ddlNewInsertObjectColumns.SelectedValue));
            }
        }
        protected void lsSalesProcessStages_ItemInserting(object sender, ListViewInsertEventArgs e)
        {
        }

        protected void btnSaveProcess_Click(object sender, EventArgs e)
        {
            saveSalesProcessStep(Convert.ToInt64(txtObjectID.Text), txtNewStageName.Text, 0, txtNewStageStep.Text, Convert.ToInt64(ddlNewInsertObjectColumns.SelectedValue));
        }

        public void saveSalesProcessStep(Int64 ObjectID, string StageName, Int64 StageID, string StepName, Int64 FormID)
        {
            SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
            objSalesProcessBLL.insertSalesProcessStage(ObjectID, StageName, StageID, StepName, FormID);
            CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
            DataSet dsObjects = objCRMBLLDeals.getAllObjects();
            for (int ob = 0; ob < dsObjects.Tables[0].Rows.Count; ob++)
            {
                dvObjectColumns.Controls.Add(new LiteralControl("<div style='float:left;cursor:pointer; margin-right:20px;' onclick=changeProcessStageStep(" + Convert.ToInt64(dsObjects.Tables[0].Rows[ob]["ID"]) + ")>" + dsObjects.Tables[0].Rows[ob]["ObjectName"].ToString() + "</div>"));
            }
            getSalesStageByObjectID(Convert.ToInt64(txtObjectID.Text));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editSalesProcess();", true);
        }

        protected void txtObjectID_Click(object sender, EventArgs e)
        {
            CRMBLLDeals objCRMBLLDeals = new CRMBLLDeals();
            DataSet dsObjects = objCRMBLLDeals.getAllObjects();
            for (int ob = 0; ob < dsObjects.Tables[0].Rows.Count; ob++)
            {
                dvObjectColumns.Controls.Add(new LiteralControl("<div style='float:left;cursor:pointer; margin-right:20px;' onclick=changeProcessStageStep(" + Convert.ToInt64(dsObjects.Tables[0].Rows[ob]["ID"]) + ")>" + dsObjects.Tables[0].Rows[ob]["ObjectName"].ToString() + "</div>"));
            }
            getSalesStageByObjectID(Convert.ToInt64(txtObjectID.Text));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editSalesProcess();", true);
        }

        public void CreateSalesProcessFields(Int64 stageID, bool plcHide)
        {

            //PlaceHolder objNewPlaceholder = new PlaceHolder();
            HtmlGenericControl objNewPlaceholder = new HtmlGenericControl("DIV");
            objNewPlaceholder.ID = "SPFields" + stageID.ToString();
            if (plcHide)
                objNewPlaceholder.Attributes.Add("style", "display:none");

            clsCommonFunctions.CheckSession();
            SalesProcessBLL objSalesProcessBLL = new SalesProcessBLL();
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataView dtColumns = objSalesProcessBLL.getSalesStageControls(stageID, 0).Tables[0].DefaultView;
            objNewPlaceholder.Controls.Add(new LiteralControl("<table style='margin-top:15px;'>"));
            int val = 1;
            DataTable dtSalesProcess = objSalesProcessBLL.getSalesProcess(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ViewState["LeadID"]), 1).Tables[0];
            for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
            {
                int rResult = 0;
                Math.DivRem(col, 4, out rResult);
                if (col == 0 || rResult == 0)
                {
                    objNewPlaceholder.Controls.Add(new LiteralControl("<tr style='vertical-align:top;height:30px;'>"));
                }
                objNewPlaceholder.Controls.Add(new LiteralControl("<td style='padding-left:40px;'>"));
                int formID = Convert.ToInt32(dtColumns.Table.DefaultView[col]["ID"]);
                Label _objLabel = new Label();
                _objLabel.Text = dtColumns.Table.DefaultView[col]["StepName"].ToString() + ":";
                objNewPlaceholder.Controls.Add(_objLabel);
                objNewPlaceholder.Controls.Add(new LiteralControl("</td><td>"));

                int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
                if (fieldType == 1)
                {
                    createSalesTextFormatControl(col, objNewPlaceholder, dtColumns.Table.DefaultView, dtSalesProcess);
                }
                else if (fieldType == 3 || fieldType == 4)
                {
                    DataTable dtColumnChoices = _objDeals.getControlChoices(formID).Tables[0];
                    createSalesSingleChoiceFormatControl(col, objNewPlaceholder, dtColumns.Table.DefaultView, dtColumnChoices, dtSalesProcess);
                }
                objNewPlaceholder.Controls.Add(new LiteralControl("</td>"));
                if (val == 4)
                {
                    val = 0;
                    objNewPlaceholder.Controls.Add(new LiteralControl("</tr>"));
                }
                val = val + 1;
            }
            objNewPlaceholder.Controls.Add(new LiteralControl("</table>"));

            placeholderSalesProcess.Controls.Add(objNewPlaceholder);
        }

        public void createSalesTextFormatControl(int index, HtmlGenericControl _objplaceholder, DataView dtColumns, DataTable dtSalesProcess)
        {
            string strColumnName = dtColumns.Table.DefaultView[index]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
            string Value = "";
            if (dtSalesProcess != null && dtSalesProcess.Rows.Count > 0)
                Value = dtSalesProcess.Rows[0][strColumnName].ToString();


            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
            if (fieldFormat == 1)//Simple text box
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.ID = strColumnName;// "sales" + dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                _objplaceholder.Controls.Add(_objTextBox);
            }
            else if (fieldFormat == 2)//Multiline text Box
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.TextMode = TextBoxMode.MultiLine;
                _objTextBox.ID = strColumnName; //"sales" + dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                _objplaceholder.Controls.Add(_objTextBox);
            }
            else if (fieldFormat == 3)//Email
            {
                TextBox _objTextBox = new TextBox();
                _objTextBox.ID = strColumnName; //"sales" + dtColumns.Table.DefaultView[index]["ID"].ToString();
                _objTextBox.Text = Value;
                _objplaceholder.Controls.Add(_objTextBox);
            }
            bool IsRequired = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsRequired"]);
            if (IsRequired)
            {
                RequiredFieldValidator myRFV = new RequiredFieldValidator();
                myRFV.ControlToValidate = strColumnName;
                myRFV.Display = ValidatorDisplay.Dynamic;
                myRFV.SetFocusOnError = true;
                myRFV.ValidationGroup = "createValidation";
                myRFV.ErrorMessage = "*";
                myRFV.CssClass = "validationerror";
                _objplaceholder.Controls.Add(myRFV);
            }
        }

        public void createSalesSingleChoiceFormatControl(int index, HtmlGenericControl _objplaceholder, DataView dtColumns, DataTable dtControlChoices, DataTable dtDeal)
        {
            string strColumnName = dtColumns.Table.DefaultView[index]["ObjectID"].ToString() + "_" + dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
            bool IsSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsSystemField"]);
            Int64 ObjectID = Convert.ToInt64(dtColumns.Table.DefaultView[index]["ObjectID"]);
            int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
            string Value = "";
            if (dtDeal != null && dtDeal.Rows.Count > 0)
                Value = dtDeal.Rows[0][strColumnName].ToString();

            if (fieldFormat == 6)//Radio Button List
            {
                RadioButtonList _objRdbList = new RadioButtonList();
                _objRdbList.ID = strColumnName;// Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                for (int c = 0; c < dtControlChoices.Rows.Count; c++)
                {
                    _objRdbList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
                    if (Value != "")
                    {
                        if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
                            _objRdbList.Items[c].Selected = true;
                    }
                    else
                        _objRdbList.Items[0].Selected = true;
                    _objplaceholder.Controls.Add(_objRdbList);
                }
            }
            if (fieldFormat == 7)//DropDownList
            {
                DropDownList _objDDLList = new DropDownList();
                _objDDLList.ID = strColumnName;// Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
                if (IsSystemField == true)
                {
                    CRMBLLDeals _objDeals = new CRMBLLDeals();
                    DataTable dtObjectChoices = _objDeals.getObjectChoices(dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString(), GlobalVariables.LoginUser.OrgID).Tables[0];

                    for (int c = 0; c < dtObjectChoices.Rows.Count; c++)
                    {
                        _objDDLList.Items.Add(new ListItem(dtObjectChoices.Rows[c]["Name"].ToString(), dtObjectChoices.Rows[c]["Name"].ToString()));
                        if (Value != "")
                        {
                            if (dtObjectChoices.Rows[c]["Name"].ToString() == Value)
                                _objDDLList.Items[c].Selected = true;
                        }
                        else
                            _objDDLList.Items[0].Selected = true;
                        _objplaceholder.Controls.Add(_objDDLList);
                    }
                }
                else
                {
                    for (int c = 0; c < dtControlChoices.Rows.Count; c++)
                    {
                        _objDDLList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
                        if (Value != "")
                        {
                            if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
                                _objDDLList.Items[c].Selected = true;
                        }
                        else
                            _objDDLList.Items[0].Selected = true;
                        _objplaceholder.Controls.Add(_objDDLList);
                    }
                }
            }

        }

        protected void btnStageID_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt64(ViewState["StageID"]) != Convert.ToInt64(txtStageID.Text))
            {
                //bool sameObject = false;
                if (Convert.ToInt64(txtSelObjectID.Text) != Convert.ToInt64(Session["ObjectID"]))
                    saveformData();
                //else
                //    saveformData();

                CRMBLLDeals objCRMDeals = new CRMBLLDeals();
                Int64 IDS = Convert.ToInt64(ViewState["SelectedLeadID"]);

                if (txtSelObjectID.Text == "4")
                {
                    DataSet dsSelectedDeal = objCRMDeals.getSelectedDeal(IDS, 4);
                    try
                    {
                        btnReactivate.Visible = false;
                        btnQualify.Visible = false;
                        btnDisqualify.Visible = false;

                        btnCloseWon.Visible = true;
                        btnCloseasLost.Visible = true;

                        ViewState["OpportunityID"] = dsSelectedDeal.Tables[0].Rows[0]["OpportunitiesID"].ToString();
                        Session["ObjectID"] = 4;
                    }
                    catch
                    {
                        btnReactivate.Visible = false;
                        btnQualify.Visible = true;
                        btnDisqualify.Visible = true;

                        btnCloseWon.Visible = false;
                        btnCloseasLost.Visible = false;

                        ViewState["OpportunityID"] = null;
                        Session["ObjectID"] = 1;
                    }
                }
                else
                {
                    DataSet dsSelectedDeal = objCRMDeals.getSelectedDeal(Convert.ToInt64(ViewState["SelectedLeadID"]), 3);
                    btnReactivate.Visible = false;
                    btnQualify.Visible = true;
                    btnDisqualify.Visible = true;
                    btnCloseWon.Visible = false;
                    btnCloseasLost.Visible = false;
                    if (Convert.ToBoolean(dsSelectedDeal.Tables[0].Rows[0]["IsActive"]) == false)
                    {
                        btnReactivate.Visible = true;
                        btnQualify.Visible = false;
                        btnDisqualify.Visible = false;
                    }


                    ViewState["OpportunityID"] = null;
                    Session["ObjectID"] = 1;
                }
                //if (!sameObject)
                CreateFields(IDS);
                getSalesStage(IDS, Convert.ToInt64(txtStageID.Text));
                ViewState["StageID"] = txtStageID.Text;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createDeal();", true);
            }
        }

        /*Customize Object*/

        //protected void btnCustomizeObject_Click(object sender, EventArgs e)
        //{
        //    CRMBLLDeals _objDeals = new CRMBLLDeals();
        //    DataSet dsObjectCol = _objDeals.getObjectsColumns(1);
        //    for (int c = 0; c < dsObjectCol.Tables[0].Rows.Count; c++)
        //    {
        //        HtmlGenericControl li = new HtmlGenericControl("li");
        //        li.InnerText = Convert.ToString(dsObjectCol.Tables[0].Rows[c]["ColumnName"]);
        //        li.ID = Convert.ToString(dsObjectCol.Tables[0].Rows[c]["GridDisplayName"]);
        //        ulFields.Controls.Add(li);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editColumns();", true);
        //}





        /* Phone Calls */
        protected void btnPhoneCall_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            ViewState["sObjectUserID"] = src.CommandArgument;
            bindPhoneCallsList();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "objectPhoneCall();", true);
        }

        protected void bindPhoneCallsList()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDocumets = objDeals.getPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"]));
            gvPhoneCalls.DataSource = dsDocumets;
            gvPhoneCalls.DataBind();
            txtPhoneCallDescription.Text = "";
            txtCallDate.Text = "";
            //ddlDirection.SelectedValue = "Outgoing";
        }

        protected void btnUploadPhoneCalls_Click(object sender, EventArgs e)
        {
            uploadphonecalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ViewState["sObjectUserID"]));
            bindPhoneCallsList();
            txtPhoneCallDescription.Text = "";
            txtCallDate.Text = "";
            ddlDirection.SelectedIndex = 0;
        }

        private void uploadphonecalls(Int64 orgid, Int64 dealid)
        {
            try
            {
                CRMBLLDeals objblldeals = new CRMBLLDeals();
                objblldeals.insertPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["objectid"]), dealid, txtPhoneCallDescription.Text, Convert.ToDateTime(txtCallDate.Text), ddlDirection.SelectedValue, Convert.ToInt64(ViewState["sObjectUserID"]), "", false);
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnDeleteAllCall_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            string IDS = "";
            for (int_index = 0; int_index <= gvPhoneCalls.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvPhoneCalls.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(gvPhoneCalls.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMDeals.deletePhoneCalls(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Phone Calls Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindPhoneCallsList();
        }


        /* Notes */
        protected void btnGridNotes_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            ViewState["sObjectUserID"] = src.CommandArgument;
            bindNotes();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "objectNotes();", true);
        }

        protected void bindNotes()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataView dsDocumets = objDeals.getDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"])).Tables[0].DefaultView;
            dsDocumets.RowFilter = "IsDocsOnly=0";
            gvNotes.DataSource = dsDocumets.Table.DefaultView;
            gvNotes.DataBind();
        }

        protected void btnUploadDocs_Click(object sender, EventArgs e)
        {
            try
            {
                //string path = Server.MapPath("~\\MyFiles\\" + Session["ObjectID"].ToString() + GlobalVariables.LoginUser.OrgID.ToString() + ViewState["sObjectUserID"].ToString());

                //if (!Directory.Exists(path))
                //{
                //    Directory.CreateDirectory(path);
                //}
                string documentName = "";
                string documentPath = "";
                //if (fleUpload.FileName != "")
                //{
                //    fleUpload.SaveAs(path + "\\" + System.IO.Path.GetFileName(fleUpload.FileName));
                //    documentName = fleUpload.FileName;
                //    documentPath = path;
                //}
                //if (documentName != "")
                //{
                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                objBLLDeals.insertDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"]), documentName, documentPath, Convert.ToInt64(ViewState["LeadID"]), "", txtNotes.Text, false, false);
                txtNotes.Text = "";
                bindNotes();
                //}
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDeleteNotes_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            string IDS = "";
            for (int_index = 0; int_index <= gvNotes.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvNotes.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(gvNotes.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMDeals.deleteNotes(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Note Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindNotes();
        }
    }
}