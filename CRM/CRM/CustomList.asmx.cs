﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Collections;
using CRMBLL;
using System.Data;

namespace CRM
{
    /// <summary>
    /// Summary description for CustomList
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class CustomList : System.Web.Services.WebService
    {
        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public string[] GetCompletionList(string prefixText, int count, string contextKey)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataTable dtObjectChoices = _objDeals.getObjectChoices(contextKey.Substring(0, contextKey.IndexOf(",")), Convert.ToInt64(contextKey.Substring(contextKey.IndexOf(",") + 1))).Tables[0];

            ArrayList filteredList = new ArrayList();
            for (int varCount = 0; varCount < dtObjectChoices.Rows.Count; varCount++)
            {
                string strName = dtObjectChoices.Rows[varCount]["Name"].ToString();

                if (strName.ToLower().StartsWith(prefixText.ToLower()))
                    filteredList.Add(strName);
            }
            return (string[])filteredList.ToArray(typeof(string));
        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public Int64 checkAvailability(string linkTable, string value, string orgID)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataView dvObjectChoices = _objDeals.getObjectChoices(linkTable, Convert.ToInt64(orgID)).Tables[0].DefaultView;
            dvObjectChoices.RowFilter = "name like '" + value + "'";

            if (dvObjectChoices.Table.DefaultView.Count > 0)
                return 1;
            else
                return 0;
        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public Int64 checkUserAvailability(string Value, Int64 selUserID)
        {
            CRMBLLUsers _objUsers = new CRMBLLUsers();
            DataView dvUsers = _objUsers.getUsers().Tables[0].DefaultView;
            dvUsers.RowFilter = "UserName = '" + Value + "' and id <> " + selUserID;

            if (dvUsers.Table.DefaultView.Count > 0)
                return 1;
            else
                return 0;
        }


        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public Int64 checkEmailAvailability(string Value, Int64 selUserID)
        {
            CRMBLLUsers _objUsers = new CRMBLLUsers();
            DataView dvUsers = _objUsers.getUsers().Tables[0].DefaultView;
            dvUsers.RowFilter = "Email = '" + Value + "' and id <> " + selUserID;

            if (dvUsers.Table.DefaultView.Count > 0)
                return 1;
            else
                return 0;
        }

        [System.Web.Services.WebMethod]
        [System.Web.Script.Services.ScriptMethod]
        public Int64 checkFilterAvailability(string Value, Int64 orgID, Int64 objectID)
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataView dvFilters = _objDeals.getFilters(orgID, objectID).Tables[0].DefaultView;
            dvFilters.RowFilter = "FilterName = '" + Value + "'";

            if (dvFilters.Table.DefaultView.Count > 0)
                return 1;
            else
                return 0;
        }


        [System.Web.Services.WebMethod(EnableSession = true)]
        [System.Web.Script.Services.ScriptMethod]
        public string[] filterGridrecords(string prefixText, int count, string contextKey)
        {
            DataTable Griddata = (DataTable)HttpContext.Current.Session["gridData"];
            Griddata.DefaultView.RowFilter = "[" + contextKey + "]" + " like '%" + prefixText + "%'";
            DataTable dt = Griddata.DefaultView.ToTable(true, contextKey);

            DataRow[] drArray = dt.Select();
            //var rr = Griddata.DefaultView.ToTable().Columns["Status"].DefaultValue;

            //CRMBLLDeals _objDeals = new CRMBLLDeals();
            //DataTable dtObjectChoices = _objDeals.getObjectChoices(contextKey.Substring(0, contextKey.IndexOf(",")), Convert.ToInt64(contextKey.Substring(contextKey.IndexOf(",") + 1))).Tables[0];

            string[] newfilteredList;
            ArrayList filteredList = new ArrayList();
            for (int varCount = 0; varCount < drArray.Count(); varCount++)
            {
                string strName = drArray.ElementAt(varCount).ItemArray.FirstOrDefault().ToString();
                filteredList.Add(strName);
            }
            return (string[])filteredList.ToArray(typeof(string));
        }
    }
}
