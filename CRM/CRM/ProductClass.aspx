﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="ProductClass.aspx.cs" Inherits="CRM.ProductClass" EnableEventValidation="false" %>

<%@ Register Assembly="JQChart.Web" Namespace="JQChart.Web.UI.WebControls" TagPrefix="jqChart" %>
<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" type="text/css" href="css/jquery.jqChart.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.jqRangeSlider.css" />
    <script src="Script/jquery.jqRangeSlider.min.js" type="text/javascript"></script>
    <script src="Script/jquery.jqChart.min.js" type="text/javascript"></script>
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <script language="javascript" type="text/javascript">
        function resizeIframe(obj) {
            obj.style.height = (obj.contentWindow.document.body.scrollHeight + 20) + 'px';
        }

        function checkExists(linkTable, controlID, orgID) {
            var txtValue = $("#ContentPlaceHolder1_" + controlID).val();
            $.ajax({
                type: "POST",
                url: "CustomList.asmx/checkAvailability",
                data: '{linkTable: "' + linkTable + '",value: "' + txtValue + '",orgID: "' + orgID + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var imgSuccess = $("#ContentPlaceHolder1_imgSuccess" + controlID)[0].id;
                    var imgFailure = $("#ContentPlaceHolder1_imgFailure" + controlID)[0].id;
                    switch (response.d) {
                        case 1:
                            $("#" + imgSuccess).show();
                            $("#" + imgFailure).hide();
                            $("#ContentPlaceHolder1_btnSave").removeAttr("disabled");
                            break;
                        case 0:
                            $("#" + imgSuccess).hide();
                            $("#" + imgFailure).show();
                            $("#ContentPlaceHolder1_btnSave").attr("disabled", true);
                            break;
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        }
    </script>
    <script type="text/javascript">
        function createProductClass() {
            var createProductClassBox = "#div_CreateProductClass-box";
            //Fade in the Popup
            $(createProductClassBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createProductClassBox).height() + 24) / 2;
            var popMargLeft = ($(createProductClassBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function advanceFind() {
            var advanceFindBox = "#div_AdvanceFind-box";
            //Fade in the Popup
            $(advanceFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(advanceFindBox).height() + 24) / 2;
            var popMargLeft = ($(advanceFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="maskAdvanceFind"></div>');
            $('#maskAdvanceFind').fadeIn(300);

            return true;
        };

        function closeAdvanceFindWindow() {
            $('#maskAdvanceFind , .message-popup').fadeOut(300);
            $('#maskAdvanceFind').remove();

            document.getElementById("ContentPlaceHolder1_btnRefreshFilterList").click();
            return false;
        }

        function objectNotes() {
            var notesFindBox = "#div_Notes-box";
            //Fade in the Popup
            $(notesFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(notesFindBox).height() + 24) / 2;
            var popMargLeft = ($(notesFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="maskNotes"></div>');
            $('#maskNotes').fadeIn(300);

            return true;
        };

        function closeNotesWindow() {
            $('#maskNotes , .message-popup').fadeOut(300);
            $('#maskNotes').remove();
            return false;
        }

        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                //window.location = "ProductClass.aspx";
                document.getElementById("ContentPlaceHolder1_btnRefreshUpdatePanel").click();
            });
            return false;
        }

        //        function firstColumnClick(contactID) {
        //            document.getElementById("ContentPlaceHolder1_txtProductClassID").value = contactID;
        //            document.getElementById("ContentPlaceHolder1_btnGridColumn").click();
        //        };
        //        function notesColumnClick(contactID) {
        //            document.getElementById("ContentPlaceHolder1_txtNoteProductClassID").value = contactID;
        //            document.getElementById("ContentPlaceHolder1_btnGridNotes").click();
        //        };

        //        function callColumnClick(DealID) {
        //            document.getElementById("ContentPlaceHolder1_txtNoteProductClassID").value = DealID;
        //            document.getElementById("ContentPlaceHolder1_btnPhoneCall").click();
        //        };

        function objectPhoneCall() {
            var notesFindBox = "#div_PhoneCall-box";
            //Fade in the Popup
            $(notesFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(notesFindBox).height() + 24) / 2;
            var popMargLeft = ($(notesFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function importData() {
            $("#trImportData").toggle();
            return false;
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.link').click(function () {
                if ($('.link').attr('class') == 'link open') {
                    $(this).removeClass('open')
                    $('.leftbox').animate({ 'left': '-520' });
                } else {
                    $(this).addClass('open')
                    $('.leftbox').animate({ 'left': '0' });
                }
            });
        });
        function chartButton() {
            if ($('.link').attr('class') == 'link open') {
                $(this).removeClass('open')
                $('.leftbox').animate({ 'left': '-520' });
            } else {
                $(this).addClass('open')
                $('.leftbox').animate({ 'left': '0' });
            }
            document.getElementById("ContentPlaceHolder1_btnRefreshUpdatePanel").click();
        }
    </script>
    <style>
        .filterDiv
        {
            width: 130px;
        }
        
        .imgfilter
        {
            width: 18px;
            display: block;
            border:none;
            background: gray url(../images/grid_search.JPG) 0 0 no-repeat;
        }
        .filterTextBox
        {
            line-height: 0px;
            float: left;
            width: 100px;
            border:none;
            height:16px;
        }
        .CompletionListCssClass
        {
            margin: 0px !important;
            background-color: inherit;
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: 'default';
            overflow: auto;
            height: 200px;
            text-align: left;
            list-style-type: none;
            z-index: 99999999 !important;
        }
        .CompletionListItemCssClass
        {
            background-color: window;
            color: windowtext;
            padding: 1px;
        }
        body
        {
            padding: 0;
            margin: 0ee;
        }
        .leftbox
        {
            position: absolute;
            top: 130px;
            left: -520px;
        }
        .feedback
        {
            width: 500px;
            background: #eee;
            float: left;
            padding: 10px;
            border: 1px solid #ccc;
        }
        .link
        {
            float: left;
            padding: 10px;
            background: #eee;
            border: 1px solid #ccc;
            position: relative;
            z-index: 5;
            margin-left: -1px;
        }
        .link a
        {
            color: #333;
        }
    </style>
    <div id="div_PhoneCall-box" class="message-popup" style="overflow: hidden;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div11" style="display: block;">
                    <div id="divPhoneCall12" style="overflow: auto; width: 100%; height: 100% !important;">
                        <table style="width: 100%;">
                            <tr>
                                <td style="vertical-align: top;">
                                    <b>Phone Calls</b><br />
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtPhoneCallDescription" Placeholder="Description" TextMode="MultiLine"
                                                    Style="min-height: 70px; min-width: 99%;" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtCallDate" Placeholder="Due date" Style="width: 99%;" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:DropDownList ID="ddlDirection" Style="width: 99%;" runat="server">
                                                    <asp:ListItem Text="Outgoing" Value="Outgoing"></asp:ListItem>
                                                    <asp:ListItem Text="Incoming" Value="Incoming"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Button ID="Button1" Style="display: none;" runat="server" CssClass="button"
                                                    OnClick="btnPhoneCall_Click" CausesValidation="False" />
                                                <asp:Button ID="Button2" runat="server" Style="float: none !important;" CssClass="btn btn-primary"
                                                    Text="Save" OnClick="btnUploadPhoneCalls_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table width="100%" style="border-right: gray 1px solid; border-top: gray 1px solid;
                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                        <tr>
                                            <th align="left" colspan="2">
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="float: left;">
                                                <asp:Button ID="Button4" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAllCall_Click"
                                                    OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Call" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvPhoneCalls" EmptyDataText="No Records Found" runat="server" DataKeyNames="ID"
                                        DataKeyField="ID" AutoGenerateColumns="false" Width="100%">
                                        <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                    <asp:Label ID="lblCallID" runat="server" Text='<%#Eval("ID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Description
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("Description")
                                                    %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Date
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("DueDate") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Direction
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("Direction") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="div_Notes-box" class="message-popup">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeNotesWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div9" style="display: block;">
                    <div id="div10" style="overflow: auto; width: 100%; height: 100% !important;">
                        <%--<iframe id="Iframe2" frameborder="0" src="ObjectNotes.aspx" onload="ImagesDone();"
                            style="overflow: hidden;" width="100%" height="495px"></iframe>--%>
                        <table style="width: 100%;">
                            <tr>
                                <td style="vertical-align: top;">
                                    <b>Notes</b><br />
                                    <asp:TextBox TextMode="MultiLine" Style="min-height: 70px; width: 98%;" ID="txtNotes"
                                        runat="server"></asp:TextBox><br />
                                    <br />
                                    <%--<asp:FileUpload runat="server" ID="fleUpload" />--%><br />
                                    <br />
                                    <asp:Button ID="btnUpload" runat="server" Style="float: none !important;" CssClass="btn btn-primary"
                                        Text="Save" OnClick="btnUploadDocs_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table width="100%" style="border-right: gray 1px solid; border-top: gray 1px solid;
                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                        <tr>
                                            <th align="left" colspan="2">
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="float: left;">
                                                <asp:Button ID="Button3" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteNotes_Click"
                                                    OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Note" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvNotes" EmptyDataText="No Records Found" runat="server" DataKeyNames="ID"
                                        DataKeyField="ID" AutoGenerateColumns="false" Width="100%">
                                        <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                    <asp:Label ID="lblNoteID" runat="server" Text='<%#Eval("ID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Notes
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("Notes") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField>
                                                <HeaderTemplate>
                                                    Document Name
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("DocumentName") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Created Date
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("CreatedDate") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="div_AdvanceFind-box" class="message-popup" style="overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeAdvanceFindWindow()"
                title="Close Window" alt="Close" /></a>
        <div id="Div2" style="display: block;">
            <div id="div3" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="ifrmAdvanceFind" frameborder="0" src="AdvanceFind.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%" height="100%"></iframe>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="div_CreateProductClass-box" style="overflow-x: hidden;" class="message-popup">
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="showpage" style="display: block;">
                    <div id="divIframe" runat="server" style="overflow: auto; width: 100%; height: 100% !important;">
                        <div>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="btnSave" runat="server" ValidationGroup="InsertProductClass" CssClass="save"
                                            OnClick="btnSave_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; text-align: left;">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <b>Summary</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top; text-align: left;">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                Description
                                                            </td>
                                                            <td>
                                                                <asp:TextBox ID="txtDescription" Style="width: 100%;" runat="server"></asp:TextBox>
                                                                <asp:RequiredFieldValidator ID="rqVName" runat="server" ControlToValidate="txtDescription"
                                                                    ErrorMessage="*" Style="color: Red; font-weight: bold;" Display="Dynamic" SetFocusOnError="true"
                                                                    ValidationGroup="InsertProductClass"></asp:RequiredFieldValidator>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Item Type
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlItemType" runat="server" Style="width: 100%;">
                                                                    <asp:ListItem Text="Finished Good" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text=" Semi-Finished Good" Value="2"></asp:ListItem>
                                                                    <asp:ListItem Text="Service" Value="3"></asp:ListItem>
                                                                    <asp:ListItem Text="Support" Value="4"></asp:ListItem>
                                                                    <asp:ListItem Text="Trading" Value="5"></asp:ListItem>
                                                                    <asp:ListItem Text="Misc." Value="6"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Validation
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlValidation" runat="server" Style="width: 100%;">
                                                                    <asp:ListItem Text="FIFO" Value="1"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Location Code
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlLocationCode" runat="server" Style="width: 100%;">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Exclude Comission
                                                            </td>
                                                            <td>
                                                                <asp:RadioButtonList ID="rdbExcludeComission" runat="server">
                                                                    <asp:ListItem Text="Yes" Selected="True" Value="True"></asp:ListItem>
                                                                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Unit of Measure
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlUnitofMeasure" runat="server" Style="width: 100%;">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Product Family
                                                            </td>
                                                            <td>
                                                                <asp:DropDownList ID="ddlProductFamily" runat="server" Style="width: 100%;">
                                                                </asp:DropDownList>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                Disallow Price Override
                                                            </td>
                                                            <td>
                                                                <asp:RadioButtonList ID="rdbPriceOverride" runat="server">
                                                                    <asp:ListItem Text="Yes" Selected="True" Value="True"></asp:ListItem>
                                                                    <asp:ListItem Text="No" Value="False"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td valign="top">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="width: 80%; vertical-align: top; text-align: left; float: right;">
                                                                <iframe id="Iframe3" src="Notes.aspx" style="width: 100%; border: none; min-height: 450px;
                                                                    overflow-x: hidden; overflow-y: scroll;" runat="server"></iframe>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gradientbginner" style="width: 90%; overflow: auto;">
                <table width="100%">
                    <tr>
                        <td width="18px">
                        </td>
                        <td>
                            <div class="maincontent" style="width: 100%">
                                <div class="searchboxcaption">
                                    <span>ProductClass</span></div>
                                <div class="searchbox">
                                    <div>
                                        <table width="100%">
                                            <tr>
                                                <div style="text-align: center;">
                                                    <asp:Label ID="lblListmsg" Style="text-align: center; color: Red; font-size: 13px;"
                                                        runat="server" EnableViewState="False"></asp:Label>
                                                </div>
                                                <td id="trallButtons" runat="server">
                                                    <asp:Button ID="btnCreateProductClass" CssClass="create_dtl" OnClick="btnEdit_Click"
                                                        Text="" runat="server" />
                                                    <asp:Button ID="btnAdvanceFind" CssClass="advance_find" OnClientClick="advanceFind();"
                                                        Text="" runat="server" />
                                                    <asp:Button ID="btnExcel" CssClass="export" Text="" OnClick="btnExcel_Click" runat="server" />
                                                    <asp:Button ID="btnImport" CssClass="import" OnClientClick="return importData();"
                                                        runat="server" Text="" />
                                                    <asp:Button ID="btnDeleteAll" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAll_Click"
                                                        OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected ProductClass"
                                                        CausesValidation="False" />
                                                </td>
                                            </tr>
                                            <tr id="trImportData" style="display: none;">
                                                <td style="padding: 10px; border: 1px solid #2966a5;">
                                                    <asp:FileUpload ID="fleImport" Style="float: left;" runat="server" />
                                                    <asp:Button ID="btnUploadData" CssClass="btn btn-primary" runat="server" OnClick="btnUpload_Click"
                                                        Text="Import" />
                                                </td>
                                            </tr>
                                            <tr id="trFilter" runat="server">
                                                <td align="center" valign="top">
                                                    <table class="customwd" style="float: left; width: 100%; border-right: gray 1px solid;
                                                        border-top: gray 1px solid; border-left: gray 1px solid; border-bottom: gray 1px solid;
                                                        border-collapse: collapse;">
                                                        <tr>
                                                            <th align="left" colspan="2">
                                                                Filter Records
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 350px;">
                                                                <asp:DropDownList ID="ddlFilters" Width="300px" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:DropDownCheckBoxes ID="ddlGridColumns1" runat="server" OnSelectedIndexChanged="checkBoxes_SelcetedIndexChanged"
                                                                    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                                                                    <Style SelectBoxWidth="300" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="120" />
                                                                    <Texts SelectBoxCaption="Columns" />
                                                                </asp:DropDownCheckBoxes>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="trGridView" runat="server">
                                                <td align="center" valign="top">
                                                    <div style="overflow: auto; width: 100%; float: left;" class="customwd">
                                                        <asp:GridView ID="dataGridProductClass" runat="server" AutoGenerateColumns="true"
                                                            Width="100%" DataKeyField="ProductClassID" DataKeyNames="ClassID" AllowPaging="true"
                                                            PageSize="10" OnRowDataBound="dataGridProductClass_RowDataBound" AllowSorting="true"
                                                            OnSorting="dataGridProductClass_Sorting" OnPageIndexChanging="dataGridProductClass_PageIndexChanging"
                                                            EmptyDataText="No Record Found" OnDataBound="dataGridProductClass_DataBound"
                                                            OnRowCreated="dataGridProductClass_RowCreated">
                                                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trButtons" runat="server">
                                                <td align="left" valign="top">
                                                    <table id="tblDeleteAll" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                            </td>
                                                            <td class="NormalText">
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnEdit" runat="server" Style="display: none;" CssClass="button"
                                                                    Text="Edit ProductClass" OnClick="btnEdit_Click" ToolTip="Edit Selected ProductClass"
                                                                    CausesValidation="False" />
                                                                <%-- <asp:Button ID="btnGridColumn" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnGridColumn_Click" CausesValidation="False" />
                                                                <asp:Button ID="btnGridNotes" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnGridNotes_Click" CausesValidation="False" />
                                                                <asp:Button ID="btnPhoneCall" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnPhoneCall_Click" CausesValidation="False" />--%>
                                                                <asp:Button ID="btnRefreshUpdatePanel" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnrefreshUpdatePanel_Click" CausesValidation="False" />
                                                                <asp:Button ID="btnRefreshFilterList" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnRefreshFilterList_Click" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="dvPaging" runat="server" style="float: right; margin-right: 5%;">
                <asp:Button ID="imgPageFirst" runat="server" Text="|<< First" CommandArgument="First"
                    CommandName="Page" OnCommand="imgPageFirst_Command" CssClass="pagingbtn" />
                <asp:Button ID="imgPagePrevious" runat="server" Text="< Prev" CommandArgument="Prev"
                    CommandName="Page" OnCommand="imgPagePrevious_Command" CssClass="pagingbtn" />
                <asp:DropDownList ID="ddCurrentPage" runat="server" Style="font-size: 11px;" CssClass="Normal"
                    AutoPostBack="True" OnSelectedIndexChanged="ddCurrentPage_SelectedIndexChanged">
                </asp:DropDownList>
                <%--<asp:Label ID="lblOf" runat="server" Text="of" CssClass="Normal"></asp:Label>--%>
                <asp:Label ID="lblTotalPage" runat="server" CssClass="Normal"></asp:Label>
                <asp:Button ID="imgPageNext" runat="server" Text="Next >" CommandArgument="Next"
                    CommandName="Page" OnCommand="imgPageNext_Command" CssClass="pagingbtn"></asp:Button>
                <asp:Button ID="imgPageLast" runat="server" Text="Last >>|" CommandArgument="Last"
                    CommandName="Page" OnCommand="imgPageLast_Command" CssClass="pagingbtn"></asp:Button>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExcel" />
            <asp:PostBackTrigger ControlID="btnImport" />
            <asp:PostBackTrigger ControlID="btnEdit" />
            <%--<asp:PostBackTrigger ControlID="btnGridColumn" />
            <asp:PostBackTrigger ControlID="btnGridNotes" />
            <asp:PostBackTrigger ControlID="btnPhoneCall" />--%>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
