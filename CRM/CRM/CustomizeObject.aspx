﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CustomizeObject.aspx.cs"
    Inherits="CRM.CustomizeObject" EnableEventValidation="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="css/ModalWindow.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link href="css/multiview.css" rel="stylesheet" type="text/css" />
    <link href="css/Mystyle.css" rel="stylesheet" type="text/css" />
    <link href="css/css.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="Script/MasterJS.js"></script>
    <script language="javascript" type="text/javascript" src="Script/GridView.js"></script>
    <script src="js/jquery-1.5.1.js" type="text/javascript"></script>
    <script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
    <style type="text/css">
        #divPlaceHolderHeader span
        {
            border: dotted 1px black !important;
        }
        
        #divPlaceHolder span
        {
            border: dotted 1px black !important;
            width: 97% !important;
        }
    </style>
    <script type="text/javascript">

        function removeConfirm() {
            if (confirm("Are you sure to delete this section?")) {
                return true;
            }
            else {
                return false;
            }
        }

        function customizeObject() {
            var customizeObjectFindBox = "#div_CustomizeObject-box";
            //Fade in the Popup
            $(customizeObjectFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(customizeObjectFindBox).height() + 24) / 2;
            var popMargLeft = ($(customizeObjectFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function newPlaceholderBox() {
            var newPlaceholderFindBox = "#div_NewPlaceholder-box";
            //Fade in the Popup
            $(newPlaceholderFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(newPlaceholderFindBox).height() + 24) / 2;
            var popMargLeft = ($(newPlaceholderFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="maskPlaceholder"></div>');
            $('#maskPlaceholder').fadeIn(300);

            return true;
        };

        function closePlaceholderWindow() {
            document.getElementById("txtCurrentFormID").value = "0";
            $('#maskPlaceholder , .small-popup').fadeOut(300, function () {
                $('#maskPlaceholder').remove();
            });
            document.getElementById("btnrefreshCustomize").click();
            return false;
        }

        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            document.getElementById("txtCurrentFormID").value = "0";
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
            });
            document.getElementById("btnrefreshCustomize").click();
            return false;
        }</script>
</head>
<body>
    <script type="text/javascript">
        $(document).ready(function () {
            var $window = $(window).on('resize', function () {
                var wh = $(window).height();
                $("#divContainer").css('height', wh - ((wh * 41) / 100));
            }).trigger('resize');
            moveControls(); moveControlsHeader();
        });

        function allowDrop(ev) {
            ev.preventDefault();
        }

        function drag(ev) {
            ev.dataTransfer.setData("Text", ev.target.id);
        }

        function drop(ev) {
            var controlID = ev.dataTransfer.getData("Text");
            var control = $("#" + controlID);
            var hdncontrol = $("#" + controlID.replace("txt", "hdn"));
            hdncontrol.val(ev.target.getAttribute("pid"));
            //ev.target.appendChild(control[0]);

            var el = ev.target;
            if (el.id != 'divPlaceHolder') {
                el = el.parentNode;
            }
            el.appendChild(control[0]);
        }

        function dropheader(ev) {
            var controlID = ev.dataTransfer.getData("Text");
            var control = $("#" + controlID);
            var hdncontrol = $("#" + controlID.replace("txt", "hdn"));
            hdncontrol.val(ev.target.getAttribute("pid"));
            //ev.target.appendChild(control[0]);

            var el = ev.target;
            if (el.id != 'divPlaceHolderHeader') {
                el = el.parentNode;
            }
            el.appendChild(control[0]);
        }

        function dropfields(ev) {
            var controlID = ev.dataTransfer.getData("Text");
            var control = $("#" + controlID);
            var hdncontrol = $("#" + controlID.replace("txt", "hdn"));
            hdncontrol.val(ev.target.getAttribute("pid"));
            //ev.target.appendChild(control[0]);

            var el = ev.target;
            if (el.id != 'divFields') {
                el = el.parentNode;
            }
            el.appendChild(control[0]);
        }

        function moveControls() {
            var controls = $("[id*=txtField]");
            for (var i = 0; i < controls.length; i++) {
                var div = $("div[id*=divPlaceHolder][pid=" + controls[i].getAttribute("pid") + "]");
                if (div != undefined && div != null && div.length > 0) {
                    div[0].appendChild(controls[i]);
                }
            }
        }

        function moveControlsHeader() {
            var controls = $("[id*=txtField]");
            for (var i = 0; i < controls.length; i++) {
                var div = $("div[id*=divPlaceHolderHeader][pid=" + controls[i].getAttribute("pid") + "]");
                if (div != undefined && div != null && div.length > 0) {
                    div[0].appendChild(controls[i]);
                }
            }
        }

        function editFormField(formID) {
            document.getElementById("txtCurrentFormID").value = formID;
            $("#btnEditField").click();
        }
    </script>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Button ID="btnrefreshCustomize" runat="server" Style="display: none;" OnClick="btnRefreshCustomize_Click" />
    <div id="div_CustomizeObject-box" class="message-popup" style="overflow: hidden;
        height: 100% !important;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div1" style="display: block; width: 100%; height: 97% !important;">
            <div id="div4" style="overflow: auto; width: 100%; height: 100% !important;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table width="100%">
                            <tr>
                                <td>
                                    Display Name<span class="Star">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDisplayName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ValidationGroup="NewField"
                                        ControlToValidate="txtDisplayName" CssClass="Star" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Column Name<span class="Star">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtColumnName" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ValidationGroup="NewField"
                                        ControlToValidate="txtColumnName" CssClass="Star" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Is Required
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="rdbIsRequired" runat="server">
                                        <asp:ListItem Text="True" Value="True"></asp:ListItem>
                                        <asp:ListItem Text="False" Value="False" Selected="True"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Type
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlFieldType" Width="150px" runat="server" OnSelectedIndexChanged="ddlFieldType_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trFormat" runat="server">
                                <td>
                                    Format
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlFieldFormat" Width="150px" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="trMaxLength" runat="server">
                                <td>
                                    Maximum Length<span class="Star">*</span>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtMaxLength" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator4" ValidationGroup="NewField"
                                        ControlToValidate="txtMaxLength" CssClass="Star" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr id="trRadioChoices" runat="server" visible="false">
                                <td>
                                    Choices
                                </td>
                                <td>
                                    <asp:TextBox TextMode="MultiLine" runat="server" ID="txtChoices"></asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" ValidationGroup="NewField"
                                        ControlToValidate="txtChoices" CssClass="Star" Display="Dynamic" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                                <td>
                                    <asp:Button ID="btnSubmit" CssClass="button" Style="width: 100px !important; border: 1px solid black;
                                        color: Black;" OnClick="btnSaveCustomize_Click" ValidationGroup="NewField" runat="server"
                                        Text="Save" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div id="div_NewPlaceholder-box" class="small-popup" style="overflow: hidden;">
        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closePlaceholderWindow()"
                        title="Close Window" alt="Close" /></a>
                <div id="Div3" style="display: block;">
                    <div id="div5" style="overflow: auto; width: 100%; height: 100% !important;">
                        Placeholder Name :<span class="Star">*</span>
                        <asp:TextBox ID="txtPlaceHolderName" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ID="rq1" ValidationGroup="NewPlaceholder"
                            ControlToValidate="txtPlaceHolderName" Display="Dynamic" ErrorMessage="*" CssClass="Star"></asp:RequiredFieldValidator>
                        <asp:Button ID="btnAddPlaceholder" runat="server" Text="Add Placeholder" ValidationGroup="NewPlaceholder"
                            OnClick="btnAddPlaceholder_Click" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Button ID="btnNewPlaceholder" runat="server" CssClass="newSection" Text="" OnClick="btnNewPlaceholder_Click" />
                <asp:Button ID="btnSave" runat="server" CssClass="save" Style="border: 1px solid black;
                    color: Black;" OnClick="btnSave_Click" />
                <asp:Button ID="btnNewField" runat="server" CssClass="newfield" Style="border: 1px solid black;
                    color: Black;" OnClick="btnNewField_Click" Text="" />
                <asp:Button ID="btnEditField" runat="server" CssClass="button" Style="border: 1px solid black;
                    color: Black; display: none;" OnClick="btnEditField_Click" Text="Edit Field" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="div2" style="width: 99%; float: left">
        <%--<asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>--%>
        <asp:DataList runat="server" ID="lstPlaceholderHeader" RepeatLayout="Flow" DataKeyField="ID">
            <ItemTemplate>
                <h1>
                    <%#Eval("PlaceHolderHeader") %></h1>
                <div style="float: right; margin-top: -10px; margin-right: -10px;">
                    <asp:ImageButton ID="img" OnClientClick="removeConfirm()" CommandName='<%#Eval("ID") %>'
                        ToolTip="Remove Placeholder" ImageUrl="images/close_pop.png" runat="server" OnClick="img_Click" />
                </div>
                <div id="divPlaceHolderHeader" pid='<%#Eval("ID") %>' style="border: dotted 1px black;
                    min-height: 70px;" ondrop="dropheader(event)" ondragover="allowDrop(event)">
                </div>
            </ItemTemplate>
        </asp:DataList>
        <%--</ContentTemplate> </asp:UpdatePanel>--%>
    </div>
    <div style="clear: both;">
    </div>
    <div style="float: left; width: 100%; height: 100%;">
        <div id="divContainer" style="margin-top: 20px; width: 35%; overflow-y: auto; overflow-x: hidden;
            float: left">
            <%--<asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>--%>
            <asp:DataList runat="server" ID="lstPlaceHolder" RepeatLayout="Flow" DataKeyField="ID">
                <ItemTemplate>
                    <h1>
                        <%#Eval("PlaceHolderHeader") %></h1>
                    <div style="float: right; margin-top: -10px;">
                        <asp:ImageButton ID="img" OnClientClick="removeConfirm()" CommandName='<%#Eval("ID") %>'
                            ToolTip="Remove Placeholder" ImageUrl="images/close_pop.png" runat="server" OnClick="img_Click" />
                    </div>
                    <div id="divPlaceHolder" pid='<%#Eval("ID") %>' style="border: dotted 1px black;
                        width: 97%; min-height: 100px;" ondrop="drop(event)" ondragover="allowDrop(event)">
                    </div>
                </ItemTemplate>
            </asp:DataList>
            <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <div style="margin-top: 20px; float: left; width: 28%;">
            <div style="font-weight: bold; width: 100px;">
                Social Pane</div>
            <div id="div6" style="float: left; width: 100%;">
                <asp:TextBox ID="txtSocialPane" runat="server" Enabled="false" Height="300px" Style="width: 96%;
                    background-color: White;"></asp:TextBox>
            </div>
        </div>
        <div style="float: left; margin-top: 20px; width: 35%; height: 300px; vertical-align: top;">
            <b>Fields</b>
            <div id="divFields" style="width: 100%; height: 350px; overflow-y: scroll; overflow-x: hidden;
                float: left; border: dotted 1px black; vertical-align: top; min-height: 100px;"
                ondrop="dropfields(event)" ondragover="allowDrop(event)">
                <%--<asp:UpdatePanel ID="UpdatePanel5" runat="server">
                    <ContentTemplate>--%>
                <asp:DataList runat="server" ID="lstFields" RepeatLayout="Table" DataKeyField="ID"
                    Style="height: 300px; vertical-align: top;">
                    <ItemTemplate>
                        <asp:Label ID='txtField' Text='<%#Eval("ColumnName") %>' ReadOnly="true" runat="server"
                            draggable="true" pid='<%#Eval("PlaceHolder") %>' Width="260px" Height="30px"
                            BorderWidth="1px" onclick=<%# "editFormField('" + Eval("ID") +"')"%> ondragstart="drag(event)"
                            Style="margin: 5px; border: dotted 1px black;"></asp:Label>
                        <asp:HiddenField runat="server" ID="hdnField" Value='<%#Eval("PlaceHolder") %>' />
                        <%--<asp:Label ID="hdnField" runat="server" Style="display: none;" Text='<%#Eval("PlaceHolder") %>'></asp:Label>--%>
                    </ItemTemplate>
                </asp:DataList>
                <asp:TextBox ID="txtCurrentFormID" Style="display: none;" Text="0" runat="server"></asp:TextBox>
                <%--</ContentTemplate>
                </asp:UpdatePanel>--%>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
