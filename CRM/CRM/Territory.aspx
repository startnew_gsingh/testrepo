﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Territory.aspx.cs" Inherits="CRM.Territory" %>

<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <style>
        .message-popup
        {
            width: 300px;
            max-height: 500px;
            min-height: 100px;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: auto;
        }
    </style>
    <script type="text/javascript">
        function createTerritory() {
            var createTerritoryBox = "#div_CreateTerritory-box";
            //Fade in the Popup
            $(createTerritoryBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createTerritoryBox).height() + 24) / 2;
            var popMargLeft = ($(createTerritoryBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };


        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                //window.location = "Territory.aspx";
                document.getElementById("ContentPlaceHolder1_btnTerritory").click();
            });
            return false;
        }
    </script>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="div_CreateTerritory-box" class="message-popup" style="overflow: hidden;">
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="showpage" style="display: block;">
                    <div id="divIframe" runat="server" style="overflow: auto; width: 100%; height: 100% !important;">
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblName" Text="Territory" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtTerritoryName" runat="server"></asp:TextBox>
                                        <asp:Label ID="lblTerritoryID" Visible="false" Text="0" runat="server"></asp:Label>
                                        <asp:RequiredFieldValidator ID="rqName" runat="server" ControlToValidate="txtTerritoryName"
                                            ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Territorys"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblParentTerritory" Text="Parent Territory" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlAllTerritorys" Width="155px" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSave" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSave_Click"
                                            ValidationGroup="Territorys" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gradientbginner" style="width: 90%; overflow: auto;">
                <table width="100%">
                    <tr>
                        <td width="18px">
                        </td>
                        <td>
                            <div class="maincontent" style="width: 100%;">
                                <div class="searchboxcaption">
                                    <span>Territorys</span></div>
                                <div class="searchbox">
                                    <div>
                                        <table width="100%">
                                            <tr>
                                                <div style="text-align: center;">
                                                    <asp:Label ID="lblListmsg" Style="text-align: center; color: Red; font-size: 13px;"
                                                        runat="server" EnableViewState="False"></asp:Label>
                                                </div>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" style="border-right: gray 1px solid; border-top: gray 1px solid;
                                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                                        <tr>
                                                            <th align="left" colspan="2">
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Button ID="btnAddTerritory" CssClass="create_dtl" OnClick="btnCreate_Click"
                                                                    runat="server" Text="" />
                                                                <asp:Button ID="btnDeleteAll" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAll_Click"
                                                                    OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Territory"
                                                                    CausesValidation="False" />
                                                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit Territory" OnClick="btnEdit_Click"
                                                                    ToolTip="Edit Selected Territory" CausesValidation="False" Style="display: none;" />
                                                                <asp:Button ID="btnTerritory" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnTerritory_Click" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="max-height: 230px;">
                                                    <div style="overflow: auto; max-height: 230px; width: 100%; float: left;" class="customwd">
                                                        <asp:GridView ID="dataGridTerritory" runat="server" AutoGenerateColumns="false" Width="100%"
                                                            DataKeyField="ID" DataKeyNames="ID" AllowPaging="true" PageSize="10" AllowSorting="true"
                                                            OnSorting="dataGridTerritory_Sorting" OnPageIndexChanging="dataGridTerritory_PageIndexChanging"
                                                            EmptyDataText="No Record Found" OnRowDataBound="dataGridTerritory_RowDataBound"
                                                            OnDataBound="dataGridTerritory_DataBound" OnRowCreated="dataGridTerritory_RowCreated">
                                                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        Territory
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="lnkTerritory" OnClick="div_editTerritory" CommandArgument='<%#Eval("ID") %>'
                                                                            runat="server" Text='<%#Eval("Territory") %>'></asp:LinkButton>
                                                                        <%--<asp:Label ID="lblTerritory" runat="server" Text='<%#Eval("Territory") %>'></asp:Label>--%>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        Parent Territory
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblParentTerritory" runat="server" Text='<%#Eval("ParentTerritory") %>'></asp:Label>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <asp:Button ID="imgPageFirst" runat="server" Text="First" CommandArgument="First"
                                                                    CommandName="Page" OnCommand="imgPageFirst_Command" />
                                                                <asp:Button ID="imgPagePrevious" runat="server" Text="Prev" CommandArgument="Prev"
                                                                    CommandName="Page" OnCommand="imgPagePrevious_Command" />
                                                                <asp:DropDownList ID="ddCurrentPage" runat="server" CssClass="Normal" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddCurrentPage_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblOf" runat="server" Text="of" CssClass="Normal"></asp:Label>
                                                                <asp:Label ID="lblTotalPage" runat="server" CssClass="Normal"></asp:Label>
                                                                <asp:Button ID="imgPageNext" runat="server" Text="Next" CommandArgument="Next" CommandName="Page"
                                                                    OnCommand="imgPageNext_Command"></asp:Button>
                                                                <asp:Button ID="imgPageLast" runat="server" Text="Last" CommandArgument="Last" CommandName="Page"
                                                                    OnCommand="imgPageLast_Command"></asp:Button>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table id="tblDeleteAll" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                            </td>
                                                            <td class="NormalText">
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
