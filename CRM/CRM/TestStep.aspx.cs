﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;

namespace CRM
{
    public partial class TestStep : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Create Header Panel
            Panel panelHead = new Panel();
            panelHead.ID = "pH";
            panelHead.CssClass = "cpHeader";
            // Add Label inside header panel to display text
            Label lblHead = new Label();
            lblHead.ID = "lblHeader";
            panelHead.Controls.Add(lblHead);

            //Create Body Panel
            Panel panelBody = new Panel();
            panelBody.ID = "pB";
            panelBody.CssClass = "cpBody";
            // Add Label inside body Panel to display text
            Label lblB = new Label();
            lblB.ID = "lblBody";
            lblB.Text = "This panel was added dynamically";
            panelBody.Controls.Add(lblB);

            // Create CollapsiblePanelExtender
            CollapsiblePanelExtender cpe = new CollapsiblePanelExtender();
            cpe.TargetControlID = panelBody.ID;
            cpe.ExpandControlID = panelHead.ID;
            cpe.CollapseControlID = panelHead.ID;
            cpe.ScrollContents = false;
            cpe.Collapsed = true;
            cpe.ExpandDirection = CollapsiblePanelExpandDirection.Vertical;
            cpe.SuppressPostBack = true;
            cpe.TextLabelID = lblHead.ID;
            cpe.CollapsedText = "Click to Show Content..";
            cpe.ExpandedText = "Click to Hide Content..";

            pnldynamic1.Controls.Add(panelHead);
            //this.UpdatePanel1.ContentTemplateContainer.Controls.Add(panelHead);
            pnldynamic1.Controls.Add(panelBody);
            pnldynamic1.Controls.Add(cpe);
        }
    }
}