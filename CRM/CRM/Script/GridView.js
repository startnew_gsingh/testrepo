﻿// JScript File
   
    function SelectAllCheckboxes(spanChk)
    {
        // Added as ASPX uses SPAN for checkbox
        var oItem = spanChk.children;
        var theBox= (spanChk.type=="checkbox") ? spanChk : spanChk.children.item[0];
        xState=theBox.checked;
        elm=theBox.form.elements;            
        for(i=0;i<elm.length;i++)
        {       
            if(elm[i].type=="checkbox" && elm[i].id!=theBox.id)
            {                      
                if(elm[i].checked!=xState)    
                    elm[i].click();                            
            }
        }           
    }
  
    var gridViewCtlId = "";
    var gridViewCtl = null;
    var curSelRow = null;
    var tempRowColor;
    
    function setGridName(s)
    {
        gridViewCtlId = s;
    }
    
    
    function getGridViewControl()
    {
        if (null == gridViewCtl)
        {
            gridViewCtl = document.getElementById(gridViewCtlId);
        }
    }
    
    function onGridViewRowSelected(rowIdx)
    {        
        var selRow = getSelectedRow(rowIdx);
        tempRowColor=selRow.style.backgroundColor;        
        if (curSelRow != null)
        {      
            var x= curSelRow.getElementsByTagName("input");
            for(i=0;i<x.length;i++)
            {
                if(x[i].type=="checkbox")
                {
                    if(x[i].checked==false)      
                    {
                        curSelRow.style.foregroundColor = "White";
                        curSelRow.style.backgroundColor = "white";
                    }                                
                }                   
            }                             
        }        
        if (null != selRow)
        {       
            curSelRow = selRow;
            curSelRow.style.foregroundColor = "White";
            curSelRow.style.backgroundColor = "#F8AE1A";//'#ffff99';
        }
    }
    
    function getSelectedRow(rowIdx)
    {
        getGridViewControl();
        if (null != gridViewCtl)
        {
            return gridViewCtl.rows[rowIdx];
        }
        return null;
    }    
    
    function onGridViewRowSelected1(rowIdx)
    {         
        var selRow = getSelectedRow(rowIdx);              
        var x= curSelRow.getElementsByTagName("input");
        for(i=0;i<x.length;i++)
        {
            if(x[i].type=="checkbox")
            {
                if(x[i].checked==false)      
                {
                    curSelRow.style.foregroundColor = "White";
                    curSelRow.style.backgroundColor = "white";
                }
            }                   
        }        
    }
    
    function check(chk,rowinx)
    {   
        curSelRow=getSelectedRow(rowinx);
        if(chk.checked)
        {
            curSelRow.style.foregroundColor = "White";
            curSelRow.style.backgroundColor = "#F8AE1A";
        }
        else
        {
            curSelRow.style.foregroundColor = "White";
            curSelRow.style.backgroundColor = "white";
        }   
   }      
   
   
   
   ////this function will check wheather any checkbox in the grid is selected or not
function CheckboxSelection()
		{
			var LIntCtr;
			var LIntSelectedCheckBoxes=0;
				
			for (LIntCtr=0; LIntCtr < document.forms[0].elements.length; LIntCtr++) 
			{
				if ((document.forms[0].elements[LIntCtr].type == 'checkbox') && (document.forms[0].elements[LIntCtr].name.indexOf('chkBoxRowID') > -1)) 
				{
					if(document.forms[0].elements[LIntCtr].checked == true)
					{
						LIntSelectedCheckBoxes = parseInt(LIntSelectedCheckBoxes) + 1;
					}
				}
			}
			if(parseInt(LIntSelectedCheckBoxes)==0)
			{
				alert('Record(s) Must Be Selected For operation !');
				return false;
			}
			else
			{
				return window.confirm('Do You Really Want To perform this operation on the  Selected Record(s) !');
			}
		}