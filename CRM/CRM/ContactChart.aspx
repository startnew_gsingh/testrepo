﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ContactChart.aspx.cs" Inherits="CRM.ContactChart" %>

<%@ Register Assembly="JQChart.Web" Namespace="JQChart.Web.UI.WebControls" TagPrefix="jqChart" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <link rel="stylesheet" type="text/css" href="css/jquery.jqChart.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.jqRangeSlider.css" />
    <script src="Script/jquery.jqRangeSlider.min.js" type="text/javascript"></script>
    <script src="Script/jquery.jqChart.min.js" type="text/javascript"></script>
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <style>
        .CompletionListCssClass
        {
            font-size: 14px;
            color: #000;
            border: 1px solid #999;
            background: #fff;
            width: 150px;
            float: left;
            z-index: 9999999999 !important;
            position: absolute;
            margin-left: 0px;
        }
        body
        {
            padding: 0;
            margin: 0ee;
        }
        .leftbox
        {
            position: absolute;
            top: 130px;
            left: -520px;
        }
        .feedback
        {
            width: 500px;
            background: #eee;
            float: left;
            padding: 10px;
            border: 1px solid #ccc;
        }
        .link
        {
            float: left;
            padding: 10px;
            background: #eee;
            border: 1px solid #ccc;
            position: relative;
            z-index: 5;
            margin-left: -1px;
        }
        .link a
        {
            color: #333;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('.link').click(function () {
                if ($('.link').attr('class') == 'link open') {
                    $(this).removeClass('open')
                    $('.leftbox').animate({ 'left': '-520' });
                } else {
                    $(this).addClass('open')
                    $('.leftbox').animate({ 'left': '0' });
                }
            });
        });
        function chartButton() {
            if ($('.link').attr('class') == 'link open') {
                $(this).removeClass('open')
                $('.leftbox').animate({ 'left': '-520' });
            } else {
                $(this).addClass('open')
                $('.leftbox').animate({ 'left': '0' });
            }
        }
    </script>
    <div class="leftbox">
        <div class="feedback">
            <div>
                <asp:DropDownList ID="ddlChartType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChartType_SelectedIndexChanged">
                    <asp:ListItem Text="Contacts By Owner" Value="1"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <jqChart:Chart ID="Chart1" Width="500px" Height="300px" runat="server">
                    <Title Text=""></Title>
                    <Animation Enabled="True" Duration="00:00:02" />
                    <Axes>
                        <jqChart:CategoryAxis Location="Bottom" ZoomEnabled="false">
                        </jqChart:CategoryAxis>
                    </Axes>
                    <Series>
                        <jqChart:ColumnSeries XValuesField="Label" YValuesField="Value1" Title="Column">
                        </jqChart:ColumnSeries>
                        <jqChart:LineSeries XValuesField="Label" YValuesField="Value1" Title="Line">
                        </jqChart:LineSeries>
                    </Series>
                </jqChart:Chart>
            </div>
        </div>
        <div class="link">
            <a href="#"><b>Chart</b></a>
        </div>
    </div>
    </form>
</body>
</html>
