﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMDAL;
using CRMBLL;
using System.Web.UI.HtmlControls;
using System.Data;

namespace CRM
{
    public partial class Deals : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            
            if (!Page.IsPostBack)
            {
                bindFiltersDropDown();
                bindDealsGrid();
            }
        }

        public void bindDealsGrid()
        {
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            
            DataSet dsDeals = objCRMDeals.getDealsData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue));
            dataGridDeals.DataSource = dsDeals;
            dataGridDeals.DataBind();
            Session["data"] = dsDeals.Tables[0];
            SetViewState(dsDeals);
        }

        public void bindFiltersDropDown()
        {
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            ddlFilters.DataSource = objCRMDeals.getFilters(GlobalVariables.LoginUser.OrgID, 1);
            ddlFilters.DataTextField = "FilterName";
            ddlFilters.DataValueField = "ID";
            ddlFilters.DataBind();
            ddlFilters.Items.Insert(0, new ListItem("All Deals", "0"));
        }

        protected void ddlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridDeals.DataSource = null;
            dataGridDeals.DataBind();
            bindDealsGrid();
        }

        protected void dataGridDeals_Sorting(object sender, GridViewSortEventArgs e)
        {
            //DataTable dataTable = Session["data"] as DataTable;

            //if (dataTable != null)
            //{
            //    DataView dataView = new DataView(dataTable);
            //    dataView.Sort = e.SortExpression + " " + ConvertSortDirection(e.SortDirection);

            //    dataGridDeals.DataSource = dataView;
            //    dataGridDeals.DataBind();
            //}

            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            GridViewSortExpression = e.SortExpression;

            //Gets the Pageindex of the GridView.
            int iPageIndex = dataGridDeals.PageIndex;
            dataGridDeals.DataSource = SortDataTable(myDataTable, false);
            dataGridDeals.DataBind();
            dataGridDeals.PageIndex = iPageIndex;
        }

        //Gets or Sets the GridView SortDirection Property
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        //Gets or Sets the GridView SortExpression Property
        private string GridViewSortExpression
        {
            get
            {
                return ViewState["SortExpression"] as string ?? string.Empty;
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        //Toggles between the Direction of the Sorting
        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        //Sorts the ResultSet based on the SortExpression and the Selected Column.
        protected DataView SortDataTable(DataTable myDataTable, bool isPageIndexChanging)
        {
            if (myDataTable != null)
            {
                DataView myDataView = new DataView(myDataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GetSortDirection());
                    }
                }
                return myDataView;
            }
            else
            {

                return new DataView();
            }
        }

        protected void dataGridDeals_DataBound(object sender, EventArgs e)
        {
            //Custom Paging
            GridViewRow dataGridDealsRow = dataGridDeals.BottomPagerRow;

            if (dataGridDealsRow == null) return;

            //Get your Controls from the GridView, in this case 
            //I use a DropDown Control for Paging
            DropDownList ddCurrentPage =
        (DropDownList)dataGridDealsRow.Cells[0].FindControl("ddCurrentPage");
            Label lblTotalPage = (Label)dataGridDealsRow.Cells[0].FindControl("lblTotalPage");

            if (ddCurrentPage != null)
            {
                //Populate Pager
                for (int i = 0; i < dataGridDeals.PageCount; i++)
                {
                    int iPageNumber = i + 1;
                    ListItem myListItem = new ListItem(iPageNumber.ToString());

                    if (i == dataGridDeals.PageIndex)
                        myListItem.Selected = true;

                    ddCurrentPage.Items.Add(myListItem);
                }
            }

            // Populate the Page Count
            if (lblTotalPage != null)
                lblTotalPage.Text = dataGridDeals.PageCount.ToString();

        }

        ////private string ConvertSortDirection(SortDirection sortDirection)
        //{
        //    string newSortDirection = String.Empty;

        //    switch (sortDirection)
        //    {
        //        case SortDirection.Ascending:
        //            newSortDirection = "ASC";
        //            break;

        //        case SortDirection.Descending:
        //            newSortDirection = "DESC";
        //            break;
        //    }

        //    return newSortDirection;
        //}

        private DataSet GetViewState()
        {
            //Gets the ViewState
            return (DataSet)ViewState["dealsDataSet"];
        }

        private void SetViewState(DataSet dealsDataSet)
        {
            //Sets the ViewState
            ViewState["dealsDataSet"] = dealsDataSet;
        }

        protected void dataGridDeals_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            dataGridDeals.DataSource = SortDataTable(myDataTable, true);

            dataGridDeals.PageIndex = e.NewPageIndex;
            dataGridDeals.DataBind();
        }

        //Change to a different page when the DropDown Page is changed
        protected void ddCurrentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                GridViewRow dataGridDealsRow = dataGridDeals.BottomPagerRow;
                DropDownList ddCurrentPage =
            (DropDownList)dataGridDealsRow.Cells[0].FindControl("ddCurrentPage");

                dataGridDeals.PageIndex = ddCurrentPage.SelectedIndex;

                //Popultate the GridView Control
                DataSet myDataSet = GetViewState();
                DataTable myDataTable = myDataSet.Tables[0];

                dataGridDeals.DataSource = SortDataTable(myDataTable, true);
                dataGridDeals.DataBind();
            }
        }

        protected void imgPageFirst_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPagePrevious_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageNext_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageLast_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void Paginate(object sender, CommandEventArgs e)
        {
            // Get the Current Page Selected
            int iCurrentIndex = dataGridDeals.PageIndex;

            switch (e.CommandArgument.ToString().ToLower())
            {
                case "first":
                    dataGridDeals.PageIndex = 0;
                    break;
                case "prev":
                    if (dataGridDeals.PageIndex != 0)
                    {
                        dataGridDeals.PageIndex = iCurrentIndex - 1;
                    }
                    break;
                case "next":
                    dataGridDeals.PageIndex = iCurrentIndex + 1;
                    break;
                case "last":
                    dataGridDeals.PageIndex = dataGridDeals.PageCount;
                    break;
            }

            //Populate the GridView Control
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];

            dataGridDeals.DataSource = SortDataTable(myDataTable, true);
            dataGridDeals.DataBind();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            string IDS = "";
            for (int_index = 0; int_index <= dataGridDeals.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridDeals.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(dataGridDeals.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMDeals.deleteLeads(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Deal Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindDealsGrid();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            Int64 IDS = 0;
            for (int_index = 0; int_index <= dataGridDeals.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridDeals.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = (Convert.ToInt64(dataGridDeals.DataKeys[int_index].Value)); return;
            }
            DataSet dsSelectedDeal = objCRMDeals.getSelectedDeal(IDS);
            //Session["LeadIDs"] = IDS;
            //CreateFields(IDS);
            //string str = "<script>createDeal();</script>";
            //Page.ClientScript.RegisterStartupScript(GetType(), "MyKey", "createDeal();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createDeal();", true);
        }

    }
}