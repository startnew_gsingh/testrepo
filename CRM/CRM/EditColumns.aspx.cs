﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;
using System.Web.UI.HtmlControls;

namespace CRM
{
    public partial class EditColumns : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
                bindObjectFields();
        }

        private void bindObjectFields()
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataSet dsObjectCol = _objDeals.getObjectsColumns(1);
            for (int c = 0; c < dsObjectCol.Tables[0].Rows.Count; c++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                li.InnerText = Convert.ToString(dsObjectCol.Tables[0].Rows[c]["ColumnName"]);
                ulFields.Controls.Add(li);
            }
        }

        [System.Web.Services.WebMethod]
        public static int getAllLiteralControls(string liValues)
        {
            DataTable dtFilterColumns = new DataTable();
            DataRow dr = null;
            dtFilterColumns.Columns.Add(new DataColumn("FilterColumn", typeof(string)));
            liValues = liValues.Substring(0, liValues.Length - 1);
            string[] columnNames = liValues.Split(',');


            for (int c = 0; c < columnNames.Count(); c++)
            {
                dr = dtFilterColumns.NewRow();
                string last = columnNames[c];
                dr["FilterColumn"] = last;
                dtFilterColumns.Rows.Add(dr);
            }
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            _objDeals.insertFilterColumn(dtFilterColumns, 1);
            return 1;
        }
    }
}