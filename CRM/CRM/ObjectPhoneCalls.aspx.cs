﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;
using System.IO;

namespace CRM
{
    public partial class ObjectPhoneCalls : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Int64 sObjectUserID = Convert.ToInt64(Request.QueryString["sObjectUserID"]);
            //Session["sObjectUserID"] = sObjectUserID;
            bindPhoneCallsList();
        }

        protected void bindPhoneCallsList()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDocumets = objDeals.getPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["sObjectUserID"]));
            gvPhoneCalls.DataSource = dsDocumets;
            gvPhoneCalls.DataBind();
        }

        //protected void btnUploadPhoneCalls_Click(object sender, EventArgs e)
        //{
        //    uploadphoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["sObjectUserID"]));
        //    bindPhoneCallsList();
        //}

        //private void uploadphoneCalls(Int64 orgID, Int64 DealID)
        //{
        //    try
        //    {
        //        CRMBLLDeals objBLLDeals = new CRMBLLDeals();
        //        objBLLDeals.insertPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), DealID, txtPhoneCallDescription.Text, Convert.ToDateTime(txtCallDate.Text), ddlDirection.SelectedValue, Convert.ToInt64(Session["sObjectUserID"]), "", false);
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //}
    }
}