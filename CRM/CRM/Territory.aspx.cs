﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;

namespace CRM
{
    public partial class Territory : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            if (!Page.IsPostBack)
            {
                //bindFiltersDropDown();
                bindTerritoryGrid();
            }
        }

        protected void btnTerritory_Click(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            bindTerritoryGrid();
        }

        public void bindTerritoryGrid()
        {
            CRMBLLUsers objCRMUsers = new CRMBLLUsers();

            DataSet dsTerritory = objCRMUsers.getTerritoryData();
            if (dsTerritory != null && dsTerritory.Tables[0].Rows.Count > 0)
            {
                dataGridTerritory.DataSource = dsTerritory;
                dataGridTerritory.DataBind();
                Session["data"] = dsTerritory.Tables[0];
                SetViewState(dsTerritory);
            }
            else
            {
                dataGridTerritory.DataSource = null;
                dataGridTerritory.DataBind();
                Session["data"] = null;
            }
        }

        protected void dataGridTerritory_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            GridViewSortExpression = e.SortExpression;

            //Gets the Pageindex of the GridView.
            int iPageIndex = dataGridTerritory.PageIndex;
            dataGridTerritory.DataSource = SortDataTable(myDataTable, false);
            dataGridTerritory.DataBind();
            dataGridTerritory.PageIndex = iPageIndex;
        }

        //Gets or Sets the GridView SortDirection Property
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        //Gets or Sets the GridView SortExpression Property
        private string GridViewSortExpression
        {
            get
            {
                return ViewState["SortExpression"] as string ?? string.Empty;
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        //Toggles between the Direction of the Sorting
        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        //Sorts the ResultSet based on the SortExpression and the Selected Column.
        protected DataView SortDataTable(DataTable myDataTable, bool isPageIndexChanging)
        {
            if (myDataTable != null)
            {
                DataView myDataView = new DataView(myDataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GetSortDirection());
                    }
                }
                return myDataView;
            }
            else
            {

                return new DataView();
            }
        }

        protected void dataGridTerritory_DataBound(object sender, EventArgs e)
        {
            //Custom Paging
            GridViewRow dataGridTerritoryRow = dataGridTerritory.BottomPagerRow;

            if (dataGridTerritoryRow == null) return;

            //Get your Controls from the GridView, in this case 
            //I use a DropDown Control for Paging
            DropDownList ddCurrentPage =
        (DropDownList)dataGridTerritoryRow.Cells[0].FindControl("ddCurrentPage");
            Label lblTotalPage = (Label)dataGridTerritoryRow.Cells[0].FindControl("lblTotalPage");

            if (ddCurrentPage != null)
            {
                //Populate Pager
                for (int i = 0; i < dataGridTerritory.PageCount; i++)
                {
                    int iPageNumber = i + 1;
                    ListItem myListItem = new ListItem(iPageNumber.ToString());

                    if (i == dataGridTerritory.PageIndex)
                        myListItem.Selected = true;

                    ddCurrentPage.Items.Add(myListItem);
                }
            }

            // Populate the Page Count
            if (lblTotalPage != null)
                lblTotalPage.Text = dataGridTerritory.PageCount.ToString();

        }

        private DataSet GetViewState()
        {
            //Gets the ViewState
            return (DataSet)ViewState["TerritoryDataSet"];
        }

        private void SetViewState(DataSet TerritoryDataSet)
        {
            //Sets the ViewState
            ViewState["TerritoryDataSet"] = TerritoryDataSet;
        }

        protected void dataGridTerritory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            dataGridTerritory.DataSource = SortDataTable(myDataTable, true);

            dataGridTerritory.PageIndex = e.NewPageIndex;
            dataGridTerritory.DataBind();
        }

        //Change to a different page when the DropDown Page is changed
        protected void ddCurrentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                GridViewRow dataGridTerritoryRow = dataGridTerritory.BottomPagerRow;
                DropDownList ddCurrentPage =
            (DropDownList)dataGridTerritoryRow.Cells[0].FindControl("ddCurrentPage");

                dataGridTerritory.PageIndex = ddCurrentPage.SelectedIndex;

                //Popultate the GridView Control
                DataSet myDataSet = GetViewState();
                DataTable myDataTable = myDataSet.Tables[0];

                dataGridTerritory.DataSource = SortDataTable(myDataTable, true);
                dataGridTerritory.DataBind();
            }
        }

        protected void imgPageFirst_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPagePrevious_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageNext_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageLast_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void Paginate(object sender, CommandEventArgs e)
        {
            // Get the Current Page Selected
            int iCurrentIndex = dataGridTerritory.PageIndex;

            switch (e.CommandArgument.ToString().ToLower())
            {
                case "first":
                    dataGridTerritory.PageIndex = 0;
                    break;
                case "prev":
                    if (dataGridTerritory.PageIndex != 0)
                    {
                        dataGridTerritory.PageIndex = iCurrentIndex - 1;
                    }
                    break;
                case "next":
                    dataGridTerritory.PageIndex = iCurrentIndex + 1;
                    break;
                case "last":
                    dataGridTerritory.PageIndex = dataGridTerritory.PageCount;
                    break;
            }

            //Populate the GridView Control
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];

            dataGridTerritory.DataSource = SortDataTable(myDataTable, true);
            dataGridTerritory.DataBind();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLUsers objCRMTerritory = new CRMBLLUsers();
            string IDS = "";
            for (int_index = 0; int_index <= dataGridTerritory.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridTerritory.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(dataGridTerritory.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMTerritory.deleteTerritory(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Territory Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindTerritoryGrid();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            CRMBLLUsers objCRMusers = new CRMBLLUsers();

            DataSet dsSelectedTerritory = objCRMusers.getTerritoryData();
            lblTerritoryID.Text = "0";
            lblTerritoryID.Visible = false;
            txtTerritoryName.Text = "";
            btnSave.Enabled = true;
            ddlAllTerritorys.DataSource = dsSelectedTerritory;
            ddlAllTerritorys.DataTextField = "Territory";
            ddlAllTerritorys.DataValueField = "Territory";
            ddlAllTerritorys.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createTerritory();", true);
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int int_index;
            CRMBLLUsers objCRMusers = new CRMBLLUsers();
            Int64 IDS = 0;
            for (int_index = 0; int_index <= dataGridTerritory.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridTerritory.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = (Convert.ToInt64(dataGridTerritory.DataKeys[int_index].Value));
            }
            DataSet dsSelectedTerritory = objCRMusers.getTerritoryData(IDS);

            if (Convert.ToBoolean(dsSelectedTerritory.Tables[0].Rows[0]["IsCompanyTerritory"]) == true)
            {
                btnSave.Enabled = false;
            }

            lblTerritoryID.Text = dsSelectedTerritory.Tables[0].Rows[0]["ID"].ToString();
            lblTerritoryID.Visible = false;
            txtTerritoryName.Text = dsSelectedTerritory.Tables[0].Rows[0]["Territory"].ToString();

            ddlAllTerritorys.DataSource = objCRMusers.getTerritoryData().Tables[0];
            ddlAllTerritorys.DataTextField = "Territory";
            ddlAllTerritorys.DataValueField = "Territory";
            ddlAllTerritorys.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createTerritory();", true);
        }

        //protected void dataGridTerritory_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        e.Row.Cells[1].Visible = false; // hides the first column
        //    }
        //    catch
        //    {
        //    }
        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {
            CRMBLLUsers _objUsers = new CRMBLLUsers();
            _objUsers.insertTerritory(txtTerritoryName.Text, ddlAllTerritorys.SelectedValue, Convert.ToInt64(lblTerritoryID.Text));
            lblTerritoryID.Text = "0";
            bindTerritoryGrid();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }

        protected void div_editTerritory(object sender, EventArgs e)
        {
            CRMBLLUsers objCRMusers = new CRMBLLUsers();
            Int64 IDS = Convert.ToInt64(((System.Web.UI.WebControls.LinkButton)(sender)).CommandArgument);

            DataSet dsSelectedTerritory = objCRMusers.getTerritoryData(IDS);
            lblTerritoryID.Text = dsSelectedTerritory.Tables[0].Rows[0]["ID"].ToString();
            lblTerritoryID.Visible = false;
            txtTerritoryName.Text = dsSelectedTerritory.Tables[0].Rows[0]["Territory"].ToString();

            if (Convert.ToBoolean(dsSelectedTerritory.Tables[0].Rows[0]["IsCompanyTerritory"]) == true)
                btnSave.Enabled = false;
            else
                btnSave.Enabled = true;

            ddlAllTerritorys.DataSource = objCRMusers.getTerritoryData().Tables[0];
            ddlAllTerritorys.DataTextField = "Territory";
            ddlAllTerritorys.DataValueField = "Territory";
            ddlAllTerritorys.DataBind();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createTerritory();", true);
        }

        protected void dataGridTerritory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // e.Row.Cells[1].Visible = false; // hides the first column
            }
            catch
            {
            }
        }

        protected void dataGridTerritory_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    var firstCell = e.Row.Cells[2];
            //    firstCell.Controls.Clear();
            //    LinkButton lnkEditPermissions = new LinkButton();
            //    lnkEditPermissions.ID = e.Row.Cells[1].Text;
            //    lnkEditPermissions.Click += new EventHandler(this.div_editTerritoryPermissions);
            //    //lnkEditPermissions.CommandArgument = e.Row.Cells[1].Text;
            //    lnkEditPermissions.Attributes.Add("runat", "server");
            //    lnkEditPermissions.Text = firstCell.Text;
            //    firstCell.Controls.Add(lnkEditPermissions);
            //}
        }
    }
}