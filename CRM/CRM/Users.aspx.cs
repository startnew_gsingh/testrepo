﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;
using CRMDAL;

namespace CRM
{
    public partial class Users : System.Web.UI.Page
    {
        static DataTable dtPermissions = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            if (!Page.IsPostBack)
            {
                //bindFiltersDropDown();
                bindUsersGrid();
            }
        }

        protected void btnRefreshUsers_Click(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            bindUsersGrid();
        }

        public void bindUsersGrid()
        {
            CRMBLLUsers objCRMUsers = new CRMBLLUsers();

            DataSet dsUser = objCRMUsers.getUsers();
            if (dsUser != null && dsUser.Tables[0].Rows.Count > 0)
            {
                dataGridUser.DataSource = dsUser;
                dataGridUser.DataBind();
                Session["data"] = dsUser.Tables[0];
                SetViewState(dsUser);
            }
            else
            {
                dataGridUser.DataSource = null;
                dataGridUser.DataBind();
                Session["data"] = null;
            }
        }

        //public void bindFiltersDropDown()
        //{
        //    CRMBLLUser objCRMUser = new CRMBLLUser();
        //    ddlFilters.DataSource = objCRMUser.getFilters(GlobalVariables.LoginUser.OrgID, 2);
        //    ddlFilters.DataTextField = "FilterName";
        //    ddlFilters.DataValueField = "ID";
        //    ddlFilters.DataBind();
        //    ddlFilters.Items.Insert(0, new ListItem("All Users", "0"));
        //}

        //protected void ddlFilters_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    dataGridUser.DataSource = null;
        //    dataGridUser.DataBind();
        //    bindUsersGrid();
        //}

        protected void dataGridUser_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            GridViewSortExpression = e.SortExpression;

            //Gets the Pageindex of the GridView.
            int iPageIndex = dataGridUser.PageIndex;
            dataGridUser.DataSource = SortDataTable(myDataTable, false);
            dataGridUser.DataBind();
            dataGridUser.PageIndex = iPageIndex;
        }

        //Gets or Sets the GridView SortDirection Property
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        //Gets or Sets the GridView SortExpression Property
        private string GridViewSortExpression
        {
            get
            {
                return ViewState["SortExpression"] as string ?? string.Empty;
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        //Toggles between the Direction of the Sorting
        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        //Sorts the ResultSet based on the SortExpression and the Selected Column.
        protected DataView SortDataTable(DataTable myDataTable, bool isPageIndexChanging)
        {
            if (myDataTable != null)
            {
                DataView myDataView = new DataView(myDataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GetSortDirection());
                    }
                }
                return myDataView;
            }
            else
            {

                return new DataView();
            }
        }

        protected void dataGridUser_DataBound(object sender, EventArgs e)
        {
            //Custom Paging
            GridViewRow dataGridUserRow = dataGridUser.BottomPagerRow;

            if (dataGridUserRow == null) return;

            //Get your Controls from the GridView, in this case 
            //I use a DropDown Control for Paging
            DropDownList ddCurrentPage =
        (DropDownList)dataGridUserRow.Cells[0].FindControl("ddCurrentPage");
            Label lblTotalPage = (Label)dataGridUserRow.Cells[0].FindControl("lblTotalPage");

            if (ddCurrentPage != null)
            {
                //Populate Pager
                for (int i = 0; i < dataGridUser.PageCount; i++)
                {
                    int iPageNumber = i + 1;
                    ListItem myListItem = new ListItem(iPageNumber.ToString());

                    if (i == dataGridUser.PageIndex)
                        myListItem.Selected = true;

                    ddCurrentPage.Items.Add(myListItem);
                }
            }

            // Populate the Page Count
            if (lblTotalPage != null)
                lblTotalPage.Text = dataGridUser.PageCount.ToString();

        }

        private DataSet GetViewState()
        {
            //Gets the ViewState
            return (DataSet)ViewState["UserDataSet"];
        }

        private void SetViewState(DataSet UserDataSet)
        {
            //Sets the ViewState
            ViewState["UserDataSet"] = UserDataSet;
        }

        protected void dataGridUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            dataGridUser.DataSource = SortDataTable(myDataTable, true);

            dataGridUser.PageIndex = e.NewPageIndex;
            dataGridUser.DataBind();
        }

        //Change to a different page when the DropDown Page is changed
        protected void ddCurrentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            {
                GridViewRow dataGridUserRow = dataGridUser.BottomPagerRow;
                DropDownList ddCurrentPage =
            (DropDownList)dataGridUserRow.Cells[0].FindControl("ddCurrentPage");

                dataGridUser.PageIndex = ddCurrentPage.SelectedIndex;

                //Popultate the GridView Control
                DataSet myDataSet = GetViewState();
                DataTable myDataTable = myDataSet.Tables[0];

                dataGridUser.DataSource = SortDataTable(myDataTable, true);
                dataGridUser.DataBind();
            }
        }

        protected void imgPageFirst_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPagePrevious_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageNext_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }
        protected void imgPageLast_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void Paginate(object sender, CommandEventArgs e)
        {
            // Get the Current Page Selected
            int iCurrentIndex = dataGridUser.PageIndex;

            switch (e.CommandArgument.ToString().ToLower())
            {
                case "first":
                    dataGridUser.PageIndex = 0;
                    break;
                case "prev":
                    if (dataGridUser.PageIndex != 0)
                    {
                        dataGridUser.PageIndex = iCurrentIndex - 1;
                    }
                    break;
                case "next":
                    dataGridUser.PageIndex = iCurrentIndex + 1;
                    break;
                case "last":
                    dataGridUser.PageIndex = dataGridUser.PageCount;
                    break;
            }

            //Populate the GridView Control
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];

            dataGridUser.DataSource = SortDataTable(myDataTable, true);
            dataGridUser.DataBind();
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLUsers objCRMUser = new CRMBLLUsers();
            string IDS = "";
            for (int_index = 0; int_index <= dataGridUser.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridUser.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(dataGridUser.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMUser.deleteUser(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected User Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindUsersGrid();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            int int_index;
            CRMBLLUsers objCRMusers = new CRMBLLUsers();
            Int64 IDS = 0;

            bindTerritoryDDL();

            for (int_index = 0; int_index <= dataGridUser.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridUser.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = (Convert.ToInt64(dataGridUser.DataKeys[int_index].Value));
            }
            if (IDS != 0)
            {
                DataSet dsSelectedUser = objCRMusers.getUsers(IDS);
                fillControls(dsSelectedUser.Tables[0]);
            }
            else
            {
                lblUserID.Text = "0";
                txtUserName.Text = "";
                txtPassword.Text = "";
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtAddress1.Text = "";
                txtAddress2.Text = "";
                txtPhone.Text = "";
                txtCountry.Text = "";
                txtState.Text = "";
                txtCity.Text = "";
                txtZipCode.Text = "";
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createUser();", true);
        }

        private void bindTerritoryDDL()
        {
            CRMBLLUsers objCRMusers = new CRMBLLUsers();
            ddlTerritory.DataSource = objCRMusers.getTerritoryData().Tables[0];
            ddlTerritory.DataTextField = "Territory";
            ddlTerritory.DataTextField = "Territory";
            ddlTerritory.DataBind();
        }

        private void fillControls(DataTable dsSelectedUser)
        {
            lblUserID.Text = dsSelectedUser.Rows[0]["ID"].ToString();
            txtUserName.Text = dsSelectedUser.Rows[0]["UserName"].ToString();// GlobalVariables.LoginUser.UserName;
            txtPassword.Text = dsSelectedUser.Rows[0]["Password"].ToString();
            txtFirstName.Text = dsSelectedUser.Rows[0]["FirstName"].ToString();
            txtLastName.Text = dsSelectedUser.Rows[0]["LastName"].ToString();
            txtEmail.Text = dsSelectedUser.Rows[0]["Email"].ToString();
            txtAddress1.Text = dsSelectedUser.Rows[0]["Address1"].ToString();
            txtAddress2.Text = dsSelectedUser.Rows[0]["Address2"].ToString();
            txtPhone.Text = dsSelectedUser.Rows[0]["Phone"].ToString();
            txtCountry.Text = dsSelectedUser.Rows[0]["Country"].ToString();
            txtState.Text = dsSelectedUser.Rows[0]["State"].ToString();
            txtCity.Text = dsSelectedUser.Rows[0]["City"].ToString();
            txtZipCode.Text = dsSelectedUser.Rows[0]["ZipCode"].ToString();
            if (dsSelectedUser.Rows[0]["Territory"].ToString() != "")
                ddlTerritory.SelectedValue = dsSelectedUser.Rows[0]["Territory"].ToString();
        }
        //protected void dataGridUser_RowCreated(object sender, GridViewRowEventArgs e)
        //{
        //    try
        //    {
        //        e.Row.Cells[1].Visible = false; // hides the first column
        //    }
        //    catch
        //    {
        //    }
        //}


        protected void btnSave_Click(object sender, EventArgs e)
        {
            StructureUserDAL _objUserDAL = new StructureUserDAL();
            _objUserDAL.ID = Convert.ToInt64(lblUserID.Text);
            _objUserDAL.OrgID = GlobalVariables.LoginUser.OrgID;
            _objUserDAL.UserName = txtUserName.Text;
            _objUserDAL.Password = txtPassword.Text;
            _objUserDAL.FirstName = txtFirstName.Text;
            _objUserDAL.LastName = txtLastName.Text;
            _objUserDAL.Address1 = txtAddress1.Text;
            _objUserDAL.Address2 = txtAddress2.Text;
            _objUserDAL.Phone = txtPhone.Text;
            _objUserDAL.Country = txtCountry.Text;
            _objUserDAL.State = txtState.Text;
            _objUserDAL.City = txtCity.Text;
            _objUserDAL.ZipCode = txtZipCode.Text;
            _objUserDAL.Territory = ddlTerritory.SelectedValue;
            _objUserDAL.Email = txtEmail.Text;


            CRMBLLUsers _objUsers = new CRMBLLUsers();
            DataView dvUsers = _objUsers.getUsers().Tables[0].DefaultView;
            dvUsers.RowFilter = "(Email = '" + txtEmail.Text + "' or UserName = '" + txtUserName.Text + "') and id <> " + lblUserID.Text;

            if (dvUsers.Table.DefaultView.Count == 0)
            {
                _objUsers.insertUser(_objUserDAL);
                lblUserID.Text = "0";
                bindUsersGrid();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeUserWindow();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createUser();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "checkExists();", true);
            }
        }

        protected void div_editUser(object sender, EventArgs e)
        {
            CRMBLLUsers objCRMusers = new CRMBLLUsers();
            Int64 IDS = 0;

            bindTerritoryDDL();

            IDS = Convert.ToInt64(((System.Web.UI.WebControls.LinkButton)(sender)).CommandArgument);
            if (IDS != 0)
            {
                DataSet dsSelectedUser = objCRMusers.getUsers(IDS);
                fillControls(dsSelectedUser.Tables[0]);
            }
            else
            {
                lblUserID.Text = "0";
                txtUserName.Text = "";
                txtPassword.Text = "";
                txtFirstName.Text = "";
                txtLastName.Text = "";
                txtAddress1.Text = "";
                txtAddress2.Text = "";
                txtPhone.Text = "";
                txtCountry.Text = "";
                txtState.Text = "";
                txtCity.Text = "";
                txtZipCode.Text = "";
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createUser();", true);
        }

        protected void btnAssignRoles_Click(object sender, EventArgs e)
        {
            int int_index;
            CRMBLLUsers objCRMusers = new CRMBLLUsers();
            Int64 IDS = Convert.ToInt64(lblUserID.Text);
            //for (int_index = 0; int_index <= dataGridUser.Rows.Count - 1; int_index++)
            //{
            //    Boolean bcheck = ((CheckBox)dataGridUser.Rows[int_index].FindControl("chkBoxRowID")).Checked;
            //    if (bcheck == true) ViewState["SelectedUserID"] = (Convert.ToInt64(dataGridUser.DataKeys[int_index].Value));
            //}
            ViewState["SelectedUserID"] = IDS;
            if (Convert.ToInt64(ViewState["SelectedUserID"]) != 0)
            {
                DataSet dsSelectedUserRoles = objCRMusers.getRolesData(0);
                gvUserRoles.DataSource = dsSelectedUserRoles;
                gvUserRoles.DataBind();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "assignRoles();", true);
        }

        protected void dataGridUser_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                // e.Row.Cells[1].Visible = false; // hides the first column
            }
            catch
            {
            }
        }

        protected void dataGridUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    var firstCell = e.Row.Cells[2];
            //    firstCell.Controls.Clear();
            //    LinkButton lnkEditPermissions = new LinkButton();
            //    lnkEditPermissions.ID = e.Row.Cells[1].Text;
            //    lnkEditPermissions.Click += new EventHandler(this.div_editUserPermissions);
            //    //lnkEditPermissions.CommandArgument = e.Row.Cells[1].Text;
            //    lnkEditPermissions.Attributes.Add("runat", "server");
            //    lnkEditPermissions.Text = firstCell.Text;
            //    firstCell.Controls.Add(lnkEditPermissions);
            //}
        }

        //protected void div_editUserPermissions(object sender, EventArgs e)
        //{
        //    //Int64 UserID=
        //    bindUserPermissions(Convert.ToInt64(((System.Web.UI.WebControls.LinkButton)(sender)).CommandArgument));
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editUserPermissions();", true);
        //}

        protected void div_editRolePermissions(object sender, EventArgs e)
        {
            //Int64 RoleID=
            bindRolePermissions(Convert.ToInt64(((System.Web.UI.WebControls.LinkButton)(sender)).CommandArgument));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "editRolePermissions();", true);
        }

        public void bindRolePermissions(Int64 RoleID)
        {
            dtPermissions = new DataTable();
            DataColumn dcPermissions = new DataColumn();

            dcPermissions.ColumnName = "ID";
            dtPermissions.Columns.Add(dcPermissions);

            dcPermissions = new DataColumn();
            dcPermissions.ColumnName = "ColumnName";
            dtPermissions.Columns.Add(dcPermissions);

            dcPermissions = new DataColumn();
            dcPermissions.ColumnName = "Value";
            dtPermissions.Columns.Add(dcPermissions);

            CRMBLLUsers objCRMUsers = new CRMBLLUsers();

            DataSet dsRole = objCRMUsers.getRolesPermissions(RoleID);
            if (dsRole != null && dsRole.Tables[0].Rows.Count > 0)
            {
                lstRolePermissions.DataSource = dsRole;
                lstRolePermissions.DataBind();
            }
            else
            {
                lstRolePermissions.DataSource = null;
                lstRolePermissions.DataBind();
            }
        }

        protected void img_ClosePopup(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closePermissionsWindow();", true);
        }

        protected void btnAssignUserRoles_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLUsers objCRMUser = new CRMBLLUsers();
            string IDS = "";
            for (int_index = 0; int_index <= gvUserRoles.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvUserRoles.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + Convert.ToInt64(((Label)gvUserRoles.Rows[int_index].FindControl("lblRoleID")).Text) + "|||";
                //if (bcheck == true) IDS = IDS + (Convert.ToInt64(dataGridUser.DataKeys[int_index].Value)) + "|||";
            }
            if (objCRMUser.assignUserRoles("|||" + IDS, GlobalVariables.LoginUser.OrgID) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected User Roles Updated Successfully";
                lblListmsg.Visible = true;
            }
            bindUsersGrid();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeUserWindow();", true);
        }


        protected void gvUserRoles_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var firstCell = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0];
                CRMBLLUsers objCRMusers = new CRMBLLUsers();
                DataSet dsSelectedUserRoles = objCRMusers.getUserRoles(Convert.ToInt64(ViewState["SelectedUserID"]), Convert.ToInt64(firstCell));

                if (dsSelectedUserRoles.Tables[0].Rows.Count > 0)
                {
                    ((CheckBox)e.Row.Cells[0].Controls[1]).Checked = true;
                }
            }
        }
    }
}