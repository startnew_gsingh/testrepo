﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;
using System.IO;
using System.Drawing;
using AjaxControlToolkit;
using System.Data.OleDb;
using System.Data.Common;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace CRM
{
    public partial class ProductClass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            Session["ObjectID"] = 9;//ProductClass
            if (!Page.IsPostBack)
            {
                //bindFiltersDropDown();
                //bindProductClassGrid();
                CRMBLLUsers objCRMBLLUsers = new CRMBLLUsers();
                DataTable dtProductClasss = objCRMBLLUsers.getUserObjectRoles(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"])).Tables[0];
                if (dtProductClasss.Rows.Count > 0 && Convert.ToBoolean(dtProductClasss.Rows[0]["Read"]) == true)
                {
                    bindFiltersDropDown();
                    bindProductClassGrid();
                    BindLocationCode();
                    BindProductFamily();
                    BindUnitofMeasure();
                    getUserObjectRoles(dtProductClasss);
                    bindgridColumnsDDL();
                    lblListmsg.Text = "";
                    trFilter.Visible = true;
                    trGridView.Visible = true;
                    trButtons.Visible = true;
                    trallButtons.Visible = true;
                }
                else
                {
                    lblListmsg.Text = "You are not authorize to view this page.";
                    trFilter.Visible = false;
                    trGridView.Visible = false;
                    trButtons.Visible = false;
                    trallButtons.Visible = false;
                }
            }
            else
            {
                string btnTarget = getPostBackControlID();
                if (btnTarget == null || btnTarget == "btnUpload")//btnTarget == "btnSave" && btnTarget == "btnSaveAndClose" && btnTarget != "dataGridDeals" && btnTarget != "ddlFilters" && btnTarget != "imgPageFirst" && btnTarget != "imgPagePrevious" && btnTarget != "ddCurrentPage" && btnTarget != "imgPageNext" && btnTarget != "imgPageLast")
                {
                    DataSet myDataSet = GetViewState();

                    DataTable myDataTable = myDataSet.Tables[0];
                    dataGridProductClass.DataSource = SortDataTable(myDataTable, true);
                    dataGridProductClass.DataBind();
                }
            }
        }

        private string getPostBackControlID()
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            if (control != null)
                return control.ID;
            else
                return null;
        }

        protected void BindLocationCode()
        {
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();
            DataSet dsLocationCode = objCRMProductClass.getLocationCode();
            ddlLocationCode.DataSource = dsLocationCode;
            ddlLocationCode.DataTextField = "LocationCode";
            ddlLocationCode.DataValueField = "ID";
            ddlLocationCode.DataBind();
        }

        protected void BindUnitofMeasure()
        {
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();
            DataSet dsUnitofMeasure = objCRMProductClass.getUnitofMeasure();
            ddlUnitofMeasure.DataSource = dsUnitofMeasure;
            ddlUnitofMeasure.DataTextField = "UnitofMeasure";
            ddlUnitofMeasure.DataValueField = "ID";
            ddlUnitofMeasure.DataBind();
        }

        protected void BindProductFamily()
        {
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();
            DataSet dsProductFamily = objCRMProductClass.getProductFamily();
            ddlProductFamily.DataSource = dsProductFamily;
            ddlProductFamily.DataTextField = "ProductFamily";
            ddlProductFamily.DataValueField = "ID";
            ddlProductFamily.DataBind();
        }

        protected void btnrefreshUpdatePanel_Click(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();
            DataSet dsProductClass = objCRMProductClass.getProductClassData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), Convert.ToInt32(ViewState["PageIndex"]), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]));
            Session["gridData"] = dsProductClass.Tables[2];
            Session["data"] = dsProductClass.Tables[2];
            SetViewState(dsProductClass);

            DataTable myDataTable = dsProductClass.Tables[0];
            dataGridProductClass.DataSource = SortDataTable(myDataTable, true);
            dataGridProductClass.DataBind();
        }

        protected void btnRefreshFilterList_Click(Object Sender, EventArgs e)
        {
            bindFiltersDropDown();
            DataSet myDataSet = GetViewState();

            DataTable myDataTable = myDataSet.Tables[0];
            dataGridProductClass.DataSource = SortDataTable(myDataTable, true);
            dataGridProductClass.DataBind();
        }

        protected void btnExcel_Click(object sender, EventArgs e)
        {
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ProductClasss.xls"));
            Response.ContentType = "application/ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dataGridProductClass.AllowPaging = false;
            //Change the Header Row back to white color
            dataGridProductClass.HeaderRow.Style.Add("background-color", "#FFFFFF");
            //Applying stlye to gridview header cells
            for (int i = 0; i < dataGridProductClass.HeaderRow.Cells.Count; i++)
            {
                dataGridProductClass.HeaderRow.Cells[i].Style.Add("background-color", "#507CD1");
            }
            int j = 1;
            //This loop is used to apply stlye to cells based on particular row
            foreach (GridViewRow gvrow in dataGridProductClass.Rows)
            {
                gvrow.BackColor = Color.White;
                if (j <= dataGridProductClass.Rows.Count)
                {
                    if (j % 2 != 0)
                    {
                        for (int k = 0; k < gvrow.Cells.Count; k++)
                        {
                            gvrow.Cells[k].Style.Add("background-color", "#EFF3FB");
                        }
                    }
                }
                j++;
            }
            dataGridProductClass.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
            /* Verifies that the control is rendered */
        }

        public void getUserObjectRoles(DataTable dtProductClasss)
        {
            btnCreateProductClass.Visible = true;
            btnEdit.Visible = true;
            btnDeleteAll.Visible = true;
            btnAdvanceFind.Visible = true;
            btnExcel.Visible = true;
            btnImport.Visible = true;

            if (Convert.ToBoolean(dtProductClasss.Rows[0]["Create"]) != true)
                btnCreateProductClass.Visible = false;

            if (Convert.ToBoolean(dtProductClasss.Rows[0]["Edit"]) != true)
                btnEdit.Visible = false;

            if (Convert.ToBoolean(dtProductClasss.Rows[0]["Delete"]) != true)
                btnDeleteAll.Visible = false;

            if (Convert.ToBoolean(dtProductClasss.Rows[0]["AdvanceFind"]) != true)
                btnAdvanceFind.Visible = false;

            if (Convert.ToBoolean(dtProductClasss.Rows[0]["Export"]) != true)
                btnExcel.Visible = false;

            if (Convert.ToBoolean(dtProductClasss.Rows[0]["Import"]) != true)
                btnImport.Visible = false;
        }

        public void bindProductClassGrid()
        {
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();

            DataSet dsProductClass = objCRMProductClass.getProductClassData(0, GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue));
            if (dsProductClass != null && dsProductClass.Tables.Count > 0 && dsProductClass.Tables[0].Rows.Count > 0)
            {
                dataGridProductClass.DataSource = dsProductClass;
                dataGridProductClass.DataBind();
                Session["gridData"] = dsProductClass.Tables[2];
                Session["data"] = dsProductClass.Tables[2];
                SetViewState(dsProductClass);

                ddCurrentPage.Items.Clear();
                ViewState["PageIndex"] = 1;
                Int64 recordCount = Convert.ToInt64(dsProductClass.Tables[1].Rows[0][0]);
                if (recordCount > 10)
                {
                    dvPaging.Visible = true;
                    Int64 totalPages = 1;
                    if ((recordCount % Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"])) == 0)
                    {
                        totalPages = recordCount / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]);
                    }
                    else
                    {
                        totalPages = (recordCount / Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"])) + 1;
                    }

                    for (int c = 1; c <= totalPages; c++)
                    {
                        ddCurrentPage.Items.Add(new ListItem(c.ToString(), c.ToString()));
                    }
                }
                else
                {
                    dvPaging.Visible = false;
                }
                imgPageFirst.Enabled = true;
                imgPagePrevious.Enabled = true;
                imgPageLast.Enabled = true;
                imgPageNext.Enabled = true;
                if (ddCurrentPage.Items.Count == Convert.ToInt32(ViewState["PageIndex"]))
                {
                    imgPageLast.Enabled = false;
                    imgPageNext.Enabled = false;
                }
                if (Convert.ToInt32(ViewState["PageIndex"]) == 1)
                {
                    imgPageFirst.Enabled = false;
                    imgPagePrevious.Enabled = false;
                }
            }
            else
            {
                dataGridProductClass.DataSource = null;
                dataGridProductClass.DataBind();
                Session["data"] = null;
            }
        }

        protected void bindgridColumnsDDL()
        {
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();

            DataSet dsProductClass = objCRMProductClass.getProductClassData(0, GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue));
            if (dsProductClass != null && dsProductClass.Tables.Count > 0)
            {
                DataTable dtProductClass = dsProductClass.Tables[0];
                for (int c = 1; c < dtProductClass.Columns.Count; c++)
                {
                    ddlGridColumns1.Items.Add(new ListItem(dtProductClass.Columns[c].ColumnName, dtProductClass.Columns[c].ColumnName));
                }

                foreach (ListItem item in ddlGridColumns1.Items)
                {
                    item.Selected = true;
                }
            }
        }

        protected void checkBoxes_SelcetedIndexChanged(object sender, EventArgs e)
        {
            //selectedItemsPanel.Controls.Clear();
            string strSelValue = "";
            for (int li = 0; li < (sender as ListControl).Items.Count; li++)
            {
                if (!(sender as ListControl).Items[li].Selected)
                {
                    if (strSelValue != "")
                        strSelValue = strSelValue + ",";

                    strSelValue = strSelValue + (li + 2).ToString();
                }
            }

            ViewState["SelDDLValues"] = strSelValue;
            bindProductClassGrid();
        }

        public void bindFiltersDropDown()
        {
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();
            ddlFilters.DataSource = objCRMProductClass.getFilters(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]));
            ddlFilters.DataTextField = "FilterName";
            ddlFilters.DataValueField = "ID";
            ddlFilters.DataBind();
            ddlFilters.Items.Insert(0, new ListItem("All ProductClass", "0"));
        }

        protected void ddlFilters_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridProductClass.DataSource = null;
            dataGridProductClass.DataBind();
            bindProductClassGrid();
        }

        protected void dataGridProductClass_Sorting(object sender, GridViewSortEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            GridViewSortExpression = e.SortExpression;

            dataGridProductClass.DataSource = SortDataTable(myDataTable, false);
            dataGridProductClass.DataBind();
        }

        //Gets or Sets the GridView SortDirection Property
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }
            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        //Gets or Sets the GridView SortExpression Property
        private string GridViewSortExpression
        {
            get
            {
                return ViewState["SortExpression"] as string ?? string.Empty;
            }
            set
            {
                ViewState["SortExpression"] = value;
            }
        }

        //Toggles between the Direction of the Sorting
        private string GetSortDirection()
        {
            switch (GridViewSortDirection)
            {
                case "ASC":
                    GridViewSortDirection = "DESC";
                    break;
                case "DESC":
                    GridViewSortDirection = "ASC";
                    break;
            }
            return GridViewSortDirection;
        }

        //Sorts the ResultSet based on the SortExpression and the Selected Column.
        protected DataView SortDataTable(DataTable myDataTable, bool isPageIndexChanging)
        {
            if (myDataTable != null)
            {
                DataView myDataView = new DataView(myDataTable);
                if (GridViewSortExpression != string.Empty)
                {
                    if (isPageIndexChanging)
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GridViewSortDirection);
                    }
                    else
                    {
                        myDataView.Sort = string.Format("{0} {1}",
                        GridViewSortExpression, GetSortDirection());
                    }
                }
                return myDataView;
            }
            else
            {

                return new DataView();
            }
        }

        protected void dataGridProductClass_DataBound(object sender, EventArgs e)
        {
            //Custom Paging
            GridViewRow dataGridProductClassRow = dataGridProductClass.BottomPagerRow;

            if (dataGridProductClassRow == null) return;

            //Get your Controls from the GridView, in this case 
            //I use a DropDown Control for Paging
            DropDownList ddCurrentPage =
        (DropDownList)dataGridProductClassRow.Cells[0].FindControl("ddCurrentPage");
            Label lblTotalPage = (Label)dataGridProductClassRow.Cells[0].FindControl("lblTotalPage");

            if (ddCurrentPage != null)
            {
                //Populate Pager
                for (int i = 0; i < dataGridProductClass.PageCount; i++)
                {
                    int iPageNumber = i + 1;
                    ListItem myListItem = new ListItem(iPageNumber.ToString());

                    if (i == dataGridProductClass.PageIndex)
                        myListItem.Selected = true;

                    ddCurrentPage.Items.Add(myListItem);
                }
            }

            // Populate the Page Count
            if (lblTotalPage != null)
                lblTotalPage.Text = dataGridProductClass.PageCount.ToString();

        }

        private DataSet GetViewState()
        {
            //Gets the ViewState
            return (DataSet)ViewState["ProductClassDataSet"];
        }

        private void SetViewState(DataSet ProductClassDataSet)
        {
            //Sets the ViewState
            ViewState["ProductClassDataSet"] = ProductClassDataSet;
        }

        protected void dataGridProductClass_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            DataSet myDataSet = GetViewState();
            DataTable myDataTable = myDataSet.Tables[0];
            dataGridProductClass.DataSource = SortDataTable(myDataTable, true);

            dataGridProductClass.PageIndex = e.NewPageIndex;
            dataGridProductClass.DataBind();
        }

        //Change to a different page when the DropDown Page is changed
        protected void ddCurrentPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["PageIndex"] = ddCurrentPage.SelectedValue;

            //Popultate the GridView Control
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();
            DataSet dsProductClass = objCRMProductClass.getProductClassData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), Convert.ToInt32(ViewState["PageIndex"]), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]));
            Session["data"] = dsProductClass.Tables[2];
            SetViewState(dsProductClass);
            if (dsProductClass.Tables.Count > 0)
            {
                DataTable myDataTable = dsProductClass.Tables[0];
                dataGridProductClass.DataSource = SortDataTable(myDataTable, true);
                dataGridProductClass.DataBind();
            }
            else
            {
                dataGridProductClass.DataSource = null;
                dataGridProductClass.DataBind();
            }
        }

        protected void imgPageFirst_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void imgPagePrevious_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void imgPageNext_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void imgPageLast_Command(object sender, CommandEventArgs e)
        {
            Paginate(sender, e);
        }

        protected void Paginate(object sender, CommandEventArgs e)
        {
            int iCurrentIndex = Convert.ToInt32(ViewState["PageIndex"]);// dataGridDeals.PageIndex;

            switch (e.CommandArgument.ToString().ToLower())
            {
                case "first":
                    ViewState["PageIndex"] = 1;//dataGridDeals.PageIndex = 0;
                    break;
                case "prev":
                    if (Convert.ToInt32(ViewState["PageIndex"]) != 1)
                    {
                        ViewState["PageIndex"] = iCurrentIndex - 1;
                    }
                    break;
                case "next":
                    ViewState["PageIndex"] = iCurrentIndex + 1;
                    break;
                case "last":
                    ViewState["PageIndex"] = ddCurrentPage.Items[ddCurrentPage.Items.Count - 1].Value;// dataGridDeals.PageCount;
                    break;
            }
            ddCurrentPage.SelectedIndex = Convert.ToInt32(ViewState["PageIndex"]) - 1;
            //Populate the GridView Control
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();
            DataSet dsProductClass = objCRMProductClass.getProductClassData(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlFilters.SelectedValue), Convert.ToInt32(ViewState["PageIndex"]), Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PageSize"]));
            Session["data"] = dsProductClass.Tables[2];
            SetViewState(dsProductClass);
            if (dsProductClass.Tables.Count > 0)
            {
                DataTable myDataTable = dsProductClass.Tables[0];
                dataGridProductClass.DataSource = SortDataTable(myDataTable, true);
                dataGridProductClass.DataBind();
            }
            else
            {
                dataGridProductClass.DataSource = null;
                dataGridProductClass.DataBind();
            }
            imgPageFirst.Enabled = true;
            imgPagePrevious.Enabled = true;
            imgPageLast.Enabled = true;
            imgPageNext.Enabled = true;
            if (ddCurrentPage.Items.Count == Convert.ToInt32(ViewState["PageIndex"]))
            {
                imgPageLast.Enabled = false;
                imgPageNext.Enabled = false;
            }
            if (Convert.ToInt32(ViewState["PageIndex"]) == 1)
            {
                imgPageFirst.Enabled = false;
                imgPagePrevious.Enabled = false;
            }
        }

        protected void btnDeleteAll_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLProductClass objCRMProductClass = new CRMBLLProductClass();
            string IDS = "";
            for (int_index = 0; int_index <= dataGridProductClass.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)dataGridProductClass.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(dataGridProductClass.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMProductClass.deleteProductClasss(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected ProductClass Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindProductClassGrid();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            ViewState["txtProductClassID"] = "0";
            //aceContact.ContextKey = "CRM_Contact," + GlobalVariables.LoginUser.OrgID;
            Session["selObjectUserID"] = "0";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createProductClass();", true);
        }

        protected void btnGridColumn_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            Int64 IDS = Convert.ToInt64(src.CommandArgument);
            ViewState["txtProductClassID"] = IDS;
            CRMBLLProductClass _objProductClass = new CRMBLLProductClass();
            DataSet dsSelectedProductClass = _objProductClass.getProductClassData(Convert.ToInt64(IDS), GlobalVariables.LoginUser.OrgID, 0);

            txtDescription.Text = dsSelectedProductClass.Tables[0].Rows[0]["Description"].ToString();

            ddlItemType.SelectedValue = dsSelectedProductClass.Tables[0].Rows[0]["ItemType"].ToString();
            ddlValidation.SelectedValue = dsSelectedProductClass.Tables[0].Rows[0]["Validation"].ToString();
            ddlLocationCode.SelectedValue = dsSelectedProductClass.Tables[0].Rows[0]["LocationCode"].ToString();
            rdbExcludeComission.SelectedValue = dsSelectedProductClass.Tables[0].Rows[0]["ExcludeCommission"].ToString();

            ddlUnitofMeasure.SelectedValue = dsSelectedProductClass.Tables[0].Rows[0]["UnitMeasure"].ToString();
            ddlProductFamily.SelectedValue = dsSelectedProductClass.Tables[0].Rows[0]["ProductFamily"].ToString();
            rdbPriceOverride.SelectedValue = dsSelectedProductClass.Tables[0].Rows[0]["DisallowPriceOverride"].ToString();

            //Session["selObjectUserID"] = txtProductClassID.Text;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createProductClass();", true);
        }

        protected void dataGridProductClass_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                e.Row.Cells[1].Visible = false; // hides the first column
                if (ViewState["SelDDLValues"] != null)
                {
                    string[] words = ViewState["SelDDLValues"].ToString().Split(',');
                    for (int w = 0; w < words.Count(); w++)
                    {
                        e.Row.Cells[Convert.ToInt32(words[w])].Visible = false; // hides the first column
                    }
                }
            }
            catch
            {
            }
        }

        protected void dataGridProductClass_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var firstCell = e.Row.Cells[2];
                firstCell.Controls.Clear();
                LinkButton lnkEditFirstCol = new LinkButton();
                lnkEditFirstCol.ID = e.Row.Cells[1].Text;

                lnkEditFirstCol.Click += new EventHandler(btnGridColumn_Click);
                lnkEditFirstCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();
                lnkEditFirstCol.Attributes.Add("runat", "server");
                lnkEditFirstCol.Text = firstCell.Text;
                firstCell.Controls.Add(lnkEditFirstCol);
                TableCell cell = new TableCell();

                LinkButton lnkNotesLastCol = new LinkButton();
                lnkNotesLastCol.Click += new EventHandler(btnGridNotes_Click);
                lnkNotesLastCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();

                lnkNotesLastCol.Attributes.Add("runat", "server");
                lnkNotesLastCol.Text = "Notes";
                cell.Controls.Add(lnkNotesLastCol);

                string NextCall = "No Call Schedule";
                CRMBLLDeals _objDeals = new CRMBLLDeals();
                if (e.Row.Cells[1].Text != "")
                {
                    DataView dtColumns = _objDeals.getUpcomingPhoneCall(GlobalVariables.LoginUser.OrgID, 8, Convert.ToInt64(e.Row.Cells[1].Text)).Tables[0].DefaultView;
                    if (dtColumns.Table.Rows.Count > 0)
                        NextCall = dtColumns.Table.Rows[0]["DueDate"].ToString();
                }

                TableCell cellPhoneCalls = new TableCell();
                LinkButton lnkPhoneCallsLastCol = new LinkButton();
                lnkPhoneCallsLastCol.Click += new EventHandler(btnPhoneCall_Click);
                lnkPhoneCallsLastCol.CommandArgument = ((System.Data.DataRowView)(e.Row.DataItem)).Row.ItemArray[0].ToString();
                lnkPhoneCallsLastCol.Attributes.Add("runat", "server");
                lnkPhoneCallsLastCol.Text = NextCall;
                cellPhoneCalls.Controls.Add(lnkPhoneCallsLastCol);

                e.Row.Controls.Add(cell);
                e.Row.Controls.Add(cellPhoneCalls);
            }
            if (e.Row.RowType == DataControlRowType.Header)
            {
                int i = e.Row.Cells.Count;
                var rowItem = e.Row.DataItem;

                for (int c = 1; c < i; c++)
                {
                    e.Row.Cells[c].Controls.Add(new LiteralControl("<br />"));
                    TextBox tx = new TextBox();  //here i m adding a control.
                    tx.ID = "txtFilter" + c;
                    tx.Width = Unit.Pixel(100);
                    tx.Attributes.Add("runat", "server");
                    e.Row.Cells[c].Controls.Add(tx);

                    AutoCompleteExtender _objautoCompleteList = new AutoCompleteExtender();
                    _objautoCompleteList.CompletionListItemCssClass = "CompletionListItemCssClass";
                    _objautoCompleteList.CompletionListCssClass = "CompletionListCssClass";

                    _objautoCompleteList.ID = "ACE" + c;
                    _objautoCompleteList.TargetControlID = "txtFilter" + c;
                    _objautoCompleteList.ServiceMethod = "filterGridrecords";
                    _objautoCompleteList.ServicePath = "CustomList.asmx";
                    _objautoCompleteList.ContextKey = ((System.Web.UI.WebControls.DataControlFieldCell)(e.Row.Cells[c])).ContainingField.ToString();
                    _objautoCompleteList.UseContextKey = true;
                    _objautoCompleteList.EnableCaching = false;
                    //_objautoCompleteList.EnableClientState = true;
                    _objautoCompleteList.MinimumPrefixLength = 1;

                    e.Row.Cells[c].Controls.Add(_objautoCompleteList);


                    ImageButton imgBtnFind = new ImageButton();  //here i m adding a control.
                    imgBtnFind.ID = "imgBtnFind" + c;
                    imgBtnFind.Attributes.Add("runat", "server");
                    imgBtnFind.ImageUrl = "../images/grid_search.JPG";
                    e.Row.Cells[c].Controls.Add(imgBtnFind);

                }
                TableHeaderCell hcell = new TableHeaderCell();
                hcell.Text = "Notes";
                e.Row.Cells.Add(hcell);

                TableHeaderCell hcellCalls = new TableHeaderCell();
                hcellCalls.Text = "Next Call";
                e.Row.Cells.Add(hcellCalls);
            }
        }

        protected DataTable getImportDataTable()
        {
            DataTable tableImport = new DataTable();
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataSet dsObjectCol = _objDeals.getObjectsColumns(Convert.ToInt64(Session["ObjectID"]), GlobalVariables.LoginUser.OrgID, 0);
            Session["objectColumnsDataSet"] = dsObjectCol;
            tableImport.Columns.Add("ClassID", typeof(Int64));
            tableImport.Columns.Add("OrgID", typeof(string));
            tableImport.Columns.Add("CreatedDate", typeof(DateTime));
            tableImport.Columns.Add("CreatedBy", typeof(string));
            tableImport.Columns.Add("ModifiedDate", typeof(DateTime));
            tableImport.Columns.Add("ModifiedBy", typeof(string));
            for (int c = 0; c < dsObjectCol.Tables[0].Rows.Count; c++)
            {
                tableImport.Columns.Add(Convert.ToString(dsObjectCol.Tables[0].Rows[c]["ColumnName"]), typeof(string));
            }
            return tableImport;
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (fleImport.HasFile)
            {
                try
                {
                    String strConnection = SqlHelper.GetConnectionString(); //"Data Source=MySystem;Initial Catalog=MySamplesDB;Integrated Security=True";

                    //file upload path
                    string path = string.Concat(Server.MapPath("~/Upload/" + fleImport.FileName));
                    fleImport.SaveAs(path);

                    // Connection String to Excel Workbook
                    string excelConnectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=Excel 8.0", path);
                    OleDbConnection excelConnection = new OleDbConnection();
                    excelConnection.ConnectionString = excelConnectionString;
                    OleDbCommand command = new OleDbCommand("select * from [Sheet1$]", excelConnection);
                    excelConnection.Open();
                    // Create DbDataReader to Data Worksheet

                    OleDbDataReader dReader = command.ExecuteReader();
                    DataTable dtImport = getImportDataTable();
                    DataSet dsObjectColumns = (DataSet)Session["objectColumnsDataSet"];
                    if (dsObjectColumns.Tables[0].Rows.Count == dReader.FieldCount)
                    {
                        while (dReader.Read())
                        {
                            DataRow dr = dtImport.NewRow();
                            dr["OrgID"] = GlobalVariables.LoginUser.OrgID;
                            dr["CreatedDate"] = DateTime.Now;
                            dr["CreatedBy"] = "Admin";
                            dr["ModifiedDate"] = DateTime.Now;
                            dr["ModifiedBy"] = "Admin";

                            for (int c = 0; c < dsObjectColumns.Tables[0].Rows.Count; c++)
                            {
                                dr[Convert.ToString(dsObjectColumns.Tables[0].Rows[c]["ColumnName"])] = dReader[Convert.ToString(dsObjectColumns.Tables[0].Rows[c]["ColumnName"])];
                            }
                            dtImport.Rows.Add(dr);
                        }

                        SqlBulkCopy sqlBulk = new SqlBulkCopy(strConnection);
                        //Give your Destination table name
                        sqlBulk.DestinationTableName = "CRM_ProductClass";
                        sqlBulk.WriteToServer(dtImport);
                        excelConnection.Close();
                        lblListmsg.Text = "Records imported successfully.";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "refreshPage();", true);
                    }
                    else
                    {
                        try
                        {
                            string checkPath = string.Concat(Server.MapPath("~/Upload/" + fleImport.FileName));
                            if (File.Exists(checkPath))
                                File.Delete(checkPath);
                        }
                        catch { }
                        lblListmsg.Text = "File columns are not matched with table.";
                    }
                }
                catch
                {
                    lblListmsg.Text = "Some error while importing records.";
                }
            }
            else
            {
                lblListmsg.Text = "Please choose file to import records.";
            }
        }

        /*Create ProductClass*/


        protected void btnSave_Click(object sender, EventArgs e)
        {
            CRMBLLProductClass _objProductClass = new CRMBLLProductClass();

            Int64 ProductClassID = _objProductClass.insertProductClass(Convert.ToInt64(ViewState["txtProductClassID"]), GlobalVariables.LoginUser.OrgID,
                                                      txtDescription.Text, Convert.ToInt32(ddlItemType.SelectedValue), Convert.ToInt32(ddlValidation.SelectedValue),
                                                      Convert.ToInt32(ddlLocationCode.SelectedValue), Convert.ToBoolean(rdbExcludeComission.SelectedValue),
                                                      Convert.ToInt32(ddlUnitofMeasure.SelectedValue), Convert.ToInt32(ddlProductFamily.SelectedValue),
                                                      Convert.ToBoolean(rdbPriceOverride.SelectedValue));
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }


        //public void CreateFields(Int64 ProductClassID)
        //{
        //    ViewState["ProductClassID"] = ProductClassID;
        //    Session["selObjectUserID"] = ProductClassID;
        //    clsCommonFunctions.CheckSession();
        //    CRMBLLProductClass _objProductClass = new CRMBLLProductClass();
        //    DataView dtColumns = _objProductClass.getColumnControls(Convert.ToInt64(Session["ObjectID"])).Tables[0].DefaultView;
        //    DataTable dtPlaceholders = _objProductClass.getPlaceholders(Convert.ToInt64(Session["ObjectID"]), GlobalVariables.LoginUser.OrgID).Tables[0];

        //    DataTable dtProductClass = _objProductClass.getSelectedProductClass(ProductClassID).Tables[0];

        //    lblProductClassID.Text = ProductClassID.ToString();
        //    placeholderGeneralInfo.Controls.Add(new LiteralControl("<table><tr><td style='padding-right:50px;vertical-align:top;'>"));
        //    for (int p = 0; p < dtPlaceholders.Rows.Count; p++)
        //    {
        //        // Create Header Panel
        //        Panel panelHead = new Panel();
        //        if (p != 0)
        //            panelHead.Attributes.Add("style", "Padding-top:15px;");
        //        else
        //            panelHead.Attributes.Add("style", "Padding-top:2px;");

        //        panelHead.ID = "pH" + dtPlaceholders.Rows[p]["ID"].ToString();
        //        Label lblHead = new Label();
        //        lblHead.ID = "lblHeader" + dtPlaceholders.Rows[p]["ID"].ToString();
        //        panelHead.Controls.Add(lblHead);

        //        Panel _objNewPlaceholder = new Panel();
        //        _objNewPlaceholder.ID = dtPlaceholders.Rows[p]["ID"].ToString();

        //        Panel _objSystemField = new Panel();
        //        dtColumns.RowFilter = "Placeholder=" + dtPlaceholders.Rows[p]["ID"].ToString();
        //        bool IsSystemField = Convert.ToBoolean(dtPlaceholders.Rows[p]["IsSystemField"]);

        //        if (IsSystemField != true)
        //        {
        //            //string strHTML = "<span style='font-weight:bold;'>" + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + "</span>" + "</br></br>";
        //            //_objNewPlaceholder.Controls.Add(new LiteralControl(strHTML));
        //        }
        //        _objNewPlaceholder.Controls.Add(new LiteralControl("<table>"));

        //        for (int col = 0; col < dtColumns.Table.DefaultView.Count; col++)
        //        {
        //            bool IsControlSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[col]["IsSystemField"]);

        //            if (IsSystemField == false)
        //                _objNewPlaceholder.Controls.Add(new LiteralControl("<tr style='vertical-align:top;height:30px;width:120px;'>"));
        //            else if (col == 0)
        //                _objNewPlaceholder.Controls.Add(new LiteralControl("<tr style='vertical-align:top;height:30px;width:800px;'>"));
        //            if (IsSystemField != true)
        //                _objNewPlaceholder.Controls.Add(new LiteralControl("<td style='width:120px;'>"));
        //            else
        //                _objNewPlaceholder.Controls.Add(new LiteralControl("<td style='background-color: #f4d89d;'>"));

        //            int formID = Convert.ToInt32(dtColumns.Table.DefaultView[col]["ID"]);
        //            Label _objLabel = new Label();
        //            _objLabel.Text = dtColumns.Table.DefaultView[col]["LabelName"].ToString();
        //            _objNewPlaceholder.Controls.Add(_objLabel);
        //            if (IsSystemField != true)
        //                _objNewPlaceholder.Controls.Add(new LiteralControl("</td><td>"));
        //            else
        //                _objNewPlaceholder.Controls.Add(new LiteralControl("</br>"));

        //            int fieldType = Convert.ToInt32(dtColumns.Table.DefaultView[col]["FieldType"]);
        //            if (fieldType == 1)
        //            {
        //                createTextFormatControl(col, _objNewPlaceholder, dtColumns.Table.DefaultView, dtProductClass, IsSystemField);
        //            }
        //            else if (fieldType == 3 || fieldType == 4)
        //            {
        //                DataTable dtColumnChoices = _objProductClass.getControlChoices(formID).Tables[0];
        //                createSingleChoiceFormatControl(col, _objNewPlaceholder, dtColumns.Table.DefaultView, dtColumnChoices, dtProductClass, _objSystemField, IsSystemField);
        //            }
        //            if (IsControlSystemField)
        //                placeholderHeaderInfo.Controls.Add(_objSystemField);

        //            if (IsSystemField == true)
        //            {
        //                _objNewPlaceholder.Controls.Add(new LiteralControl("</td>"));

        //            }
        //            else
        //                _objNewPlaceholder.Controls.Add(new LiteralControl("</td></tr>"));
        //        }
        //        //_objNewPlaceholder.Controls.Add(new LiteralControl("</table>"));

        //        CollapsiblePanelExtender cpe = new CollapsiblePanelExtender();
        //        cpe.ID = "CPE" + _objNewPlaceholder.ID;
        //        cpe.TargetControlID = _objNewPlaceholder.ID;
        //        cpe.ExpandControlID = panelHead.ID;
        //        cpe.CollapseControlID = panelHead.ID;
        //        cpe.ScrollContents = false;
        //        cpe.Collapsed = false;
        //        cpe.AutoExpand = true;
        //        cpe.ExpandDirection =
        //        CollapsiblePanelExpandDirection.Vertical;
        //        cpe.SuppressPostBack = true;
        //        cpe.TextLabelID = lblHead.ID;
        //        //cpe.CollapsedImage = "images/collapse.jpg";
        //        //cpe.ExpandedImage = "images/icon_expand.gif";
        //        cpe.CollapsedText = "<b><span style='font-size:23px;'>+</span> " + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + " tab</b>";
        //        cpe.ExpandedText = "<b><span style='font-size:23px;'>-</span> " + dtPlaceholders.Rows[p]["PlaceholderHeader"].ToString() + " tab</b>";

        //        if (IsSystemField == true)
        //        {
        //            _objNewPlaceholder.Controls.Add(new LiteralControl("</tr></table>"));

        //            placeholderHeaderInfo.Controls.Add(_objNewPlaceholder);
        //        }
        //        else
        //        {
        //            _objNewPlaceholder.Controls.Add(new LiteralControl("</table>"));
        //            placeholderGeneralInfo.Controls.Add(panelHead);
        //            placeholderGeneralInfo.Controls.Add(_objNewPlaceholder);
        //            placeholderGeneralInfo.Controls.Add(cpe);
        //        }
        //    }
        //    placeholderGeneralInfo.Controls.Add(new LiteralControl("</td>"));
        //    placeholderGeneralInfo.Controls.Add(new LiteralControl("</tr></table>"));
        //    if (ProductClassID != 0)
        //        iFrameGrid.Visible = true;
        //    else
        //        iFrameGrid.Visible = false;
        //}



        //public void createTextFormatControl(int index, Panel _objplaceholder, DataView dtColumns, DataTable dtProductClass, bool IsSystemField)
        //{
        //    string strColumnName = dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
        //    string Value = "";
        //    if (dtProductClass != null && dtProductClass.Rows.Count > 0)
        //        Value = dtProductClass.Rows[0][strColumnName].ToString();


        //    int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
        //    if (fieldFormat == 1)//Simple text box
        //    {
        //        TextBox _objTextBox = new TextBox();
        //        _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
        //        _objTextBox.Text = Value;
        //        if (!IsSystemField)
        //            _objTextBox.Width = Unit.Pixel(200);
        //        _objplaceholder.Controls.Add(_objTextBox);
        //    }
        //    else if (fieldFormat == 2)//Multiline text Box
        //    {
        //        TextBox _objTextBox = new TextBox();
        //        _objTextBox.TextMode = TextBoxMode.MultiLine;
        //        _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
        //        _objTextBox.Text = Value;
        //        if (!IsSystemField)
        //            _objTextBox.Width = Unit.Pixel(200);
        //        _objplaceholder.Controls.Add(_objTextBox);
        //    }
        //    else if (fieldFormat == 3)//Email
        //    {
        //        TextBox _objTextBox = new TextBox();
        //        _objTextBox.ID = dtColumns.Table.DefaultView[index]["ID"].ToString();
        //        _objTextBox.Text = Value;
        //        if (!IsSystemField)
        //            _objTextBox.Width = Unit.Pixel(200);
        //        _objplaceholder.Controls.Add(_objTextBox);
        //    }
        //    bool IsRequired = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsRequired"]);
        //    if (IsRequired)
        //    {
        //        RequiredFieldValidator myRFV = new RequiredFieldValidator();
        //        myRFV.ControlToValidate = dtColumns.Table.DefaultView[index]["ID"].ToString();
        //        myRFV.Display = ValidatorDisplay.Dynamic;
        //        myRFV.SetFocusOnError = true;
        //        myRFV.ValidationGroup = "createValidation";
        //        myRFV.ErrorMessage = "*";
        //        myRFV.CssClass = "validationerror";
        //        _objplaceholder.Controls.Add(myRFV);
        //    }
        //}

        //public void createSingleChoiceFormatControl(int index, Panel _objplaceholder, DataView dtColumns, DataTable dtControlChoices, DataTable dtProductClass, Panel _objSystemField, bool IsPlcSystemField)
        //{
        //    string strColumnName = dtColumns.Table.DefaultView[index]["ColumnName"].ToString();
        //    bool IsSystemField = Convert.ToBoolean(dtColumns.Table.DefaultView[index]["IsSystemField"]);
        //    Int64 ObjectID = Convert.ToInt64(dtColumns.Table.DefaultView[index]["ObjectID"]);
        //    int fieldFormat = Convert.ToInt32(dtColumns.Table.DefaultView[index]["FieldFormat"]);
        //    string Value = "";
        //    if (dtProductClass != null && dtProductClass.Rows.Count > 0)
        //        Value = dtProductClass.Rows[0][strColumnName].ToString();

        //    if (fieldFormat == 6)//Radio Button List
        //    {
        //        RadioButtonList _objRdbList = new RadioButtonList();
        //        _objRdbList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
        //        for (int c = 0; c < dtControlChoices.Rows.Count; c++)
        //        {
        //            _objRdbList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
        //            if (Value != "")
        //            {
        //                if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
        //                    _objRdbList.Items[c].Selected = true;
        //            }
        //            else
        //                _objRdbList.Items[0].Selected = true;
        //            _objplaceholder.Controls.Add(_objRdbList);
        //        }

        //    }
        //    if (fieldFormat == 7)//DropDownList
        //    {

        //        if (IsSystemField == true)
        //        {
        //            TextBox _objTextBox = new TextBox();
        //            if (!IsPlcSystemField)
        //                _objTextBox.Width = Unit.Pixel(200);
        //            _objTextBox.ID = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
        //            _objTextBox.Attributes.Add("Onchange", "checkExists('" + dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString() + "','" + _objTextBox.ID + "','" + GlobalVariables.LoginUser.OrgID + "')");
        //            _objTextBox.Attributes.Add("autocomplete", "off");
        //            _objTextBox.Attributes.Add("runat", "server");

        //            //Label _objLabel = new Label();
        //            //_objLabel.ID = "lbl" + Convert.ToString(_objTextBox.ID);

        //            System.Web.UI.WebControls.Image _imgSuccess = new System.Web.UI.WebControls.Image();
        //            _imgSuccess.ID = "imgSuccess" + Convert.ToString(_objTextBox.ID);
        //            _imgSuccess.ImageUrl = "images/update.jpg";
        //            _imgSuccess.Attributes.Add("style", "height:20px;width:20px; display:none;");

        //            System.Web.UI.WebControls.Image _imgFailure = new System.Web.UI.WebControls.Image();
        //            _imgFailure.ID = "imgFailure" + Convert.ToString(_objTextBox.ID);
        //            _imgFailure.ImageUrl = "images/Cancel.jpg";
        //            _imgFailure.Attributes.Add("style", "height:20px;width:20px;display:none;");


        //            AutoCompleteExtender _objautoCompleteList = new AutoCompleteExtender();
        //            _objautoCompleteList.CompletionListItemCssClass = "CompletionListCssClass";
        //            _objautoCompleteList.CompletionListCssClass = "CompletionListCssClass";

        //            _objautoCompleteList.ID = "ACE" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
        //            _objautoCompleteList.TargetControlID = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
        //            _objautoCompleteList.ServiceMethod = "GetCompletionList";
        //            _objautoCompleteList.ServicePath = "CustomList.asmx";
        //            _objautoCompleteList.ContextKey = dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString() + "," + GlobalVariables.LoginUser.OrgID.ToString();
        //            _objautoCompleteList.UseContextKey = true;
        //            _objautoCompleteList.EnableCaching = true;
        //            _objautoCompleteList.MinimumPrefixLength = 1;

        //            //CRMBLLDeals _objDeals = new CRMBLLDeals();
        //            //DataTable dtObjectChoices = _objDeals.getObjectChoices(dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString(), GlobalVariables.LoginUser.OrgID).Tables[0];

        //            string linkTableValue = dtColumns.Table.DefaultView[index]["SystemFieldLinkTable"].ToString();
        //            if (Value != "")
        //            {
        //                _objTextBox.Text = Value;
        //            }
        //            else if (linkTableValue == "CRM_Users")
        //            {
        //                _objTextBox.Text = GlobalVariables.LoginUser.UserName;
        //            }

        //            _objplaceholder.Controls.Add(_objTextBox);
        //            _objplaceholder.Controls.Add(_imgSuccess);
        //            _objplaceholder.Controls.Add(_imgFailure);
        //            _objSystemField.Controls.Add(_objautoCompleteList);

        //            RequiredFieldValidator myRFV = new RequiredFieldValidator();
        //            myRFV.ControlToValidate = "txt" + Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
        //            myRFV.Display = ValidatorDisplay.Dynamic;
        //            myRFV.SetFocusOnError = true;
        //            myRFV.ValidationGroup = "createValidation";
        //            myRFV.ErrorMessage = "*";
        //            myRFV.CssClass = "validationerror";
        //            _objplaceholder.Controls.Add(myRFV);
        //        }
        //        else
        //        {
        //            DropDownList _objDDLList = new DropDownList();
        //            if (!IsPlcSystemField)
        //                _objDDLList.Width = Unit.Pixel(200);
        //            _objDDLList.ID = Convert.ToString(dtColumns.Table.DefaultView[index]["ID"]);
        //            for (int c = 0; c < dtControlChoices.Rows.Count; c++)
        //            {
        //                _objDDLList.Items.Add(new ListItem(dtControlChoices.Rows[c]["ControlChoices"].ToString(), dtControlChoices.Rows[c]["ControlChoices"].ToString()));
        //                if (Value != "")
        //                {
        //                    if (dtControlChoices.Rows[c]["ControlChoices"].ToString() == Value)
        //                        _objDDLList.Items[c].Selected = true;
        //                }
        //                else
        //                    _objDDLList.Items[0].Selected = true;
        //                _objplaceholder.Controls.Add(_objDDLList);
        //            }
        //        }
        //    }
        //}




        /* Phone Calls */
        protected void btnPhoneCall_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            ViewState["sObjectUserID"] = src.CommandArgument;
            bindPhoneCallsList();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "objectPhoneCall();", true);
        }

        protected void bindPhoneCallsList()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDocumets = objDeals.getPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"]));
            gvPhoneCalls.DataSource = dsDocumets;
            gvPhoneCalls.DataBind();
            txtPhoneCallDescription.Text = "";
            txtCallDate.Text = "";
            //ddlDirection.SelectedValue = "Outgoing";
        }

        protected void btnUploadPhoneCalls_Click(object sender, EventArgs e)
        {
            uploadphonecalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ViewState["sObjectUserID"]));
            bindPhoneCallsList();
            txtPhoneCallDescription.Text = "";
            txtCallDate.Text = "";
            ddlDirection.SelectedIndex = 0;
        }

        private void uploadphonecalls(Int64 orgid, Int64 dealid)
        {
            try
            {
                CRMBLLDeals objblldeals = new CRMBLLDeals();
                objblldeals.insertPhoneCalls(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["objectid"]), dealid, txtPhoneCallDescription.Text, Convert.ToDateTime(txtCallDate.Text), ddlDirection.SelectedValue, Convert.ToInt64(ViewState["sObjectUserID"]), "", false);
            }
            catch (Exception ex)
            {
            }
        }

        protected void btnDeleteAllCall_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            string IDS = "";
            for (int_index = 0; int_index <= gvPhoneCalls.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvPhoneCalls.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(gvPhoneCalls.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMDeals.deletePhoneCalls(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Phone Calls Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindPhoneCallsList();
        }



        /* Notes */
        protected void btnGridNotes_Click(object sender, EventArgs e)
        {
            LinkButton src = (LinkButton)sender;
            ViewState["sObjectUserID"] = src.CommandArgument;
            bindNotes();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "objectNotes();", true);
        }

        protected void bindNotes()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataView dsDocumets = objDeals.getDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"])).Tables[0].DefaultView;
            dsDocumets.RowFilter = "IsDocsOnly=0";
            gvNotes.DataSource = dsDocumets.Table.DefaultView;
            gvNotes.DataBind();
        }

        protected void btnUploadDocs_Click(object sender, EventArgs e)
        {
            try
            {
                //string path = Server.MapPath("~\\MyFiles\\" + Session["ObjectID"].ToString() + GlobalVariables.LoginUser.OrgID.ToString() + ViewState["sObjectUserID"].ToString());

                //if (!Directory.Exists(path))
                //{
                //    Directory.CreateDirectory(path);
                //}
                string documentName = "";
                string documentPath = "";
                //if (fleUpload.FileName != "")
                //{
                //    fleUpload.SaveAs(path + "\\" + System.IO.Path.GetFileName(fleUpload.FileName));
                //    documentName = fleUpload.FileName;
                //    documentPath = path;
                //}
                //if (documentName != "")
                //{
                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                objBLLDeals.insertDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(ViewState["sObjectUserID"]), documentName, documentPath, Convert.ToInt64(ViewState["LeadID"]), "", txtNotes.Text, false, false);
                txtNotes.Text = "";
                bindNotes();
                //}
            }
            catch (Exception ex)
            {

            }
        }

        protected void btnDeleteNotes_Click(object sender, EventArgs e)
        {
            int int_index = 1;
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            string IDS = "";
            for (int_index = 0; int_index <= gvNotes.Rows.Count - 1; int_index++)
            {
                Boolean bcheck = ((CheckBox)gvNotes.Rows[int_index].FindControl("chkBoxRowID")).Checked;
                if (bcheck == true) IDS = IDS + (Convert.ToInt64(gvNotes.DataKeys[int_index].Value)) + ",";
            }
            if (objCRMDeals.deleteNotes(IDS) == 0)
            {
                lblListmsg.Text = "Error:";
                lblListmsg.Visible = true;
            }
            else
            {
                lblListmsg.Text = "Selected Note Deleted Successfully";
                lblListmsg.Visible = true;
            }
            bindNotes();
        }
    }
}