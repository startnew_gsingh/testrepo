﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using CRMDAL;
using System.Data;

namespace CRM
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //Header.Title = TransportValley.BLL.clsGlobalVariables;
            txtUserName.Focus();
            if (IsPostBack == false)
            {
                Session.Abandon();
            }
            if (Request["Logout"] != null)
            {
                Label1.Text = "Logout Successfully.";
                Label1.Visible = true;
            }
            else if (Request["SessionExpired"] != null)
            {
                Label1.Text = "Session Expired.";
                Label1.Visible = true;
            }

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            CRMBLLUsers objCRMUsers = new CRMBLLUsers();
            StructureLoginDAL _objstrcLogin = new StructureLoginDAL();
            _objstrcLogin.UserName = txtUserName.Text;
            _objstrcLogin.Password = txtPassword.Text;

            _objstrcLogin = objCRMUsers.authenticateUser(_objstrcLogin);

            if (_objstrcLogin.OrgID != 0)
            {
                GlobalVariables.LoginUser = _objstrcLogin;
                //Dictionary<int, string> _objPage = new Dictionary<int, string>();
                //CRMBLLUsers _objBLLUsers = new CRMBLLUsers();
                //DataSet _objObjectRoles = _objBLLUsers.getUserObjectRoles(GlobalVariables.LoginUser.OrgID, 0);
                //if (_objObjectRoles.Tables[0].Rows.Count > 0)
                //{
                //    _objPage.Add(1, "Deals.aspx");
                //    _objPage.Add(2, "Contacts.aspx");
                //    _objPage.Add(3, "Account.aspx");
                //    _objPage.Add(4, "Opportunities.aspx");
                //    _objPage.Add(5, "Quote.aspx");
                //    _objPage.Add(6, "WorkOrder.aspx");
                //    _objPage.Add(7, "Invoice.aspx");
                //    _objPage.Add(8, "Vendor.aspx");
                //    _objPage.Add(9, "ProductClass.aspx");
                //    for (int r = 0; r < _objObjectRoles.Tables[0].Rows.Count; r++)
                //    {
                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "1" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(1);

                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "2" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(2);

                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "3" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(3);

                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "4" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(4);

                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "5" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(5);

                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "6" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(6);

                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "7" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(7);

                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "8" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(8);

                //        if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "9" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                //            _objPage.Remove(9);
                //    }
                //}

                //if (_objPage.Count > 0)
                //{
                //    string _Value = _objPage.FirstOrDefault().Value;
                //    Response.Redirect(_Value);
                //}
                Response.Redirect("Dashboard.aspx");
            }
            else
            {
                Label1.Text = GlobalVariables.ErrorMessage;
                Label1.Visible = true;
            }
        }

        public void Page_Error(object sender, EventArgs e)
        {
            Exception objErr = Server.GetLastError().GetBaseException();
            string err = "<b>Sorry there is an Error in Page</b><hr><br>" +
                    "<br><b>Error in: </b>" + Request.Url.ToString() +
                    "<br><b>Error Message: </b>" + objErr.Message.ToString() +
                    "<br><b>Error From: </b> " + objErr.Source.ToString();

            Response.Write(err.ToString());
            Server.ClearError();
            Response.End();
        }
    }
}