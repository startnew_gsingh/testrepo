﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Deals.aspx.cs" Inherits="CRM.Deals" EnableEventValidation="false" %>

<%@ Register Assembly="JQChart.Web" Namespace="JQChart.Web.UI.WebControls" TagPrefix="jqChart" %>
<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" type="text/css" href="css/jquery.jqChart.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.jqRangeSlider.css" />
    <script src="Script/jquery.jqRangeSlider.min.js" type="text/javascript"></script>
    <script src="Script/jquery.jqChart.min.js" type="text/javascript"></script>
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <link rel="Stylesheet" type="text/css" href="css/CustomDDStyles.css" />
    <script type="text/javascript">
        //$(document).ready(function () {
        //  var ctw = $('.gradientbginner').width() - 20;
        //$('.customwd').css({ 'width': ctw });
        //alert(ctw);
        //});

        function docReady() {
            //var ctw = $('.gradientbginner').width() - 20;
            //$('.customwd').css({ 'width': ctw });
        }
    </script>
    <script type="text/javascript">


        function createDeal() {
            var createDealBox = "#div_CreateDeal-box";
            //Fade in the Popup
            $(createDealBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createDealBox).height() + 24) / 2;
            var popMargLeft = ($(createDealBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function advanceFind() {
            var advanceFindBox = "#div_AdvanceFind-box";
            //Fade in the Popup
            $(advanceFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(advanceFindBox).height() + 24) / 2;
            var popMargLeft = ($(advanceFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function objectNotes() {
            var notesFindBox = "#div_Notes-box";
            //Fade in the Popup
            $(notesFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(notesFindBox).height() + 24) / 2;
            var popMargLeft = ($(notesFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                window.location = "Deals.aspx";
            });
            return false;
        }

        function editColumns() {
            var editColumnsFindBox = "#div_EditColumns-box";
            //Fade in the Popup
            $(editColumnsFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(editColumnsFindBox).height() + 24) / 2;
            var popMargLeft = ($(editColumnsFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function customizeForm() {
            var editColumnsFindBox = "#div_Customize-box";
            //Fade in the Popup
            $(editColumnsFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(editColumnsFindBox).height() + 24) / 2;
            var popMargLeft = ($(editColumnsFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function editSalesProcess() {
            var salesProcessBox = "#div_SalesProcess-box";
            //Fade in the Popup
            $(salesProcessBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(salesProcessBox).height() + 24) / 2;
            var popMargLeft = ($(salesProcessBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function closeEditSalesWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                docReady();
                //document.getElementById("ContentPlaceHolder1_btnEdit").click();
                window.location = "Deals.aspx";
            });
            return false;
        }

        function changeProcessStage(stageID, ObjectID) {
            if (document.getElementById("ContentPlaceHolder1_txtStageID").value != stageID) {
                $("#" + stageID).css('background-color', '#0072c6');
                $("#" + document.getElementById("ContentPlaceHolder1_txtStageID").value).css('background-color', '#d7d0d0');

                document.getElementById("ContentPlaceHolder1_txtSelObjectID").value = ObjectID;
                document.getElementById("ContentPlaceHolder1_txtStageID").value = stageID;
                document.getElementById("ContentPlaceHolder1_btnStageID").click();
            }
        };

        function changeProcessStageStep(objectID) {
            document.getElementById("ContentPlaceHolder1_txtObjectID").value = objectID;
            document.getElementById("ContentPlaceHolder1_btnObjectID").click();
        };

        function firstColumnClick(DealID) {
            document.getElementById("ContentPlaceHolder1_txtDealID").value = DealID;
            document.getElementById("ContentPlaceHolder1_btnGridColumn").click();
        };

        function notesColumnClick(DealID) {
            document.getElementById("ContentPlaceHolder1_txtNoteDealID").value = DealID;
            document.getElementById("ContentPlaceHolder1_btnGridNotes").click();
        };

        function callColumnClick(DealID) {
            document.getElementById("ContentPlaceHolder1_txtNoteDealID").value = DealID;
            document.getElementById("ContentPlaceHolder1_btnPhoneCall").click();
        };

        function objectPhoneCall() {
            var notesFindBox = "#div_PhoneCall-box";
            //Fade in the Popup
            $(notesFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(notesFindBox).height() + 24) / 2;
            var popMargLeft = ($(notesFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

    </script>
    <script>
        $(document).ready(function () {
            $('.link').click(function () {
                if ($('.link').attr('class') == 'link open') {
                    $(this).removeClass('open')
                    $('.leftbox').animate({ 'left': '-520' });
                } else {
                    $(this).addClass('open')
                    $('.leftbox').animate({ 'left': '0' });
                }
            });
        });
        function chartButton() {
            if ($('.link').attr('class') == 'link open') {
                $(this).removeClass('open')
                $('.leftbox').animate({ 'left': '-520' });
            } else {
                $(this).addClass('open')
                $('.leftbox').animate({ 'left': '0' });
            }
        }
    </script>
    <style>
        .CompletionListCssClass
        {
            margin: 0px !important;
            background-color: inherit;
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: 'default';
            overflow: auto;
            height: 200px;
            text-align: left;
            list-style-type: none;
            z-index: 99999999 !important;
        }
        .CompletionListItemCssClass
        {
            background-color: window;
            color: windowtext;
            padding: 1px;
        }
        
        body
        {
            padding: 0;
            margin: 0ee;
        }
        .leftbox
        {
            position: absolute;
            top: 130px;
            left: -520px;
        }
        .feedback
        {
            width: 500px;
            background: #eee;
            float: left;
            padding: 10px;
            border: 1px solid #ccc;
        }
        .link
        {
            float: left;
            padding: 10px;
            background: #eee;
            border: 1px solid #ccc;
            position: relative;
            z-index: 5;
            margin-left: -1px;
        }
        .link a
        {
            color: #333;
        }
    </style>
    <asp:TextBox ID="txtDealID" Style="display: none;" Text="" runat="server"></asp:TextBox>
    <asp:TextBox ID="txtNoteDealID" Style="display: none;" Text="" runat="server"></asp:TextBox>
    <%--<asp:TextBox ID="txtDelDocuments" Style="display: none;" Text="" runat="server"></asp:TextBox>--%>
    <div id="div_PhoneCall-box" class="message-popup">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div11" style="display: block;">
            <div id="div12" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="Iframe3" frameborder="0" src="ObjectPhoneCalls.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%" height="495px"></iframe>
            </div>
        </div>
    </div>
    <div id="div_Notes-box" class="message-popup">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div9" style="display: block;">
            <div id="div10" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="Iframe2" frameborder="0" src="ObjectNotes.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%" height="495px"></iframe>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="Script/jqmin.js"></script>
    <script type="text/javascript">
        $('#ContentPlaceHolder1_btnAdvanceFind').click(function () {
            alert("AS");
            var popw = $('#div_AdvanceFind-box').width();
            var poph = $('#div_AdvanceFind-box').height();
            alert(popw);
            var ww = $(window).width();
            var wh = $(window).height();
            $('#div_AdvanceFind-box').css({ 'left': (ww - popw) / 2, 'top': (wh - poph) / 2 });
        });
    	
    </script>
    <div id="div_AdvanceFind-box" class="message-popup" style="overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div2" style="display: block;">
            <div id="div3" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="ifrmAdvanceFind" frameborder="0" src="AdvanceFind.aspx" onload="ImagesDone();"
                    style="overflow: scroll;" width="100%" height="495px"></iframe>
            </div>
        </div>
    </div>
    <div id="div_EditColumns-box" class="message-popup" style="overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div1" style="display: block;">
            <div id="div4" style="overflow: scroll; width: 100%; height: 97% !important;">
            </div>
        </div>
    </div>
    <div id="div_Customize-box" class="message-popup" style="overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div6" style="display: block;">
            <div id="div7" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="Iframe1" frameborder="0" src="CustomizeObject.aspx" onload="ImagesDone();"
                    style="overflow: scroll; min-height: 560px;" width="100%"></iframe>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div id="div_CreateDeal-box" class="message-popup" style="overflow-x: hidden;">
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="showpage" style="display: block; overflow-x: scroll; width: 100%; height: 97% !important;">
                    <div id="divIframe" runat="server">
                        <div>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Button ID="btnSave" runat="server" CssClass="save" ValidationGroup="createValidation"
                                                        OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnEditSalesProcess" CssClass="editSalesProcess" OnClick="btnEditSalesProcess_Click"
                                                        runat="server" Text="" />
                                                    <asp:Button ID="btnQualify" OnClick="btnQualify_Click" CssClass="qualify" ValidationGroup="createValidation"
                                                        runat="server" Text="" />
                                                    <asp:Button ID="btnDisqualify" OnClick="btnDisqualify_Click" ValidationGroup="createValidation"
                                                        CssClass="disqualify" runat="server" Text="" />
                                                    <asp:Button ID="btnReactivate" runat="server" Visible="false" CssClass="reactivateDeal"
                                                        Text="" OnClick="btnReactivate_Click" />
                                                    <asp:Button ID="btnCloseWon" Visible="false" CssClass="closeaswon" runat="server"
                                                        OnClick="btnCloseWon_Click" Text="" />
                                                    <asp:Button ID="btnCloseasLost" Visible="false" CssClass="closeaslost" runat="server"
                                                        OnClick="btnCloseLost_Click" Text="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    > Lead
                                                </td>
                                                <td>
                                                    <div style="float: right;">
                                                        <asp:PlaceHolder ID="placeholderHeaderInfo" runat="server"></asp:PlaceHolder>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <div id="dvStages" runat="server" style="float: left;">
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="border: 1px solid #0072c6 !important;">
                                                                <asp:TextBox ID="txtSelObjectID" Enabled="false" runat="server" Style="display: none;"></asp:TextBox>
                                                                <asp:TextBox ID="txtStageID" Text="0" Enabled="false" runat="server" Style="display: none;"></asp:TextBox>
                                                                <asp:Button ID="btnStageID" ValidationGroup="createValidation" runat="server" OnClick="btnStageID_Click"
                                                                    Style="display: none;" />
                                                                <asp:PlaceHolder ID="placeholderSalesProcess" runat="server"></asp:PlaceHolder>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <table width="100%">
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <asp:PlaceHolder ID="placeholderGeneralInfo" runat="server"></asp:PlaceHolder>
                                                    <asp:Label ID="lblLeadID" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="width: 80%; vertical-align: top; text-align: left; float: right;">
                                                                <iframe id="Iframe4" src="Notes.aspx" style="width: 100%; border: none; min-height: 270px;
                                                                    overflow-x: hidden; overflow-y: scroll;" runat="server"></iframe>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!---->
            <div id="div_SalesProcess-box" class="salesprocess-popup" style="overflow: hidden;">
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeEditSalesWindow()"
                        title="Close Window" alt="Close" /></a>
                <div id="Div8" style="display: block; overflow: hidden;" width="100%" height="100%">
                    <div id="dvObjectColumns" runat="server" style="overflow: hidden; width: 100%; height: 97% !important;">
                    </div>
                    <div style="overflow: hidden; width: 100%; height: 97% !important;">
                        <asp:TextBox ID="txtObjectID" Enabled="false" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:Button ID="btnObjectID" runat="server" OnClick="txtObjectID_Click" Style="display: none;" />
                        <table id="tbNewStage" runat="server">
                            <tr>
                                <td style="width: 250px; vertical-align: top;">
                                    <asp:TextBox runat="server" placeholder="Stage Name" ID="txtNewStageName"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewStageName"
                                        Display="Dynamic" ValidationGroup="newStageStep" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 210px; vertical-align: top;">
                                    <asp:TextBox ID="txtNewStageStep" placeholder="Step Name" runat="server" />
                                    <asp:RequiredFieldValidator ID="rqnewStageStep" runat="server" ControlToValidate="txtNewStageStep"
                                        Display="Dynamic" ValidationGroup="newStageStep" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 210px; vertical-align: top;">
                                    <asp:DropDownList ID="ddlNewInsertObjectColumns" DataTextField="GridDisplayName"
                                        OnLoad="ddlInsertObjectColumns_load" DataValueField="FormID" runat="server">
                                    </asp:DropDownList>
                                    <asp:Button ID="InsertButton" ValidationGroup="newStageStep" runat="server" OnClick="btnSaveProcess_Click"
                                        CssClass="savebtn" Text="New Stage" />
                                </td>
                            </tr>
                        </table>
                        <asp:ListView ID="lsSalesProcessStages" runat="server" DataKeyNames="ID" InsertItemPosition="LastItem"
                            OnItemCommand="lsSalesProcessStages_ItemCommand">
                            <LayoutTemplate>
                                <table border="0" cellpadding="1" style="width: 100%;">
                                    <tr style="">
                                        <th align="left" style="width: 253px;">
                                            STAGES
                                        </th>
                                        <th align="left" style="width: 208px;">
                                            STEPS
                                        </th>
                                        <th align="left" style="width: 202px;">
                                            FIELDS
                                        </th>
                                        <th align="left">
                                            Action
                                        </th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 255px; vertical-align: top;">
                                            <asp:Label runat="server" ID="lblStageName" Text='<%#Eval("StageName") %>'></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:ListView ID="lvStageFields" runat="server" DataKeyNames="StageID" DataSource='<%# Eval("ProcessSetps") %>'
                                                ItemPlaceholderID="addressPlaceHolder" InsertItemPosition="LastItem" OnItemCommand="ListView1_ItemCommand">
                                                <LayoutTemplate>
                                                    <table cellpadding="0" cellspacing="0">
                                                        <asp:PlaceHolder runat="server" ID="addressPlaceHolder" />
                                                    </table>
                                                    <hr />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblStepID" Style="display: none;" runat="server" Text='<%# Eval("StepID") %>'></asp:Label>
                                                                <asp:TextBox ID="txtStepName" Style="width: 210px;" Enabled="false" Text='<%# Eval("StepName") %>'
                                                                    runat="server" />
                                                            </td>
                                                            <td style="width: 210px;">
                                                                <asp:DropDownList ID="ddlObjectColumns" Style="width: 210px;" Enabled="false" DataSource='<%# Eval("EntityColumns") %>'
                                                                    DataTextField="GridDisplayName" OnDataBound="ddlColumns_load" DataValueField="FormID"
                                                                    runat="server">
                                                                </asp:DropDownList>
                                                                <%--<asp:Label ID="lblObjectUserID" Text='<%# Eval("ColumnName") %>' runat="server" />--%>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="DeleteStageButton" CommandName='<%# Eval("StageID") %>' runat="server"
                                                                    OnClick="btnDelete_Click" Style="width: 18px; height: 17px;" ImageUrl="~/images/delete.png" />
                                                                <%--<asp:Button ID="DeleteStageButton" CommandName='<%# Eval("StageID") %>' runat="server"
                                                                    OnClick="btnDelete_Click" CssClass="button" Style="width: 130px;" Text="Delete" />--%>
                                                            </td>
                                                        </tr>
                                                        <%--<tr>
                                                            <td>
                                                                <asp:Button ID="btnNewField" OnClick="btnNewStage_Click" runat="server" Text="New Field" />
                                                            </td>
                                                        </tr>--%>
                                                    </table>
                                                </ItemTemplate>
                                                <InsertItemTemplate>
                                                    <tr>
                                                        <td style="width: 255px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 210px; padding-left: 4px;">
                                                            <asp:TextBox ID="txtStepName" Style="width: 210px;" placeholder="Step Name" runat="server" />
                                                            <asp:RequiredFieldValidator ID="rqStepName" runat="server" ControlToValidate="txtStepName"
                                                                Display="Dynamic" ValidationGroup="stepName" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td style="width: 210px; padding-left: 4px; float: left;">
                                                            <asp:DropDownList ID="ddlInsertObjectColumns" Style="width: 210px;" DataTextField="GridDisplayName"
                                                                OnLoad="ddlInsertObjectColumns_load" DataValueField="FormID" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="float: left; padding-left: 4px;">
                                                            <asp:Button ID="InsertButton" ValidationGroup="stepName" runat="server" CommandName="Insert"
                                                                CssClass="savebtn" Text="New Step" />
                                                        </td>
                                                    </tr>
                                                </InsertItemTemplate>
                                            </asp:ListView>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <InsertItemTemplate>
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="vertical-align: top; width: 28%; float: left;">
                                            <asp:TextBox runat="server" Width="97%" placeholder="Stage Name" ID="txtNewStageName"></asp:TextBox>
                                        </td>
                                        <td style="vertical-align: top; width: 23.2%; float: left;">
                                            <asp:TextBox ID="txtNewStageStep" Width="97%" placeholder="Step Name" runat="server" />
                                            <%--<asp:RequiredFieldValidator ID="rqnewStageStep" runat="server" ControlToValidate="txtNewStage"
                                            Display="Dynamic" ValidationGroup="newStageStep" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                        </td>
                                        <td style="vertical-align: top; width: 33.2%; float: left;">
                                            <asp:Button ID="InsertButton" ValidationGroup="newStageStep" runat="server" CommandName="Insert"
                                                CssClass="savebtn" Style="float: right;" Text="New Stage" />
                                            <asp:DropDownList ID="ddlNewInsertObjectColumns" Style="vertical-align: top;" Width="68%"
                                                DataTextField="GridDisplayName" OnLoad="ddlInsertObjectColumns_load" DataValueField="FormID"
                                                runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </InsertItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
            <!---->
            <div class="gradientbginner" style="width: 90%; overflow: auto;">
                <table>
                    <tr>
                        <td width="18px">
                        </td>
                        <td>
                            <div class="maincontent">
                                <div class="searchboxcaption">
                                    <span>Deals</span></div>
                                <div class="searchbox">
                                    <div>
                                        <table width="100%">
                                            <tr>
                                                <%--<td align="center" valign="middle" style="padding-right: 3px; padding-left: 3px;
                                                    padding-bottom: 3px; padding-top: 3px">--%><div style="text-align: center;">
                                                        <asp:Label ID="lblListmsg" Style="text-align: center; color: Red; font-size: 13px;"
                                                            runat="server" EnableViewState="False"></asp:Label>
                                                    </div>
                                                <%--</td>--%>
                                                <td>
                                                    <asp:Button ID="btnCreateDeal" CssClass="create_dtl" OnClick="btnEdit_Click" Text=""
                                                        runat="server" />
                                                    <asp:Button ID="btnAdvanceFind" CssClass="advance_find" OnClientClick="advanceFind();"
                                                        Text="" runat="server" />
                                                    <asp:Button ID="btnCustomizeObject" CssClass="customize" Text="" OnClientClick="customizeForm()"
                                                        runat="server" />
                                                    <asp:Button ID="btnExcel" CssClass="export" Text="" OnClick="btnExcel_Click" runat="server" />
                                                    <asp:Button ID="btnImport" CssClass="import" Text="" runat="server" />
                                                    <input type="button" class="delete_data" value="" />
                                                    <asp:Button ID="btnDeleteAll" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAll_Click"
                                                        OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Deal" CausesValidation="False" />
                                                </td>
                                            </tr>
                                            <tr id="trFilter" runat="server">
                                                <td align="center" valign="top">
                                                    <table class="customwd" style="float: left; border-right: gray 1px solid; border-top: gray 1px solid;
                                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                                        <tr>
                                                            <th align="left" colspan="2">
                                                                Filter Records
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;">
                                                                <asp:DropDownList ID="ddlFilters" Width="300px" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:DropDownCheckBoxes ID="ddlGridColumns1" runat="server" OnSelectedIndexChanged="checkBoxes_SelcetedIndexChanged"
                                                                    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                                                                    <Style SelectBoxWidth="300" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="120" />
                                                                    <Texts SelectBoxCaption="Columns" />
                                                                </asp:DropDownCheckBoxes>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="trGridView" runat="server">
                                                <td align="center" valign="top" style="max-height: 230px;">
                                                    <div style="overflow: auto; max-height: 230px;" class="customwd">
                                                        <asp:GridView ID="dataGridDeals" runat="server" AutoGenerateColumns="true" Width="100%"
                                                            DataKeyField="LeadID" DataKeyNames="LeadID" AllowPaging="true" PageSize="10"
                                                            OnPageIndexChanging="dataGridDeals_PageIndexChanging" AllowSorting="true" OnSorting="dataGridDeals_Sorting"
                                                            EmptyDataText="No Record Found" OnDataBound="dataGridDeals_DataBound" OnRowCreated="dataGridDeals_RowCreated"
                                                            OnRowDataBound="dataGridDeals_RowDataBound">
                                                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <%--<asp:ImageButton ID="imgPageFirst" runat="server" ImageUrl="~/Images/Icons/PageFirst.png"
                                                                CommandArgument="First" CommandName="Page" OnCommand="imgPageFirst_Command"></asp:ImageButton>--%>
                                                                <asp:Button ID="imgPageFirst" runat="server" Text="First" CommandArgument="First"
                                                                    CommandName="Page" OnCommand="imgPageFirst_Command" />
                                                                <%--<asp:ImageButton ID="imgPagePrevious" runat="server" ImageUrl="~/Images/Icons/PagePrevious.png"
                                                                CommandArgument="Prev" CommandName="Page" OnCommand="imgPagePrevious_Command">
                                                            </asp:ImageButton>--%>
                                                                <asp:Button ID="imgPagePrevious" runat="server" Text="Prev" CommandArgument="Prev"
                                                                    CommandName="Page" OnCommand="imgPagePrevious_Command" />
                                                                <asp:DropDownList ID="ddCurrentPage" runat="server" CssClass="Normal" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddCurrentPage_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblOf" runat="server" Text="of" CssClass="Normal"></asp:Label>
                                                                <asp:Label ID="lblTotalPage" runat="server" CssClass="Normal"></asp:Label>
                                                                <%--<asp:ImageButton ID="imgPageNext" runat="server" ImageUrl="~/Images/Icons/PageNext.png"
                                                                CommandArgument="Next" CommandName="Page" OnCommand="imgPageNext_Command"></asp:ImageButton>
                                                            <asp:ImageButton ID="imgPageLast" runat="server" ImageUrl="~/Images/Icons/PageLast.png"
                                                                CommandArgument="Last" CommandName="Page" OnCommand="imgPageLast_Command"></asp:ImageButton>--%>
                                                                <asp:Button ID="imgPageNext" runat="server" Text="Next" CommandArgument="Next" CommandName="Page"
                                                                    OnCommand="imgPageNext_Command"></asp:Button>
                                                                <asp:Button ID="imgPageLast" runat="server" Text="Last" CommandArgument="Last" CommandName="Page"
                                                                    OnCommand="imgPageLast_Command"></asp:Button>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trButtons" runat="server">
                                                <td align="left" valign="top">
                                                    <table id="tblDeleteAll" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                            </td>
                                                            <td class="NormalText">
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnEdit" runat="server" Style="display: none;" CssClass="button"
                                                                    Text="Edit Deal" OnClick="btnEdit_Click" ToolTip="Edit Selected Deal" CausesValidation="False" />
                                                                <asp:Button ID="btnGridColumn" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnGridColumn_Click" CausesValidation="False" />
                                                                <asp:Button ID="btnGridNotes" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnGridNotes_Click" CausesValidation="False" />
                                                                <asp:Button ID="btnPhoneCall" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnPhoneCall_Click" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div class="leftbox">
                                                        <div class="feedback">
                                                            <div>
                                                                <asp:DropDownList ID="ddlChartType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChartType_SelectedIndexChanged">
                                                                    <asp:ListItem Text="Deals By Owner" Value="1"></asp:ListItem>
                                                                    <asp:ListItem Text="Deals By Status" Value="2"></asp:ListItem>
                                                                </asp:DropDownList>
                                                            </div>
                                                            <div>
                                                                <jqChart:Chart ID="statusChart" Width="500px" Height="300px" runat="server">
                                                                    <Title Text=""></Title>
                                                                    <Animation Enabled="True" Duration="00:00:02" />
                                                                    <Axes>
                                                                        <jqChart:CategoryAxis Location="Bottom" ZoomEnabled="false">
                                                                        </jqChart:CategoryAxis>
                                                                    </Axes>
                                                                    <Series>
                                                                        <%--<jqChart:ColumnSeries XValuesField="Label" YValuesField="Value1" Title="Column">
                                                            </jqChart:ColumnSeries>
                                                            <jqChart:LineSeries XValuesField="Label" YValuesField="Value1" Title="Line">
                                                            </jqChart:LineSeries>--%>
                                                                        <jqChart:PieSeries AxisX="Label" AxisY="Value1" DataLabelsField="Label" DataValuesField="Value1"
                                                                            Labels-ValueType="DataValue" Title="Pie">
                                                                        </jqChart:PieSeries>
                                                                    </Series>
                                                                </jqChart:Chart>
                                                                <jqChart:Chart ID="Chart1" Width="500px" Height="300px" runat="server">
                                                                    <Title Text=""></Title>
                                                                    <Animation Enabled="True" Duration="00:00:02" />
                                                                    <Axes>
                                                                        <jqChart:CategoryAxis Location="Bottom" ZoomEnabled="false">
                                                                        </jqChart:CategoryAxis>
                                                                    </Axes>
                                                                    <Series>
                                                                        <jqChart:ColumnSeries XValuesField="Label" YValuesField="Value1" Title="Column">
                                                                        </jqChart:ColumnSeries>
                                                                        <jqChart:LineSeries XValuesField="Label" YValuesField="Value1" Title="Line">
                                                                        </jqChart:LineSeries>
                                                                    </Series>
                                                                </jqChart:Chart>
                                                            </div>
                                                        </div>
                                                        <div class="link">
                                                            <a href="#"><b>Chart</b></a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="ddlFilters" />
            <asp:PostBackTrigger ControlID="btnExcel" />
            <asp:PostBackTrigger ControlID="ddlChartType" />
            <asp:PostBackTrigger ControlID="btnSave" />
            <asp:PostBackTrigger ControlID="btnEdit" />
            <asp:PostBackTrigger ControlID="btnGridColumn" />
            <asp:PostBackTrigger ControlID="btnGridNotes" />
            <asp:PostBackTrigger ControlID="btnPhoneCall" />
            <asp:PostBackTrigger ControlID="InsertButton" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
