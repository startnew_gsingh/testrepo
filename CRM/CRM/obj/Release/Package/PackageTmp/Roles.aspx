﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Roles.aspx.cs" Inherits="CRM.Roles" %>

<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <style>
        #UpdateProgress2
        {
            display: none !important;
        }
        .message-popup
        {
            width: 300px;
            max-height: 500px;
            min-height: 100px;
            margin: 0 auto;
            left: 0;
            right: 0;
            top: auto;
        }
    </style>
    <script type="text/javascript">

        function createRole() {
            var createRoleBox = "#div_CreateRole-box";
            //Fade in the Popup
            $(createRoleBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createRoleBox).height() + 24) / 2;
            var popMargLeft = ($(createRoleBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };


        function editRolePermissions() {
            var editRolePermissionsBox = "#div_editRolePermissions-box";
            //Fade in the Popup
            $(editRolePermissionsBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(editRolePermissionsBox).height() + 24) / 2;
            var popMargLeft = ($(editRolePermissionsBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);
            return true;
        };

        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                //window.location = "Roles.aspx";
                document.getElementById("ContentPlaceHolder1_btnRoles").click();
            });
            return false;
        }
    </script>
    <div id="div_CreateRole-box" class="message-popup" style="overflow: hidden;">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="showpage" style="display: block;">
                    <div id="divIframe" runat="server" style="overflow: auto; width: 100%; height: 100% !important;">
                        <div>
                            <table>
                                <tr>
                                    <td>
                                        <asp:Label ID="lblName" Text="Name" runat="server"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtRoleName" runat="server"></asp:TextBox>
                                        <asp:Label ID="lblRoleID" Visible="false" Text="0" runat="server"></asp:Label>
                                        <asp:RequiredFieldValidator ID="rqName" runat="server" ControlToValidate="txtRoleName"
                                            ErrorMessage="*" CssClass="Star" SetFocusOnError="true" ValidationGroup="Roles"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClick="btnSave_Click"
                                            ValidationGroup="Roles" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="div_editRolePermissions-box" class="message-popup" style="overflow: hidden;
        width: 65%;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" style="margin-right: -7px;" onclick="closeWindow()"
                title="Close Window" alt="Close" /></a>
        <div id="Div2" style="display: block;">
            <div id="div3" runat="server" style="overflow: auto; width: 100%; height: 100% !important;">
                <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                    <ContentTemplate>
                        <table width="100%">
                            <tr>
                                <td>
                                    <asp:ListView ID="lstRolePermissions" runat="server" DataKeyNames="ID">
                                        <LayoutTemplate>
                                            <table border="0" cellpadding="1" width="100%">
                                                <tr style="background-color: #E5E5FE; color: White;">
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkentity" Style="color: White;" runat="server">Entity</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkCreate" Style="color: White;" runat="server">Create</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkEdit" Style="color: White;" runat="server">Edit</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkDelete" Style="color: White;" runat="server">Delete</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkImport" Style="color: White;" runat="server">Import</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkExport" Style="color: White;" runat="server">Export</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="lnkRead" Style="color: White;" runat="server">Read</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="LinkButton1" Style="color: White;" runat="server">Customize</asp:LinkButton>
                                                    </th>
                                                    <th align="left">
                                                        <asp:LinkButton ID="LinkButton2" Style="color: White;" runat="server">Advance Find</asp:LinkButton>
                                                    </th>
                                                </tr>
                                                <tr id="itemPlaceholder" runat="server">
                                                </tr>
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td style="text-align: left;">
                                                    <asp:Label runat="server" ClientIDMode="AutoID" ID="chkEntity" Text='<%#Eval("ObjectName") %>'></asp:Label>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" ClientIDMode="AutoID" AutoPostBack="true" OnCheckedChanged="Check_Clicked"
                                                        ID="Create" Checked='<%#Eval("Create") %>'></asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" ClientIDMode="AutoID" AutoPostBack="true" OnCheckedChanged="Check_Clicked"
                                                        ID="Edit" Checked='<%#Eval("Edit") %>'></asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" ClientIDMode="AutoID" AutoPostBack="true" OnCheckedChanged="Check_Clicked"
                                                        ID="Delete" Checked='<%#Eval("Delete") %>'></asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" ClientIDMode="AutoID" AutoPostBack="true" OnCheckedChanged="Check_Clicked"
                                                        ID="Import" Checked='<%#Eval("Import") %>'></asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" ClientIDMode="AutoID" AutoPostBack="true" OnCheckedChanged="Check_Clicked"
                                                        ID="Export" Checked='<%#Eval("Export") %>'></asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" ClientIDMode="AutoID" AutoPostBack="true" OnCheckedChanged="Check_Clicked"
                                                        ID="Read" Checked='<%#Eval("Read") %>'></asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" ClientIDMode="AutoID" AutoPostBack="true" OnCheckedChanged="Check_Clicked"
                                                        ID="Customize" Checked='<%#Eval("Customize") %>'></asp:CheckBox>
                                                </td>
                                                <td style="text-align: center;">
                                                    <asp:CheckBox runat="server" ClientIDMode="AutoID" AutoPostBack="true" OnCheckedChanged="Check_Clicked"
                                                        ID="AdvanceFind" Checked='<%#Eval("AdvanceFind") %>'></asp:CheckBox>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:Button ID="btnRolePermissions" runat="server" CssClass="btn btn-primary" Text="Save"
                                        OnClick="btnRolePermissions_Click" />
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <div class="gradientbginner" style="width: 90%; overflow: auto;">
                <table width="100%">
                    <tr>
                        <td width="18px">
                        </td>
                        <td>
                            <div class="maincontent" style="width: 100%;">
                                <div class="searchboxcaption">
                                    <span>Roles</span></div>
                                <div class="searchbox">
                                    <div>
                                        <table width="100%">
                                            <tr>
                                                <div style="text-align: center;">
                                                    <asp:Label ID="lblListmsg" Style="text-align: center; color: Red; font-size: 13px;"
                                                        runat="server" EnableViewState="False"></asp:Label>
                                                </div>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" style="border-right: gray 1px solid; border-top: gray 1px solid;
                                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                                        <tr>
                                                            <th align="left" colspan="2">
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <asp:Button ID="btnAddRole" CssClass="create_dtl" OnClick="btnCreate_Click" runat="server"
                                                                    Text="" />
                                                                <asp:Button ID="btnDeleteAll" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAll_Click"
                                                                    OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Role" CausesValidation="False" />
                                                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit Role" OnClick="btnEdit_Click"
                                                                    ToolTip="Edit Selected Role" CausesValidation="False" Style="display: none;" />
                                                                <asp:Button ID="btnRoles" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnRoles_Click" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top" style="max-height: 230px;">
                                                    <asp:GridView ID="dataGridRole" runat="server" AutoGenerateColumns="false" Width="100%"
                                                        DataKeyField="ID" DataKeyNames="ID" AllowPaging="true" PageSize="10" AllowSorting="true"
                                                        OnSorting="dataGridRole_Sorting" OnPageIndexChanging="dataGridRole_PageIndexChanging"
                                                        EmptyDataText="No Record Found" OnRowDataBound="dataGridRole_RowDataBound" OnDataBound="dataGridRole_DataBound"
                                                        OnRowCreated="dataGridRole_RowCreated">
                                                        <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Name
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="lnkName" OnClick="div_editRolePermissions" CommandArgument='<%#Eval("ID") %>'
                                                                        runat="server" Text='<%#Eval("RoleName") %>'></asp:LinkButton>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <%--<asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    Name
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblRoleName" runat="server" Text='<%#Eval("RoleName") %>'></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>--%>
                                                        </Columns>
                                                        <PagerTemplate>
                                                            <asp:Button ID="imgPageFirst" runat="server" Text="First" CommandArgument="First"
                                                                CommandName="Page" OnCommand="imgPageFirst_Command" />
                                                            <asp:Button ID="imgPagePrevious" runat="server" Text="Prev" CommandArgument="Prev"
                                                                CommandName="Page" OnCommand="imgPagePrevious_Command" />
                                                            <asp:DropDownList ID="ddCurrentPage" runat="server" CssClass="Normal" AutoPostBack="True"
                                                                OnSelectedIndexChanged="ddCurrentPage_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblOf" runat="server" Text="of" CssClass="Normal"></asp:Label>
                                                            <asp:Label ID="lblTotalPage" runat="server" CssClass="Normal"></asp:Label>
                                                            <asp:Button ID="imgPageNext" runat="server" Text="Next" CommandArgument="Next" CommandName="Page"
                                                                OnCommand="imgPageNext_Command"></asp:Button>
                                                            <asp:Button ID="imgPageLast" runat="server" Text="Last" CommandArgument="Last" CommandName="Page"
                                                                OnCommand="imgPageLast_Command"></asp:Button>
                                                        </PagerTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table id="tblDeleteAll" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                            </td>
                                                            <td class="NormalText">
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
