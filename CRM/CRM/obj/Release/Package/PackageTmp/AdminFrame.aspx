<%@ Page Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true"
    CodeFile="AdminFrame.aspx.cs" Inherits="CRM.AdminFrame" Title="CRM" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div id="hidepage" align="left" style="z-index: 90; vertical-align: middle; left: 0px;
            top: 0px; background-color: White; width: 600pt; height: 200pt; background-image: url(/../images/bg.jpg);
            background-repeat: repeat-x;" class="buttontext">
            <table id="tblLoader" style="vertical-align: middle; z-index: 100; width: 437px;"
                align="center" border="0" class="innertextheading">
                <tr>
                    <td align="center" colspan="2" rowspan="1" class="NormalText">
                        <img src="images/progress_bar.gif" height="15" width="110" />
                    </td>
                </tr>
                <tr>
                    <td rowspan="1" align="center" colspan="2" class="NormalText">
                        Loading...
                    </td>
                </tr>
                <tr>
                    <td rowspan="1" align="center" colspan="2" class="NormalText">
                        Please Wait.
                    </td>
                </tr>
            </table>
        </div>
        <div id="showpage" style="display: block;">
            <div id="divIframe" style="overflow: auto; width: 100%; height:100% !important;">
                <iframe id="ifrmMain" frameborder="0" src="AdminHome.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%" height="100%"></iframe>
            </div>
        </div>
    </div>
</asp:Content>
