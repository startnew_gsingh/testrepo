﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ObjectPhoneCalls.aspx.cs"
    Inherits="CRM.ObjectPhoneCalls" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <link rel="Stylesheet" type="text/css" href="css/Mystyle.css" />
    <%-- <style>
        #UpdateProgress2
        {
            display: none !important;
        }
    </style>--%>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="mgr" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel3" runat="server">
        <ContentTemplate>
            <asp:GridView ID="gvPhoneCalls" runat="server" AutoGenerateColumns="false" Width="100%">
                <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Description
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("Description") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Date
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("DueDate") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Direction
                        </HeaderTemplate>
                        <ItemTemplate>
                            <%#Eval("Direction") %>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--<div>
        <table style="width: 100%;">
            <tr>
                <td style="vertical-align: top;">
                    <b>Calls</b><br />
                    <table>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtPhoneCallDescription" Placeholder="Description" TextMode="MultiLine"
                                    Style="min-height: 70px; min-width: 99%;" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtCallDate" Placeholder="Due date" Style="width: 99%;" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:DropDownList ID="ddlDirection" Style="width: 99%;" runat="server">
                                    <asp:ListItem Text="Outgoing" Value="Outgoing"></asp:ListItem>
                                    <asp:ListItem Text="Incoming" Value="Incoming"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="Button2" runat="server" CssClass="button" Text="Save" OnClick="btnUploadPhoneCalls_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    
                </td>
            </tr>
        </table>
    </div>--%>
    </form>
</body>
</html>
