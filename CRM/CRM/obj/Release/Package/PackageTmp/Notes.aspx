﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Notes.aspx.cs" Inherits="CRM.Notes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <link href="css/multiview.css" rel="stylesheet" type="text/css" />
    <link href="css/css.css" rel="stylesheet" type="text/css" />
    <link href="css/Mystyle.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript" src="Script/MasterJS.js"></script>
    <script language="javascript" type="text/javascript" src="Script/GridView.js"></script>
    <script type="text/javascript">

        function delDocumentOnly(id, notesID) {
            $('#' + id).hide();
            $('#' + notesID).hide();
            var oldIdValue = $("#txtDelDocumentsOnly").val();
            if (oldIdValue != "") {
                oldIdValue = oldIdValue + "," + id.replace('deldocs', '');
            }
            else {
                oldIdValue = id.replace('deldocs', '');
            }
            $("#txtDelDocumentsOnly").val(oldIdValue);
            $("#btnDelDocs").click();
        }

        function delDocument(id, notesID) {
            $('#' + id).hide();
            $('#' + notesID).hide();
            var oldIdValue = $("#txtDelDocuments").val();
            if (oldIdValue != "") {
                oldIdValue = oldIdValue + "," + id.replace('del', '');
            }
            else {
                oldIdValue = id.replace('del', '');
            }
            $("#txtDelDocuments").val(oldIdValue);
            $("#btnDeleteNote").click();
        }

        function delTask(id, taskID, tasksDesc) {
            $('#' + id).hide();
            $('#' + taskID).hide();
            $('#' + tasksDesc).hide();
            var oldIdValue = $("#txtDelTasks").val();
            if (oldIdValue != "") {
                oldIdValue = oldIdValue + "," + id.replace('delTasks', '');
            }
            else {
                oldIdValue = id.replace('delTasks', '');
            }
            $("#txtDelTasks").val(oldIdValue);
            $("#btnDeleteTasks").click();
        }

        function delPhoneCalls(id, callID, phoneCallsDesc, descCall) {
            $('#' + id).hide();
            $('#' + callID).hide();
            $('#' + phoneCallsDesc).hide();
            $('#' + descCall).hide();
            var oldIdValue = $("#txtDelPhoneCalls").val();
            if (oldIdValue != "") {
                oldIdValue = oldIdValue + "," + id.replace('delPhoneCalls', '');
            }
            else {
                oldIdValue = id.replace('delPhoneCalls', '');
            }
            $("#txtDelPhoneCalls").val(oldIdValue);
            $("#btnDelPhoneCalls").click();
        }

        function delDiscussion(id, DiscussionID, DiscussionDesc) {
            $('#' + id).hide();
            $('#' + DiscussionID).hide();
            $('#' + DiscussionDesc).hide();
            var oldIdValue = $("#txtDelDiscussion").val();
            if (oldIdValue != "") {
                oldIdValue = oldIdValue + "," + id.replace('delDiscussion', '');
            }
            else {
                oldIdValue = id.replace('delDiscussion', '');
            }
            $("#txtDelDiscussion").val(oldIdValue);
            $("#btnDeleteDiscussion").click();
        }

        function PhoneCalls() {
            $("#tdNotes").hide();
            $("#tdTasks").hide();
            $("#tdDocuments").hide();
            $("#tdPhoneCalls").show();
            $("#tdDiscussion").hide();
        }

        function Documents() {
            $("#tdNotes").hide();
            $("#tdTasks").hide();
            $("#tdDocuments").show();
            $("#tdPhoneCalls").hide();
            $("#tdDiscussion").hide();
        }

        function Notes() {
            $("#tdNotes").show();
            $("#tdTasks").hide();
            $("#tdDocuments").hide();
            $("#tdPhoneCalls").hide();
            $("#tdDiscussion").hide();
        }
        function Tasks() {
            $("#tdNotes").hide();
            $("#tdTasks").show();
            $("#tdDocuments").hide();
            $("#tdPhoneCalls").hide();
            $("#tdDiscussion").hide();
        }

        function Discussion() {
            $("#tdNotes").hide();
            $("#tdTasks").hide();
            $("#tdDocuments").hide();
            $("#tdPhoneCalls").hide();
            $("#tdDiscussion").show();
        }

        function emailToUsers() {
            var sendEmail = "#div_emailUser-box";
            //Fade in the Popup
            $(sendEmail).fadeIn(300);

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function discussionUsers() {
            var discussionU = "#div_Discussion_box";
            //Fade in the Popup
            $(discussionU).fadeIn(300);

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function closeWindow() {
            $('#mask , .email-popup').fadeOut(300);
            $('#mask').remove();
            document.getElementById("btnRefresh").click();
            return false;
        }

        function closeAfterSaveWindow(value) {
            if (value == "Notes")
                Notes();
            if (value == "Tasks")
                Tasks();
            if (value == "PhoneCalls")
                PhoneCalls();
            if (value == "Discussion")
                Discussion();
            return false;
        }
    </script>
    <style>
        .email-popup
        {
            width: 36%;
            margin: 0 31%;
            min-height: 200px;
            max-height: 500px;
        }
    </style>
    <script src="Script/jquery.ui.timepicker.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"
        type="text/javascript"></script>
    <link href="css/ModalWindow.css" rel="stylesheet" type="text/css" />
    <link href="css/jquery-ui-1.8.19.custom.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function pageLoad() {
            $(function () {
                $("#txtDueDate").datepicker({ minDate: 0 });
                $("#txtCallDate").datepicker({ minDate: 0 });
            });
        }
    </script>
</head>
<body style="background: none !important;">
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="UpdateProgress2" runat="server">
        <ProgressTemplate>
            <div style="position: absolute; width: 100%; height: 100%; opacity: 0.8; z-index: 999999;
                background: #000; filter: alpha(opacity=80); -moz-opacity: 0.8; opacity: 0.8;">
                <img style="position: absolute; left: 45%; top: 45%;" src="images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="div_emailUser-box" class="email-popup" style="overflow: hidden;">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div12" style="display: block;">
                    <div id="div13" style="overflow: scroll; width: 100%; height: 97% !important;">
                        <asp:GridView ID="dataGridUser" runat="server" AutoGenerateColumns="false" Width="100%"
                            DataKeyField="OrgID" DataKeyNames="OrgID" AllowPaging="false" AllowSorting="true"
                            EmptyDataText="No Record Found">
                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Email
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Name
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("FirstName") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <tr>
                    <td align="left" valign="top">
                        <table id="Table1" runat="server">
                            <tr>
                                <td align="left">
                                    <asp:Image ID="Image2" runat="server" ImageUrl="images/arrow_ltr.png" />
                                </td>
                                <td class="NormalText">
                                    &nbsp; by selecting checkboxes &nbsp; &nbsp; &nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="Button3" runat="server" CssClass="button" Text="Send Email" OnClick="btnSendEmail_Click"
                                        OnClientClick="return CheckboxSelection();" CausesValidation="False" />
                                    <asp:Button ID="btnRefresh" runat="server" CssClass="button" Style="display: none;"
                                        Text="" OnClick="btnRefresh_Click" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="div_Discussion_box" class="email-popup" style="overflow: hidden;">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div2" style="display: block;">
                    <div id="div3" style="overflow: scroll; width: 100%; height: 97% !important;">
                        <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="false" Width="100%"
                            DataKeyField="OrgID" DataKeyNames="OrgID" AllowPaging="false" AllowSorting="true"
                            EmptyDataText="No Record Found">
                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Email
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserName" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        Name
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <%#Eval("FirstName") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                <tr>
                    <td align="left" valign="top">
                        <table id="Table2" runat="server">
                            <tr>
                                <td align="left">
                                    <asp:Image ID="Image1" runat="server" ImageUrl="images/arrow_ltr.png" />
                                </td>
                                <td class="NormalText">
                                    &nbsp; by selecting checkboxes &nbsp; &nbsp; &nbsp;&nbsp;
                                </td>
                                <td>
                                    <asp:Button ID="Button4" runat="server" CssClass="button" Text="Send" OnClick="btnSendDiscussion_Click"
                                        OnClientClick="return CheckboxSelection();" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div>
                <table id="tdNotes" width="100%">
                    <tr>
                        <td style="height: 34px; vertical-align: top;">
                            <b onclick="Notes();" style="font-size: 11px; margin-right: 23px; cursor: pointer;">
                                Notes(<asp:Label ID="lblNotes1" runat="server"></asp:Label>) </b><span onclick="Tasks();"
                                    style="font-size: 11px; cursor: pointer; margin-right: 40px;">Activities(<asp:Label
                                        ID="lblActivities1" runat="server"></asp:Label>)</span><span onclick="PhoneCalls();"
                                            style="font-size: 11px; margin-right: 23px; cursor: pointer;">Phone Calls(<asp:Label
                                                ID="lblPhoneCalls1" runat="server"></asp:Label>)</span><span onclick="Documents();"
                                                    style="font-size: 11px; margin-right: 23px; cursor: pointer;"> Documents(<asp:Label
                                                        ID="lblDocuments1" runat="server"></asp:Label>)</span><span onclick="Discussion();"
                                                            style="font-size: 11px; cursor: pointer;">Discussion(<asp:Label ID="Label8" runat="server"></asp:Label>)</span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtDelDocuments" Style="display: none;" Text="" runat="server"></asp:TextBox>
                            <asp:TextBox TextMode="MultiLine" Style="min-height: 70px; min-width: 90%;" ID="txtNotes"
                                runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="rqNotes" ControlToValidate="txtNotes" runat="server"
                                ErrorMessage="Please enter a value" CssClass="validationerror" ValidationGroup="rqNotes"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <%--<tr>
                <td colspan="2">
                    <asp:FileUpload runat="server" ID="fleUpload" />
                </td>
            </tr>--%>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnUploadNote" runat="server" ValidationGroup="rqNotes" CssClass="button"
                                Text="Save" OnClick="btnUploadNotes_Click" />
                            <asp:Button ID="btnDeleteNote" runat="server" Style="display: none;" CssClass="button"
                                Text="Save" OnClick="btnDeleteNotes_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="formElement" id="lstDocuments" runat="server" style="float: left; width: 250px;">
                            </div>
                        </td>
                    </tr>
                </table>
                <table id="tdTasks" style="width: 100%; display: none;">
                    <tr>
                        <td style="vertical-align: top; text-align: left;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="height: 34px; vertical-align: top;">
                                        <span onclick="Notes();" style="font-size: 11px; margin-right: 23px; cursor: pointer;">
                                            Notes(<asp:Label ID="lblNotes2" runat="server"></asp:Label>)</span> <b onclick="Tasks();"
                                                style="font-size: 11px; margin-right: 23px; cursor: pointer;">Activities(<asp:Label
                                                    ID="lblActivities2" runat="server"></asp:Label>)</b><span onclick="PhoneCalls();"
                                                        style="font-size: 11px; margin-right: 23px; cursor: pointer;">Phone Calls(<asp:Label
                                                            ID="lblPhoneCalls2" runat="server"></asp:Label>)</span><span onclick="Documents();"
                                                                style="font-size: 11px; margin-right: 23px; cursor: pointer;"> Documents(<asp:Label
                                                                    ID="lblDocuments2" runat="server"></asp:Label>)</span><span onclick="Discussion();"
                                                                        style="font-size: 11px; cursor: pointer;">Discussion(<asp:Label ID="Label7" runat="server"></asp:Label>)</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtDelTasks" Style="display: none; width: 99%;" Text="" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtSubject" Placeholder="Subject" Style="width: 99%;" runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqSubject" runat="server" ValidationGroup="rqTasks"
                                            ControlToValidate="txtSubject" ErrorMessage="*" Style="color: Red; font-weight: bold;"
                                            SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtDescription" Placeholder="Description" TextMode="MultiLine" Style="min-height: 70px;
                                            min-width: 99%;" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtDueDate" Placeholder="Due date" Style="width: 99%;" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlPrioirty" Style="width: 99%;" runat="server">
                                            <asp:ListItem Text="Normal" Value="Normal"></asp:ListItem>
                                            <asp:ListItem Text="High" Value="High"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnUploadTasks" ValidationGroup="rqTasks" runat="server" CssClass="button"
                                            Text="Save" OnClick="btnUploadTasks_Click" />
                                        <asp:Button ID="btnDeleteTasks" Style="display: none;" runat="server" CssClass="button"
                                            Text="Delete" OnClick="btnDeleteTasks_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="formElement" id="lsUploadTasks" runat="server" style="float: left; width: 350px;">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table id="tdPhoneCalls" style="width: 100%; display: none;">
                    <tr>
                        <td style="vertical-align: top; text-align: left;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="height: 34px; vertical-align: top;">
                                        <span onclick="Notes();" style="font-size: 11px; margin-right: 23px; cursor: pointer;">
                                            Notes(<asp:Label ID="lblNotes3" runat="server"></asp:Label>)</span> <span onclick="Tasks();"
                                                style="font-size: 11px; margin-right: 23px; cursor: pointer;">Activities(<asp:Label
                                                    ID="lblActivities3" runat="server"></asp:Label>)</span> <b onclick="PhoneCalls();"
                                                        style="font-size: 11px; margin-right: 23px; cursor: pointer;">Phone Calls(<asp:Label
                                                            ID="lblPhoneCalls3" runat="server"></asp:Label>)</b><span onclick="Documents();"
                                                                style="font-size: 11px; margin-right: 23px; cursor: pointer;"> Documents(<asp:Label
                                                                    ID="lblDocuments3" runat="server"></asp:Label>)</span><span onclick="Discussion();"
                                                                        style="font-size: 11px; cursor: pointer;">Discussion(<asp:Label ID="Label6" runat="server"></asp:Label>)</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtDelPhoneCalls" Style="display: none; width: 99%;" Text="" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtPhoneCallDescription" Placeholder="Description" TextMode="MultiLine"
                                            Style="min-height: 70px; min-width: 99%;" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtCallDate" Placeholder="Due date" Style="width: 99%;" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlDirection" Style="width: 99%;" runat="server">
                                            <asp:ListItem Text="Outgoing" Value="Outgoing"></asp:ListItem>
                                            <asp:ListItem Text="Incoming" Value="Incoming"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="Button2" runat="server" CssClass="button" Text="Save" OnClick="btnUploadPhoneCalls_Click" />
                                        <asp:Button ID="btnDelPhoneCalls" runat="server" Style="display: none;" CssClass="button"
                                            Text="Save" OnClick="btnDelPhoneCalls_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="formElement" id="lsUploadPhoneCalls" runat="server" style="float: left;
                                            width: 350px;">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table id="tdDocuments" style="width: 100%; display: none;">
                    <tr>
                        <td style="height: 34px; vertical-align: top;">
                            <span onclick="Notes();" style="font-size: 11px; cursor: pointer; margin-right: 40px;">
                                Notes(<asp:Label ID="lblNotes4" runat="server"></asp:Label>)</span> <span onclick="Tasks();"
                                    style="font-size: 11px; cursor: pointer; margin-right: 40px;">Activities(<asp:Label
                                        ID="lblActivities4" runat="server"></asp:Label>)</span><span onclick="PhoneCalls();"
                                            style="font-size: 11px; margin-right: 23px; cursor: pointer;">Phone Calls(<asp:Label
                                                ID="lblPhoneCalls4" runat="server"></asp:Label>)</span><b onclick="Documents();"
                                                    style="font-size: 11px; margin-right: 23px; cursor: pointer;"> Documents(<asp:Label
                                                        ID="lblDocuments4" runat="server"></asp:Label>)</b><span onclick="Discussion();"
                                                            style="font-size: 11px; cursor: pointer;">Discussion(<asp:Label ID="Label5" runat="server"></asp:Label>)</span>
                        </td>
                    </tr>
                    <%--<tr>
                <td colspan="2">
                    <asp:TextBox TextMode="MultiLine" Style="min-height: 70px; min-width: 100%;" ID="TextBox2"
                        runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtNotes"
                        runat="server" ErrorMessage="Please enter a value" CssClass="validationerror"
                        ValidationGroup="rqNotes"></asp:RequiredFieldValidator>
                </td>
            </tr>--%>
                    <tr>
                        <td colspan="2">
                            <asp:TextBox ID="txtDelDocumentsOnly" Style="display: none;" Text="" runat="server"></asp:TextBox>
                            <asp:FileUpload runat="server" ID="fleUploadDocuments" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="Button1" runat="server" CssClass="button" Text="Upload Document"
                                OnClick="btnUploadDocs_Click" />
                            <asp:Button ID="btnDelDocs" runat="server" CssClass="button" Style="display: none;"
                                Text="Del Document" OnClick="btnDelDocs_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div class="formElement" id="lstOnlyDocuments" runat="server" style="float: left;
                                width: 250px;">
                            </div>
                        </td>
                    </tr>
                </table>
                <table id="tdDiscussion" style="width: 100%; display: none;">
                    <tr>
                        <td style="vertical-align: top; text-align: left;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="height: 34px; vertical-align: top;">
                                        <span onclick="Notes();" style="font-size: 11px; margin-right: 23px; cursor: pointer;">
                                            Notes(<asp:Label ID="Label1" runat="server"></asp:Label>)</span> <span onclick="Tasks();"
                                                style="font-size: 11px; margin-right: 23px; cursor: pointer;">Activities(<asp:Label
                                                    ID="Label2" runat="server"></asp:Label>)</span><span onclick="PhoneCalls();" style="font-size: 11px;
                                                        margin-right: 23px; cursor: pointer;">Phone Calls(<asp:Label ID="Label3" runat="server"></asp:Label>)</span><span
                                                            onclick="Documents();" style="font-size: 11px; margin-right: 23px; cursor: pointer;">
                                                            Documents(<asp:Label ID="Label4" runat="server"></asp:Label>)</span>
                                        <b onclick="Discussion();" style="font-size: 11px; cursor: pointer;">Discussion(<asp:Label
                                            ID="lblDiscussionCount" runat="server"></asp:Label>)</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtDelDiscussion" Style="display: none; width: 99%;" Text="" runat="server"></asp:TextBox>
                                        <asp:TextBox ID="txtDiscussionSubject" Placeholder="Subject" Style="width: 99%;"
                                            runat="server"></asp:TextBox>
                                        <asp:RequiredFieldValidator ID="rqDiscussionSubject" runat="server" ValidationGroup="rqDiscussion"
                                            ControlToValidate="txtDiscussionSubject" ErrorMessage="*" Style="color: Red;
                                            font-weight: bold;" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtDiscussionDescription" Placeholder="Description" TextMode="MultiLine"
                                            Style="min-height: 70px; min-width: 99%;" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlEntity" runat="server" Style="min-width: 99%;">
                                            <asp:ListItem Text="Lead" Value="Lead"></asp:ListItem>
                                            <asp:ListItem Text="Contact" Value="Contact"></asp:ListItem>
                                            <asp:ListItem Text="Account" Value="Account"></asp:ListItem>
                                            <asp:ListItem Text="Opportunity" Value="Opportunity"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        <asp:Button ID="btnSaveDiscussion" ValidationGroup="rqDiscussion" runat="server"
                                            CssClass="button" Text="Save" OnClick="btnSaveDiscussion_Click" />
                                        <asp:Button ID="btnDeleteDiscussion" Style="display: none;" runat="server" CssClass="button"
                                            Text="Delete" OnClick="btnDeleteDiscussion_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div class="formElement" id="lsUploadDiscussion" runat="server" style="float: left;
                                            width: 350px;">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Button1" />
        </Triggers>
    </asp:UpdatePanel>
    </form>
</body>
</html>
