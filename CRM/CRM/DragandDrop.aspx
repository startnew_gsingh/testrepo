﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DragandDrop.aspx.cs" Inherits="CRM.DragandDrop" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8" />
    <title></title>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <style>
        h1
        {
            padding: .2em;
            margin: 0;
        }
        #fields
        {
            float: left;
            width: 500px;
            margin-right: 2em;
        }
        #placeholder
        {
            width: 200px;
            float: left;
            margin-top: 1em;
        }
        #placeholder ol
        {
            margin: 0;
            padding: 1em 0 1em 3em;
        }
    </style>
    <script>
        $(function () {

            $("#fieldName").accordion();

            $("#fieldName li").draggable({
                appendTo: "body",
                helper: "clone"
            });

            $("#placeholder ol").droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ":not(.ui-sortable-helper)",
                drop: function (event, ui) {
                    $(this).find(".placeholder").remove();
                    $("<li></li>").text(ui.draggable.text()).appendTo(this);
                }
            }).sortable({
                items: "li:not(.placeholder)",
                sort: function () {
                    // gets added unintentionally by droppable interacting with sortable
                    // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
                    $(this).removeClass("ui-state-default");
                }
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="fields">
            <h1 class="ui-widget-header">
                Fields</h1>
            <div id="fieldName">
                <div>
                    <ul>
                        <li>
                            <asp:TextBox ID="txtName" Text="First Name"></asp:TextBox></li>
                        <li>First Name</li>
                        <li>Last Name</li>
                        <li>Email</li>
                        <li>Address 1</li>
                        <li>Address 2</li>
                        <li>City</li>
                        <li>State</li>
                        <li>Zip</li>
                        <li>Phone</li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="placeholder">
            <h1 class="ui-widget-header">
                Placeholder 1</h1>
            <div class="ui-widget-content">
                <ol>
                    <li class="placeholder">Add your controls here</li>
                </ol>
            </div>
        </div>
        <div id="placeholder">
            <h1 class="ui-widget-header">
                Placeholder 2</h1>
            <div class="ui-widget-content">
                <ol>
                    <li class="placeholder">Add your controls here</li>
                </ol>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
