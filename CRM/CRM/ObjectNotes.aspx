﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ObjectNotes.aspx.cs" Inherits="CRM.ObjectNotes" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <link rel="Stylesheet" type="text/css" href="css/Mystyle.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table style="width: 100%;">
            <tr>
                <td style="vertical-align: top;">
                    <b>Notes</b><br />
                    <asp:TextBox TextMode="MultiLine" Style="min-height: 70px; min-width: 380px;" ID="txtNotes"
                        runat="server"></asp:TextBox><br />
                    <br />
                    <asp:FileUpload runat="server" ID="fleUpload" /><br />
                    <br />
                    <asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUploadDocs_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="gvNotes" runat="server" AutoGenerateColumns="false" Width="100%">
                        <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                        <Columns>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Notes
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("Notes") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Document Name
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("DocumentName") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Created Date
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <%#Eval("CreatedDate") %>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
