﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Quote.aspx.cs" Inherits="CRM.Quote1" %>

<%@ Register Assembly="JQChart.Web" Namespace="JQChart.Web.UI.WebControls" TagPrefix="jqChart" %>
<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <script type="text/javascript">
        function createQuote() {
            var createQuoteBox = "#div_CreateQuote-box";
            //Fade in the Popup
            $(createQuoteBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createQuoteBox).height() + 24) / 2;
            var popMargLeft = ($(createQuoteBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function advanceFind() {
            var advanceFindBox = "#div_AdvanceFind-box";
            //Fade in the Popup
            $(advanceFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(advanceFindBox).height() + 24) / 2;
            var popMargLeft = ($(advanceFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                window.location = "Quote.aspx";
            });
            return false;
        }
    </script>
    <script>
        $(document).ready(function () {
            $('.link').click(function () {
                if ($('.link').attr('class') == 'link open') {
                    $(this).removeClass('open')
                    $('.leftbox').animate({ 'left': '-520' });
                } else {
                    $(this).addClass('open')
                    $('.leftbox').animate({ 'left': '0' });
                }
            });
        });
        function chartButton() {
            if ($('.link').attr('class') == 'link open') {
                $(this).removeClass('open')
                $('.leftbox').animate({ 'left': '-520' });
            } else {
                $(this).addClass('open')
                $('.leftbox').animate({ 'left': '0' });
            }
        }
    </script>
    <style>
        .CompletionListCssClass
        {
            font-size: 14px;
            color: #000;
            border: 1px solid #999;
            background: #fff;
            width: 150px;
            float: left;
            z-index: 9999999999 !important;
            position: absolute;
            margin-left: 0px;
        }
        
        body
        {
            padding: 0;
            margin: 0ee;
        }
        .leftbox
        {
            position: absolute;
            top: 130px;
            left: -520px;
        }
        .feedback
        {
            width: 500px;
            background: #eee;
            float: left;
            padding: 10px;
            border: 1px solid #ccc;
        }
        .link
        {
            float: left;
            padding: 10px;
            background: #eee;
            border: 1px solid #ccc;
            position: relative;
            z-index: 5;
            margin-left: -1px;
        }
        .link a
        {
            color: #333;
        }
    </style>
    <div id="div_AdvanceFind-box" class="message-popup" style="overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div2" style="display: block;">
            <div id="div3" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="ifrmAdvanceFind" frameborder="0" src="AdvanceFind.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%" height="100%"></iframe>
            </div>
        </div>
    </div>
    <div id="div_CreateQuote-box" class="message-popup" style="overflow-x: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="showpage" style="display: block; overflow-x: scroll; width: 100%; height: 97% !important;">
            <div id="divIframe" runat="server">
                <div>
                    <table width="100%">
                        <tr>
                            <td>
                                <asp:Button ID="btnSave" runat="server" CssClass="save" ValidationGroup="createValidation" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Summary</b>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <asp:Panel ID="pnlGeneralHead" runat="server" CssClass="pnlCSS">
                                    <asp:Label ID="lblGeneralMessage" runat="server" />
                                    <asp:Label ID="lblQuoteID" runat="server" Visible="false"></asp:Label>
                                </asp:Panel>
                                <asp:Panel ID="pnlGeneral" runat="server">
                                    <table>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Quote Number
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtQuoteNumber" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Customer
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCustomer" runat="server" Width="200px"></asp:TextBox>
                                                <ajaxtoolkit:AutoCompleteExtender ID="ACECustomer" runat="server" TargetControlID="txtCustomer"
                                                    ServiceMethod="GetCompletionList" ServicePath="CustomList.asmx" ContextKey="CRM_Account"
                                                    UseContextKey="true" EnableCaching="true" MinimumPrefixLength="1" CompletionListCssClass="autocomplete_completionListElement"
                                                    CompletionListItemCssClass="CompletionListCssClass" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem">
                                                </ajaxtoolkit:AutoCompleteExtender>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Contact
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtContact" runat="server" Width="200px"></asp:TextBox>
                                                <ajaxtoolkit:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" TargetControlID="txtContact"
                                                    ServiceMethod="GetCompletionList" ServicePath="CustomList.asmx" ContextKey="CRM_Contact"
                                                    UseContextKey="true" EnableCaching="true" MinimumPrefixLength="1" CompletionListCssClass="autocomplete_completionListElement"
                                                    CompletionListItemCssClass="CompletionListCssClass" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem">
                                                </ajaxtoolkit:AutoCompleteExtender>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Territory
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTerritory" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Market Segment
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMarketSegment" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Market Sub-Segment
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtMarketSubSegment" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Currency
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtCurrency" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Exchange Rate
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtExchangeRate" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Ship To Address
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtShipAddress" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Quote Status
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlQuoteStatus" runat="server">
                                                    <asp:ListItem Text="Draft" Value="Draft"></asp:ListItem>
                                                    <asp:ListItem Text="Pending" Value="Pending"></asp:ListItem>
                                                    <asp:ListItem Text="Accepted" Value="Accepted"></asp:ListItem>
                                                    <asp:ListItem Text="Active" Value="Active"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <ajaxtoolkit:CollapsiblePanelExtender ID="CPEGeneral" runat="server" TargetControlID="pnlGeneral"
                                    ExpandControlID="pnlGeneralHead" CollapseControlID="pnlGeneralHead" ScrollContents="false"
                                    Collapsed="false" AutoExpand="true" ExpandDirection="Vertical" SuppressPostBack="true"
                                    TextLabelID="lblGeneralMessage" CollapsedText="Show General Tab" ExpandedText="Hide General Tab">
                                </ajaxtoolkit:CollapsiblePanelExtender>
                                <asp:Panel ID="pnlProductsHead" runat="server" CssClass="pnlCSS">
                                    <asp:Label ID="lblProductsMessage" runat="server" />
                                </asp:Panel>
                                <asp:Panel ID="pnlProducts" runat="server">
                                    <table>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Line#
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlLine" runat="server">
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                    <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                    <asp:ListItem Text="30" Value="30"></asp:ListItem>
                                                    <asp:ListItem Text="40" Value="40"></asp:ListItem>
                                                    <asp:ListItem Text="50" Value="50"></asp:ListItem>
                                                    <asp:ListItem Text="60" Value="60"></asp:ListItem>
                                                    <asp:ListItem Text="70" Value="70"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Product Id
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtProductID" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Product Desc.
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtProdDesc" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Quantity
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtQuantity" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Unit Price
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtunitPrice" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Discount Percent
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDiscountPer" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Discounted Amount
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtDiscountedAmount" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Extended Amount
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtExtendedAmount" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Total Discount
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTotalDiscount" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Sub Total
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtSubTotal" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Total Taxes
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTotalTaxes" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Total Amount
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtTotalAmount" runat="server" Width="200px"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <ajaxtoolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender1" runat="server"
                                    TargetControlID="pnlProducts" ExpandControlID="pnlProductsHead" CollapseControlID="pnlProductsHead"
                                    ScrollContents="false" Collapsed="false" AutoExpand="true" ExpandDirection="Vertical"
                                    SuppressPostBack="true" TextLabelID="lblProductsMessage" CollapsedText="Show Products Tab"
                                    ExpandedText="Hide Products Tab">
                                </ajaxtoolkit:CollapsiblePanelExtender>
                                <asp:Panel ID="pnlAdministrationHead" runat="server" CssClass="pnlCSS">
                                    <asp:Label ID="lblAdminMessage" runat="server" />
                                </asp:Panel>
                                <asp:Panel ID="pnlAdministration" runat="server">
                                    <table>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Owner
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtOwner" runat="server" Width="200px"></asp:TextBox>
                                                <ajaxtoolkit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtOwner"
                                                    ServiceMethod="GetCompletionList" ServicePath="CustomList.asmx" ContextKey="CRM_Users"
                                                    UseContextKey="true" EnableCaching="true" MinimumPrefixLength="1" CompletionListCssClass="autocomplete_completionListElement"
                                                    CompletionListItemCssClass="CompletionListCssClass" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem">
                                                </ajaxtoolkit:AutoCompleteExtender>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Freight Terms
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlFreightTerms" runat="server">
                                                    <asp:ListItem Text="EXW" Value="EXW"></asp:ListItem>
                                                    <asp:ListItem Text="Collect" Value="Collect"></asp:ListItem>
                                                    <asp:ListItem Text="DDP" Value="DDP"></asp:ListItem>
                                                    <asp:ListItem Text="DDU" Value="DDU"></asp:ListItem>
                                                    <asp:ListItem Text="FOB" Value="FOB"></asp:ListItem>
                                                    <asp:ListItem Text="Prepaid" Value="Prepaid"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr style='vertical-align: top; height: 30px; width: 800px;'>
                                            <td>
                                                Shipping Method
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlShippingMethod" runat="server">
                                                    <asp:ListItem Text="FedEx" Value="FedEx"></asp:ListItem>
                                                    <asp:ListItem Text="Purolator" Value="Purolator"></asp:ListItem>
                                                    <asp:ListItem Text="DHL" Value="DHL"></asp:ListItem>
                                                    <asp:ListItem Text="UPS" Value="UPS"></asp:ListItem>
                                                    <asp:ListItem Text="Hand Delivery" Value="Hand Delivery"></asp:ListItem>
                                                    <asp:ListItem Text="Freight Forwarder" Value="Freight Forwarder"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <ajaxtoolkit:CollapsiblePanelExtender ID="CollapsiblePanelExtender2" runat="server"
                                    TargetControlID="pnlAdministration" ExpandControlID="pnlAdministrationHead" CollapseControlID="pnlAdministrationHead"
                                    ScrollContents="false" Collapsed="false" AutoExpand="true" ExpandDirection="Vertical"
                                    SuppressPostBack="true" TextLabelID="lblAdminMessage" CollapsedText="Show Administrator Tab"
                                    ExpandedText="Hide Administrator Tab">
                                </ajaxtoolkit:CollapsiblePanelExtender>
                            </td>
                            <td valign="top">
                                <table width="100%">
                                    <tr>
                                        <td style="vertical-align: top; text-align: left;">
                                            <iframe id="Iframe1" src="Notes.aspx" style="width: 430px; border: none; min-height: 270px;
                                                overflow-x: hidden; overflow-y: scroll;" runat="server"></iframe>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="gradientbginner" style="width: 90%; overflow: auto;">
                <table>
                    <tr>
                        <td width="18px">
                        </td>
                        <td>
                            <div class="maincontent" style="width: 100%">
                                <div class="searchboxcaption">
                                    <span>Quote</span></div>
                                <div class="searchbox">
                                    <div>
                                        <table width="100%">
                                            <tr>
                                                <%--<td align="center" valign="middle" style="padding-right: 3px; padding-left: 3px;
                                                    padding-bottom: 3px; padding-top: 3px">--%><div style="text-align: center;">
                                                        <asp:Label ID="lblListmsg" runat="server" Style="text-align: center; color: Red;
                                                            font-size: 13px;" EnableViewState="False"></asp:Label>
                                                    </div>
                                                <%--</td>--%>
                                                <%--</tr>
                                            <tr>--%>
                                                <td>
                                                    <asp:Button ID="btnCreateQuote" CssClass="create_dtl" OnClick="btnEdit_Click" Text=""
                                                        runat="server" />
                                                    <asp:Button ID="btnAdvanceFind" CssClass="advance_find" OnClientClick="advanceFind();"
                                                        Text="" runat="server" />
                                                    <asp:Button ID="btnCustomizeObject" CssClass="customize" Style="display: none;" Text=""
                                                        OnClientClick="customizeForm()" runat="server" />
                                                    <asp:Button ID="btnDeleteAll" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAll_Click"
                                                        OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Quote" CausesValidation="False" />
                                                    <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit Quote" OnClick="btnEdit_Click"
                                                        ToolTip="Edit Selected Quote" Style="display: none;" CausesValidation="False" />
                                                </td>
                                            </tr>
                                            <tr id="trFilter" runat="server">
                                                <td align="center" valign="top">
                                                    <table width="100%" style="border-right: gray 1px solid; border-top: gray 1px solid;
                                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                                        <tr>
                                                            <th align="left" colspan="2">
                                                                Filter Records
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 350px;">
                                                                <asp:DropDownList ID="ddlFilters" Width="300px" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:DropDownCheckBoxes ID="ddlGridColumns1" runat="server" OnSelectedIndexChanged="checkBoxes_SelcetedIndexChanged"
                                                                    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                                                                    <Style SelectBoxWidth="300" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="120" />
                                                                    <Texts SelectBoxCaption="Columns" />
                                                                </asp:DropDownCheckBoxes>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="trGridView" runat="server">
                                                <td align="center" valign="top" style="height: 230px;">
                                                    <div style="overflow: auto; max-height: 230px; float: left;" class="customwd">
                                                        <asp:GridView ID="dataGridQuote" runat="server" AutoGenerateColumns="true" Width="100%"
                                                            DataKeyField="QuoteID" DataKeyNames="QuoteID" AllowPaging="true" PageSize="10"
                                                            AllowSorting="true" OnSorting="dataGridQuote_Sorting" OnPageIndexChanging="dataGridQuote_PageIndexChanging"
                                                            EmptyDataText="No Record Found" OnDataBound="dataGridQuote_DataBound" OnRowCreated="dataGridQuote_RowCreated">
                                                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerTemplate>
                                                                <asp:Button ID="imgPageFirst" runat="server" Text="First" CommandArgument="First"
                                                                    CommandName="Page" OnCommand="imgPageFirst_Command" />
                                                                <asp:Button ID="imgPagePrevious" runat="server" Text="Prev" CommandArgument="Prev"
                                                                    CommandName="Page" OnCommand="imgPagePrevious_Command" />
                                                                <asp:DropDownList ID="ddCurrentPage" runat="server" CssClass="Normal" AutoPostBack="True"
                                                                    OnSelectedIndexChanged="ddCurrentPage_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:Label ID="lblOf" runat="server" Text="of" CssClass="Normal"></asp:Label>
                                                                <asp:Label ID="lblTotalPage" runat="server" CssClass="Normal"></asp:Label>
                                                                <asp:Button ID="imgPageNext" runat="server" Text="Next" CommandArgument="Next" CommandName="Page"
                                                                    OnCommand="imgPageNext_Command"></asp:Button>
                                                                <asp:Button ID="imgPageLast" runat="server" Text="Last" CommandArgument="Last" CommandName="Page"
                                                                    OnCommand="imgPageLast_Command"></asp:Button>
                                                            </PagerTemplate>
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trButtons" runat="server">
                                                <td align="left" valign="top">
                                                    <table id="tblDeleteAll" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                            </td>
                                                            <td class="NormalText">
                                                            </td>
                                                            <td>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <p class="searchboxbottom">
                                </p>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
