﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;

namespace CRM
{
    public partial class MasterPage : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            lblUserName.Text = GlobalVariables.LoginUser.UserName;

            CRMBLLUsers _objBLLUsers = new CRMBLLUsers();
            DataSet _objObjectRoles = _objBLLUsers.getUserObjectRoles(GlobalVariables.LoginUser.OrgID, 0);
            if (_objObjectRoles.Tables[0].Rows.Count > 0)
            {
                for (int r = 0; r < _objObjectRoles.Tables[0].Rows.Count; r++)
                {
                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "1" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liDeals.Attributes.Add("style", "display:none");
                    }

                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "2" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liContacts.Attributes.Add("style", "display:none");
                    }

                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "3" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liAccounts.Attributes.Add("style", "display:none");
                    }
                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "4" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liOpportunities.Attributes.Add("style", "display:none");
                    }

                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "5" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liQuotes.Attributes.Add("style", "display:none");
                    }

                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "6" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liWorkOrder.Attributes.Add("style", "display:none");
                    }

                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "7" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liInvoice.Attributes.Add("style", "display:none");
                    }

                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "8" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liVendor.Attributes.Add("style", "display:none");
                    }

                    if (_objObjectRoles.Tables[0].Rows[r]["ObjectID"].ToString() == "9" && _objObjectRoles.Tables[0].Rows[r]["Read"].ToString() == "False")
                    {
                        liProductClass.Attributes.Add("style", "display:none");
                    }
                }
            }
        }

        protected void lbtnLogout_Click(object sender, EventArgs e)
        {
            GlobalVariables.LoginUser = null;
            Response.Redirect("Default.aspx?Status=Logout");
        }
    }
}