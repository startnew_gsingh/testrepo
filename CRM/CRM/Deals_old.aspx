﻿<%@ Page Language="C#" MasterPageFile="MasterPage.master" AutoEventWireup="true"
    CodeFile="Deals.aspx.cs" Inherits="CRM.Deals" Title="CRM" %>

<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script>
        function createDeal() {
            var createDealBox = "#div_CreateDeal-box";
            //Fade in the Popup
            $(createDealBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createDealBox).height() + 24) / 2;
            var popMargLeft = ($(createDealBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function advanceFind() {
            var advanceFindBox = "#div_AdvanceFind-box";
            //Fade in the Popup
            $(advanceFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(advanceFindBox).height() + 24) / 2;
            var popMargLeft = ($(advanceFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300, function () {
                $('#mask').remove();
                window.location = "Deals.aspx";
            });
            return false;
        }
    </script>
    <div id="div_CreateDeal-box" class="message-popup" style="height: 490px; width: 900px;
        margin-left: -100px;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="showpage" style="display: block;">
            <div id="divIframe" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="ifrmMain" frameborder="0" src="CreateDeals.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%" height="100%"></iframe>
            </div>
        </div>
    </div>
    <div id="div_AdvanceFind-box" class="message-popup" style="height: 490px; width: 950px;
        margin-left: -100px;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div2" style="display: block;">
            <div id="div3" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="ifrmAdvanceFind" frameborder="0" src="AdvanceFind.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%" height="100%"></iframe>
            </div>
        </div>
    </div>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="gradientbginner">
                <table>
                    <tr>
                        <td width="18px">
                        </td>
                        <td>
                            <div class="maincontent">
                                <div class="searchboxcaption">
                                    <span>Deals</span></div>
                                <div class="searchbox">
                                    <div>
                                        <table width="100%">
                                            <tr style="display: none;">
                                                <td align="center" valign="middle" style="padding-right: 3px; padding-left: 3px;
                                                    padding-bottom: 3px; padding-top: 3px">
                                                    <asp:Label ID="lblListmsg" runat="server" CssClass="MsgBox" EnableViewState="False"></asp:Label>
                                                    <asp:TextBox ID="txtsd" runat="server"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <table width="100%" style="border-right: gray 1px solid; table-layout: fixed; border-top: gray 1px solid;
                                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                                        <tr>
                                                            <th align="left" colspan="2">
                                                                Filter Records
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:DropDownList ID="ddlFilters" Width="300px" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnCreateDeal" OnClientClick="createDeal();" Text="Create Deal" runat="server" />
                                                                <asp:Button ID="btnAdvanceFind" OnClientClick="advanceFind();" Text="Advance Find"
                                                                    runat="server" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="center" valign="top">
                                                    <asp:GridView ID="dataGridDeals" runat="server" AutoGenerateColumns="true" Width="100%"
                                                        DataKeyField="LeadID" DataKeyNames="LeadID" AllowPaging="true" PageSize="10"
                                                        OnPageIndexChanging="dataGridDeals_PageIndexChanging" AllowSorting="true" OnSorting="dataGridDeals_Sorting"
                                                        EmptyDataText="No Record Found" OnDataBound="dataGridDeals_DataBound">
                                                        <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <HeaderTemplate>
                                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                                </HeaderTemplate>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <PagerTemplate>
                                                            <%--<asp:ImageButton ID="imgPageFirst" runat="server" ImageUrl="~/Images/Icons/PageFirst.png"
                                                                CommandArgument="First" CommandName="Page" OnCommand="imgPageFirst_Command"></asp:ImageButton>--%>
                                                            <asp:Button ID="imgPageFirst" runat="server" Text="First" CommandArgument="First"
                                                                CommandName="Page" OnCommand="imgPageFirst_Command" />
                                                            <%--<asp:ImageButton ID="imgPagePrevious" runat="server" ImageUrl="~/Images/Icons/PagePrevious.png"
                                                                CommandArgument="Prev" CommandName="Page" OnCommand="imgPagePrevious_Command">
                                                            </asp:ImageButton>--%>
                                                            <asp:Button ID="imgPagePrevious" runat="server" Text="Prev" CommandArgument="Prev"
                                                                CommandName="Page" OnCommand="imgPagePrevious_Command" />
                                                            <asp:DropDownList ID="ddCurrentPage" runat="server" CssClass="Normal" AutoPostBack="True"
                                                                OnSelectedIndexChanged="ddCurrentPage_SelectedIndexChanged">
                                                            </asp:DropDownList>
                                                            <asp:Label ID="lblOf" runat="server" Text="of" CssClass="Normal"></asp:Label>
                                                            <asp:Label ID="lblTotalPage" runat="server" CssClass="Normal"></asp:Label>
                                                            <%--<asp:ImageButton ID="imgPageNext" runat="server" ImageUrl="~/Images/Icons/PageNext.png"
                                                                CommandArgument="Next" CommandName="Page" OnCommand="imgPageNext_Command"></asp:ImageButton>
                                                            <asp:ImageButton ID="imgPageLast" runat="server" ImageUrl="~/Images/Icons/PageLast.png"
                                                                CommandArgument="Last" CommandName="Page" OnCommand="imgPageLast_Command"></asp:ImageButton>--%>
                                                            <asp:Button ID="imgPageNext" runat="server" Text="Next" CommandArgument="Next" CommandName="Page"
                                                                OnCommand="imgPageNext_Command"></asp:Button>
                                                            <asp:Button ID="imgPageLast" runat="server" Text="Last" CommandArgument="Last" CommandName="Page"
                                                                OnCommand="imgPageLast_Command"></asp:Button>
                                                        </PagerTemplate>
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" valign="top">
                                                    <table id="tblDeleteAll" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                                <asp:Image ID="Image1" runat="server" ImageUrl="images/arrow_ltr.png" />
                                                            </td>
                                                            <td class="NormalText">
                                                                &nbsp; by selecting checkboxes &nbsp; &nbsp; &nbsp;&nbsp;
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnDeleteAll" runat="server" CssClass="button" Text="Delete Deal(s)"
                                                                    OnClick="btnDeleteAll_Click" OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Deal"
                                                                    CausesValidation="False" />
                                                                <asp:Button ID="btnEdit" runat="server" CssClass="button" Text="Edit Deal" OnClick="btnEdit_Click"
                                                                    ToolTip="Edit Selected Deal" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <p class="searchboxbottom">
                                </p>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
