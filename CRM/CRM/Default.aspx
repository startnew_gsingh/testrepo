<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="CRM.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Untitled Document</title>
    <link href="css/css.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .cbButton
        {
            padding: 1pt 15pt 1pt 15pt;
            background-color: #F98C0B;
            color: #ffffff;
            text-decoration: none;
            font-family: Arial;
            font-size: 8pt;
            font-weight: Bold;
            border: double 1px #ffffff;
            cursor: hand;
        }
        .cbButton:hover
        {
            padding: 1pt 15pt 1pt 15pt;
            background-color: #F98C0B;
            color: #ffffff;
            text-decoration: none;
            font-family: Arial;
            font-size: 8pt;
            font-weight: Bold;
            border: inset 1px #ffffff;
            cursor: hand;
        }
    </style>
    <script type="text/javascript" language="javascript">
        if (parent.document.getElementById('divIframe') != null) {
            parent.location = window.location;
            alert("Your Session has been expired. Please login again !!");
        }
    </script>
</head>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0">
    <form id="form1" runat="server">
    <div>
        <div class="gradientbg">
            <div class="orangeslip01">
            </div>
        </div>
        <div class="gradientbg">
            <div id="logo">
            </div>
            <div class="homeicon">
                <%--<a href="Default.aspx" target="_self">home</a>--%>
                <br />
            </div>
            <div class="greysplitter">
            </div>
        </div>
        <div class="gradientbg">
            <br />
            <br />
            <br />
            <br />
            <br />
            <div style="height:510px;">
                <div align="center">
                    <div class="login-userpanelcaption">
                        <span>Admin Login</span>
                    </div>
                    <div class="login-userpanel" align="center">
                        <table>
                            <tr>
                                <td colspan="3" align="center">
                                    <asp:Label ID="Label1" runat="server" CssClass="MsgBox" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="images/user_icon02.jpg" hspace="0" class="icon" width="31" height="46" />
                                </td>
                                <td>
                                    <span class="usertxt">Username :: </span>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtUserName" runat="server" CssClass="txtfield" EnableTheming="False"
                                        Columns="25"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src="images/password_icon.jpg" hspace="0" class="icon" width="31" height="35" />
                                </td>
                                <td>
                                    <span class="usertxt">Password :: </span>
                                </td>
                                <td align="left">
                                    <asp:TextBox ID="txtPassword" runat="server" CssClass="txtfield" Columns="30" TextMode="Password"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" align="center">
                                    <br />
                                    <asp:Button ID="LinkButton1" runat="server" Font-Bold="True" OnClick="btnLogin_Click"
                                        Text="Login" CssClass="cbButton"></asp:Button>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <p class="login-userpanelbottom">
                    </p>
                </div>
                <div>
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                    <br />
                </div>
            </div>
        </div>
        <div class="loginfooter">
            <div align="center">
                <table height="40" width="90%">
                    <tr>
                        <td align="center" colspan="2" valign="middle">
                            <%--<span class="poweredtxt">Copyright.&copy;
                                <%=DateTime.Now.Year.ToString() +"-" +Convert.ToString (DateTime.Now.Year+1).Substring(2)%>.
                                All rights reserved.<a class="poweredtxt" href="http://www.paylesswebsolutions.com" target="_blank">
                                        Pay Less Web Solution. </a></span>--%>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
    <map name="Map" id="Map">
        <area shape="rect" coords="7,122,329,156" href="../../Default.aspx" target="_self"
            title="home page" alt="home page" />
        <area shape="rect" coords="61,14,273,111" href="../../Default.aspx" title="home page"
            target="_self" alt="home page" />
    </map>
    </form>
</body>
</html>
