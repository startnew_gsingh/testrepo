﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;
using System.Collections;
using CRMDAL;
using System.Text;
using System.Web.UI.HtmlControls;

namespace CRM
{
    public partial class AdvanceFind : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            lblOrgID.Text = GlobalVariables.LoginUser.OrgID.ToString();
            if (!Page.IsPostBack)
            {
                bindLookforDropdownList();
                bindFiltersDropDown();
                SetInitialRow();
                lblSelObjectID.Text = ddlLookfor.SelectedValue;
                /* Edit Columns*/
                //bindObjectFields();
                Session["CurrentTable"] = null;
            }
            string btnTarget = getPostBackControlID();
            if (btnTarget == null || btnTarget == "btnSaveFilterColumns" || btnTarget == "btnEditFilterColumns" || btnTarget == "btnbBindQueryResults" || btnTarget == "btnSave")
                bindObjectFields(0);
            //if (btnTarget == null || btnTarget == "btnEditFilterColumns" || btnTarget == "btnbBindQueryResults" || btnTarget == "ddlSavedView")
            //    bindObjectFields(0);
            //if (btnTarget == "btnSaveFilterColumns")
            //    bindObjectFields(1);
            //if ()
            //{
            //    bindObjectFields(1);
            //}
        }

        private string getPostBackControlID()
        {
            Control control = null;
            //first we will check the "__EVENTTARGET" because if post back made by       the controls
            //which used "_doPostBack" function also available in Request.Form collection.
            string ctrlname = Page.Request.Params["__EVENTTARGET"];
            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);
            }
            // if __EVENTTARGET is null, the control is a button type and we need to
            // iterate over the form collection to find it
            else
            {
                string ctrlStr = String.Empty;
                Control c = null;
                foreach (string ctl in Page.Request.Form)
                {
                    //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                    //mouse x and y coordinates
                    if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                    {
                        ctrlStr = ctl.Substring(0, ctl.Length - 2);
                        c = Page.FindControl(ctrlStr);
                    }
                    else
                    {
                        c = Page.FindControl(ctl);
                    }
                    if (c is System.Web.UI.WebControls.Button ||
                             c is System.Web.UI.WebControls.ImageButton)
                    {
                        control = c;
                        break;
                    }
                }
            }
            if (control != null)
                return control.ID;
            else
                return null;
        }

        protected void ddlLookfor_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["ObjectID"] = ddlLookfor.SelectedValue;
            bindLookforDropdownList();
            bindFiltersDropDown();
            SetInitialRow();
            txtFilterName.Text = "";
            /* Edit Columns*/
            //bindObjectFields();
            dataGridDeals.DataSource = null;
            dataGridDeals.DataBind();
            Session["CurrentTable"] = null;
            lblSelObjectID.Text = ddlLookfor.SelectedValue;
        }

        public void bindLookforDropdownList()
        {
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            ddlLookfor.DataSource = objCRMDeals.getObjects();
            ddlLookfor.DataTextField = "ObjectName";
            ddlLookfor.DataValueField = "ID";
            ddlLookfor.SelectedValue = Convert.ToString(Session["ObjectID"]);
            ddlLookfor.DataBind();
        }

        private void SetInitialRow()
        {
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataTable dt = new DataTable();
            DataRow dr = null;
            //dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("ColObjects", typeof(string)));
            dt.Columns.Add(new DataColumn("ColFilter", typeof(string)));
            dt.Columns.Add(new DataColumn("ColValue", typeof(string)));
            dt.Columns.Add(new DataColumn("ColRelation", typeof(string)));
            //dt.Columns.Add(new DataColumn("ControlValues", typeof(string)));

            dr = dt.NewRow();
            //dr["RowNumber"] = 1;
            dr["ColObjects"] = string.Empty;
            dr["ColFilter"] = string.Empty;
            dr["ColValue"] = string.Empty;
            dr["ColRelation"] = string.Empty;
            //dr["ControlValues"] = string.Empty;
            dt.Rows.Add(dr);
            //dr = dt.NewRow();

            //Store the DataTable in ViewState
            ViewState["CurrentTable"] = dt;

            gvFilters.DataSource = dt;
            gvFilters.DataBind();
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            AddNewRowToGrid();
            //bindObjectFields();
        }

        private void SetPreviousData()
        {
            int rowIndex = 0;
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        DropDownList ddlColumns = (DropDownList)gvFilters.Rows[rowIndex].Cells[1].FindControl("ddlColumns");
                        DropDownList ddlFilterCondition = (DropDownList)gvFilters.Rows[rowIndex].Cells[2].FindControl("ddlFilterCondition");
                        TextBox box3 = (TextBox)gvFilters.Rows[rowIndex].Cells[3].FindControl("txtBoxValue");
                        DropDownList ddlRelation = (DropDownList)gvFilters.Rows[rowIndex].Cells[4].FindControl("ddlRelation");
                        DropDownList ddlControlValues = (DropDownList)gvFilters.Rows[rowIndex].Cells[3].FindControl("ddlControlValues");

                        ddlColumns.SelectedValue = dt.Rows[i]["ColObjects"].ToString();
                        ddlFilterCondition.SelectedValue = dt.Rows[i]["ColFilter"].ToString();
                        box3.Text = dt.Rows[i]["ColValue"].ToString();
                        ddlRelation.SelectedValue = dt.Rows[i]["ColRelation"].ToString();
                        if (ddlControlValues != null && ddlControlValues.Items.Count > 0)
                            ddlControlValues.SelectedValue = dt.Rows[i]["ColValue"].ToString();



                        CRMBLLDeals _objDeals = new CRMBLLDeals();
                        DataSet dscontrolChoices = _objDeals.getControlChoicesByColumnName(ddlColumns.SelectedValue);
                        if (dscontrolChoices.Tables[0].Rows.Count > 0)
                        {
                            box3.Visible = false;
                            ddlControlValues.Visible = true;

                            ddlControlValues.DataSource = dscontrolChoices;
                            ddlControlValues.DataTextField = "ControlChoices";
                            ddlControlValues.DataValueField = "ControlChoices";
                            ddlControlValues.DataBind();
                            ddlControlValues.SelectedValue = dt.Rows[i]["ColValue"].ToString();
                        }
                        else
                        {
                            box3.Visible = true;
                            ddlControlValues.Visible = false;

                            ddlControlValues.DataSource = null;
                            ddlControlValues.DataBind();
                        }

                        rowIndex++;
                    }
                }
            }
        }

        private void AddNewRowToGrid()
        {
            int rowIndex = 0;

            if (ViewState["CurrentTable"] != null)
            {
                DataTable dtCurrentTable = (DataTable)ViewState["CurrentTable"];
                DataRow drCurrentRow = null;
                if (dtCurrentTable.Rows.Count > 0)
                {
                    for (int i = 1; i <= dtCurrentTable.Rows.Count; i++)
                    {
                        //extract the TextBox values
                        DropDownList ddlColumns = (DropDownList)gvFilters.Rows[rowIndex].Cells[1].FindControl("ddlColumns");
                        DropDownList ddlFilterCondition = (DropDownList)gvFilters.Rows[rowIndex].Cells[2].FindControl("ddlFilterCondition");
                        TextBox txtBoxValue = (TextBox)gvFilters.Rows[rowIndex].Cells[3].FindControl("txtBoxValue");
                        DropDownList ddlRelation = (DropDownList)gvFilters.Rows[rowIndex].Cells[4].FindControl("ddlRelation");
                        DropDownList ddlControlValues = (DropDownList)gvFilters.Rows[rowIndex].Cells[3].FindControl("ddlControlValues");

                        drCurrentRow = dtCurrentTable.NewRow();
                        //drCurrentRow["RowNumber"] = i + 1;

                        dtCurrentTable.Rows[i - 1]["ColObjects"] = ddlColumns.SelectedValue;
                        dtCurrentTable.Rows[i - 1]["ColFilter"] = ddlFilterCondition.SelectedValue;
                        if (ddlControlValues.Items.Count > 0)
                            dtCurrentTable.Rows[i - 1]["ColValue"] = ddlControlValues.SelectedValue;
                        else
                            dtCurrentTable.Rows[i - 1]["ColValue"] = txtBoxValue.Text;
                        dtCurrentTable.Rows[i - 1]["ColRelation"] = ddlRelation.SelectedValue;
                        //dtCurrentTable.Rows[i - 1]["ControlValues"] = ddlControlValues.SelectedValue;

                        rowIndex++;
                    }
                    dtCurrentTable.Rows.Add(drCurrentRow);
                    ViewState["CurrentTable"] = dtCurrentTable;

                    gvFilters.DataSource = dtCurrentTable;
                    gvFilters.DataBind();
                }
            }
            else
            {
                Response.Write("ViewState is null");
            }

            //Set Previous Data on Postbacks
            SetPreviousData();
        }

        protected void ddlColumns_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList _objddlColumns = (DropDownList)sender;
            GridViewRow row = (GridViewRow)_objddlColumns.NamingContainer;
            CRMBLLDeals _objDeals = new CRMBLLDeals();

            DataSet dscontrolChoices = _objDeals.getControlChoicesByColumnName(_objddlColumns.SelectedValue);
            TextBox box3 = (TextBox)gvFilters.Rows[row.RowIndex].Cells[3].FindControl("txtBoxValue");
            DropDownList ddlControlValues = (DropDownList)gvFilters.Rows[row.RowIndex].Cells[4].FindControl("ddlControlValues");
            if (dscontrolChoices.Tables[0].Rows.Count > 0)
            {
                box3.Visible = false;
                ddlControlValues.Visible = true;

                ddlControlValues.DataSource = dscontrolChoices;
                ddlControlValues.DataTextField = "ControlChoices";
                ddlControlValues.DataValueField = "ControlChoices";
                ddlControlValues.DataBind();
            }
            else
            {
                box3.Visible = true;
                ddlControlValues.Visible = false;

                ddlControlValues.DataSource = null;
                ddlControlValues.DataBind();
            }
            bindObjectFields(1);
        }

        protected void ddl1_load(object sender, EventArgs e)
        {
            DropDownList _objddlColumns = (DropDownList)sender;
            GridViewRow row = (GridViewRow)_objddlColumns.NamingContainer;

            CRMBLLDeals _objDeals = new CRMBLLDeals();
            if (_objddlColumns.Items.Count == 0)
            {
                _objddlColumns.DataSource = _objDeals.getObjectsColumns(Convert.ToInt64(ddlLookfor.SelectedValue), GlobalVariables.LoginUser.OrgID, 0);
                _objddlColumns.DataTextField = "GridDisplayName";
                _objddlColumns.DataValueField = "ColumnName";
                _objddlColumns.DataBind();
            }
            try
            {
                DataSet dscontrolChoices = _objDeals.getControlChoicesByColumnName(_objddlColumns.SelectedValue);
                TextBox box3 = (TextBox)gvFilters.Rows[row.RowIndex].Cells[3].FindControl("txtBoxValue");
                DropDownList ddlControlValues = (DropDownList)gvFilters.Rows[row.RowIndex].Cells[4].FindControl("ddlControlValues");
                if (dscontrolChoices.Tables[0].Rows.Count > 0)
                {
                    box3.Visible = false;
                    ddlControlValues.Visible = true;
                }
                else
                {
                    box3.Visible = true;
                    ddlControlValues.Visible = false;
                }
            }
            catch
            { }
        }

        private void bindObjects(DropDownList _objddl)
        {

        }

        protected void ButtonSave_Click(object sender, EventArgs e)
        {
            try
            {
                StructureFilterDAL ObjStructureFilterDAL = new StructureFilterDAL();
                ObjStructureFilterDAL.OrgID = GlobalVariables.LoginUser.OrgID;
                ObjStructureFilterDAL.ObjectID = Convert.ToInt64(ddlLookfor.SelectedValue);
                ObjStructureFilterDAL.FilterName = txtFilterName.Text.Trim();

                DataTable dtFilterQuery = new DataTable();

                //dtFilterQuery.Columns.Add(new DataColumn("FilterID", typeof(Int64)));
                dtFilterQuery.Columns.Add(new DataColumn("ColumnName", typeof(string)));
                dtFilterQuery.Columns.Add(new DataColumn("FilterCondition", typeof(string)));
                dtFilterQuery.Columns.Add(new DataColumn("Value", typeof(string)));
                dtFilterQuery.Columns.Add(new DataColumn("Relation", typeof(string)));
                //dtFilterQuery.Columns.Add(new DataColumn("ControlValues", typeof(string)));


                ObjStructureFilterDAL.Query = createQueryString(dtFilterQuery, true).ToString();// queryString.ToString();

                CRMBLLDeals _objDeals = new CRMBLLDeals();
                int successFilter = _objDeals.insertFilters(ObjStructureFilterDAL);

                _objDeals.insertFilterQuery(dtFilterQuery, successFilter);
                DataTable dteditColumns = (DataTable)Session["CurrentTable"];
                for (int d = 0; d < dteditColumns.Rows.Count; d++)
                {
                    //((System.Data.DataRow)(dt)).ItemArray[1] = ((System.Data.DataRow)(dt)).ItemArray[1].ToString().Remove(((System.Data.DataRow)(dt)).ItemArray[1].ToString().Length - 1);
                    dteditColumns.Rows[d][1] = dteditColumns.Rows[d][1].ToString().Remove(dteditColumns.Rows[d][1].ToString().Length - 1);
                }
                if (dteditColumns != null)
                    _objDeals.insertFilterColumn(dteditColumns, successFilter);

                txtFilterName.Text = "";
                bindFiltersDropDown();
                SetInitialRow();
                SetPreviousData();
                dataGridDeals.DataSource = null;
                dataGridDeals.DataBind();
            }
            catch { }
        }

        public StringBuilder createQueryString(DataTable dtFilterQuery, bool dataTable = false)
        {
            string objectColumn = "";

            if (ddlLookfor.SelectedValue == "1")
                objectColumn = "L";
            if (ddlLookfor.SelectedValue == "2")
                objectColumn = "C";
            if (ddlLookfor.SelectedValue == "3")
                objectColumn = "A";
            if (ddlLookfor.SelectedValue == "4")
                objectColumn = "O";
            if (ddlLookfor.SelectedValue == "5")
                objectColumn = "Q";
            if (ddlLookfor.SelectedValue == "6")
                objectColumn = "W";
            if (ddlLookfor.SelectedValue == "7")
                objectColumn = "I";

            DataRow dr = null;
            StringBuilder queryString = new StringBuilder();
            for (int i = 0; i < gvFilters.Rows.Count; i++)
            {
                //extract the TextBox values
                DropDownList ddlColumns = (DropDownList)gvFilters.Rows[i].Cells[0].FindControl("ddlColumns");
                DropDownList ddlFilterCondition = (DropDownList)gvFilters.Rows[i].Cells[1].FindControl("ddlFilterCondition");
                TextBox txtBoxValue = (TextBox)gvFilters.Rows[i].Cells[2].FindControl("txtBoxValue");
                DropDownList ddlRelation = (DropDownList)gvFilters.Rows[i].Cells[3].FindControl("ddlRelation");
                DropDownList ddlControlValues = (DropDownList)gvFilters.Rows[i].Cells[2].FindControl("ddlControlValues");


                if (ddlColumns.SelectedIndex >= 0 && ddlFilterCondition.SelectedIndex >= 0)
                {
                    string Value = "";
                    if (txtBoxValue.Visible)
                        Value = txtBoxValue.Text;
                    else
                        Value = ddlControlValues.SelectedValue;
                    if (Value != "")
                    {
                        if (dataTable)
                        {
                            dr = dtFilterQuery.NewRow();
                            if (objectColumn != "")
                                dr["ColumnName"] = objectColumn + "." + ddlColumns.SelectedValue;
                            else
                                dr["ColumnName"] = ddlColumns.SelectedValue;

                            dr["FilterCondition"] = ddlFilterCondition.SelectedValue;
                            if (ddlControlValues.Items.Count > 0)
                                dr["Value"] = ddlControlValues.SelectedValue;
                            else
                                dr["Value"] = txtBoxValue.Text;
                            //dr["ControlValues"] = ddlControlValues.SelectedValue;
                            dtFilterQuery.Rows.Add(dr);
                        }



                        string _textBoxValue = "";
                        if (ddlFilterCondition.SelectedIndex == 2)
                            _textBoxValue = "Like '%" + Value + "%'";
                        else if (ddlFilterCondition.SelectedIndex == 3)
                            _textBoxValue = "Not Like '%" + Value + "%'";
                        else if (ddlFilterCondition.SelectedIndex == 4)
                            _textBoxValue = "Like '" + Value + "%'";
                        else if (ddlFilterCondition.SelectedIndex == 5)
                            _textBoxValue = "Not Like '" + Value + "%'";
                        else if (ddlFilterCondition.SelectedIndex == 6)
                            _textBoxValue = "Like '" + "%" + Value + "'";
                        else if (ddlFilterCondition.SelectedIndex == 7)
                            _textBoxValue = "not Like '" + "%" + Value + "'";
                        else
                            _textBoxValue = ddlFilterCondition.SelectedValue + "  '" + Value + "' ";

                        //if (Value != "")
                        //{
                        if (ddlRelation.SelectedValue != "0" && i < gvFilters.Rows.Count - 1)
                        {
                            _textBoxValue = _textBoxValue + " " + ddlRelation.SelectedValue + " ";
                            if (dataTable)
                                dr["Relation"] = ddlRelation.SelectedValue;
                        }
                        else if (ddlRelation.SelectedValue == "0" && i < gvFilters.Rows.Count - 1)
                        {
                            _textBoxValue = _textBoxValue + " OR ";
                            if (dataTable)
                                dr["Relation"] = "OR";
                        }
                        if (dataTable)
                            if (objectColumn != "")
                                queryString.Append(objectColumn + "." + ddlColumns.SelectedValue + " " + _textBoxValue);
                            else
                                queryString.Append(ddlColumns.SelectedValue + " " + _textBoxValue);
                        else
                            queryString.Append("[" + ddlColumns.SelectedItem.Text + "]" + " " + _textBoxValue);
                        //}
                    }
                }
                else
                {
                    queryString = null;
                }
            }
            if (queryString != null && queryString.ToString() != "")
            {
                string strData = queryString.ToString().Trim().Substring(queryString.ToString().Trim().Length - 3);
                if (strData.Trim().ToLower() == "and")
                    queryString.Remove(queryString.Length - 4, 4);
                else if (strData.Trim().ToLower() == "or")
                    queryString.Remove(queryString.Length - 3, 3);
            }
            return queryString;
        }

        public void bindFiltersDropDown()
        {
            CRMBLLDeals objCRMDeals = new CRMBLLDeals();
            ddlSavedView.DataSource = objCRMDeals.getFilters(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlLookfor.SelectedValue));
            ddlSavedView.DataTextField = "FilterName";
            ddlSavedView.DataValueField = "ID";
            ddlSavedView.DataBind();
            ddlSavedView.Items.Insert(0, new ListItem("Select", "0"));
        }

        public void bindFilterGrid()
        {
            DataTable dt = new DataTable();
            DataRow dr = null;
            //dt.Columns.Add(new DataColumn("RowNumber", typeof(string)));
            dt.Columns.Add(new DataColumn("ColObjects", typeof(string)));
            dt.Columns.Add(new DataColumn("ColFilter", typeof(string)));
            dt.Columns.Add(new DataColumn("ColValue", typeof(string)));
            dt.Columns.Add(new DataColumn("ColRelation", typeof(string)));
            //dt.Columns.Add(new DataColumn("ControlValues", typeof(string)));

            CRMBLLDeals objCRMDeals = new CRMBLLDeals();

            if (ddlSavedView.SelectedValue == "0")
                SetInitialRow();
            else
            {
                DataSet dvDeals = objCRMDeals.getFiltersQuery(Convert.ToInt64(ddlSavedView.SelectedValue));

                //txtFilterName.Text = dvDeals[0]["FilterName"].ToString();

                for (int f = 0; f < dvDeals.Tables[0].Rows.Count; f++)
                {
                    dr = dt.NewRow();
                    //dr["RowNumber"] = 1;
                    if (Convert.ToInt64(ddlLookfor.SelectedValue) > 7)
                        dr["ColObjects"] = dvDeals.Tables[0].Rows[f]["ColumnName"].ToString();
                    else
                        dr["ColObjects"] = dvDeals.Tables[0].Rows[f]["ColumnName"].ToString().Substring(2);// dvDeals.Tables[0].[f]["ObjectID"];

                    dr["ColFilter"] = dvDeals.Tables[0].Rows[f]["FilterCondition"];
                    dr["ColValue"] = dvDeals.Tables[0].Rows[f]["Value"];
                    dr["ColRelation"] = dvDeals.Tables[0].Rows[f]["Relation"];
                    dr["ColRelation"] = dvDeals.Tables[0].Rows[f]["Value"];
                    dt.Rows.Add(dr);
                    //dr = dt.NewRow();
                }
                //Store the DataTable in ViewState
                ViewState["CurrentTable"] = dt;
                gvFilters.DataSource = dt;
                gvFilters.DataBind();
                SetPreviousData();
            }
        }

        protected void ddlSavedView_SelectedIndexChanged(object sender, EventArgs e)
        {
            Session["CurrentTable"] = null;
            ViewState["SavedViewTable"] = 1;
            bindFilterGrid();
            //bindObjectFields(1);
        }

        protected void btnbBindQueryResults_Click(object sender, EventArgs e)
        {
            bindDealsGrid();
            //bindObjectFields(1);
        }

        protected void btnNewFilter_Click(object sender, EventArgs e)
        {
            SetInitialRow();
            txtFilterName.Text = "";
            dataGridDeals.DataSource = null;
            dataGridDeals.DataBind();
            HttpContext.Current.Session["CurrentTable"] = null;
            //bindObjectFields();
        }

        public void bindDealsGrid()
        {
            DataTable dtFilterColumns = (DataTable)HttpContext.Current.Session["CurrentTable"];

            CRMBLLDeals objCRMDeals = null;
            CRMBLLContact objCRMContacts = null;
            CRMBLLAccount objCRMAccount = null;
            CRMBLLOpportunities objCRMOpportunities = null;
            CRMBLLQuote objCRMQuote = null;
            CRMBLLWorkOrder objCRMWorkOrder = null;
            CRMBLLInvoice objCRMInvoice = null;
            CRMBLLVendor objCRMVendor = null;
            CRMBLLProductClass objCRMProductClass = null;

            DataView dsDeals = null;
            if (Convert.ToInt64(Session["ObjectID"]) == 1) //Deals
            {
                objCRMDeals = new CRMBLLDeals();
                dsDeals = objCRMDeals.getDealsData(GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }
            else if (Convert.ToInt64(Session["ObjectID"]) == 2) //Contacts
            {
                objCRMContacts = new CRMBLLContact();
                dsDeals = objCRMContacts.getContactData(GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }
            else if (Convert.ToInt64(Session["ObjectID"]) == 3) //Accounts
            {
                objCRMAccount = new CRMBLLAccount();
                dsDeals = objCRMAccount.getAccountData(GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }
            else if (Convert.ToInt64(Session["ObjectID"]) == 4) //Opportunities
            {
                objCRMOpportunities = new CRMBLLOpportunities();
                dsDeals = objCRMOpportunities.getOpportunitiesData(GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }
            else if (Convert.ToInt64(Session["ObjectID"]) == 5) //Quote
            {
                objCRMQuote = new CRMBLLQuote();
                dsDeals = objCRMQuote.getQuoteData(GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }
            else if (Convert.ToInt64(Session["ObjectID"]) == 6) //Work Order
            {
                objCRMWorkOrder = new CRMBLLWorkOrder();
                dsDeals = objCRMWorkOrder.getWorkOrderData(GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }
            else if (Convert.ToInt64(Session["ObjectID"]) == 7) //Invoice
            {
                objCRMInvoice = new CRMBLLInvoice();
                dsDeals = objCRMInvoice.getInvoiceData(GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }

            else if (Convert.ToInt64(Session["ObjectID"]) == 8) //Invoice
            {
                objCRMVendor = new CRMBLLVendor();
                dsDeals = objCRMVendor.getVendorData(0, GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }

            else if (Convert.ToInt64(Session["ObjectID"]) == 9) //Invoice
            {
                objCRMProductClass = new CRMBLLProductClass();
                dsDeals = objCRMProductClass.getProductClassData(0, GlobalVariables.LoginUser.OrgID, 0).Tables[0].DefaultView;
            }

            if (dsDeals != null && dsDeals.Table.Rows.Count > 0)
            {
                string queryString = createQueryString(null, false).ToString();
                if (queryString != null && queryString != "")
                    dsDeals.RowFilter = queryString;

                DataTable ds = dsDeals.ToTable();

                if (dtFilterColumns != null)
                {
                    foreach (var c in dsDeals.Table.Columns)
                    {
                        //DataRow[] colExists = dtFilterColumns.Select("GridDisplayName='" + c.ToString() + "S'");
                        DataRow[] colNExists = dtFilterColumns.Select("GridDisplayName='" + c.ToString() + "s'");
                        if (colNExists.Count() == 0)
                            ds.Columns.Remove(c.ToString());
                        //else if (colNExists.Count() == 0)
                        //    ds.Columns.Remove(c.ToString());
                        //if (!colExists)
                        //    
                    }
                }

                dataGridDeals.DataSource = ds;
                dataGridDeals.DataBind();
            }
            else
            {
                dataGridDeals.DataSource = null;
                dataGridDeals.DataBind();
            }
        }

        protected void gvFilters_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (ViewState["CurrentTable"] != null)
            {
                DataTable dt = (DataTable)ViewState["CurrentTable"];
                //DataRow drCurrentRow = null;
                int rowIndex = Convert.ToInt32(e.RowIndex);
                if (dt.Rows.Count > 1)
                {
                    dt.Rows.Remove(dt.Rows[rowIndex]);
                    //drCurrentRow = dt.NewRow();
                    ViewState["CurrentTable"] = dt;
                    gvFilters.DataSource = dt;
                    gvFilters.DataBind();
                    SetPreviousData();
                    //for (int i = 0; i < grvStudentDetails.Rows.Count - 1; i++)
                    //{
                    //    grvStudentDetails.Rows[i].Cells[0].Text = Convert.ToString(i + 1);
                    //}
                    //SetPreviousData();
                }
                else
                    SetInitialRow();
            }
        }

        protected void dataGridDeals_RowCreated(object sender, GridViewRowEventArgs e)
        {
            try
            {
                //e.Row.Cells[0].Visible = false; // hides the first column
            }
            catch
            {
            }
        }



        /* EDIT COLUMNS */
        protected void btnAfterSaveFilterCols_Click(object sender, EventArgs e)
        {
        }

        private void bindObjectFields(int table1 = 0)
        {
            bool sessionExists = false;
            //HttpContext.Current.Session["CurrentTable"] = null;
            CRMBLLDeals _objDeals = new CRMBLLDeals();
            DataSet dsObjectCol = _objDeals.getObjectsColumns(Convert.ToInt64(ddlLookfor.SelectedValue), GlobalVariables.LoginUser.OrgID, Convert.ToInt64(ddlSavedView.SelectedValue));
            for (int c = 0; c < dsObjectCol.Tables[0].Rows.Count; c++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                li.InnerText = Convert.ToString(dsObjectCol.Tables[0].Rows[c]["ColumnName"]);
                li.ID = Convert.ToString(dsObjectCol.Tables[0].Rows[c]["GridDisplayName"]);
                ulFields.Controls.Add(li);
            }
            DataTable dtSubTable = null;
            if (table1 == 1 && Session["CurrentTable"] != null)
            {
                dtSubTable = (DataTable)HttpContext.Current.Session["CurrentTable"];
                sessionExists = true;
            }
            else
            {
                if (HttpContext.Current.Session["CurrentTable"] != null)
                {
                    dtSubTable = (DataTable)HttpContext.Current.Session["CurrentTable"];
                    sessionExists = true;
                }
                else
                    dtSubTable = dsObjectCol.Tables[1];
            }

            for (int d = 0; d < dtSubTable.Rows.Count; d++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                li.InnerText = Convert.ToString(dtSubTable.Rows[d]["ColumnName"]);
                if (sessionExists)
                    li.ID = Convert.ToString(dtSubTable.Rows[d]["GridDisplayName"]);
                else
                    li.ID = Convert.ToString(dtSubTable.Rows[d]["GridDisplayName"]) + "s";
                olPlaceholderControls.Controls.Add(li);
            }
            addDataTable(dtSubTable, sessionExists);
        }

        public void addDataTable(DataTable dtObjectCol, bool sessionExists)
        {
            DataTable dtFilterColumns = new DataTable();
            DataRow dr = null;

            dtFilterColumns.Columns.Add(new DataColumn("ColumnName", typeof(string)));
            dtFilterColumns.Columns.Add(new DataColumn("GridDisplayName", typeof(string)));

            for (int ObjectCol = 0; ObjectCol < dtObjectCol.Rows.Count; ObjectCol++)
            {
                dr = dtFilterColumns.NewRow();
                dr["ColumnName"] = dtObjectCol.Rows[ObjectCol]["ColumnName"];
                if (sessionExists)
                    dr["GridDisplayName"] = dtObjectCol.Rows[ObjectCol]["GridDisplayName"];
                else
                    dr["GridDisplayName"] = dtObjectCol.Rows[ObjectCol]["GridDisplayName"] + "s";
                dtFilterColumns.Rows.Add(dr);
            }
            if (dtFilterColumns.Rows.Count > 0)
                HttpContext.Current.Session["CurrentTable"] = dtFilterColumns;
            else
                HttpContext.Current.Session["CurrentTable"] = null;
        }

        [System.Web.Services.WebMethod]
        public static int getAllLiteralControls(string liValues, string liID)
        {
            DataTable dtFilterColumns = new DataTable();
            DataRow dr = null;

            dtFilterColumns.Columns.Add(new DataColumn("ColumnName", typeof(string)));
            dtFilterColumns.Columns.Add(new DataColumn("GridDisplayName", typeof(string)));

            if (liValues != "")
            {
                liValues = liValues.Substring(0, liValues.Length - 1);
                string[] columnNames = liValues.Split(',');

                liID = liID.Substring(0, liID.Length - 1);
                string[] columnIDs = liID.Split(',');


                for (int c = 0; c < columnNames.Count(); c++)
                {
                    dr = dtFilterColumns.NewRow();
                    string last = columnNames[c];
                    string Ids = columnIDs[c];
                    dr["ColumnName"] = last;
                    dr["GridDisplayName"] = Ids.Replace("%", " ");
                    dtFilterColumns.Rows.Add(dr);
                }
                //CRMBLLDeals _objDeals = new CRMBLLDeals();

                if (dtFilterColumns.Rows.Count > 0)
                    HttpContext.Current.Session["CurrentTable"] = dtFilterColumns;
                else
                    HttpContext.Current.Session["CurrentTable"] = null;
            }
            else
                HttpContext.Current.Session["CurrentTable"] = null;
            return 1;
        }
    }
}