﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;

namespace CRM
{
    public partial class CustomizeObject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            clsCommonFunctions.CheckSession();
            if (!Page.IsPostBack)
            {
                BindFieldList();
                BindFieldTypes();
            }
        }

        void BindFieldTypes()
        {
            CRMBLLCustomizeObject obj = new CRMBLLCustomizeObject();
            ddlFieldType.DataSource = obj.getFieldTypes();
            ddlFieldType.DataTextField = "Type";
            ddlFieldType.DataValueField = "ID";
            ddlFieldType.DataBind();
            ddlFieldType.SelectedIndex = 0;
            BindFieldFormat();
        }

        void BindFieldFormat()
        {
            ddlFieldFormat.DataSource = null;
            ddlFieldFormat.DataBind();
            CRMBLLCustomizeObject obj = new CRMBLLCustomizeObject();
            ddlFieldFormat.DataSource = obj.getFieldFormat(Convert.ToInt64(ddlFieldType.SelectedValue));
            ddlFieldFormat.DataTextField = "Format";
            ddlFieldFormat.DataValueField = "ID";
            ddlFieldFormat.DataBind();
            ddlFieldFormat.SelectedIndex = 0;
        }

        void BindFieldList()
        {
            CRMBLLCustomizeObject obj = new CRMBLLCustomizeObject();
            lstFields.DataSource = obj.getFields(Convert.ToInt64(Session["ObjectID"]));
            lstFields.DataBind();


            DataView dtPlaceHolders = obj.getPlaceHolders(Convert.ToInt64(Session["ObjectID"])).DefaultView;

            dtPlaceHolders.RowFilter = "IsSystemField=0";
            lstPlaceHolder.DataSource = dtPlaceHolders;// obj.getPlaceHolders(Convert.ToInt64(Session["ObjectID"]));
            lstPlaceHolder.DataBind();

            dtPlaceHolders.RowFilter = "IsSystemField=1";
            lstPlaceholderHeader.DataSource = dtPlaceHolders;// obj.getPlaceHolders(Convert.ToInt64(Session["ObjectID"]));
            lstPlaceholderHeader.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DataTable dtbl = new DataTable();
            dtbl.Columns.Add("ID");
            dtbl.Columns.Add("PlaceHolder");
            dtbl.Columns.Add("ObjectID");
            for (int i = 0; i < lstFields.Items.Count; i++)
            {
                DataRow dr = dtbl.NewRow();
                dr["ID"] = lstFields.DataKeys[i].ToString();
                HiddenField hdn = (HiddenField)lstFields.Items[i].FindControl("hdnField");
                dr["PlaceHolder"] = hdn.Value;
                dr["ObjectID"] = Convert.ToInt64(Session["ObjectID"]);
                dtbl.Rows.Add(dr);

            }
            if (dtbl.Rows.Count > 0)
            {
                CRMBLLCustomizeObject obj = new CRMBLLCustomizeObject();
                obj.SaveCustomize(dtbl);
                BindFieldList();
            }
        }

        protected void btnAddPlaceholder_Click(object sender, EventArgs e)
        {
            CRMBLLCustomizeObject obj = new CRMBLLCustomizeObject();
            obj.AddNewPlaceholder(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), txtPlaceHolderName.Text);
        }

        protected void btnRefreshCustomize_Click(object sender, EventArgs e)
        {
            BindFieldList();
            BindFieldTypes();
        }

        protected void ddlFieldType_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindFieldFormat();
            if (ddlFieldType.SelectedValue == "3" || ddlFieldType.SelectedValue == "4" || ddlFieldType.SelectedValue == "5")
            {
                trFormat.Visible = false;
                trMaxLength.Visible = false;
                trRadioChoices.Visible = true;
                txtMaxLength.Text = " ";
                txtChoices.Text = null;
            }
            else
            {
                txtMaxLength.Text = null;
                txtChoices.Text = " ";

                trFormat.Visible = true;
                trMaxLength.Visible = true;
                trRadioChoices.Visible = false;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "customizeObject();", true);
        }

        protected void btnSaveCustomize_Click(object sender, EventArgs e)
        {
            string choices = "";
            if (txtChoices.Text.Contains("\r\n"))
                choices = txtChoices.Text.Replace("\r\n", "||||");
            else
                choices = txtChoices.Text.Replace("\n", "||||");

            CRMBLLCustomizeObject obj = new CRMBLLCustomizeObject();
            int output = obj.AddNewField(Convert.ToInt64(txtCurrentFormID.Text), Convert.ToInt64(ddlFieldType.SelectedValue), Convert.ToInt64(ddlFieldFormat.SelectedValue), txtMaxLength.Text, Convert.ToInt64(Session["ObjectID"]), GlobalVariables.LoginUser.OrgID, txtColumnName.Text.Trim().Replace(" ", ""), txtDisplayName.Text, Convert.ToBoolean(rdbIsRequired.SelectedValue), choices);

            BindFieldList();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "closeWindow();", true);
        }

        protected void btnNewField_Click(object sender, EventArgs e)
        {
            txtDisplayName.Text = "";
            txtColumnName.Text = "";
            BindFieldTypes();
            txtMaxLength.Text = "";
            txtChoices.Text = "";
            txtCurrentFormID.Text = "0";
            ddlFieldFormat.Enabled = true;
            ddlFieldType.Enabled = true;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "customizeObject();", true);
        }

        protected void btnEditField_Click(object sender, EventArgs e)
        {
            txtDisplayName.Text = "";
            txtColumnName.Text = "";
            BindFieldTypes();
            txtMaxLength.Text = "";
            txtChoices.Text = "";
            ddlFieldFormat.Enabled = false;
            ddlFieldType.Enabled = false;

            CRMBLLCustomizeObject obj = new CRMBLLCustomizeObject();
            DataTable dtFormFields = obj.getFormField(Convert.ToInt64(txtCurrentFormID.Text));
            bool isSystemField = false;
            bool isEditable = true;
            if (dtFormFields.Rows.Count > 0)
            {
                isEditable = Convert.ToBoolean(dtFormFields.Rows[0]["IsEditable"]);
                isSystemField = Convert.ToBoolean(dtFormFields.Rows[0]["IsSystemField"]);
                txtDisplayName.Text = dtFormFields.Rows[0]["LabelName"].ToString();
                txtColumnName.Text = dtFormFields.Rows[0]["ColumnName"].ToString();
                rdbIsRequired.SelectedValue = Convert.ToString(dtFormFields.Rows[0]["IsRequired"]);
                ddlFieldType.SelectedValue = Convert.ToString(dtFormFields.Rows[0]["FieldType"]);
                BindFieldFormat();
                ddlFieldFormat.SelectedValue = Convert.ToString(dtFormFields.Rows[0]["FieldFormat"]);
                txtMaxLength.Text = Convert.ToString(dtFormFields.Rows[0]["MaximumLength"]);

                if (ddlFieldType.SelectedValue == "3" || ddlFieldType.SelectedValue == "4" || ddlFieldType.SelectedValue == "5")
                {
                    DataTable dtFormFieldChoices = obj.getFormControlChoices(Convert.ToInt64(txtCurrentFormID.Text));
                    for (int c = 0; c < dtFormFieldChoices.Rows.Count; c++)
                    {
                        if (c != 0)
                            txtChoices.Text = txtChoices.Text + "\r\n";
                        txtChoices.Text = txtChoices.Text + dtFormFieldChoices.Rows[c]["ControlChoices"].ToString();
                    }
                }

                if (ddlFieldType.SelectedValue == "3" || ddlFieldType.SelectedValue == "4" || ddlFieldType.SelectedValue == "5")
                {
                    trFormat.Visible = false;
                    trMaxLength.Visible = false;
                    trRadioChoices.Visible = true;
                    txtMaxLength.Text = " ";
                }
                else
                {
                    txtChoices.Text = " ";
                    trFormat.Visible = true;
                    trMaxLength.Visible = true;
                    trRadioChoices.Visible = false;
                }
            }
            if (isSystemField || isEditable == false)
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "alert('You can not edit system field')", true);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "customizeObject();", true);
        }

        protected void btnNewPlaceholder_Click(object sender, EventArgs e)
        {
            txtPlaceHolderName.Text = "";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "newPlaceholderBox();", true);
        }

        protected void img_Click(object sender, ImageClickEventArgs e)
        {
            CRMBLLCustomizeObject obj = new CRMBLLCustomizeObject();
            obj.deletePlaceholder(Convert.ToInt64(((System.Web.UI.WebControls.ImageButton)(sender)).CommandName), Convert.ToInt64(Session["ObjectID"]));
            BindFieldList();
        }
    }
}