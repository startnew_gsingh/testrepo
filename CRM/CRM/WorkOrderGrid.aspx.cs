﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.IO;
using System.Data;

namespace CRM
{
    public partial class WorkOrderGrid : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Session["selObjectUserID"] != null)
                    BindWorkOrderGrid(Convert.ToInt64(Session["selObjectUserID"]));
            }
        }

        protected void BindWorkOrderGrid(Int64 IDS)
        {
            CRMBLLWorkOrder _objWorkOrder = new CRMBLLWorkOrder();
            DataSet ds = _objWorkOrder.getWorkOrderGridData(GlobalVariables.LoginUser.OrgID, IDS);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvDetails.DataSource = ds;
                gvDetails.DataBind();
            }
            else
            {
                ds.Tables[0].Rows.Add(ds.Tables[0].NewRow());
                gvDetails.DataSource = ds;
                gvDetails.DataBind();
                int columncount = gvDetails.Rows[0].Cells.Count;
                gvDetails.Rows[0].Cells.Clear();
                gvDetails.Rows[0].Cells.Add(new TableCell());
                gvDetails.Rows[0].Cells[0].ColumnSpan = columncount;
                gvDetails.Rows[0].Cells[0].Text = "No Records Found";
            }
        }

        protected void gvDetails_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvDetails.EditIndex = e.NewEditIndex;
            BindWorkOrderGrid(Convert.ToInt64(Session["selObjectUserID"]));
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createWorkOrder();", true);
        }

        protected void gvDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int Id = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Value.ToString());
            TextBox txteditLine = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txteditLine");
            TextBox txtProductID = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtProductID");

            TextBox txtProductDesc = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtProductDesc");
            TextBox txtQuantity = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtQuantity");

            TextBox txtUnitPrice = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtUnitPrice");
            TextBox txtDiscountPercent = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtDiscountPercent");

            TextBox txtDiscountedAmount = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtDiscountedAmount");
            TextBox txtExtendedAmount = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtExtendedAmount");

            TextBox txtShippedDate = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtShippedDate");
            TextBox txtQuantitytoinvoice = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtQuantitytoinvoice");
            TextBox txtQuantityFulfill = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtQuantityFulfill");
            TextBox txtQuantityCancelled = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtQuantityCancelled");
            TextBox txtMONumber = (TextBox)gvDetails.Rows[e.RowIndex].FindControl("txtftrMONumber");


            CRMBLLWorkOrder _objWorkOrder = new CRMBLLWorkOrder();
            Int64 ds = _objWorkOrder.insertWorkOrderGrid(Id, Convert.ToInt64(Session["selObjectUserID"]), GlobalVariables.LoginUser.OrgID, txteditLine.Text, txtProductID.Text, txtProductDesc.Text,
                                                                    txtQuantity.Text, txtUnitPrice.Text, txtDiscountPercent.Text, txtDiscountedAmount.Text, txtExtendedAmount.Text, txtShippedDate.Text, txtQuantitytoinvoice.Text,
                                                                    txtQuantityFulfill.Text, txtQuantityCancelled.Text, txtMONumber.Text);

            gvDetails.EditIndex = -1;
            BindWorkOrderGrid(Convert.ToInt64(Session["selObjectUserID"]));
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createWorkOrder();", true);
        }

        protected void gvDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvDetails.EditIndex = -1;
            BindWorkOrderGrid(Convert.ToInt64(Session["selObjectUserID"]));
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createWorkOrder();", true);
        }

        protected void gvDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int selID = Convert.ToInt32(gvDetails.DataKeys[e.RowIndex].Value.ToString());
            CRMBLLWorkOrder _objWorkOrder = new CRMBLLWorkOrder();
            int delCount = _objWorkOrder.deleteWorkOrderGrid(selID);

            //if (delCount == 1)
            //{
            BindWorkOrderGrid(Convert.ToInt64(Session["selObjectUserID"]));
            //}
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createWorkOrder();", true);
        }

        protected void gvDetails_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("AddNew"))
            {
                TextBox txteditLine = (TextBox)gvDetails.FooterRow.FindControl("txtftrLine");
                TextBox txtProductID = (TextBox)gvDetails.FooterRow.FindControl("txtftrProductID");

                TextBox txtProductDesc = (TextBox)gvDetails.FooterRow.FindControl("txtftrProductDesc");
                TextBox txtQuantity = (TextBox)gvDetails.FooterRow.FindControl("txtftrQuantity");

                TextBox txtUnitPrice = (TextBox)gvDetails.FooterRow.FindControl("txtftrUnitPrice");
                TextBox txtDiscountPercent = (TextBox)gvDetails.FooterRow.FindControl("txtftrDiscountPercent");

                TextBox txtDiscountedAmount = (TextBox)gvDetails.FooterRow.FindControl("txtftrDiscountedAmount");
                TextBox txtExtendedAmount = (TextBox)gvDetails.FooterRow.FindControl("txtftrExtendedAmount");

                TextBox txtShippedDate = (TextBox)gvDetails.FooterRow.FindControl("txtftrShippedDate");
                TextBox txtQuantitytoinvoice = (TextBox)gvDetails.FooterRow.FindControl("txtftrQuantitytoinvoice");
                TextBox txtQuantityFulfill = (TextBox)gvDetails.FooterRow.FindControl("txtftrQuantityFulfill");
                TextBox txtQuantityCancelled = (TextBox)gvDetails.FooterRow.FindControl("txtftrQuantityCancelled");
                TextBox txtMONumber = (TextBox)gvDetails.FooterRow.FindControl("txtftrMONumber");

                CRMBLLWorkOrder _objWorkOrder = new CRMBLLWorkOrder();
                Int64 ds = _objWorkOrder.insertWorkOrderGrid(0, Convert.ToInt64(Session["selObjectUserID"]), GlobalVariables.LoginUser.OrgID, txteditLine.Text, txtProductID.Text, txtProductDesc.Text,
                                                                        txtQuantity.Text, txtUnitPrice.Text, txtDiscountPercent.Text, txtDiscountedAmount.Text, txtExtendedAmount.Text, txtShippedDate.Text, txtQuantitytoinvoice.Text,
                                                                    txtQuantityFulfill.Text, txtQuantityCancelled.Text, txtMONumber.Text);


                BindWorkOrderGrid(Convert.ToInt64(Session["selObjectUserID"]));
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "isActive", "createWorkOrder();", true);
            }
        }
    }
}