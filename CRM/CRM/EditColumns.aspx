﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EditColumns.aspx.cs" Inherits="CRM.EditColumns" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="css/jquery-ui.css" />
    <script src="js/jquery-1.9.1.js" type="text/javascript"></script>
    <script src="js/jquery-ui.js" type="text/javascript"></script>
    <style>
        h1
        {
            margin: 0;
        }
        #fields
        {
            float: left;
            width: 500px;
            margin-right: 2em;
        }
        #placeholder
        {
            width: 200px;
            float: left;
        }
        #placeholder ol
        {
            margin: 0;
            padding: 1em 0 1em 3em;
        }
    </style>
    <script>
        function getAllLiteralControls() {
            var str = "";
            $("#olPlaceholderControls li").each(function () {
                str = $(this).text() + "," + str;
            });

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "EditColumns.aspx/getAllLiteralControls",
                data: JSON.stringify({ liValues: str }),
                dataType: "json",
                success: function (data) {
                    alert("Success");
                },
                error: function (result) {
                    alert("Error");
                }
            });
        }

        $(function () {
            $("#fieldName").accordion();

            $("#fieldName li").draggable({
                appendTo: "body",
                helper: "clone"
            });

            $("#placeholder ol").droppable({
                activeClass: "ui-state-default",
                hoverClass: "ui-state-hover",
                accept: ":not(.ui-sortable-helper)",
                drop: function (event, ui) {
                    $(this).find(".placeholder").remove();
                    $("<li id=" + ui.draggable.text() + "></li>").text(ui.draggable.text()).appendTo(this);
                }
            }).sortable({
                items: "li:not(.placeholder)",
                sort: function () {
                    // gets added unintentionally by droppable interacting with sortable
                    // using connectWithSortable fixes this, but doesn't allow you to customize active/hoverClass options
                    $(this).removeClass("ui-state-default");
                }
            });

        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div id="fields">
            <h1 class="ui-widget-header">
                Fields</h1>
            <div id="fieldName">
                <div>
                    <ul runat="server" id="ulFields">
                    </ul>
                </div>
            </div>
        </div>
        <div id="placeholder">
            <h1 class="ui-widget-header">
                Columns</h1>
            <div class="ui-widget-content">
                <ol id="olPlaceholderControls">
                </ol>
            </div>
        </div>
        <div>
            <asp:Button ID="btnSaveFilterColumns" runat="server" Text="Save" OnClientClick="getAllLiteralControls()" />
        </div>
    </div>
    </form>
</body>
</html>
