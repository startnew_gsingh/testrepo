﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMBLL;
using System.Data;
using System.IO;

namespace CRM
{
    public partial class ObjectNotes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindNotes();
        }

        protected void bindNotes()
        {
            CRMBLLDeals objDeals = new CRMBLLDeals();
            DataSet dsDocumets = objDeals.getDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["sObjectUserID"]));
            gvNotes.DataSource = dsDocumets;
            gvNotes.DataBind();
        }

        protected void btnUploadDocs_Click(object sender, EventArgs e)
        {
            try
            {
                string path = Server.MapPath("~\\MyFiles\\" + Session["ObjectID"].ToString() + GlobalVariables.LoginUser.OrgID.ToString() + Session["sObjectUserID"].ToString());

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                string documentName = "";
                string documentPath = "";
                if (fleUpload.FileName != "")
                {
                    fleUpload.SaveAs(path + "\\" + System.IO.Path.GetFileName(fleUpload.FileName));
                    documentName = fleUpload.FileName;
                    documentPath = path;
                }
                //if (documentName != "")
                //{
                CRMBLLDeals objBLLDeals = new CRMBLLDeals();
                objBLLDeals.insertDocuments(GlobalVariables.LoginUser.OrgID, Convert.ToInt64(Session["ObjectID"]), Convert.ToInt64(Session["sObjectUserID"]), documentName, documentPath, Convert.ToInt64(ViewState["LeadID"]), "", txtNotes.Text, false, false);
                bindNotes();
                //}
            }
            catch (Exception ex)
            {

            }
        }
    }
}