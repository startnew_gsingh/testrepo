﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="NewDeals.aspx.cs" Inherits="CRM.NewDeals" EnableEventValidation="false" %>

<%@ Register Assembly="DevExpress.Web.v13.2, Version=13.2.5.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxGridView" TagPrefix="dx" %>
<%@ Register Assembly="JQChart.Web" Namespace="JQChart.Web.UI.WebControls" TagPrefix="jqChart" %>
<%@ Register TagPrefix="ajaxtoolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <link rel="stylesheet" type="text/css" href="css/jquery.jqChart.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.jqRangeSlider.css" />
    <script src="Script/jquery.jqRangeSlider.min.js" type="text/javascript"></script>
    <script src="Script/jquery.jqChart.min.js" type="text/javascript"></script>
    <link rel="Stylesheet" type="text/css" href="css/Site.css" />
    <link rel="Stylesheet" type="text/css" href="css/CustomDDStyles.css" />
    <script type="text/javascript">
        //        $(document).ready(function () {
        //            var ctw = $('.gradientbginner').width() - 20;
        //            $('.customwd').css({ 'width': ctw });
        //            //alert(ctw);
        //        });

        function checkExists(linkTable, controlID, orgID) {
            var txtValue = $("#ContentPlaceHolder1_" + controlID).val();
            $.ajax({
                type: "POST",
                url: "CustomList.asmx/checkAvailability",
                data: '{linkTable: "' + linkTable + '",value: "' + txtValue + '",orgID: "' + orgID + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var imgSuccess = $("#ContentPlaceHolder1_imgSuccess" + controlID)[0].id;
                    var imgFailure = $("#ContentPlaceHolder1_imgFailure" + controlID)[0].id;
                    switch (response.d) {
                        case 1:
                            $("#" + imgSuccess).show();
                            $("#" + imgFailure).hide();
                            $("#ContentPlaceHolder1_btnSave").removeAttr("disabled");
                            $("#ContentPlaceHolder1_btnSaveAndClose").removeAttr("disabled");
                            $("#ContentPlaceHolder1_btnQualify").removeAttr("disabled");
                            $("#ContentPlaceHolder1_btnDisqualify").removeAttr("disabled");
                            break;
                        case 0:
                            $("#" + imgSuccess).hide();
                            $("#" + imgFailure).show();
                            $("#ContentPlaceHolder1_btnSave").attr("disabled", true);
                            $("#ContentPlaceHolder1_btnSaveAndClose").attr("disabled", true);
                            $("#ContentPlaceHolder1_btnQualify").attr("disabled", true);
                            $("#ContentPlaceHolder1_btnDisqualify").attr("disabled", true);
                            break;
                    }
                },
                failure: function (response) {
                    alert(response);
                }
            });
        }

        function docReady() {
            var ctw = $('.gradientbginner').width() - 20;
            $('.customwd').css({ 'width': ctw });
        }
    </script>
    <script type="text/javascript">

        function createDeal() {
            alert("AS");
            var createDealBox = "#div_CreateDeal-box";
            //Fade in the Popup
            $(createDealBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createDealBox).height() + 24) / 2;
            var popMargLeft = ($(createDealBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function advanceFind() {
            var advanceFindBox = "#div_AdvanceFind-box";
            //Fade in the Popup
            $(advanceFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(advanceFindBox).height() + 24) / 2;
            var popMargLeft = ($(advanceFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="maskAdvanceFind"></div>');
            $('#maskAdvanceFind').fadeIn(300);

            return true;
        };

        function closeAdvanceFindWindow() {
            $('#maskAdvanceFind , .message-popup').fadeOut(300);
            $('#maskAdvanceFind').remove();

            document.getElementById("ContentPlaceHolder1_btnRefreshFilterList").click();
            return false;
        }

        // When clicking on the button close or the mask layer the popup closed
        function closeWindow() {
            $('#mask , .message-popup').fadeOut(300);
            $('#mask').remove();

            document.getElementById("ContentPlaceHolder1_btnRefreshUpdatePanel").click();
            return false;
        }

        function editColumns() {
            var editColumnsFindBox = "#div_EditColumns-box";
            //Fade in the Popup
            $(editColumnsFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(editColumnsFindBox).height() + 24) / 2;
            var popMargLeft = ($(editColumnsFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function customizeForm() {
            var editColumnsFindBox = "#div_Customize-box";
            //Fade in the Popup
            $(editColumnsFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(editColumnsFindBox).height() + 24) / 2;
            var popMargLeft = ($(editColumnsFindBox).width() + 24) / 2;

            var wh = $(window).height();
            $("#Iframe1").css('height', wh)

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function editSalesProcess() {
            var salesProcessBox = "#div_SalesProcess-box";
            //Fade in the Popup
            $(salesProcessBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(salesProcessBox).height() + 24) / 2;
            var popMargLeft = ($(salesProcessBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="maskSalesProcess"></div>');
            $('#maskSalesProcess').fadeIn(300);

            return true;
        };

        function closeEditSalesWindow() {
            $('#maskSalesProcess , .salesprocess-popup').fadeOut(300)
            $('#maskSalesProcess').remove();
            //docReady();

            document.getElementById("ContentPlaceHolder1_btnSalesProcess").click();

            return false;
        }

        function changeProcessStage(stageID, ObjectID) {
            if (document.getElementById("ContentPlaceHolder1_txtStageID").value != stageID) {
                $("#ContentPlaceHolder1_SPFields" + document.getElementById("ContentPlaceHolder1_txtStageID").value).hide();
                $("#ContentPlaceHolder1_SPFields" + stageID).show();

                $("#" + stageID).css('background-color', '#0072c6');
                $("#" + stageID).css('color', 'White');

                $("#" + document.getElementById("ContentPlaceHolder1_txtStageID").value).css('background-color', '#d7d0d0');
                $("#" + document.getElementById("ContentPlaceHolder1_txtStageID").value).css('color', 'black');

                document.getElementById("ContentPlaceHolder1_txtSelObjectID").value = ObjectID;
                document.getElementById("ContentPlaceHolder1_txtStageID").value = stageID;
                //document.getElementById("ContentPlaceHolder1_btnStageID").click();
            }
        };

        function changeProcessStageStep(objectID) {
            document.getElementById("ContentPlaceHolder1_txtObjectID").value = objectID;
            document.getElementById("ContentPlaceHolder1_btnObjectID").click();
        };

        //        function firstColumnClick(DealID) {
        //            document.getElementById("ContentPlaceHolder1_txtDealID").value = DealID;
        //            document.getElementById("ContentPlaceHolder1_btnGridColumn").click();
        //        };

        //        function notesColumnClick(DealID) {
        //            document.getElementById("ContentPlaceHolder1_txtNoteDealID").value = DealID;
        //            document.getElementById("ContentPlaceHolder1_btnGridNotes").click();
        //            //objectNotes();
        //        };

        function objectNotes() {
            var notesFindBox = "#div_Notes-box";
            //Fade in the Popup
            $(notesFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(notesFindBox).height() + 24) / 2;
            var popMargLeft = ($(notesFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="maskNotes"></div>');
            $('#maskNotes').fadeIn(300);

            return true;
        };

        function closeNotesWindow() {
            $('#maskNotes , .message-popup').fadeOut(300);
            $('#maskNotes').remove();
            return false;
        }

        //        function callColumnClick(DealID) {
        //            document.getElementById("ContentPlaceHolder1_txtNoteDealID").value = DealID;
        //            document.getElementById("ContentPlaceHolder1_btnPhoneCall").click();
        //            //document.getElementById("ContentPlaceHolder1_Button2").click();
        //            objectPhoneCall();
        //        };

        function objectPhoneCall() {
            var notesFindBox = "#div_PhoneCall-box";
            //Fade in the Popup
            $(notesFindBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(notesFindBox).height() + 24) / 2;
            var popMargLeft = ($(notesFindBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };

        function importData() {
            $("#trImportData").toggle();
            return false;
        }

        function emailToUsers() {
            var createDealBox = "#div_emailUser-box";
            //Fade in the Popup
            $(createDealBox).fadeIn(300);

            //Set the center alignment padding + border see css style
            var popMargTop = ($(createDealBox).height() + 24) / 2;
            var popMargLeft = ($(createDealBox).width() + 24) / 2;

            // Add the mask to body
            $('body').append('<div id="mask"></div>');
            $('#mask').fadeIn(300);

            return true;
        };
    </script>
    <script>
        $(document).ready(function () {
            $('.link').click(function () {
                if ($('.link').attr('class') == 'link open') {
                    $(this).removeClass('open')
                    $('.leftbox').animate({ 'left': '-520' });
                } else {
                    $(this).addClass('open')
                    $('.leftbox').animate({ 'left': '0' });
                }
            });
        });
        function chartButton() {
            if ($('.link').attr('class') == 'link open') {
                $(this).removeClass('open')
                $('.leftbox').animate({ 'left': '-520' });
            } else {
                $(this).addClass('open')
                $('.leftbox').animate({ 'left': '0' });
            }
            document.getElementById("ContentPlaceHolder1_btnRefreshUpdatePanel").click();
        }
    </script>
    <style>
        .filterDiv
        {
            width: 130px;
        }
        
        .imgfilter
        {
            width: 18px;
            display: block;
            border: none;
            background: gray url(../images/grid_search.JPG) 0 0 no-repeat;
        }
        .filterTextBox
        {
            line-height: 0px;
            float: left;
            width: 100px;
            border: none;
            height: 16px;
        }
        
        .CompletionListCssClass
        {
            margin: 0px !important;
            background-color: inherit;
            color: windowtext;
            border: buttonshadow;
            border-width: 1px;
            border-style: solid;
            cursor: 'default';
            overflow: auto;
            height: 200px;
            text-align: left;
            list-style-type: none;
            z-index: 9999999 !important;
        }
        .CompletionListItemCssClass
        {
            background-color: window;
            color: windowtext;
            padding: 1px;
        }
        
        body
        {
            padding: 0;
            margin: 0ee;
        }
        .leftbox
        {
            position: absolute;
            top: 130px;
            left: -520px;
        }
        .feedback
        {
            width: 500px;
            background: #eee;
            float: left;
            padding: 10px;
            border: 1px solid #ccc;
        }
        .link
        {
            float: left;
            padding: 10px;
            background: #eee;
            border: 1px solid #ccc;
            position: relative;
            z-index: 5;
            margin-left: -1px;
        }
        .link a
        {
            color: #333;
        }
    </style>
    <%--<asp:TextBox ID="txtDealID" Style="display: none;" Text="" runat="server"></asp:TextBox>--%>
    <%--<asp:TextBox ID="txtNoteDealID" Style="display: none;" Text="" runat="server"></asp:TextBox>--%>
    <%--<asp:TextBox ID="txtDelDocuments" Style="display: none;" Text="" runat="server"></asp:TextBox>--%>
    <div id="div_PhoneCall-box" class="message-popup">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div11" style="display: block;">
                    <div id="divPhoneCall12" style="overflow: auto; width: 100%; height: 100% !important;">
                        <table style="width: 100%;">
                            <tr>
                                <td style="vertical-align: top;">
                                    <b>Phone Calls</b><br />
                                    <table width="100%">
                                        <tr>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtPhoneCallDescription" Placeholder="Description" TextMode="MultiLine"
                                                    Style="min-height: 70px; min-width: 99%;" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:TextBox ID="txtCallDate" Placeholder="Due date" Style="width: 99%;" runat="server"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <asp:DropDownList ID="ddlDirection" Style="width: 99%;" runat="server">
                                                    <asp:ListItem Text="Outgoing" Value="Outgoing"></asp:ListItem>
                                                    <asp:ListItem Text="Incoming" Value="Incoming"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:Button ID="btnPhoneCall" Style="display: none;" runat="server" CssClass="button"
                                                    OnClick="btnPhoneCall_Click" CausesValidation="False" />
                                                <asp:Button ID="Button2" runat="server" Style="float: none !important;" CssClass="btn btn-primary"
                                                    Text="Save" OnClick="btnUploadPhoneCalls_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table width="100%" style="border-right: gray 1px solid; border-top: gray 1px solid;
                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                        <tr>
                                            <th align="left" colspan="2">
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="float: left;">
                                                <asp:Button ID="Button4" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAllCall_Click"
                                                    OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Call" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvPhoneCalls" runat="server" DataKeyNames="ID" DataKeyField="ID"
                                        AutoGenerateColumns="false" Width="100%" EmptyDataText="No Records Found">
                                        <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                    <asp:Label ID="lblCallID" runat="server" Text='<%#Eval("ID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Description
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("Description")
                                                    %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Date
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("DueDate") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Direction
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("Direction") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div id="div_Notes-box" class="message-popup">
        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeNotesWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="Div9" style="display: block;">
                    <div id="div10" style="overflow: auto; width: 100%; height: 100% !important;">
                        <%--<iframe id="Iframe2" frameborder="0" src="ObjectNotes.aspx" onload="ImagesDone();"
                            style="overflow: hidden;" width="100%" height="495px"></iframe>--%>
                        <table style="width: 100%;">
                            <tr>
                                <td style="vertical-align: top;">
                                    <b>Notes</b><br />
                                    <asp:TextBox TextMode="MultiLine" Style="min-height: 70px; width: 98%;" ID="txtNotes"
                                        runat="server"></asp:TextBox><br />
                                    <br />
                                    <%--<asp:FileUpload runat="server" ID="fleUpload" />--%><br />
                                    <br />
                                    <asp:Button ID="btnUpload" runat="server" Style="float: none !important;" CssClass="btn btn-primary"
                                        Text="Save" OnClick="btnUploadDocs_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table width="100%" style="border-right: gray 1px solid; border-top: gray 1px solid;
                                        border-left: gray 1px solid; border-bottom: gray 1px solid; border-collapse: collapse;">
                                        <tr>
                                            <th align="left" colspan="2">
                                            </th>
                                        </tr>
                                        <tr>
                                            <td style="float: left;">
                                                <asp:Button ID="Button3" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteNotes_Click"
                                                    OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Note" CausesValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:GridView ID="gvNotes" EmptyDataText="No Records Found" runat="server" DataKeyNames="ID"
                                        DataKeyField="ID" AutoGenerateColumns="false" Width="100%">
                                        <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                        <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                    <asp:Label ID="lblNoteID" runat="server" Text='<%#Eval("ID") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Notes
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("Notes") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField>
                                                <HeaderTemplate>
                                                    Document Name
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("DocumentName") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    Created Date
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%#Eval("CreatedDate") %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <script type="text/javascript" src="Script/jqmin.js"></script>
    <script type="text/javascript">
        $('#ContentPlaceHolder1_btnAdvanceFind').click(function () {
            alert("AS");
            var popw = $('#div_AdvanceFind-box').width();
            var poph = $('#div_AdvanceFind-box').height();
            alert(popw);
            var ww = $(window).width();
            var wh = $(window).height();
            $('#div_AdvanceFind-box').css({ 'left': (ww - popw) / 2, 'top': (wh - poph) / 2 });
        });
    	
    </script>
    <%--<div id="div_emailUser-box" class="message-popup" style="overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div12" style="display: block;">
            <div id="div13" style="overflow: scroll; width: 100%; height: 97% !important;">
                <asp:GridView ID="dataGridUser" runat="server" AutoGenerateColumns="false" Width="100%"
                    DataKeyField="OrgID" DataKeyNames="OrgID" AllowPaging="false" AllowSorting="true"
                    EmptyDataText="No Record Found">
                    <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                    <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Name
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("FirstName") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Phone
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("Phone") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Country
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("Country") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                State
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("State") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                City
                            </HeaderTemplate>
                            <ItemTemplate>
                                <%#Eval("City") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>--%>
    <div id="div_AdvanceFind-box" class="message-popup" style="overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeAdvanceFindWindow()"
                title="Close Window" alt="Close" /></a>
        <div id="Div2" style="display: block;">
            <div id="div3" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="ifrmAdvanceFind" frameborder="0" src="AdvanceFind.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%" height="100%"></iframe>
            </div>
        </div>
    </div>
    <div id="div_EditColumns-box" class="message-popup" style="overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div1" style="display: block;">
            <div id="div4" style="overflow: scroll; width: 100%; height: 97% !important;">
            </div>
        </div>
    </div>
    <div id="div_Customize-box" class="message-popup" style="height: 100%; overflow: hidden;">
        <a href="#" class="close">
            <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                alt="Close" /></a>
        <div id="Div6" style="display: block;">
            <div id="div7" style="overflow: auto; width: 100%; height: 100% !important;">
                <iframe id="Iframe1" frameborder="0" src="CustomizeObject.aspx" onload="ImagesDone();"
                    style="overflow: hidden;" width="100%"></iframe>
            </div>
        </div>
    </div>
    <div id="div_CreateDeal-box" class="message-popup">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeWindow()" title="Close Window"
                        alt="Close" /></a>
                <div id="showpage" style="display: block; width: 100%; height: 97% !important;">
                    <div id="divIframe" runat="server">
                        <div>
                            <table width="100%">
                                <tr>
                                    <td colspan="2">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2">
                                                    <asp:Button ID="btnSave" runat="server" CssClass="save" ValidationGroup="createValidation"
                                                        OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnSaveAndClose" runat="server" CssClass="saveandClose" ValidationGroup="createValidation"
                                                        OnClick="btnSave_Click" />
                                                    <asp:Button ID="btnEditSalesProcess" CssClass="editSalesProcess" OnClick="btnEditSalesProcess_Click"
                                                        runat="server" Text="" />
                                                    <asp:Button ID="btnQualify" OnClick="btnQualify_Click" CssClass="qualify" ValidationGroup="createValidation"
                                                        runat="server" Text="" />
                                                    <asp:Button ID="btnDisqualify" OnClick="btnDisqualify_Click" ValidationGroup="createValidation"
                                                        CssClass="disqualify" runat="server" Text="" />
                                                    <asp:Button ID="btnReactivate" runat="server" Visible="false" CssClass="reactivateDeal"
                                                        Text="" OnClick="btnReactivate_Click" />
                                                    <asp:Button ID="btnCloseWon" Visible="false" CssClass="closeaswon" runat="server"
                                                        OnClick="btnCloseWon_Click" Text="" />
                                                    <asp:Button ID="btnCloseasLost" Visible="false" CssClass="closeaslost" runat="server"
                                                        OnClick="btnCloseLost_Click" Text="" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    > <span id="spObjectText" runat="server"></span>
                                                </td>
                                                <td>
                                                    <div style="float: right;">
                                                        <asp:PlaceHolder ID="placeholderHeaderInfo" runat="server"></asp:PlaceHolder>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="2">
                                                    <table width="100%">
                                                        <tr>
                                                            <td>
                                                                <div id="dvStages" runat="server" style="float: left;">
                                                                </div>
                                                                <div style="clear: both;">
                                                                </div>
                                                                <div style="border: 1px solid #0072c6 !important;">
                                                                    <asp:PlaceHolder ID="placeholderSalesProcess" runat="server"></asp:PlaceHolder>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <asp:TextBox ID="txtSelObjectID" Enabled="false" runat="server" Style="display: none;"></asp:TextBox>
                                                                <asp:TextBox ID="txtStageID" Text="0" Enabled="false" runat="server" Style="display: none;"></asp:TextBox>
                                                                <asp:Button ID="btnStageID" ValidationGroup="createValidation" runat="server" OnClick="btnStageID_Click"
                                                                    Style="display: none;" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top;">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <b>Summary</b>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top;">
                                                    <asp:PlaceHolder ID="placeholderGeneralInfo" runat="server"></asp:PlaceHolder>
                                                    <asp:Label ID="lblLeadID" runat="server" Visible="false"></asp:Label>
                                                </td>
                                                <td valign="top">
                                                    <table width="100%">
                                                        <tr>
                                                            <td style="width: 100%; vertical-align: top; text-align: left; float: right;">
                                                                <iframe id="Iframe4" src="Notes.aspx" style="width: 100%; border: none; min-height: 450px;
                                                                    overflow-x: hidden; overflow-y: scroll;" runat="server"></iframe>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="4">
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <asp:UpdatePanel ID="UpdatePanel2" runat="server">
        <ContentTemplate>
            <asp:Button ID="btnSalesProcess" Style="display: none;" OnClick="btnRefreshSalesProcess_Click"
                runat="server" />
            <div id="div_SalesProcess-box" class="salesprocess-popup" style="overflow-y: auto;
                overflow-x: hidden; min-height: 300px;">
                <a href="#" class="close">
                    <img src="images/close_pop.png" class="btn_close" onclick="closeEditSalesWindow()"
                        title="Close Window" alt="Close" /></a>
                <div id="Div8" style="display: block; overflow: hidden; width: 100%; height: 100%;">
                    <div id="dvObjectColumns" runat="server" style="overflow: hidden; width: 100%; height: 97% !important;">
                    </div>
                    <div style="overflow: hidden; width: 100%; height: 97% !important;">
                        <asp:TextBox ID="txtObjectID" Enabled="false" runat="server" Style="display: none;"></asp:TextBox>
                        <asp:Button ID="btnObjectID" runat="server" OnClick="txtObjectID_Click" Style="display: none;" />
                        <table id="tbNewStage" runat="server">
                            <tr>
                                <td style="width: 250px; vertical-align: top;">
                                    <asp:TextBox runat="server" placeholder="Stage Name" ID="txtNewStageName"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNewStageName"
                                        Display="Dynamic" ValidationGroup="newStageStep" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 210px; vertical-align: top;">
                                    <asp:TextBox ID="txtNewStageStep" placeholder="Step Name" runat="server" />
                                    <asp:RequiredFieldValidator ID="rqnewStageStep" runat="server" ControlToValidate="txtNewStageStep"
                                        Display="Dynamic" ValidationGroup="newStageStep" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </td>
                                <td style="width: 210px; vertical-align: top;">
                                    <asp:DropDownList ID="ddlNewInsertObjectColumns" DataTextField="GridDisplayName"
                                        OnLoad="ddlInsertObjectColumns_load" DataValueField="FormID" runat="server">
                                    </asp:DropDownList>
                                    <asp:Button ID="InsertButton" ValidationGroup="newStageStep" runat="server" OnClick="btnSaveProcess_Click"
                                        CssClass="savebtn" Text="New Stage" />
                                </td>
                            </tr>
                        </table>
                        <asp:ListView ID="lsSalesProcessStages" OnItemInserting="lsSalesProcessStages_ItemInserting"
                            runat="server" DataKeyNames="ID" InsertItemPosition="LastItem" OnItemCommand="lsSalesProcessStages_ItemCommand">
                            <LayoutTemplate>
                                <table border="0" cellpadding="1" style="width: 100%;">
                                    <tr style="">
                                        <th align="left" style="width: 253px;">
                                            STAGES
                                        </th>
                                        <th align="left" style="width: 208px;">
                                            STEPS
                                        </th>
                                        <th align="left" style="width: 202px;">
                                            FIELDS
                                        </th>
                                        <th align="left">
                                            Action
                                        </th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </table>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <table>
                                    <tr>
                                        <td style="width: 255px; vertical-align: top;">
                                            <asp:Label runat="server" ID="lblStageName" Text='<%#Eval("StageName") %>'></asp:Label>
                                        </td>
                                        <td colspan="2">
                                            <asp:ListView ID="lvStageFields" OnItemInserting="ListView1_ItemInserting" runat="server"
                                                DataKeyNames="StageID" DataSource='<%# Eval("ProcessSetps") %>' ItemPlaceholderID="addressPlaceHolder"
                                                InsertItemPosition="LastItem" OnItemCommand="ListView1_ItemCommand">
                                                <LayoutTemplate>
                                                    <table cellpadding="0" cellspacing="0">
                                                        <asp:PlaceHolder runat="server" ID="addressPlaceHolder" />
                                                    </table>
                                                    <hr />
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <table>
                                                        <tr>
                                                            <td>
                                                                <asp:Label ID="lblStepID" Style="display: none;" runat="server" Text='<%# Eval("StepID") %>'></asp:Label>
                                                                <asp:TextBox ID="txtStepName" Style="width: 210px;" Enabled="false" Text='<%# Eval("StepName") %>'
                                                                    runat="server" />
                                                            </td>
                                                            <td style="width: 210px;">
                                                                <asp:DropDownList ID="ddlObjectColumns" Style="width: 210px;" Enabled="false" DataSource='<%# Eval("EntityColumns") %>'
                                                                    DataTextField="GridDisplayName" OnDataBound="ddlColumns_load" DataValueField="FormID"
                                                                    runat="server">
                                                                </asp:DropDownList>
                                                                <%--<asp:Label ID="lblObjectUserID" Text='<%# Eval("ColumnName") %>' runat="server" />--%>
                                                            </td>
                                                            <td>
                                                                <asp:ImageButton ID="DeleteStageButton" CommandName='<%# Eval("StageID") %>' runat="server"
                                                                    OnClick="btnDelete_Click" Style="width: 18px; height: 17px;" ImageUrl="~/images/delete.png" />
                                                                <%--<asp:Button ID="DeleteStageButton" CommandName='<%# Eval("StageID") %>' runat="server"
                                                                    OnClick="btnDelete_Click" CssClass="button" Style="width: 130px;" Text="Delete" />--%>
                                                            </td>
                                                        </tr>
                                                        <%--<tr>
                                                            <td>
                                                                <asp:Button ID="btnNewField" OnClick="btnNewStage_Click" runat="server" Text="New Field" />
                                                            </td>
                                                        </tr>--%>
                                                    </table>
                                                </ItemTemplate>
                                                <InsertItemTemplate>
                                                    <tr>
                                                        <td style="width: 255px;">
                                                            &nbsp;
                                                        </td>
                                                        <td style="width: 210px; padding-left: 4px;">
                                                            <asp:TextBox ID="txtStepName" Style="width: 210px;" placeholder="Step Name" runat="server" />
                                                            <%--<asp:RequiredFieldValidator ID="rqStepName" runat="server" ControlToValidate="txtStepName"
                                                                Display="Dynamic" ValidationGroup="stepName" ErrorMessage="*"></asp:RequiredFieldValidator>--%>
                                                        </td>
                                                        <td style="width: 210px; padding-left: 4px; float: left;">
                                                            <asp:DropDownList ID="ddlInsertObjectColumns" Style="width: 210px;" DataTextField="GridDisplayName"
                                                                OnLoad="ddlInsertObjectColumns_load" DataValueField="FormID" runat="server">
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td style="float: left; padding-left: 4px;">
                                                            <asp:Button ID="InsertButton" runat="server" CommandName="Insert" CssClass="savebtn"
                                                                Text="New Step" />
                                                        </td>
                                                    </tr>
                                                </InsertItemTemplate>
                                            </asp:ListView>
                                        </td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                            <InsertItemTemplate>
                                <table style="width: 100%;">
                                    <tr>
                                        <td style="vertical-align: top; width: 28%; float: left;">
                                            <asp:TextBox runat="server" Width="97%" placeholder="Stage Name" ID="txtNewStageName"></asp:TextBox>
                                        </td>
                                        <td style="vertical-align: top; width: 23.2%; float: left;">
                                            <asp:TextBox ID="txtNewStageStep" Width="97%" placeholder="Step Name" runat="server" />
                                        </td>
                                        <td style="vertical-align: top; width: 33.2%; float: left;">
                                            <asp:Button ID="InsertButton" ValidationGroup="newStageStep" runat="server" CommandName="Insert"
                                                CssClass="savebtn" Style="float: right;" Text="New Stage" />
                                            <asp:DropDownList ID="ddlNewInsertObjectColumns" Style="vertical-align: top;" Width="68%"
                                                DataTextField="GridDisplayName" OnLoad="ddlInsertObjectColumns_load" DataValueField="FormID"
                                                runat="server">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </InsertItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
            </div>
            <div class="gradientbginner" style="width: 90%; overflow: auto;">
                <table width="100%">
                    <tr>
                        <td width="18px">
                        </td>
                        <td>
                            <div class="maincontent" style="width: 100%">
                                <div class="searchboxcaption">
                                    <span>Leads</span></div>
                                <div class="searchbox">
                                    <div>
                                        <table width="100%">
                                            <tr>
                                                <%--<td align="center" valign="middle" style="padding-right: 3px; padding-left: 3px;
                                                    padding-bottom: 3px; padding-top: 3px">--%><div style="text-align: center;">
                                                        <asp:Label ID="lblListmsg" Style="text-align: center; color: Red; font-size: 13px;"
                                                            runat="server" EnableViewState="False"></asp:Label>
                                                    </div>
                                                <%--</td>--%>
                                                <td id="trallButtons" runat="server">
                                                    <asp:Button ID="btnCreateDeal" CssClass="create_dtl" OnClick="btnEdit_Click" Text=""
                                                        runat="server" />
                                                    <asp:Button ID="btnAdvanceFind" CssClass="advance_find" OnClientClick="advanceFind();"
                                                        Text="" runat="server" />
                                                    <asp:Button ID="btnCustomizeObject" CssClass="customize" Text="" OnClientClick="customizeForm()"
                                                        runat="server" />
                                                    <asp:Button ID="btnExcel" CssClass="export" Text="" OnClick="btnExcel_Click" runat="server" />
                                                    <asp:Button ID="btnImport" CssClass="import" OnClientClick="return importData();"
                                                        runat="server" Text="" />
                                                    <asp:Button ID="btnDeleteAll" runat="server" CssClass="delete_data" Text="" OnClick="btnDeleteAll_Click"
                                                        OnClientClick="return CheckboxSelection();" ToolTip="Delete Selected Deal" CausesValidation="False" />
                                                </td>
                                            </tr>
                                            <tr id="trImportData" style="display: none;">
                                                <td style="padding: 10px; border: 1px solid #2966a5;">
                                                    <asp:FileUpload ID="fleImport" Style="float: left;" runat="server" />
                                                    <asp:Button ID="btnUploadData" CssClass="btn btn-primary" runat="server" OnClick="btnUpload_Click"
                                                        Text="Import" />
                                                </td>
                                            </tr>
                                            <tr id="trFilter" runat="server">
                                                <td align="center" valign="top">
                                                    <table class="customwd" style="float: left; width: 100%; border-right: gray 1px solid;
                                                        border-top: gray 1px solid; border-left: gray 1px solid; border-bottom: gray 1px solid;
                                                        border-collapse: collapse;">
                                                        <tr>
                                                            <th align="left" colspan="2">
                                                                Filter Records
                                                            </th>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 100%;">
                                                                <asp:DropDownList ID="ddlFilters" Width="300px" runat="server" AutoPostBack="true"
                                                                    OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged">
                                                                </asp:DropDownList>
                                                                <asp:DropDownCheckBoxes ID="ddlGridColumns1" runat="server" OnSelectedIndexChanged="checkBoxes_SelcetedIndexChanged"
                                                                    AddJQueryReference="True" UseButtons="True" UseSelectAllNode="True">
                                                                    <Style SelectBoxWidth="300" DropDownBoxBoxWidth="300" DropDownBoxBoxHeight="120" />
                                                                    <Texts SelectBoxCaption="Columns" />
                                                                </asp:DropDownCheckBoxes>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr id="trGridView" runat="server">
                                                <td align="center" valign="top">
                                                    <div style="overflow: auto; float: left; width: 100%;" class="customwd">
                                                        <dx:ASPxGridView ID="dataGridLeads" OnDataBound="dataGridLeads_DataBound" OnCustomButtonCallback="dataGridLeads_CustomCallBack"
                                                            ClientInstanceName="grid" OnRowCommand="dataGridLeads_RowCommand" KeyFieldName="LeadID"
                                                            OnClientLayout="dataGridLeads_ClientLayout" OnHtmlRowPrepared="dataGridLeads_HtmlRowPrepared"
                                                            runat="server" EnableCallBacks="true">
                                                        </dx:ASPxGridView>
                                                        <%--<asp:GridView ID="dataGridDeals" runat="server" AutoGenerateColumns="true" DataKeyField="LeadID"
                                                            DataKeyNames="LeadID" AllowPaging="true" PageSize="10" OnPageIndexChanging="dataGridDeals_PageIndexChanging"
                                                            AllowSorting="true" OnSorting="dataGridDeals_Sorting" EmptyDataText="No Record Found"
                                                            OnDataBound="dataGridDeals_DataBound" OnRowCreated="dataGridDeals_RowCreated"
                                                            Width="100%" OnRowDataBound="dataGridDeals_RowDataBound">
                                                            <RowStyle HorizontalAlign="Left" CssClass="NormalText" />
                                                            <HeaderStyle HorizontalAlign="Left" ForeColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <HeaderTemplate>
                                                                        <input name="chkBoxSelectAll" onclick="SelectAllCheckboxes(this)" type="checkbox">
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkBoxRowID" runat="Server" EnableViewState="False" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            
                                                        </asp:GridView>--%>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr id="trButtons" runat="server">
                                                <td align="left" valign="top">
                                                    <table id="tblDeleteAll" runat="server">
                                                        <tr>
                                                            <td align="left">
                                                            </td>
                                                            <td class="NormalText">
                                                            </td>
                                                            <td>
                                                                <asp:Button ID="btnEdit" runat="server" Style="display: none;" CssClass="button"
                                                                    Text="Edit Deal" OnClick="btnEdit_Click" ToolTip="Edit Selected Deal" CausesValidation="False" />
                                                                <asp:Button ID="btnRefreshFilterList" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnRefreshFilterList_Click" CausesValidation="False" />
                                                                <asp:Button ID="btnRefreshUpdatePanel" Style="display: none;" runat="server" CssClass="button"
                                                                    OnClick="btnrefreshUpdatePanel_Click" CausesValidation="False" />
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                                <br />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <%--<div id="dvPaging" runat="server" style="float: right; margin-right: 5%;">
                <asp:Button ID="imgPageFirst" runat="server" Text="|<< First" CommandArgument="First"
                    CommandName="Page" OnCommand="imgPageFirst_Command" CssClass="pagingbtn" />
                <asp:Button ID="imgPagePrevious" runat="server" Text="< Prev" CommandArgument="Prev"
                    CommandName="Page" OnCommand="imgPagePrevious_Command" CssClass="pagingbtn" />
                <asp:DropDownList ID="ddCurrentPage" runat="server" Style="font-size: 11px;" CssClass="Normal"
                    AutoPostBack="True" OnSelectedIndexChanged="ddCurrentPage_SelectedIndexChanged">
                </asp:DropDownList>
                <asp:Label ID="lblTotalPage" runat="server" CssClass="Normal"></asp:Label>
                <asp:Button ID="imgPageNext" runat="server" Text="Next >" CommandArgument="Next"
                    CommandName="Page" OnCommand="imgPageNext_Command" CssClass="pagingbtn"></asp:Button>
                <asp:Button ID="imgPageLast" runat="server" Text="Last >>|" CommandArgument="Last"
                    CommandName="Page" OnCommand="imgPageLast_Command" CssClass="pagingbtn"></asp:Button>
            </div>--%>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExcel" />
            <asp:PostBackTrigger ControlID="ddlChartType" />
            <asp:PostBackTrigger ControlID="btnUploadData" />
            <asp:PostBackTrigger ControlID="btnEdit" />
            <%--<asp:PostBackTrigger ControlID="btnGridColumn" />--%>
            <%--<asp:PostBackTrigger ControlID="btnGridNotes" />--%>
            <%--<asp:PostBackTrigger ControlID="btnPhoneCall" />--%>
            <%--<asp:PostBackTrigger
    ControlID="btnRefreshUpdatePanel" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <div class="leftbox">
        <div class="feedback">
            <div>
                <asp:DropDownList ID="ddlChartType" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlChartType_SelectedIndexChanged">
                    <asp:ListItem Text="Leads By Owner" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Leads By Status" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div>
                <jqChart:Chart ID="statusChart" Width="500px" Height="300px" runat="server">
                    <Title Text=""></Title>
                    <Animation Enabled="True" Duration="00:00:02" />
                    <Axes>
                        <jqChart:CategoryAxis Location="Bottom" ZoomEnabled="false">
                        </jqChart:CategoryAxis>
                    </Axes>
                    <Series>
                        <jqChart:PieSeries AxisX="Label" AxisY="Value1" DataLabelsField="Label" DataValuesField="Value1"
                            Labels-ValueType="DataValue" Title="Pie">
                        </jqChart:PieSeries>
                    </Series>
                </jqChart:Chart>
                <jqChart:Chart ID="Chart1" Width="500px" Height="300px" runat="server">
                    <Title Text=""></Title>
                    <Animation Enabled="True" Duration="00:00:02" />
                    <Axes>
                        <jqChart:CategoryAxis Location="Bottom" ZoomEnabled="false">
                        </jqChart:CategoryAxis>
                    </Axes>
                    <Series>
                        <jqChart:ColumnSeries XValuesField="Label" YValuesField="Value1" Title="Column">
                        </jqChart:ColumnSeries>
                        <jqChart:LineSeries XValuesField="Label" YValuesField="Value1" Title="Line">
                        </jqChart:LineSeries>
                    </Series>
                </jqChart:Chart>
            </div>
        </div>
        <div class="link">
            <a href="#"><b>Chart</b></a>
        </div>
    </div>
</asp:Content>
