﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLQuote
    {
        public DataSet getQuoteData(Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10, string filterQuery = "")
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getQuoteData(OrgID, FilterID, PageNo, PageSize, filterQuery);
        }

        public DataSet getSelectedQuote(Int64 QuoteID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getSelectedQuote(QuoteID);
        }

        public DataSet getAllObjects()
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getAllObjects();
        }

        //public DataSet getObjectsColumns(Int64 ObjectID)
        //{
        //    QuoteDAL _objQuoteDAL = new QuoteDAL();
        //    return _objQuoteDAL.getObjectsColumns(ObjectID);
        //}

        public int insertFilters(StructureFilterDAL ObjFilter)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.insertFilter(ObjFilter);
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getFilters(OrgID, ObjectID);
        }

        public int insertFilterQuery(DataTable objFilterQuery, Int64 filterID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.insertFilterQuery(objFilterQuery, filterID);
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getFiltersQuery(FilterID);
        }

        public int insertQuote(Int64 orgID, string columnValues, Int64 QuoteID, string salesColNames, string salesColValues)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.insertQuote(orgID, columnValues, QuoteID, salesColNames, salesColValues);
        }

        public DataSet getColumnControls(Int64 ObjectID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getColumnControls(ObjectID);
        }

        public DataSet getControlChoices(Int64 formID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getControlChoices(formID);
        }

        public int insertFilterColumn(DataTable objFilterColumn, Int64 filterID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.insertFilterColumn(objFilterColumn, filterID);
        }

        public DataSet getControlChoicesByColumnName(string ColumnName)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getControlChoicesByColumnName(ColumnName);
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getPlaceholders(ObjectID, OrgID);
        }

        public int deleteQuotes(string QuoteID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.deleteQuote(QuoteID);
        }



        public DataSet getQuoteGridData(Int64 OrgID, Int64 QuoteID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.getQuoteGridData(OrgID, QuoteID);
        }

        public int insertQuoteGrid(Int64 ID, Int64 QuoteID, Int64 OrgID, string Line, string ProductID, string ProductDesc, string Quantity, string UnitPrice,
                                            string DiscountPercent, string DiscountAmount, string ExtendedPercent)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.insertQuoteGrid(ID, QuoteID, OrgID, Line, ProductID, ProductDesc, Quantity, UnitPrice, DiscountPercent, DiscountAmount, ExtendedPercent);
        }

        public int deleteQuoteGrid(Int64 ID)
        {
            QuoteDAL _objQuoteDAL = new QuoteDAL();
            return _objQuoteDAL.deleteQuoteGrid(ID);
        }

        public int insertMCAnswers(DataTable dtInsertRows, Int64 LeadID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertMCAnswers(dtInsertRows, LeadID);
        }

        public DataSet getMCAnswers(Int64 OrgID, Int64 ObjectID, Int64 ObjectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getMCAnswers(OrgID, ObjectID, ObjectUserID);
        }
    }
}
