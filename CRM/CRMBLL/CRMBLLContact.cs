﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLContact
    {
        public DataSet getContactData(Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10, string filterQuery = "")
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getContactData(OrgID, FilterID, PageNo, PageSize, filterQuery);
        }

        public DataSet getSelectedContact(Int64 ContactID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getSelectedContact(ContactID);
        }

        public DataSet getAllObjects()
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getAllObjects();
        }

        //public DataSet getObjectsColumns(Int64 ObjectID)
        //{
        //    ContactDAL _objContactDAL = new ContactDAL();
        //    return _objContactDAL.getObjectsColumns(ObjectID);
        //}

        public int insertFilters(StructureFilterDAL ObjFilter)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.insertFilter(ObjFilter);
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getFilters(OrgID, ObjectID);
        }

        public int insertFilterQuery(DataTable objFilterQuery, Int64 filterID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.insertFilterQuery(objFilterQuery, filterID);
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getFiltersQuery(FilterID);
        }

        public int insertContact(Int64 orgID, string columnValues, Int64 ContactID, string salesColNames, string salesColValues)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.insertContact(orgID, columnValues, ContactID, salesColNames, salesColValues);
        }

        public DataSet getColumnControls(Int64 ObjectID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getColumnControls(ObjectID);
        }

        public DataSet getControlChoices(Int64 formID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getControlChoices(formID);
        }

        public int insertFilterColumn(DataTable objFilterColumn, Int64 filterID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.insertFilterColumn(objFilterColumn, filterID);
        }

        public DataSet getControlChoicesByColumnName(string ColumnName)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getControlChoicesByColumnName(ColumnName);
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.getPlaceholders(ObjectID, OrgID);
        }

        public int deleteContacts(string ContactID)
        {
            ContactDAL _objContactDAL = new ContactDAL();
            return _objContactDAL.deleteContact(ContactID);
        }



        public int insertMCAnswers(DataTable dtInsertRows, Int64 LeadID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertMCAnswers(dtInsertRows, LeadID);
        }

        public DataSet getMCAnswers(Int64 OrgID, Int64 ObjectID, Int64 ObjectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getMCAnswers(OrgID, ObjectID, ObjectUserID);
        }
    }
}
