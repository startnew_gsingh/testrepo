﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLInvoice
    {
        public DataSet getInvoiceData(Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getInvoiceData(OrgID, FilterID, PageNo, PageSize);
        }

        public DataSet getSelectedInvoice(Int64 InvoiceID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getSelectedInvoice(InvoiceID);
        }

        public DataSet getAllObjects()
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getAllObjects();
        }

        //public DataSet getObjectsColumns(Int64 ObjectID)
        //{
        //    InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
        //    return _objInvoiceDAL.getObjectsColumns(ObjectID);
        //}

        public int insertFilters(StructureFilterDAL ObjFilter)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.insertFilter(ObjFilter);
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getFilters(OrgID, ObjectID);
        }

        public int insertFilterQuery(DataTable objFilterQuery, Int64 filterID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.insertFilterQuery(objFilterQuery, filterID);
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getFiltersQuery(FilterID);
        }

        public int insertInvoice(Int64 orgID, string columnValues, Int64 InvoiceID, string salesColNames, string salesColValues)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.insertInvoice(orgID, columnValues, InvoiceID, salesColNames, salesColValues);
        }

        public DataSet getColumnControls(Int64 ObjectID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getColumnControls(ObjectID);
        }

        public DataSet getControlChoices(Int64 formID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getControlChoices(formID);
        }

        public int insertFilterColumn(DataTable objFilterColumn, Int64 filterID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.insertFilterColumn(objFilterColumn, filterID);
        }

        public DataSet getControlChoicesByColumnName(string ColumnName)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getControlChoicesByColumnName(ColumnName);
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getPlaceholders(ObjectID, OrgID);
        }

        public int deleteInvoices(string InvoiceID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.deleteInvoice(InvoiceID);
        }


        public DataSet getInvoiceGridData(Int64 OrgID, Int64 InvoiceID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.getInvoiceGridData(OrgID, InvoiceID);
        }

        public int insertInvoiceGrid(Int64 ID, Int64 InvoiceID, Int64 OrgID, string Line, string ProductID, string ProductDesc, string Quantity, string UnitPrice,
                                            string DiscountPercent, string DiscountAmount, string ExtendedPercent, string ShippedDate, string QuantitytoInvoice
                                                , string QuantityFullfill, string QuantityCancelled)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.insertInvoiceGrid(ID, InvoiceID, OrgID, Line, ProductID, ProductDesc, Quantity, UnitPrice, DiscountPercent,
                                                        DiscountAmount, ExtendedPercent, ShippedDate, QuantitytoInvoice, QuantityFullfill, QuantityCancelled);
        }

        public int deleteInvoiceGrid(Int64 ID)
        {
            InvoiceDAL _objInvoiceDAL = new InvoiceDAL();
            return _objInvoiceDAL.deleteInvoiceGrid(ID);
        }
    }
}
