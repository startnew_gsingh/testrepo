﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLWorkOrder
    {
        public DataSet getWorkOrderData(Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getWorkOrderData(OrgID, FilterID, PageNo, PageSize);
        }

        public DataSet getSelectedWorkOrder(Int64 WorkOrderID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getSelectedWorkOrder(WorkOrderID);
        }

        public DataSet getAllObjects()
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getAllObjects();
        }

        //public DataSet getObjectsColumns(Int64 ObjectID)
        //{
        //    WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
        //    return _objWorkOrderDAL.getObjectsColumns(ObjectID);
        //}

        public int insertFilters(StructureFilterDAL ObjFilter)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.insertFilter(ObjFilter);
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getFilters(OrgID, ObjectID);
        }

        public int insertFilterQuery(DataTable objFilterQuery, Int64 filterID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.insertFilterQuery(objFilterQuery, filterID);
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getFiltersQuery(FilterID);
        }

        public int insertWorkOrder(Int64 orgID, string columnValues, Int64 WorkOrderID, string salesColNames, string salesColValues)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.insertWorkOrder(orgID, columnValues, WorkOrderID, salesColNames, salesColValues);
        }

        public DataSet getColumnControls(Int64 ObjectID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getColumnControls(ObjectID);
        }

        public DataSet getControlChoices(Int64 formID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getControlChoices(formID);
        }

        public int insertFilterColumn(DataTable objFilterColumn, Int64 filterID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.insertFilterColumn(objFilterColumn, filterID);
        }

        public DataSet getControlChoicesByColumnName(string ColumnName)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getControlChoicesByColumnName(ColumnName);
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getPlaceholders(ObjectID, OrgID);
        }

        public int deleteWorkOrders(string WorkOrderID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.deleteWorkOrder(WorkOrderID);
        }



        public DataSet getWorkOrderGridData(Int64 OrgID, Int64 WorkOrderID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.getWorkOrderGridData(OrgID, WorkOrderID);
        }

        public int insertWorkOrderGrid(Int64 ID, Int64 WorkOrderID, Int64 OrgID, string Line, string ProductID, string ProductDesc, string Quantity, string UnitPrice,
                                            string DiscountPercent, string DiscountAmount, string ExtendedPercent, string ShippedDate, string QuantitytoInvoice
                                                , string QuantityFullfill, string QuantityCancelled, string MONumber)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.insertWorkOrderGrid(ID, WorkOrderID, OrgID, Line, ProductID, ProductDesc, Quantity, UnitPrice, DiscountPercent,
                                                        DiscountAmount, ExtendedPercent, ShippedDate, QuantitytoInvoice, QuantityFullfill, QuantityCancelled, MONumber);
        }

        public int deleteWorkOrderGrid(Int64 ID)
        {
            WorkOrderDAL _objWorkOrderDAL = new WorkOrderDAL();
            return _objWorkOrderDAL.deleteWorkOrderGrid(ID);
        }
    }
}
