﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLVendor
    {
        public DataSet getVendorData(Int64 VendorID, Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10)
        {
            VendorDAL _objVendorDAL = new VendorDAL();
            return _objVendorDAL.getVendorData(VendorID, OrgID, FilterID, PageNo, PageSize);
        }

        //public int insertFilters(StructureFilterDAL ObjFilter)
        //{
        //    VendorDAL _objVendorDAL = new VendorDAL();
        //    return _objVendorDAL.insertFilter(ObjFilter);
        //}

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            VendorDAL _objVendorDAL = new VendorDAL();
            return _objVendorDAL.getFilters(OrgID, ObjectID);
        }

        //public int insertFilterQuery(DataTable objFilterQuery, Int64 filterID)
        //{
        //    VendorDAL _objVendorDAL = new VendorDAL();
        //    return _objVendorDAL.insertFilterQuery(objFilterQuery, filterID);
        //}

        //public int insertFilterColumn(DataTable objFilterColumn, Int64 filterID)
        //{
        //    VendorDAL _objVendorDAL = new VendorDAL();
        //    return _objVendorDAL.insertFilterColumn(objFilterColumn, filterID);
        //}

        //public DataSet getFiltersQuery(Int64 FilterID)
        //{
        //    VendorDAL _objVendorDAL = new VendorDAL();
        //    return _objVendorDAL.getFiltersQuery(FilterID);
        //}

        public int insertVendor(Int64 VendorID, Int64 orgID, string VendorName, string Contact, string Address1, string Address2, string Address3, string City, string ZipCode,
                                string State, string Country, string Telephone, string Fax, string PaymentTerms, string ShippingMethod, string RemitTo, string ShipFrom, string Purchase)
        {
            VendorDAL _objVendorDAL = new VendorDAL();
            return _objVendorDAL.insertVendor(VendorID, orgID, VendorName, Contact,
                                                Address1, Address2, Address3, City, ZipCode, State, Country, Telephone,
                                                Fax, PaymentTerms, ShippingMethod, RemitTo, ShipFrom, Purchase);
        }

        public int deleteVendors(string VendorID)
        {
            VendorDAL _objVendorDAL = new VendorDAL();
            return _objVendorDAL.deleteVendor(VendorID);
        }


        public DataSet getPaymentTerms()
        {
            VendorDAL _objVendorDAL = new VendorDAL();
            return _objVendorDAL.getPaymentTerms();
        }

        public DataSet getShippingMethod()
        {
            VendorDAL _objVendorDAL = new VendorDAL();
            return _objVendorDAL.getShippingMethods();
        }
    }
}
