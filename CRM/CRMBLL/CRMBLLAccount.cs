﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLAccount
    {
        public DataSet getAccountData(Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10, string filterQuery = "")
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getAccountData(OrgID, FilterID, PageNo, PageSize, filterQuery);
        }

        public DataSet getSelectedAccount(Int64 AccountID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getSelectedAccount(AccountID);
        }

        public DataSet getAllObjects()
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getAllObjects();
        }

        //public DataSet getObjectsColumns(Int64 ObjectID)
        //{
        //    AccountDAL _objAccountDAL = new AccountDAL();
        //    return _objAccountDAL.getObjectsColumns(ObjectID);
        //}

        public int insertFilters(StructureFilterDAL ObjFilter)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.insertFilter(ObjFilter);
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getFilters(OrgID, ObjectID);
        }

        public int insertFilterQuery(DataTable objFilterQuery, Int64 filterID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.insertFilterQuery(objFilterQuery, filterID);
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getFiltersQuery(FilterID);
        }

        public int insertAccount(Int64 orgID, string columnValues, Int64 AccountID, string salesColNames, string salesColValues)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.insertAccount(orgID, columnValues, AccountID, salesColNames, salesColValues);
        }

        public DataSet getColumnControls(Int64 ObjectID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getColumnControls(ObjectID);
        }

        public DataSet getControlChoices(Int64 formID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getControlChoices(formID);
        }

        public int insertFilterColumn(DataTable objFilterColumn, Int64 filterID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.insertFilterColumn(objFilterColumn, filterID);
        }

        public DataSet getControlChoicesByColumnName(string ColumnName)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getControlChoicesByColumnName(ColumnName);
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.getPlaceholders(ObjectID, OrgID);
        }

        public int deleteAccounts(string AccountID)
        {
            AccountDAL _objAccountDAL = new AccountDAL();
            return _objAccountDAL.deleteAccount(AccountID);
        }


        public int insertMCAnswers(DataTable dtInsertRows, Int64 LeadID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertMCAnswers(dtInsertRows, LeadID);
        }

        public DataSet getMCAnswers(Int64 OrgID, Int64 ObjectID, Int64 ObjectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getMCAnswers(OrgID, ObjectID, ObjectUserID);
        }
    }
}
