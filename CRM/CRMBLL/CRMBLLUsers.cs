﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLUsers
    {
        public StructureLoginDAL authenticateUser(StructureLoginDAL _objLogin)
        {
            LoginDAL _objLoginDAL = new LoginDAL();
            return _objLoginDAL.authenticateUser(_objLogin);
        }

        public DataSet getRolesData(Int64 RoleID = 0)
        {
            LoginDAL _objAccountDAL = new LoginDAL();
            return _objAccountDAL.getRoles(RoleID);
        }

        public int deleteRole(string RoleID)
        {
            LoginDAL _objRoleDAL = new LoginDAL();
            return _objRoleDAL.deleteRole(RoleID);
        }

        public int insertRole(string RoleName, Int64 RoleID = 0)
        {
            LoginDAL _objRoleDAL = new LoginDAL();
            return _objRoleDAL.insertRole(RoleName, RoleID);
        }

        public DataSet getRolesPermissions(Int64 RoleID)
        {
            LoginDAL _objAccountDAL = new LoginDAL();
            return _objAccountDAL.getRolePermissions(RoleID);
        }

        public int insertRolePermissions(DataTable dtPermissions)
        {
            LoginDAL _objRoleDAL = new LoginDAL();
            return _objRoleDAL.insertRolePermissions(dtPermissions);
        }


        public DataSet getUsers(Int64 UserId = 0)
        {
            LoginDAL _objAccountDAL = new LoginDAL();
            return _objAccountDAL.getUsers(UserId);
        }

        public int deleteUser(string UserID)
        {
            LoginDAL _objUserDAL = new LoginDAL();
            return _objUserDAL.deleteUsers(UserID);
        }

        public int insertUser(StructureUserDAL objUser)
        {
            LoginDAL _objUserDAL = new LoginDAL();
            return _objUserDAL.insertUser(objUser);
        }


        public DataSet getUserRoles(Int64 OrgID, Int64 RoleID)
        {
            LoginDAL _objAccountDAL = new LoginDAL();
            return _objAccountDAL.getUserRoles(OrgID, RoleID);
        }

        public int assignUserRoles(string RoleID, Int64 OrgID)
        {
            LoginDAL _objUserDAL = new LoginDAL();
            return _objUserDAL.assignUserRoles(RoleID, OrgID);
        }

        public DataSet getUserObjectRoles(Int64 OrgID, Int64 ObjectID)
        {
            LoginDAL _objAccountDAL = new LoginDAL();
            return _objAccountDAL.getUserObjectRoles(OrgID, ObjectID);
        }



        public DataSet getTerritoryData(Int64 TerritoryID = 0)
        {
            LoginDAL _objTerritoryDAL = new LoginDAL();
            return _objTerritoryDAL.getTerritory(TerritoryID);
        }

        public int deleteTerritory(string TerritoryID)
        {
            LoginDAL _objTerritoryDAL = new LoginDAL();
            return _objTerritoryDAL.deleteTerritory(TerritoryID);
        }

        public int insertTerritory(string TerritoryName, string parentTerritoryName, Int64 TerritoryID = 0)
        {
            LoginDAL _objTerritoryDAL = new LoginDAL();
            return _objTerritoryDAL.insertTerritory(TerritoryName, parentTerritoryName, TerritoryID);
        }
    }
}
