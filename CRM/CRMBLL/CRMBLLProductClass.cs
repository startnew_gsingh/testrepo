﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLProductClass
    {
        public DataSet getProductClassData(Int64 ProductClassID, Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10)
        {
            ProductClassDAL _objProductClassDAL = new ProductClassDAL();
            return _objProductClassDAL.getProductClassData(ProductClassID, OrgID, FilterID, PageNo, PageSize);
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            ProductClassDAL _objProductClassDAL = new ProductClassDAL();
            return _objProductClassDAL.getFilters(OrgID, ObjectID);
        }

        public int insertProductClass(Int64 ProductClassID, Int64 orgID, string Description, int ItemType, int Validation, int LocationCode,
                                        bool ExcludeCommission, int UnitMeasure, int ProductFamily, bool DisallowPriceOverride)
        {
            ProductClassDAL _objProductClassDAL = new ProductClassDAL();
            return _objProductClassDAL.insertProductClass(ProductClassID, orgID, Description, ItemType, Validation, LocationCode,
                                                            ExcludeCommission, UnitMeasure, ProductFamily, DisallowPriceOverride);
        }

        public int deleteProductClasss(string ProductClassID)
        {
            ProductClassDAL _objProductClassDAL = new ProductClassDAL();
            return _objProductClassDAL.deleteProductClass(ProductClassID);
        }


        public DataSet getProductFamily()
        {
            ProductClassDAL _objProductClassDAL = new ProductClassDAL();
            return _objProductClassDAL.getProductFamily();
        }

        public DataSet getUnitofMeasure()
        {
            ProductClassDAL _objProductClassDAL = new ProductClassDAL();
            return _objProductClassDAL.getUnitofMeasure();
        }

        public DataSet getLocationCode()
        {
            ProductClassDAL _objProductClassDAL = new ProductClassDAL();
            return _objProductClassDAL.getLocationCode();
        }
    }
}
