﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLDeals
    {
        public DataSet getDealsData(Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10, string filterQuery = "", string chartQuery = "")
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getDealsData(OrgID, FilterID, PageNo, PageSize, filterQuery, chartQuery);
        }

        public DataSet getSelectedDeal(Int64 LeadID, Int64 ObjectID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getSelectedDeal(LeadID, ObjectID);
        }

        public DataSet getObjects()
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getObjects();
        }

        public DataSet getAllObjects()
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getAllObjects();
        }

        public DataSet getObjectsColumns(Int64 ObjectID, Int64 OrgID, Int64 FilterID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getObjectsColumns(ObjectID, OrgID, FilterID);
        }

        public int insertFilters(StructureFilterDAL ObjFilter)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertFilter(ObjFilter);
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getFilters(OrgID, ObjectID);
        }

        public int insertFilterQuery(DataTable objFilterQuery, Int64 filterID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertFilterQuery(objFilterQuery, filterID);
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getFiltersQuery(FilterID);
        }

        public int insertDeals(Int64 orgID, string columnValues, Int64 leadID, string salesColNames, string salesColValues, Int64 StageID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertDeals(orgID, columnValues, leadID, salesColNames, salesColValues, StageID);
        }

        public DataSet getColumnControls(Int64 ObjectID, Int64 allControls)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getColumnControls(ObjectID, allControls);
        }

        public DataSet getControlChoices(Int64 formID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getControlChoices(formID);
        }

        public DataSet getObjectChoices(string SystemFieldLinkTable, Int64 OrgID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getObjectChoices(SystemFieldLinkTable, OrgID);
        }

        public int insertFilterColumn(DataTable objFilterColumn, Int64 filterID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertFilterColumn(objFilterColumn, filterID);
        }

        public DataSet getControlChoicesByColumnName(string ColumnName)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getControlChoicesByColumnName(ColumnName);
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getPlaceholders(ObjectID, OrgID);
        }

        public int deleteLeads(string LeadID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.deleteLeads(LeadID);
        }

        public int changeLeadStatus(Int64 LeadID, string Status)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.changeLeadStatus(LeadID, Status);
        }
        public int reactivateDeal(Int64 LeadID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.reactivateDeal(LeadID);
        }

        public int changeOpportunityStatus(Int64 OpportunityID, string Status, bool isActive)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.changeOpportunityStatus(OpportunityID, Status, isActive);
        }

        public int ReactivateOpportunityDeal(Int64 OpportunityID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.ReactivateOpportunityDeal(OpportunityID);
        }

        public int insertDocuments(Int64 orgID, Int64 objectID, Int64 objectUserID, string documentname, string documentPath, Int64 oldLeadID, string delLeadID, string notes, bool IsDocsOnly, bool IsDelOnly)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertDocuments(orgID, objectID, objectUserID, documentname, documentPath, oldLeadID, delLeadID, notes, IsDocsOnly, IsDelOnly);
        }

        public DataSet getDocuments(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getDocuments(orgID, objectID, objectUserID);
        }

        public int insertTasks(Int64 orgID, Int64 objectID, Int64 objectUserID, string subject, string description, DateTime dueDate, string Priority, Int64 oldLeadID, string delLeadID, bool IsDelOnly)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertTasks(orgID, objectID, objectUserID, subject, description, dueDate, Priority, oldLeadID, delLeadID, IsDelOnly);
        }

        public DataSet getTasks(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getTasks(orgID, objectID, objectUserID);
        }

        public int deleteNotes(string NoteID)
        {
            DealsDAL _objDealDAL = new DealsDAL();
            return _objDealDAL.deleteNote(NoteID);
        }



        public int insertPhoneCalls(Int64 orgID, Int64 objectID, Int64 objectUserID, string description, DateTime dueDate, string Direction, Int64 oldLeadID, string delLeadID, bool IsDelOnly)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertPhoneCalls(orgID, objectID, objectUserID, description, dueDate, Direction, oldLeadID, delLeadID, IsDelOnly);
        }

        public DataSet getPhoneCalls(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getPhoneCalls(orgID, objectID, objectUserID);
        }

        public int deletePhoneCalls(string CallID)
        {
            DealsDAL _objDealDAL = new DealsDAL();
            return _objDealDAL.deletePhoneCall(CallID);
        }

        public DataSet getUpcomingPhoneCall(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getUpcomingPhoneCall(orgID, objectID, objectUserID);
        }

        public DataSet countActivities(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.countActivities(orgID, objectID, objectUserID);
        }



        public int insertDiscussion(Int64 orgID, Int64 objectID, Int64 objectUserID, string subject, string description, Int64 oldLeadID, string delLeadID, bool IsDelOnly, string entity)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertDiscussion(orgID, objectID, objectUserID, subject, description, oldLeadID, delLeadID, IsDelOnly, GlobalVariables.LoginUser.UserName, entity);
        }

        public DataSet getDiscussions(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getDiscussions(orgID, objectID, objectUserID);
        }

        public int deleteDiscussion(string DiscussionID)
        {
            DealsDAL _objDealDAL = new DealsDAL();
            return _objDealDAL.deleteDiscussion(DiscussionID, GlobalVariables.LoginUser.OrgID);
        }

        public int insertUserDiscussion(Int64 DiscussionID, Int64 orgID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertUserDiscussion(DiscussionID, orgID);
        }

        public DataSet getDashboardDiscussions(Int64 orgID, Int64 ID = 0)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getDashboardDiscussions(orgID, ID);
        }

        public DataSet getDashboardTasks(Int64 orgID, Int64 ID = 0)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getDashboardTasks(orgID, ID);
        }

        public DataSet getSalesDashboard(int year, int month, int opportunityMonth)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getSalesDashboard(year, month, opportunityMonth);
        }




        public int insertMCAnswers(DataTable dtInsertRows, Int64 LeadID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertMCAnswers(dtInsertRows, LeadID);
        }

        public DataSet getMCAnswers(Int64 OrgID, Int64 ObjectID, Int64 ObjectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getMCAnswers(OrgID, ObjectID, ObjectUserID);
        }
    }
}
