﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CRMDAL;

namespace CRMBLL
{
    public class CRMBLLCustomizeObject
    {
        public DataTable getFieldTypes()
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.getFieldTypes();
        }

        public DataTable getFieldFormat(Int64 fieldType)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.getFieldFormat(fieldType);
        }

        public DataTable getFields(Int64 ObjID)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.getFields(ObjID, GlobalVariables.LoginUser.OrgID);
        }

        public DataTable getPlaceHolders(Int64 ObjID)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.getPlaceholders(ObjID, GlobalVariables.LoginUser.OrgID);
        }

        public Boolean SaveCustomize(DataTable tbl)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.SaveCustomize(tbl, GlobalVariables.LoginUser.OrgID);
        }

        public int AddNewPlaceholder(Int64 orgID, Int64 objectID, string placeholderName)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.AddNewPlaceholder(orgID, objectID, placeholderName);
        }

        public int AddNewField(Int64 formID, Int64 fieldType, Int64 fieldFormat, string maxLength, Int64 objectID, Int64 orgID, string columnName, string labelName, bool isRequired, string txtChoices)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.AddNewField(formID, fieldType, fieldFormat, maxLength, objectID, orgID, columnName, labelName, isRequired, txtChoices);
        }

        public int deletePlaceholder(Int64 PlaceholderID, Int64 ObjectID)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.deletePlaceholder(PlaceholderID, ObjectID);
        }

        public DataTable getFormField(Int64 formID)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.getFormField(formID);
        }

        public DataTable getFormControlChoices(Int64 formID)
        {
            CustomizeObjectDAl _objDAL = new CustomizeObjectDAl();
            return _objDAL.getFormControlChoices(formID);
        }
    }
}
