﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net;

namespace CRMBLL
{
    public class clsCommonFunctions
    {
        public static void CheckSession()
        {
            if (GlobalVariables.LoginUser == null)
                System.Web.HttpContext.Current.Response.Redirect("Default.aspx?Status=Session Expired");
        }
    }
}
