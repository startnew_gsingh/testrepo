﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Web;

namespace CRMBLL
{
    public class GlobalVariables
    {
        public static string ErrorMessage { get; set; }
        public static StructureLoginDAL LoginUser
        {
            get { return (StructureLoginDAL)HttpContext.Current.Session["LoginUser"]; }
            set { HttpContext.Current.Session["LoginUser"] = value; }
        }
    }

    public class chartData
    {
        public string Label { get; set; }
        public double Value1 { get; set; }
    }
}
