﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRMDAL;
using System.Data;

namespace CRMBLL
{
    public class CRMBLLOpportunities
    {
        public DataSet getOpportunitiesData(Int64 OrgID, Int64 FilterID, int PageNo = 1, int PageSize = 10, string filterQuery = "")
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getOpportunitiesData(OrgID, FilterID, PageNo, PageSize, filterQuery);
        }

        public DataSet getSelectedOpportunities(Int64 OpportunitiesID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getSelectedOpportunities(OpportunitiesID);
        }

        public DataSet getAllObjects()
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getAllObjects();
        }

        //public DataSet getObjectsColumns(Int64 ObjectID)
        //{
        //    OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
        //    return _objOpportunitiesDAL.getObjectsColumns(ObjectID);
        //}

        public int insertFilters(StructureFilterDAL ObjFilter)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.insertFilter(ObjFilter);
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getFilters(OrgID, ObjectID);
        }

        public int insertFilterQuery(DataTable objFilterQuery, Int64 filterID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.insertFilterQuery(objFilterQuery, filterID);
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getFiltersQuery(FilterID);
        }

        public int insertOpportunities(Int64 orgID, string columnValues, Int64 OpportunitiesID, string salesColNames, string salesColValues, Int64 StageID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.insertOpportunities(orgID, columnValues, OpportunitiesID, salesColNames, salesColValues, StageID);
        }

        public DataSet getColumnControls(Int64 ObjectID, Int64 allControls)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getColumnControls(ObjectID, allControls);
        }

        public DataSet getControlChoices(Int64 formID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getControlChoices(formID);
        }

        public int insertFilterColumn(DataTable objFilterColumn, Int64 filterID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.insertFilterColumn(objFilterColumn, filterID);
        }

        public DataSet getControlChoicesByColumnName(string ColumnName)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getControlChoicesByColumnName(ColumnName);
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getPlaceholders(ObjectID, OrgID);
        }

        public int deleteOpportunities(string OpportunitiesID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.deleteOpportunities(OpportunitiesID);
        }

        public DataSet getOpportunitiesGridData(Int64 OrgID, Int64 OpportunityID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.getOpportunitiesGridData(OrgID, OpportunityID);
        }

        public int insertOpportunityGrid(Int64 ID, Int64 OpportunityID, Int64 OrgID, string Line, string ProductID, string ProductDesc, string Quantity, string UnitPrice,
                                            string DiscountPercent, string DiscountAmount, string ExtendedPercent)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.insertOpportunityGrid(ID, OpportunityID, OrgID, Line, ProductID, ProductDesc, Quantity, UnitPrice, DiscountPercent, DiscountAmount, ExtendedPercent);
        }

        public int deleteOpportunitiesGrid(Int64 ID)
        {
            OpportunitiesDAL _objOpportunitiesDAL = new OpportunitiesDAL();
            return _objOpportunitiesDAL.deleteOpportunitiesGrid(ID);
        }


        public int insertMCAnswers(DataTable dtInsertRows, Int64 LeadID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.insertMCAnswers(dtInsertRows, LeadID);
        }

        public DataSet getMCAnswers(Int64 OrgID, Int64 ObjectID, Int64 ObjectUserID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            return _objDealsDAL.getMCAnswers(OrgID, ObjectID, ObjectUserID);
        }
    }
}
