﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CRMDAL;

namespace CRMBLL
{
    public class SalesProcessBLL
    {
        public DataSet getSalesStages(int ObjectID)
        {
            SalesProcessDAL _objSalesProcessDAL = new SalesProcessDAL();
            return _objSalesProcessDAL.getSalesStages(ObjectID, GlobalVariables.LoginUser.OrgID);
        }

        public DataSet getOpportunitySalesStages(Int64 ObjectID)
        {
            SalesProcessDAL _objSalesProcessDAL = new SalesProcessDAL();
            return _objSalesProcessDAL.getSalesStages(ObjectID, GlobalVariables.LoginUser.OrgID);
        }

        public DataSet getActiveSalesStages(Int64 ObjectUserID, Int64 ObjectID)
        {
            SalesProcessDAL _objSalesProcessDAL = new SalesProcessDAL();
            return _objSalesProcessDAL.getActiveSalesStages(ObjectUserID, ObjectID);
        }

        public SaleProcessSteps listObjectColumns(Int64 _ObjectID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            DataSet dsColumnName = _objDealsDAL.getObjectsColumns(_ObjectID, 0, 0);
            List<ObjectColumns> _objColumnNameLists = new List<ObjectColumns>();
            for (int dc = 0; dc < dsColumnName.Tables[0].Rows.Count; dc++)
            {
                string FormID = Convert.ToString(dsColumnName.Tables[0].Rows[dc]["ID"]);
                string GridDisplayName = Convert.ToString(dsColumnName.Tables[0].Rows[dc]["GridDisplayName"]);

                ObjectColumns _objColumnNames = new ObjectColumns();
                _objColumnNames.FormID = FormID;
                _objColumnNames.GridDisplayName = GridDisplayName;

                _objColumnNameLists.Add(_objColumnNames);
            }

            SaleProcessSteps _objSPS = new SaleProcessSteps();
            _objSPS.FormID = "0";
            _objSPS.StepName = "";
            _objSPS.StageID = "0";
            _objSPS.EntityColumns = new List<ObjectColumns>();
            _objSPS.EntityColumns.AddRange(_objColumnNameLists);
            return _objSPS;
        }

        public List<SalesProcessStages> getSalesStageSteps(Int64 ObjectID)
        {
            DealsDAL _objDealsDAL = new DealsDAL();
            SalesProcessDAL _objSalesProcessDAL = new SalesProcessDAL();
            DataSet dssalesStages = _objSalesProcessDAL.getSalesStages(ObjectID, GlobalVariables.LoginUser.OrgID);
            List<SalesProcessStages> lsSteps = new List<SalesProcessStages>();
            for (int d = 0; d < dssalesStages.Tables[0].Rows.Count; d++)
            {
                SalesProcessStages _objSalesPrcess = new SalesProcessStages();

                _objSalesPrcess.ID = Convert.ToInt64(dssalesStages.Tables[0].Rows[d]["ID"]);
                _objSalesPrcess.ObjectID = Convert.ToInt64(dssalesStages.Tables[0].Rows[d]["ObjectID"]);
                _objSalesPrcess.StageName = Convert.ToString(dssalesStages.Tables[0].Rows[d]["StageName"]);
                _objSalesPrcess.ProcessSetps = new List<SaleProcessSteps>();

                DataSet dsColumnName = _objDealsDAL.getObjectsColumns(_objSalesPrcess.ObjectID, 0, 0);
                List<ObjectColumns> _objColumnNameLists = new List<ObjectColumns>();
                for (int dc = 0; dc < dsColumnName.Tables[0].Rows.Count; dc++)
                {
                    string FormID = Convert.ToString(dsColumnName.Tables[0].Rows[dc]["ID"]);
                    string GridDisplayName = Convert.ToString(dsColumnName.Tables[0].Rows[dc]["GridDisplayName"]);

                    ObjectColumns _objColumnNames = new ObjectColumns();
                    _objColumnNames.FormID = FormID;
                    _objColumnNames.GridDisplayName = GridDisplayName;

                    _objColumnNameLists.Add(_objColumnNames);
                }

                DataSet dssalesStageSteps = _objSalesProcessDAL.getSalesStageSteps(Convert.ToInt64(dssalesStages.Tables[0].Rows[d]["ID"]));
                for (int s = 0; s < dssalesStageSteps.Tables[0].Rows.Count; s++)
                {
                    string stepName = Convert.ToString(dssalesStageSteps.Tables[0].Rows[s]["StepName"]);
                    string FormID = Convert.ToString(dssalesStageSteps.Tables[0].Rows[s]["FormID"]);
                    string SalesStageID = Convert.ToString(dssalesStageSteps.Tables[0].Rows[s]["SalesStageID"]);

                    SaleProcessSteps _objSPS = new SaleProcessSteps();
                    _objSPS.StepID = Convert.ToInt64(dssalesStageSteps.Tables[0].Rows[s]["ID"]);
                    _objSPS.FormID = FormID;
                    _objSPS.StepName = stepName;
                    _objSPS.StageID = SalesStageID;
                    _objSPS.EntityColumns = new List<ObjectColumns>();
                    _objSPS.EntityColumns.AddRange(_objColumnNameLists);

                    _objSalesPrcess.ProcessSetps.Add(_objSPS);

                }
                lsSteps.Add(_objSalesPrcess);
            }
            return lsSteps;
        }

        public int insertSalesProcessStage(Int64 ObjectID, string StageName, Int64 StageID, string StepName, Int64 FormID)
        {
            SalesProcessDAL _objSalesProcessDAL = new SalesProcessDAL();
            return _objSalesProcessDAL.insertSalesProcessStage(ObjectID, StageName, StageID, StepName, FormID);
        }

        public DataSet getSalesStageControls(Int64 stageID, int ObjectID)
        {
            SalesProcessDAL _objSalesProcessDAL = new SalesProcessDAL();
            return _objSalesProcessDAL.getSalesStageControls(stageID, ObjectID);
        }

        public DataSet getSalesProcess(Int64 orgID, Int64 selUserID, Int64 ObjectID)
        {
            SalesProcessDAL _objSalesProcessDAL = new SalesProcessDAL();
            return _objSalesProcessDAL.getSalesProcess(orgID, selUserID, ObjectID);
        }

        public int deleteSalesStage(Int64 stageID, Int64 stepID)
        {
            SalesProcessDAL _objSalesProcessDAL = new SalesProcessDAL();
            return _objSalesProcessDAL.deleteSalesStage(stageID, stepID);
        }
    }

    public class SalesProcessStages
    {
        public Int64 ID { get; set; }
        public Int64 ObjectID { get; set; }
        public string StageName { get; set; }
        public List<SaleProcessSteps> ProcessSetps { get; set; }
    }

    public class SaleProcessSteps
    {
        public Int64 StepID { get; set; }
        public string StepName { get; set; }
        public string ColumnName { get; set; }
        public string FormID { get; set; }
        public string StageID { get; set; }
        public List<ObjectColumns> EntityColumns { get; set; }
    }

    public class ObjectColumns
    {
        public string FormID { get; set; }
        public string GridDisplayName { get; set; }
    }
}
