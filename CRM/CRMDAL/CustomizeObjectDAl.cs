﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace CRMDAL
{
    public class CustomizeObjectDAl
    {
        public DataTable getFieldTypes()
        {
            try
            {
                DataTable dsFields = new DataTable();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "Select * from CRM_FieldType";
                    _objCmd.CommandType = CommandType.Text;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsFields);
                }
                return dsFields;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable getFieldFormat(Int64 fieldType)
        {
            try
            {
                DataTable dsFields = new DataTable();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "Select * from CRM_FieldFormat Where FieldTypeID=" + fieldType;
                    _objCmd.CommandType = CommandType.Text;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsFields);
                }
                return dsFields;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable getFields(Int64 objectID, Int64 orgID)
        {
            try
            {
                DataTable dsFields = new DataTable();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFields]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@ObjectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", orgID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsFields);
                }
                return dsFields;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            try
            {
                DataTable dsObjects = new DataTable();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectPlaceholders]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool SaveCustomize(DataTable dtbl, Int64 OrgID)
        {

            try
            {
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    foreach (DataRow dr in dtbl.Rows)
                    {
                        con.Open();
                        SqlCommand _objCmd = new SqlCommand();
                        _objCmd.CommandText = "CRM_SaveCustomizeObject";
                        _objCmd.CommandType = CommandType.StoredProcedure;
                        _objCmd.Connection = con;
                        _objCmd.UpdatedRowSource = UpdateRowSource.None;

                        _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                        _objCmd.Parameters.Add(new SqlParameter("@ObjectID", dr["ObjectID"].ToString()));
                        _objCmd.Parameters.Add(new SqlParameter("@ID", dr["ID"].ToString()));
                        _objCmd.Parameters.Add(new SqlParameter("@PlaceHolder", dr["PlaceHolder"].ToString()));
                        _objCmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int AddNewPlaceholder(Int64 orgID, Int64 objectID, string placeholderName)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertPlaceholder]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@PlaceholderHeader", placeholderName));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgId", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));


                    _objCmd.ExecuteNonQuery();
                    con.Close();
                    return 1;
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int AddNewField(Int64 formID, Int64 fieldType, Int64 fieldFormat, string maxLength, Int64 objectID, Int64 orgID, string columnName, string labelName, bool isRequired, string txtChoices)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertField]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@formID", formID));
                    _objCmd.Parameters.Add(new SqlParameter("@fieldType", fieldType));
                    _objCmd.Parameters.Add(new SqlParameter("@fieldFormat", fieldFormat));
                    _objCmd.Parameters.Add(new SqlParameter("@MaxLength", maxLength));

                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@ColumnName", columnName));

                    _objCmd.Parameters.Add(new SqlParameter("@LabelName", labelName));
                    _objCmd.Parameters.Add(new SqlParameter("@IsRequired", isRequired));
                    _objCmd.Parameters.Add(new SqlParameter("@radioChoices", txtChoices));
                    _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    _objCmd.ExecuteNonQuery();
                    success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
                    con.Close();
                    return success;
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deletePlaceholder(Int64 PlaceholderID, Int64 ObjectID)
        {
            try
            {
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeletePlaceholder]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@PlaceholderID", PlaceholderID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectID", ObjectID));

                    _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable getFormField(Int64 formID)
        {
            try
            {
                DataTable dsFields = new DataTable();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFormField]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@formID", formID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsFields);
                }
                return dsFields;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataTable getFormControlChoices(Int64 formID)
        {
            try
            {
                DataTable dsFields = new DataTable();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFormControlChoices]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@formID", formID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsFields);
                }
                return dsFields;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
