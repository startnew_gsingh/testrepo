﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CRMDAL
{
    public class StructureLoginDAL
    {
        //public Int64 ID { get; set; }
        public Int64 OrgID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public string Territory { get; set; }
        public string Customer { get; set; }
        public string Contact { get; set; }
        //public bool IsAdmin { get; set; }
        //public bool IsSuperAdmin { get; set; }
    }

    public class StructureFilterDAL
    {
        public Int64 ID { get; set; }
        public Int64 OrgID { get; set; }
        public Int64 ObjectID { get; set; }
        public string FilterName { get; set; }
        public string Query { get; set; }
        public bool IsActive { get; set; }
    }

    public class StructureUserDAL
    {
        public Int64 ID { get; set; }
        public Int64 OrgID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }


        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Phone { get; set; }
        public string Country { get; set; }


        public string State { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }
        public string Territory { get; set; }
        public string Email { get; set; }
    }
}
