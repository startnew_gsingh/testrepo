﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;


namespace CRMDAL
{
    public class LoginDAL
    {
        public StructureLoginDAL authenticateUser(StructureLoginDAL _objLogin)
        {
            try
            {
                StructureLoginDAL _objtableLogin = new StructureLoginDAL();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_AuthenticateUser]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlParameter[] _objPar = new SqlParameter[2];
                    _objPar[0] = new SqlParameter("@UserName", _objLogin.UserName);
                    _objPar[1] = new SqlParameter("@Password", _objLogin.Password);
                    _objCmd.Parameters.AddRange(_objPar);

                    SqlDataReader _objReader = _objCmd.ExecuteReader();
                    if (_objReader.HasRows)
                    {
                        while (_objReader.Read())
                        {
                            //_objtableLogin.ID = Convert.ToInt64(_objReader.GetValue(0));
                            _objtableLogin.OrgID = Convert.ToInt64(_objReader.GetValue(0));
                            _objtableLogin.UserName = Convert.ToString(_objReader.GetValue(1));
                            _objtableLogin.IsActive = Convert.ToBoolean(_objReader.GetValue(2));
                            _objtableLogin.Territory = Convert.ToString(_objReader.GetValue(3));
                            _objtableLogin.Customer = Convert.ToString(_objReader.GetValue(4));
                            _objtableLogin.Contact = Convert.ToString(_objReader.GetValue(5));
                        }
                    }
                }
                return _objtableLogin;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getRoles(Int64 RoleID = 0)
        {
            try
            {
                DataSet dsRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectRoles]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlParameter _objPar = new SqlParameter();
                    _objPar = new SqlParameter("@RoleID", RoleID);
                    _objCmd.Parameters.Add(_objPar);

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsRoles);
                }
                return dsRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteRole(string RoleID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteRole]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@RoleID", RoleID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertRole(string RoleName, Int64 RoleID = 0)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertRole]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@roleID", RoleID));
                    _objCmd.Parameters.Add(new SqlParameter("@roleName", RoleName));

                    recordsInserted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getRolePermissions(Int64 RoleID)
        {
            try
            {
                DataSet dsRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectRolePermissions]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlParameter _objPar = new SqlParameter();
                    _objPar = new SqlParameter("@RoleID", RoleID);
                    _objCmd.Parameters.Add(_objPar);

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsRoles);
                }
                return dsRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertRolePermissions(DataTable dtPermissions)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    foreach (DataRow dr in dtPermissions.Rows)
                    {
                        con.Open();
                        SqlCommand _objCmd = new SqlCommand();
                        _objCmd.CommandText = "[CRM_InsertRolePermissions]";
                        _objCmd.CommandType = CommandType.StoredProcedure;
                        _objCmd.Connection = con;

                        _objCmd.Parameters.Add(new SqlParameter("@cName", dr["ColumnName"].ToString()));
                        _objCmd.Parameters.Add(new SqlParameter("@ID", dr["ID"].ToString()));
                        _objCmd.Parameters.Add(new SqlParameter("@chkValue", dr["Value"].ToString()));

                        recordsInserted = _objCmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }




        public DataSet getUsers(Int64 UserID = 0)
        {
            try
            {
                DataSet dsUsers = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectUsers]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlParameter _objPar = new SqlParameter();
                    _objPar = new SqlParameter("@UserID", UserID);
                    _objCmd.Parameters.Add(_objPar);

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsUsers);
                }
                return dsUsers;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteUsers(string OrgID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteUser]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertUser(StructureUserDAL objUser)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertUser]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@ID", objUser.ID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", objUser.OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@UserName", objUser.UserName.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@Password", objUser.Password.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@FirstName", objUser.FirstName.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@LastName", objUser.LastName.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@Address1", objUser.Address1.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@Address2", objUser.Address2.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@Phone", objUser.Phone.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@Country", objUser.Country.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@State", objUser.State.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@City", objUser.City.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@ZipCode", objUser.ZipCode.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@Territory", objUser.Territory.ToString()));
                    _objCmd.Parameters.Add(new SqlParameter("@Email", objUser.Email.ToString()));

                    recordsInserted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }




        public DataSet getUserRoles(Int64 OrgID, Int64 RoleID)
        {
            try
            {
                DataSet dsUserRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectUserRoles]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@RoleID", RoleID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsUserRoles);
                }
                return dsUserRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int assignUserRoles(string RoleID, Int64 OrgID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertuserRoles]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@RoleID", RoleID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getUserObjectRoles(Int64 OrgID, Int64 ObjectID)
        {
            try
            {
                DataSet dsUserRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectUserObjectRoles]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsUserRoles);
                }
                return dsUserRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }




        public DataSet getTerritory(Int64 TerritoryID = 0)
        {
            try
            {
                DataSet dsTerritory = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectTerritory]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@TerritoryID", TerritoryID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsTerritory);
                }
                return dsTerritory;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteTerritory(string TerritoryID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteTerritory]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@TerritoryID", TerritoryID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertTerritory(string TerritoryName, string parentTerritoryName, Int64 TerritoryID = 0)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertTerritory]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@TerritoryID", TerritoryID));
                    _objCmd.Parameters.Add(new SqlParameter("@TerritoryName", TerritoryName));
                    _objCmd.Parameters.Add(new SqlParameter("@ParentTerritoryName", parentTerritoryName));

                    recordsInserted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
