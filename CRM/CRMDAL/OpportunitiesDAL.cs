﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace CRMDAL
{
    public class OpportunitiesDAL
    {
        public DataSet getOpportunitiesData(Int64 OrgID, Int64 FilterID, int PageNo, int PageSize, string filterQuery = "")
        {
            try
            {
                DataSet dsOpportunities = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectOpportunitiess]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));
                    _objCmd.Parameters.Add(new SqlParameter("@PageNo", PageNo));
                    _objCmd.Parameters.Add(new SqlParameter("@PageSize", PageSize));
                    _objCmd.Parameters.Add(new SqlParameter("@filterQuery", filterQuery));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsOpportunities);
                }
                return dsOpportunities;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getSelectedOpportunities(Int64 OpportunitiesID)
        {
            try
            {
                DataSet dsOpportunities = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectOpportunities]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OpportunitiesID", OpportunitiesID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsOpportunities);
                }
                return dsOpportunities;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getAllObjects()
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectObjects]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public DataSet getObjectsColumns(Int64 ObjectID)
        //{
        //    try
        //    {
        //        DataSet dsObjects = new DataSet();
        //        using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
        //        {
        //            con.Open();
        //            SqlCommand _objCmd = new SqlCommand();
        //            _objCmd.CommandText = "[CRM_SelectObjectColumns]";
        //            _objCmd.CommandType = CommandType.StoredProcedure;
        //            _objCmd.Connection = con;

        //            _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

        //            SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
        //            objAdapter.Fill(dsObjects);
        //        }
        //        return dsObjects;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public int insertFilter(StructureFilterDAL ObjFilter)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilter]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", ObjFilter.OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjFilter.ObjectID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterName", ObjFilter.FilterName));
                    _objCmd.Parameters.Add(new SqlParameter("@Query", ObjFilter.Query));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.Now));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedBy", "Admin"));
                    _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    _objCmd.ExecuteNonQuery();
                    success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFilters]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertFilterQuery(DataTable dtInsertRows, Int64 filterID)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilterQuery]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", filterID));
                    _objCmd.Parameters.Add("@ColumnName", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);
                    _objCmd.Parameters.Add("@FilterCondition", SqlDbType.VarChar, 200, dtInsertRows.Columns[1].ColumnName);
                    _objCmd.Parameters.Add("@Value", SqlDbType.VarChar, 200, dtInsertRows.Columns[2].ColumnName);
                    _objCmd.Parameters.Add("@Relation", SqlDbType.VarChar, 100, dtInsertRows.Columns[3].ColumnName);

                    SqlDataAdapter adpt = new SqlDataAdapter();
                    adpt.InsertCommand = _objCmd;
                    adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
                    recordsInserted = adpt.Update(dtInsertRows);
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFiltersQuery]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertOpportunities(Int64 orgID, string columnValues, Int64 OpportunitiesID, string salesColNames, string salesColValues, Int64 StageID)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertOpportunities]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OpportunitiesID", OpportunitiesID));
                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@colValues", columnValues));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.Now));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedBy", "Admin"));
                    _objCmd.Parameters.Add(new SqlParameter("@salesColNames", salesColNames));
                    _objCmd.Parameters.Add(new SqlParameter("@salesColValues", salesColValues));
                    _objCmd.Parameters.Add(new SqlParameter("@StageID", StageID));
                    _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    //_objCmd.Parameters.Add(new SqlParameter("@selUserID", selUserID));

                    _objCmd.ExecuteNonQuery();
                    success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getColumnControls(Int64 ObjectID, Int64 allControls)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectColumnControls]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));
                    _objCmd.Parameters.Add(new SqlParameter("@allControls", allControls));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getControlChoices(Int64 formID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFormControlChoices]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@formID", formID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertFilterColumn(DataTable dtInsertRows, Int64 filterID)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilterColumns]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", filterID));
                    _objCmd.Parameters.Add("@FilterColumn", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);

                    SqlDataAdapter adpt = new SqlDataAdapter();
                    adpt.InsertCommand = _objCmd;
                    adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
                    recordsInserted = adpt.Update(dtInsertRows);
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getControlChoicesByColumnName(string columnName)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectControlChoices]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@columnName", columnName));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectPlaceholders]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteOpportunities(string OpportunitiesID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteOpportunities]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OpportunitiesID", OpportunitiesID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public DataSet getOpportunitiesGridData(Int64 OrgID, Int64 OpportunityID)
        {
            try
            {
                DataSet dsOpportunities = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectOpportunityGrid]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@OpportunityID", OpportunityID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsOpportunities);
                }
                return dsOpportunities;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertOpportunityGrid(Int64 ID, Int64 OpportunityID, Int64 OrgID, string Line, string ProductID, string ProductDesc, string Quantity, string UnitPrice,
                                            string DiscountPercent, string DiscountAmount, string ExtendedPercent)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertOpportunityGrid]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@ID", ID));
                    _objCmd.Parameters.Add(new SqlParameter("@OpportunityID", OpportunityID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));

                    _objCmd.Parameters.Add(new SqlParameter("@Line", Line));
                    _objCmd.Parameters.Add(new SqlParameter("@ProductID", ProductID));
                    _objCmd.Parameters.Add(new SqlParameter("@ProductDesc", ProductDesc));
                    _objCmd.Parameters.Add(new SqlParameter("@Quantity", Quantity));
                    _objCmd.Parameters.Add(new SqlParameter("@UnitPrice", UnitPrice));

                    _objCmd.Parameters.Add(new SqlParameter("@DiscountPercent", DiscountPercent));
                    _objCmd.Parameters.Add(new SqlParameter("@DiscountAmount", DiscountAmount));
                    _objCmd.Parameters.Add(new SqlParameter("@ExtendedPercent", ExtendedPercent));

                    recordsInserted = _objCmd.ExecuteNonQuery();

                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public int deleteOpportunitiesGrid(Int64 ID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteOpportunityGrid]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@ID", ID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
