﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace CRMDAL
{
    public class DealsDAL
    {
        public DataSet getDealsData(Int64 OrgID, Int64 FilterID, int PageNo, int PageSize, string filterQuery = "", string chartQuery = "")
        {
            try
            {
                DataSet dsDeals = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectDeals]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));
                    _objCmd.Parameters.Add(new SqlParameter("@PageNo", PageNo));
                    _objCmd.Parameters.Add(new SqlParameter("@PageSize", PageSize));
                    _objCmd.Parameters.Add(new SqlParameter("@filterQuery", filterQuery));
                    _objCmd.Parameters.Add(new SqlParameter("@chartQuery", chartQuery));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsDeals);
                }
                return dsDeals;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getSelectedDeal(Int64 LeadID, Int64 ObjectID)
        {
            try
            {
                DataSet dsDeals = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectDeal]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@DealID", LeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsDeals);
                }
                return dsDeals;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getObjects()
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectAllObjects]";

                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getAllObjects()
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectObjects]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getObjectsColumns(Int64 ObjectID, Int64 OrgID, Int64 FilterID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectObjectColumns]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertFilter(StructureFilterDAL ObjFilter)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilter]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", ObjFilter.OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjFilter.ObjectID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterName", ObjFilter.FilterName));
                    _objCmd.Parameters.Add(new SqlParameter("@Query", ObjFilter.Query));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.Now));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedBy", "Admin"));
                    _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    _objCmd.ExecuteNonQuery();
                    success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFilters]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertFilterQuery(DataTable dtInsertRows, Int64 filterID)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilterQuery]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", filterID));
                    _objCmd.Parameters.Add("@ColumnName", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);
                    _objCmd.Parameters.Add("@FilterCondition", SqlDbType.VarChar, 200, dtInsertRows.Columns[1].ColumnName);
                    _objCmd.Parameters.Add("@Value", SqlDbType.VarChar, 200, dtInsertRows.Columns[2].ColumnName);
                    _objCmd.Parameters.Add("@Relation", SqlDbType.VarChar, 100, dtInsertRows.Columns[3].ColumnName);

                    SqlDataAdapter adpt = new SqlDataAdapter();
                    adpt.InsertCommand = _objCmd;
                    adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
                    recordsInserted = adpt.Update(dtInsertRows);
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFiltersQuery]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertDeals(Int64 orgID, string columnValues, Int64 leadID, string salesColNames, string salesColValues, Int64 StageID)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertDeals]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@leadID", leadID));
                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@colValues", columnValues));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.Now));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedBy", "Admin"));
                    _objCmd.Parameters.Add(new SqlParameter("@salesColNames", salesColNames));
                    _objCmd.Parameters.Add(new SqlParameter("@salesColValues", salesColValues));
                    _objCmd.Parameters.Add(new SqlParameter("@StageID", StageID));
                    _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    //_objCmd.Parameters.Add(new SqlParameter("@selUserID", selUserID));

                    _objCmd.ExecuteNonQuery();
                    success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getColumnControls(Int64 ObjectID, Int64 allControls)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectColumnControls]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));
                    _objCmd.Parameters.Add(new SqlParameter("@allControls", allControls));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getControlChoices(Int64 formID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFormControlChoices]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@formID", formID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getObjectChoices(string SystemFieldLinkTable, Int64 orgID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectObjectChoices]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@SystemFieldLinkTable", SystemFieldLinkTable));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", orgID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertFilterColumn(DataTable dtInsertRows, Int64 filterID)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilterColumns]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", filterID));
                    _objCmd.Parameters.Add("@FilterColumn", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);

                    SqlDataAdapter adpt = new SqlDataAdapter();
                    adpt.InsertCommand = _objCmd;
                    adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
                    recordsInserted = adpt.Update(dtInsertRows);
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getControlChoicesByColumnName(string columnName)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectControlChoices]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@columnName", columnName));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectPlaceholders]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteLeads(string leadID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteLeads]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@LeadID", leadID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int changeLeadStatus(Int64 LeadID, string Status)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_ChangeLeadStatus]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@LeadID", LeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@Status", Status));
                    _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    _objCmd.ExecuteNonQuery();
                    success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int reactivateDeal(Int64 LeadID)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_ReactivateDeal]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@LeadID", LeadID));

                    _objCmd.ExecuteNonQuery();
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int ReactivateOpportunityDeal(Int64 OpportunityID)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_ReactivateOpportunityDeal]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OpportunityID", OpportunityID));

                    _objCmd.ExecuteNonQuery();
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int changeOpportunityStatus(Int64 opportunityID, string status, bool isActive)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_changeOpportunityStatus]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@opportunityID", opportunityID));
                    _objCmd.Parameters.Add(new SqlParameter("@status", status));
                    _objCmd.Parameters.Add(new SqlParameter("@active", isActive));

                    _objCmd.ExecuteNonQuery();
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertDocuments(Int64 orgID, Int64 objectID, Int64 objectUserID, string documentname, string documentPath, Int64 oldLeadID, string delLeadID, string notes, bool IsDocsOnly, bool IsDelOnly)
        {
            try
            {
                //int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertDocuments]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));
                    _objCmd.Parameters.Add(new SqlParameter("@DocumentName", documentname));
                    _objCmd.Parameters.Add(new SqlParameter("@DocumentPath", documentPath));
                    _objCmd.Parameters.Add(new SqlParameter("@oldLeadID", oldLeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@delLeadID", delLeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@IsDocsOnly", IsDocsOnly));
                    _objCmd.Parameters.Add(new SqlParameter("@Notes", notes));
                    _objCmd.Parameters.Add(new SqlParameter("@delonly", IsDelOnly));


                    _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getDocuments(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectDocuments]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertTasks(Int64 orgID, Int64 objectID, Int64 objectUserID, string subject, string description, DateTime dueDate, string priority, Int64 oldLeadID, string delLeadID, bool IsDelOnly)
        {
            try
            {
                //int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertTasks]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));
                    _objCmd.Parameters.Add(new SqlParameter("@Subject", subject));
                    _objCmd.Parameters.Add(new SqlParameter("@Description", description));
                    _objCmd.Parameters.Add(new SqlParameter("@DueDate", dueDate));
                    _objCmd.Parameters.Add(new SqlParameter("@Priority", priority));
                    _objCmd.Parameters.Add(new SqlParameter("@oldLeadID", oldLeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@delLeadID", delLeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@delonly", IsDelOnly));

                    _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getTasks(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectTasks]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteNote(string NoteID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteNote]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@NoteID", NoteID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }




        public int insertPhoneCalls(Int64 orgID, Int64 objectID, Int64 objectUserID, string description, DateTime dueDate, string direction, Int64 oldLeadID, string delLeadID, bool IsDelOnly)
        {
            try
            {
                //int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertPhoneCall]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));
                    _objCmd.Parameters.Add(new SqlParameter("@Description", description));
                    _objCmd.Parameters.Add(new SqlParameter("@DueDate", dueDate));
                    _objCmd.Parameters.Add(new SqlParameter("@Direction", direction));
                    _objCmd.Parameters.Add(new SqlParameter("@oldLeadID", oldLeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@delLeadID", delLeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@delonly", IsDelOnly));


                    _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getPhoneCalls(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectPhoneCalls]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deletePhoneCall(string CallID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeletePhoneCall]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@CallID", CallID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        public DataSet getUpcomingPhoneCall(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectUpcomingPhoneCall]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet countActivities(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_CountActivities]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }






        public int insertDiscussion(Int64 orgID, Int64 objectID, Int64 objectUserID, string subject, string description,
                                        Int64 oldLeadID, string delLeadID, bool IsDelOnly, string CreatedBy, string entity)
        {
            try
            {
                int result = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertDiscussions]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));
                    _objCmd.Parameters.Add(new SqlParameter("@Subject", subject));
                    _objCmd.Parameters.Add(new SqlParameter("@Description", description));
                    _objCmd.Parameters.Add(new SqlParameter("@oldLeadID", oldLeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@delLeadID", delLeadID));
                    _objCmd.Parameters.Add(new SqlParameter("@delonly", IsDelOnly));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedBy", CreatedBy));
                    _objCmd.Parameters.Add(new SqlParameter("@Entity", entity));

                    SqlParameter sp = new SqlParameter("@output", SqlDbType.Int);
                    sp.Direction = ParameterDirection.Output;
                    _objCmd.Parameters.Add(sp);

                    _objCmd.ExecuteNonQuery();
                    result = Convert.ToInt32(sp.Value);
                    con.Close();
                }
                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getDiscussions(Int64 orgID, Int64 objectID, Int64 objectUserID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectDiscussions]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", objectUserID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteDiscussion(string DiscussionID, Int64 OrgID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteDiscussion]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@DiscussionID", DiscussionID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public DataSet getDashboardDiscussions(Int64 orgID, Int64 ID = 0)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectDashboardDiscussions]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@ID", ID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getDashboardTasks(Int64 orgID, Int64 ID = 0)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectDashboardTaks]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@ID", ID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getSalesDashboard(int year, int month, int opportunityMonth)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectSalesDashboard]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@year", year));
                    _objCmd.Parameters.Add(new SqlParameter("@month", month));
                    _objCmd.Parameters.Add(new SqlParameter("@opportunitymonth", opportunityMonth));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertUserDiscussion(Int64 DiscussionID, Int64 orgID)
        {
            try
            {
                //int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertUserDiscussions]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@DiscussionID", DiscussionID));
                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));

                    _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw;
            }
        }



        public int insertMCAnswers(DataTable dtInsertRows, Int64 LeadID)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFormControlMCAnswers]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add("@orgID", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);
                    _objCmd.Parameters.Add("@objectID", SqlDbType.VarChar, 200, dtInsertRows.Columns[1].ColumnName);
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", LeadID));
                    _objCmd.Parameters.Add("@oldLeadID", SqlDbType.VarChar, 200, dtInsertRows.Columns[2].ColumnName);
                    _objCmd.Parameters.Add("@ControlChoiceID", SqlDbType.VarChar, 100, dtInsertRows.Columns[3].ColumnName);
                    

                    SqlDataAdapter adpt = new SqlDataAdapter();
                    adpt.InsertCommand = _objCmd;
                    adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
                    recordsInserted = adpt.Update(dtInsertRows);
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getMCAnswers(Int64 OrgID, Int64 ObjectID, Int64 ObjectUserID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFormControlMCAnswers]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", ObjectUserID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
