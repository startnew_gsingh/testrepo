﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace CRMDAL
{
    public class AccountDAL
    {
        public DataSet getAccountData(Int64 OrgID, Int64 FilterID, int PageNo, int PageSize, string filterQuery = "")
        {
            try
            {
                DataSet dsAccount = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectAccounts]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));
                    _objCmd.Parameters.Add(new SqlParameter("@PageNo", PageNo));
                    _objCmd.Parameters.Add(new SqlParameter("@PageSize", PageSize));
                    _objCmd.Parameters.Add(new SqlParameter("@filterQuery", filterQuery));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsAccount);
                }
                return dsAccount;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getSelectedAccount(Int64 AccountID)
        {
            try
            {
                DataSet dsAccount = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectAccount]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@AccountID", AccountID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsAccount);
                }
                return dsAccount;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getAllObjects()
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectObjects]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public DataSet getObjectsColumns(Int64 ObjectID)
        //{
        //    try
        //    {
        //        DataSet dsObjects = new DataSet();
        //        using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
        //        {
        //            con.Open();
        //            SqlCommand _objCmd = new SqlCommand();
        //            _objCmd.CommandText = "[CRM_SelectObjectColumns]";
        //            _objCmd.CommandType = CommandType.StoredProcedure;
        //            _objCmd.Connection = con;

        //            _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

        //            SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
        //            objAdapter.Fill(dsObjects);
        //        }
        //        return dsObjects;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public int insertFilter(StructureFilterDAL ObjFilter)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilter]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", ObjFilter.OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjFilter.ObjectID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterName", ObjFilter.FilterName));
                    _objCmd.Parameters.Add(new SqlParameter("@Query", ObjFilter.Query));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.Now));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedBy", "Admin"));
                    _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    _objCmd.ExecuteNonQuery();
                    success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFilters]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertFilterQuery(DataTable dtInsertRows, Int64 filterID)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilterQuery]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", filterID));
                    _objCmd.Parameters.Add("@ColumnName", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);
                    _objCmd.Parameters.Add("@FilterCondition", SqlDbType.VarChar, 200, dtInsertRows.Columns[1].ColumnName);
                    _objCmd.Parameters.Add("@Value", SqlDbType.VarChar, 200, dtInsertRows.Columns[2].ColumnName);
                    _objCmd.Parameters.Add("@Relation", SqlDbType.VarChar, 100, dtInsertRows.Columns[3].ColumnName);

                    SqlDataAdapter adpt = new SqlDataAdapter();
                    adpt.InsertCommand = _objCmd;
                    adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
                    recordsInserted = adpt.Update(dtInsertRows);
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getFiltersQuery(Int64 FilterID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFiltersQuery]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertAccount(Int64 orgID, string columnValues, Int64 AccountID, string salesColNames, string salesColValues)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertAccount]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@AccountID", AccountID));
                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@colValues", columnValues));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.Now));
                    _objCmd.Parameters.Add(new SqlParameter("@CreatedBy", "Admin"));
                    _objCmd.Parameters.Add(new SqlParameter("@salesColNames", salesColNames));
                    _objCmd.Parameters.Add(new SqlParameter("@salesColValues", salesColValues));
                    _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;
                    //_objCmd.Parameters.Add(new SqlParameter("@selUserID", selUserID));

                    _objCmd.ExecuteNonQuery();
                    success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getColumnControls(Int64 ObjectID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectColumnControls]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getControlChoices(Int64 formID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFormControlChoices]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@formID", formID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertFilterColumn(DataTable dtInsertRows, Int64 filterID)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertFilterColumns]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", filterID));
                    _objCmd.Parameters.Add("@FilterColumn", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);

                    SqlDataAdapter adpt = new SqlDataAdapter();
                    adpt.InsertCommand = _objCmd;
                    adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
                    recordsInserted = adpt.Update(dtInsertRows);
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getControlChoicesByColumnName(string columnName)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectControlChoices]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@columnName", columnName));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getPlaceholders(Int64 ObjectID, Int64 OrgID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectPlaceholders]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@orgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteAccount(string AccountID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteAccount]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@AccountID", AccountID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
