﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;

namespace CRMDAL
{
    public class SalesProcessDAL
    {

        public DataSet getActiveSalesStages(Int64 ObjectUserID, Int64 ObjectID)
        {
            try
            {
                DataSet dsRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectActiveSalesStage]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@objectUserID", ObjectUserID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsRoles);
                }
                return dsRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getSalesStages(Int64 objectID, Int64 OrgID)
        {
            try
            {
                DataSet dsRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectSalesStage]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@objectID", objectID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsRoles);
                }
                return dsRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getSalesStageSteps(Int64 StageID)
        {
            try
            {
                DataSet dsRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectStageSteps]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@StageID", StageID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsRoles);
                }
                return dsRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertSalesProcessStage(Int64 ObjectID, string StageName, Int64 StageID, string StepName, Int64 FormID)
        {
            try
            {
                int recordsInserted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertSaleProcessStages]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;
                    _objCmd.UpdatedRowSource = UpdateRowSource.None;

                    _objCmd.Parameters.Add(new SqlParameter("@ObjectID", ObjectID));
                    _objCmd.Parameters.Add(new SqlParameter("@StageName", StageName));
                    _objCmd.Parameters.Add(new SqlParameter("@StageID", StageID));
                    _objCmd.Parameters.Add(new SqlParameter("@StepName", StepName));
                    _objCmd.Parameters.Add(new SqlParameter("@FormID", FormID));

                    recordsInserted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsInserted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getSalesStageControls(Int64 stageID, Int64 ObjectID)
        {
            try
            {
                DataSet dsRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectSalesStageControls]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@stageID", stageID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsRoles);
                }
                return dsRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getSalesProcess(Int64 orgID, Int64 selUserID, Int64 ObjectID)
        {
            try
            {
                DataSet dsRoles = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectSalesProcess]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@ObjectUserID", selUserID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsRoles);
                }
                return dsRoles;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteSalesStage(Int64 stageID, Int64 stepID)
        {
            try
            {
                //Int64 success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteSalesProcessStage]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@StageID", stageID));
                    _objCmd.Parameters.Add(new SqlParameter("@stepID", stepID));

                    _objCmd.ExecuteNonQuery();
                }
                return 1;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
