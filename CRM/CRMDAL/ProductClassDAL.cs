﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace CRMDAL
{
    public class ProductClassDAL
    {
        public DataSet getProductClassData(Int64 ProductClassID, Int64 OrgID, Int64 FilterID, int PageNo, int PageSize)
        {
            try
            {
                DataSet dsProductClass = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectProductClass]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@ClassID", ProductClassID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));
                    _objCmd.Parameters.Add(new SqlParameter("@PageNo", PageNo));
                    _objCmd.Parameters.Add(new SqlParameter("@PageSize", PageSize));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsProductClass);
                }
                return dsProductClass;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFilters]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int insertProductClass(Int64 ProductClassID, Int64 orgID, string Description, int ItemType, int Validation, int LocationCode,
                                        bool ExcludeCommission, int UnitMeasure, int ProductFamily, bool DisallowPriceOverride)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertProductClass]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@ClassID", ProductClassID));
                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@Description", Description));
                    _objCmd.Parameters.Add(new SqlParameter("@ItemType", ItemType));
                    _objCmd.Parameters.Add(new SqlParameter("@validation", Validation));
                    _objCmd.Parameters.Add(new SqlParameter("@LocationCode", LocationCode));
                    _objCmd.Parameters.Add(new SqlParameter("@ExcludeCommission", ExcludeCommission));

                    _objCmd.Parameters.Add(new SqlParameter("@UnitMeasure", UnitMeasure));
                    _objCmd.Parameters.Add(new SqlParameter("@ProductFamily", ProductFamily));
                    _objCmd.Parameters.Add(new SqlParameter("@DisallowPriceOverride", DisallowPriceOverride));

                    _objCmd.ExecuteNonQuery();
                    success = 1;
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteProductClass(string ProductClassID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteProductClass]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@ClassID", ProductClassID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getProductFamily()
        {
            try
            {
                DataSet dsProductClass = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectProductFamily]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsProductClass);
                }
                return dsProductClass;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getUnitofMeasure()
        {
            try
            {
                DataSet dsProductClass = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectUnitofMeasure]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsProductClass);
                }
                return dsProductClass;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getLocationCode()
        {
            try
            {
                DataSet dsProductClass = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectLocationCode]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsProductClass);
                }
                return dsProductClass;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
