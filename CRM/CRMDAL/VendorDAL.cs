﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Data;

namespace CRMDAL
{
    public class VendorDAL
    {
        public DataSet getVendorData(Int64 VendorID, Int64 OrgID, Int64 FilterID, int PageNo, int PageSize)
        {
            try
            {
                DataSet dsVendor = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectVendors]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@vendorID", VendorID));
                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));
                    _objCmd.Parameters.Add(new SqlParameter("@PageNo", PageNo));
                    _objCmd.Parameters.Add(new SqlParameter("@PageSize", PageSize));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsVendor);
                }
                return dsVendor;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public int insertFilter(StructureFilterDAL ObjFilter)
        //{
        //    try
        //    {
        //        int success = 0;
        //        using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
        //        {
        //            con.Open();
        //            SqlCommand _objCmd = new SqlCommand();
        //            _objCmd.CommandText = "[CRM_InsertFilter]";
        //            _objCmd.CommandType = CommandType.StoredProcedure;
        //            _objCmd.Connection = con;

        //            _objCmd.Parameters.Add(new SqlParameter("@orgID", ObjFilter.OrgID));
        //            _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjFilter.ObjectID));
        //            _objCmd.Parameters.Add(new SqlParameter("@FilterName", ObjFilter.FilterName));
        //            _objCmd.Parameters.Add(new SqlParameter("@Query", ObjFilter.Query));
        //            _objCmd.Parameters.Add(new SqlParameter("@CreatedDate", DateTime.Now));
        //            _objCmd.Parameters.Add(new SqlParameter("@CreatedBy", "Admin"));
        //            _objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;

        //            _objCmd.ExecuteNonQuery();
        //            success = Convert.ToInt32(_objCmd.Parameters["@outID"].Value);
        //        }
        //        return success;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public DataSet getFilters(Int64 OrgID, Int64 ObjectID)
        {
            try
            {
                DataSet dsObjects = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectFilters]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@OrgID", OrgID));
                    _objCmd.Parameters.Add(new SqlParameter("@objectID", ObjectID));

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsObjects);
                }
                return dsObjects;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        //public int insertFilterQuery(DataTable dtInsertRows, Int64 filterID)
        //{
        //    try
        //    {
        //        int recordsInserted = 0;
        //        using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
        //        {
        //            con.Open();

        //            SqlCommand _objCmd = new SqlCommand();
        //            _objCmd.CommandText = "[CRM_InsertFilterQuery]";
        //            _objCmd.CommandType = CommandType.StoredProcedure;
        //            _objCmd.Connection = con;
        //            _objCmd.UpdatedRowSource = UpdateRowSource.None;

        //            _objCmd.Parameters.Add(new SqlParameter("@FilterID", filterID));
        //            _objCmd.Parameters.Add("@ColumnName", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);
        //            _objCmd.Parameters.Add("@FilterCondition", SqlDbType.VarChar, 200, dtInsertRows.Columns[1].ColumnName);
        //            _objCmd.Parameters.Add("@Value", SqlDbType.VarChar, 200, dtInsertRows.Columns[2].ColumnName);
        //            _objCmd.Parameters.Add("@Relation", SqlDbType.VarChar, 100, dtInsertRows.Columns[3].ColumnName);

        //            SqlDataAdapter adpt = new SqlDataAdapter();
        //            adpt.InsertCommand = _objCmd;
        //            adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
        //            recordsInserted = adpt.Update(dtInsertRows);
        //            con.Close();
        //        }
        //        return recordsInserted;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        //public DataSet getFiltersQuery(Int64 FilterID)
        //{
        //    try
        //    {
        //        DataSet dsObjects = new DataSet();
        //        using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
        //        {
        //            con.Open();
        //            SqlCommand _objCmd = new SqlCommand();
        //            _objCmd.CommandText = "[CRM_SelectFiltersQuery]";
        //            _objCmd.CommandType = CommandType.StoredProcedure;
        //            _objCmd.Connection = con;

        //            _objCmd.Parameters.Add(new SqlParameter("@FilterID", FilterID));

        //            SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
        //            objAdapter.Fill(dsObjects);
        //        }
        //        return dsObjects;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}
        //public int insertFilterColumn(DataTable dtInsertRows, Int64 filterID)
        //{
        //    try
        //    {
        //        int recordsInserted = 0;
        //        using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
        //        {
        //            con.Open();

        //            SqlCommand _objCmd = new SqlCommand();
        //            _objCmd.CommandText = "[CRM_InsertFilterColumns]";
        //            _objCmd.CommandType = CommandType.StoredProcedure;
        //            _objCmd.Connection = con;
        //            _objCmd.UpdatedRowSource = UpdateRowSource.None;

        //            _objCmd.Parameters.Add(new SqlParameter("@FilterID", filterID));
        //            _objCmd.Parameters.Add("@FilterColumn", SqlDbType.VarChar, 200, dtInsertRows.Columns[0].ColumnName);

        //            SqlDataAdapter adpt = new SqlDataAdapter();
        //            adpt.InsertCommand = _objCmd;
        //            adpt.UpdateBatchSize = dtInsertRows.Rows.Count;
        //            recordsInserted = adpt.Update(dtInsertRows);
        //            con.Close();
        //        }
        //        return recordsInserted;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }
        //}

        public int insertVendor(Int64 VendorID, Int64 orgID, string VendorName, string Contact, string Address1, string Address2, string Address3, string City, string ZipCode,
                                string State, string Country, string Telephone, string Fax, string PaymentTerms, string ShippingMethod, string RemitTo, string ShipFrom, string Purchase)
        {
            try
            {
                int success = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_InsertVendor]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@VendorID", VendorID));
                    _objCmd.Parameters.Add(new SqlParameter("@orgID", orgID));
                    _objCmd.Parameters.Add(new SqlParameter("@VendorName", VendorName));
                    _objCmd.Parameters.Add(new SqlParameter("@Contact", Contact));
                    _objCmd.Parameters.Add(new SqlParameter("@Address1", Address1));
                    _objCmd.Parameters.Add(new SqlParameter("@Address2", Address2));
                    _objCmd.Parameters.Add(new SqlParameter("@Address3", Address3));

                    _objCmd.Parameters.Add(new SqlParameter("@City", City));
                    _objCmd.Parameters.Add(new SqlParameter("@ZipCode", ZipCode));
                    _objCmd.Parameters.Add(new SqlParameter("@State", State));

                    _objCmd.Parameters.Add(new SqlParameter("@Country", Country));
                    _objCmd.Parameters.Add(new SqlParameter("@Telephone", Telephone));
                    _objCmd.Parameters.Add(new SqlParameter("@Fax", Fax));

                    _objCmd.Parameters.Add(new SqlParameter("@PaymentTerms", PaymentTerms));
                    _objCmd.Parameters.Add(new SqlParameter("@ShippingMethod", ShippingMethod));
                    _objCmd.Parameters.Add(new SqlParameter("@RemitTo", RemitTo));

                    _objCmd.Parameters.Add(new SqlParameter("@ShipFrom", ShipFrom));
                    _objCmd.Parameters.Add(new SqlParameter("@Purchase", Purchase));

                    //_objCmd.Parameters.Add("@outID", SqlDbType.Int).Direction = ParameterDirection.Output;

                    _objCmd.ExecuteNonQuery();
                    success = 1;
                }
                return success;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public int deleteVendor(string VendorID)
        {
            try
            {
                int recordsDeleted = 0;
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();

                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_DeleteVendor]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    _objCmd.Parameters.Add(new SqlParameter("@VendorID", VendorID));

                    recordsDeleted = _objCmd.ExecuteNonQuery();
                    con.Close();
                }
                return recordsDeleted;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getPaymentTerms()
        {
            try
            {
                DataSet dsVendor = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectPaymentTerms]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsVendor);
                }
                return dsVendor;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public DataSet getShippingMethods()
        {
            try
            {
                DataSet dsVendor = new DataSet();
                using (SqlConnection con = new SqlConnection(SqlHelper.GetConnectionString()))
                {
                    con.Open();
                    SqlCommand _objCmd = new SqlCommand();
                    _objCmd.CommandText = "[CRM_SelectShippingMethod]";
                    _objCmd.CommandType = CommandType.StoredProcedure;
                    _objCmd.Connection = con;

                    SqlDataAdapter objAdapter = new SqlDataAdapter(_objCmd);
                    objAdapter.Fill(dsVendor);
                }
                return dsVendor;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
